<cfinclude template="/Query_Header.cfm">
<cfquery name="panier" datasource="f8w_test" maxrows="1">
    Select * from cbpanier Where id='#url.idpan#'
</cfquery>

<cfif #tipisite.CBBanque# eq 0 and #tipisite.CBTipi# eq 0>
	<cflocation url="/home/" addtoken="no">
</cfif>
<!--- chargement panier non payé ou nouveau --->

<cfif #panier.uuid# neq #session.uuidpanier#>
	<cflocation url="/home/" addtoken="no">
</cfif>
<cfif #panier.CodeReponse# neq "00000" and #panier.CodeReponse# neq "P" and #panier.recordcount# eq 1 and #panier.uuid# neq "">
<cfelse>
    <!--- pas normal ont sort --->
    <cflocation url="/home/" addtoken="no">
</cfif>

<script  type="text/javascript">
	function payecb(Laform) {
		eval("document."+Laform+".submit()");
		var urltipi = document.getElementById("url").value;
		window.open(urltipi,'_blank','height=700, width=900, toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, directories=no, status=no');
		document.getElementById("dssq").disabled=1;
	}
</script>

<!--- pour verouillage panier si achat en cour --->
<cfquery name="cb" datasource="f8w_test" maxrows="1">
    Select * from cb use index(siret_uuid) Where siret='#tipisite.siret#' 
    and uuid='#session.uuidpanier#' order by id desc limit 0,1
</cfquery>
<cfset verouxpanier = "non">
<cfif #cb.resultrans# eq "V">
	<cfset delaimin = datediff("n",#cb.creamaj#,now())>
	<cfset delaiaattendre = 30 - #delaimin#>
    <cfif #delaiaattendre# gt 0>
        <cfset verouxpanier = "oui">
	</cfif>
</cfif>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Factures</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

    <body>
        <div id="bgBlur">
            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>

            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-7 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p><h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>
                            #lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3></p>
                        <p class="description ps-5 pe-5">Veuillez trouver ci-dessus un récapitulatif de votre réservation.</p>
                    </div>
                    <!-- CONTENT -->

                    <div class="row d-flex justify-content-between mt-5 content">
                        <div class="p-2 p-lg-5 bg-white section scroll">
                            <cfquery name="prestation_reserve_a" datasource="f8w_test">
                                Select * from prestation_reserve_a where id='#panier.Id_prestation_reserve_a#'
                            </cfquery>
                            <cfquery name="enfant" datasource="f8w_test">
                                Select nom,prénom from enfant use index(siret_numenfant) where siret='#lect_client.siret#' 
                                and numenfant='#panier.numenfant#' 
                            </cfquery>
                            <cfquery name="prestation" datasource="f8w_test">
                                Select désignation from prestation use index(siret_numprestation) where siret='#panier.siret#' 
                                and numPrestation='#panier.numprestation#'
                            </cfquery>
                            <p class="m-0">Pour : <cfoutput>#enfant.nom# #enfant.prénom#</cfoutput></p>
                            <p class="m-0">Prestation : <cfoutput>#prestation.désignation#</cfoutput></p>
                            <p class="m-0">Session : <cfoutput>#prestation_reserve_a.Designation#</cfoutput></p>

                            <cfset premjouran1 = createdate(#prestation_reserve_a.DebutPerioPrestaAn#,1,1)>
                            <cfset ouvertedu = dateadd("d",#prestation_reserve_a.DebutPerioPresta# -1,#premjouran1#)>
                            <cfset ouvertedu1 = lsdateformat(#ouvertedu#,"dd/MM/YYYY")>        
                            <cfset premjouran1 = createdate(#prestation_reserve_a.FinPerioPrestaAn#,1,1)>
                            <cfset ouverteau = dateadd("d",#prestation_reserve_a.FinPerioPresta# -1,#premjouran1#)>
                            <cfset ouverteau1 = lsdateformat(#ouverteau#,"dd/MM/YYYY")>

                            <p class="m-0"><cfoutput>Du #ouvertedu1# au #ouverteau1#</cfoutput></p>
                            <p class="m-0">Tarif <cfoutput>#panier.Montant#</cfoutput> €</p>

                            <cfquery name="tipisite" datasource="f8w_test">
                                Select SiteTIPI,SaisieTIPI from application_client where siret='#panier.siret#'
                            </cfquery>
                            <cfif #verouxpanier# eq "non">  
                                <cfset montant = #panier.Montant# * 100>
                                <cfif len(#montant#) eq 3>
                                    <cfset montant = "0" & #montant#>
                                </cfif> 
                                <cfset leobjet = "Achat Tickets " & lsdateformat(now(),"ddMMYYYY") & " " & lstimeformat(now(),"HHmmss")>
                                <cfset leobjet1 = mid(#leobjet#,1,99)>
                                <cfset exer = lsdateformat(now(),"YYYY")>
                                <cfset lemail = trim(#lect_user.login#)>
                                <cfquery name="add" datasource="f8w_test">
                                    Insert into cb (siret,uuid,numcli,exer,objet,montant,mel) 
                                    Values ('#panier.siret#','#session.uuidpanier#',
                                    '#tipisite.SiteTIPI#','#exer#',
                                    <cfqueryparam value="#leobjet1#" cfsqltype="cf_sql_varchar">,'#montant#','#lemail#')
                                </cfquery>
                                <cfquery name="cb" datasource="f8w_test" maxrows="1">
                                    Select * from cb use index(siret_uuid) Where siret='#panier.siret#' 
                                    and uuid='#session.uuidpanier#' order by id desc limit 0,1
                                </cfquery>
                                <cfset montant= #cb.montant#>
                                <cfset objet = replacenocase(#cb.objet#,"é","e","all")>
                                <cfset objet = replacenocase(#objet#,"è","e","all")>
                                <cfset objet = replacenocase(#objet#,"ê","e","all")>
                                <cfset objet = urlencodedformat(#objet#,"utf-8")>    
                                <cfset exer = #cb.exer#>
                                <cfset refdet = #session.uuidpanier#>
                                <cfset lemail = trim(#lect_user.login#)>
                                <br/>
                                <p class="text-red mb-1">En cliquant sur "Payer mes tickets par CB", une fenêtre popup va s’ouvrir sur le <span class="bold">site sécurisé tipi.budget.gouv.fr</span> pour procéder au paiement.</p>
                                <p class="mb-1">Aucune donnée de votre CB ne sera connue et conservé par Cantine de France.</p>
                                <p class="mb-1">Si rien ne se passe après avoir cliqué sur "Payer mes tickets par CB", <span class="bold">vérifiez que les popups sont autorisés pour https://tipi.budget.gouv.fr sinon autorisez les ou changez de navigateur.</span></p>
                                <br/>
                                <cfoutput>
                                    <cfset urltipi = "https://www.tipi.budget.gouv.fr/tpa/paiement.web?numcli=#cb.numcli#&exer=#exer#&refdet=#refdet#&objet=#objet#&montant=#montant#&mel=#lemail#&urlcl=http://gestion.cantine-de-france.fr/cb/&saisie=#tipisite.SaisieTIPI#">
                                    <form method="post" name="ssds" id="ssds" action="/facture/cbtipi/pstcb.cfm" target="frm">
                                        <input type="button" class="btn btn-primary bold" value="Payer par CB et réserver" name="dssq" id="dssq" onClick="payecb('ssds');">
                                        <input type="hidden" name="idcb" value="#cb.id#">
                                        <input type="hidden" name="url" id="url" value="#urltipi#">
                                    </form>
                                    <iframe name="frm" id="frm" width="0" height="0"></iframe>
                                </cfoutput>                           
                            <cfelse>                                                    
                                <cfset delaimin = datediff("n",#cb.creamaj#,now())>
                                <cfset delaiaattendre = 30 - #delaimin#>
                                <p class="text-red bold">Une tentative de paiement a eu lieu il y a <cfoutput>#delaimin# minutes</cfoutput>, merci de patienter <cfoutput>#delaiaattendre#</cfoutput> minutes avant de réitérer l'opération.</p>
                            </cfif>                                
                        </div>
                    </div>
                </div>

                <!-- SECTION RIGHT -->
                <div class="col-lg-3 section-right">
                    <div class="mt-5">
                        <cfinclude template="/home/calendar.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions légales et confidentialité</a>
                <p class="m-0 pe-3 ps-3">N° de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" alt="bouton menu déroulant" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">

        <script type="text/javascript"> //Script gestion menu burger
            function openNav() {
                document.getElementById("navMobileItems").style.display= "block";
                document.getElementById("navMobileLabel").style.display= "none";
                const background = document.getElementById("bgBlur");
                background.className = "bg-blur";
                background.addEventListener("click", closeNav);
            }
            function closeNav() {
                document.getElementById("navMobileItems").style.display= "none";
                document.getElementById("bgBlur").classList.remove("bg-blur");
                document.getElementById("navMobileLabel").style.display= "block";
            }
        </script>
    </body>
</html>
