﻿<cfset msg = "">
<cfset msg1 = "">
<cfinclude template="/Query_Header.cfm">
<cfif isdefined("form.go")>
	<cfset laheure = #form.h# & ":" & #form.m#>
    <cfif len(#form.hh#)>    
        <cfset laheureardep = #form.hh# & ":" & #form.mm#>
	<cfelse>
        <cfset laheureardep = "">
    </cfif>        
    <cfquery name="maj" datasource="f8w_test">
    	Update inscriptionplanning set HeureArriv='#laheure#', HeureArrivDep='#laheureardep#' where id='#form.inscriptionamodifierpourheurearrive#'
    </cfquery>
    <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#form.sav_idprestation#&id_enfant=#form.sav_idenfant#&msg=#urlencodedformat(form.msg)#&msg1=#urlencodedformat(form.msg1)#" addtoken="no"> 
</cfif>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Inscription à la prestation</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

<cfif len(#msg#)>
    <body onLoad="showMB('err_login')">
        <cfmessagebox name="err_login" type="alert" message="#msg#" icon="error" title="Attention, erreur de saisie"  />
<cfelseif len(#msg1#)>
    <body onLoad="showMB('err_login')">
        <cfmessagebox name="err_login" type="alert" message="#msg1#" icon="info" title="Confirmation"  />
<cfelse>
    <body>
</cfif>
        <div id="bgBlur">
            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>

            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-7 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p><h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>
                            #lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3></p>
                        <p class="description ps-5 pe-5">Gérez ci-dessous les inscriptions de vos enfants.</p>
                    </div>
                    <!-- CONTENT -->

                    <div class="row d-flex justify-content-between mt-5 content">
                        <div class="p-2 p-lg-5 bg-white section scroll">
                            <cfform name="log" method="post" class="blue-select">
                                <cfset dateaff = createdate(#mid(url.datejour,7,4)#,#mid(url.datejour,4,2)#,#mid(url.datejour,1,2)#)>
                                <cfquery name="verifhoraireprestation" datasource="f8w_test">
                                    Select Pointage,heuredébut,heurefin from prestation Where id='#url.id_prestation#'
                                </cfquery>
                                <cfset depuis = 0>
                                <cfset jusqua = 24>
                                <cfif len(#verifhoraireprestation.heuredébut#)>
                                    <cfset depuis = mid(#verifhoraireprestation.heuredébut#,1,2)>
                                </cfif>
                                <cfif len(#verifhoraireprestation.heurefin#)>
                                    <cfset jusqua = mid(#verifhoraireprestation.heurefin#,1,2)>
                                </cfif>
                                <cfset ardep = "d'arrivé">
                                <cfset ardepar = "de départ">    
    <!---						<cfif findnocase("DE",#verifhoraireprestation.Pointage#)>
                                    <cfset ardep = "de départ">
                                </cfif>
    --->                            
                                <cfif #depuis# gt 12>
                                    <cfset ardep = "de départ">
                                    <cfset ardepar = "d'arrivé">
                                </cfif>
                                <cfoutput>
                                    <p>Pour terminer votre modification, merci de renseigner l'heure #ardep# de votre enfant pour le <span class="text-red bold">#lsdateformat(dateaff,"dddd")# #lsdateformat(dateaff,"dd")# #lsdateformat(dateaff,"mmmm")# #lsdateformat(dateaff,"yyyy")#</span> :</p>
                                </cfoutput>

                                <cfselect name="h">
                                    <cfloop from="#depuis#" to="#jusqua#" index="idx">
                                        <cfif len(#idx#) eq 1>
                                            <cfset idx = "0" & #idx#>
                                        </cfif>
                                        <cfoutput>
                                            <option class="dropdown-item" value="#idx#">#idx#</option>
                                        </cfoutput>
                                    </cfloop>
                                </cfselect>&nbsp;heure&nbsp;
                                <cfselect name="m">
                                    <cfloop from="0" to="59" index="idx">
                                        <cfif len(#idx#) eq 1>
                                            <cfset idx = "0" & #idx#>
                                        </cfif>
                                        <cfoutput>
                                            <option class="dropdown-item" value="#idx#">#idx#</option>
                                        </cfoutput>
                                    </cfloop>
                                </cfselect>&nbsp;minute

                                <cfoutput>
                                    <br/><br/>
                                    <p>Et éventuellement l'heure #ardepar# :</p>
                                </cfoutput>
                                    
                                <cfselect name="hh">
                                    <option class="dropdown-item" value=""></option>
                                    <cfloop from="#depuis#" to="#jusqua#" index="idx">
                                        <cfif len(#idx#) eq 1>
                                            <cfset idx = "0" & #idx#>
                                        </cfif>
                                        <cfoutput>
                                            <option class="dropdown-item" value="#idx#">#idx#</option>
                                        </cfoutput>
                                    </cfloop>
                                </cfselect>&nbsp;heure&nbsp;
                                <cfselect name="mm">
                                    <option class="dropdown-item" value=""></option>
                                    <cfloop from="0" to="59" index="idx">
                                        <cfif len(#idx#) eq 1>
                                            <cfset idx = "0" & #idx#>
                                        </cfif>
                                        <cfoutput>
                                            <option class="dropdown-item" value="#idx#">#idx#</option>
                                        </cfoutput>
                                    </cfloop>
                                </cfselect>&nbsp;minute
                                        
                                <cfinput type="submit" class="btn btn-primary" name="go" value="Valider">
                                <cfinput type="hidden" name="ardep" value="#ardep#">
                                <cfinput type="hidden" name="sav_idprestation" value="#url.id_prestation#">
                                <cfinput type="hidden" name="sav_idenfant" value="#url.id_enfant#">
                                <cfinput type="hidden" name="msg" value="#URLDecode(url.msg)#">
                                <cfinput type="hidden" name="msg1" value="#URLDecode(url.msg1)#">
                                <cfinput type="hidden" name="numjour" value="#url.numjour#">
                                <cfinput type="hidden" name="datejour" value="#url.datejour#">
                                <cfinput type="hidden" name="numenfant" value="#url.numenfant#">
                                <cfinput type="hidden" name="numprestation" value="#url.numprestation#">
                                <cfinput type="hidden" name="action" value="#url.action#">
                                <cfinput type="hidden" name="nature" value="#url.nature#">
                                <cfinput type="hidden" name="inscriptionamodifierpourheurearrive" value="#url.inscriptionamodifierpourheurearrive#">
                            </cfform>
                        </div>
                    </div>
                </div>

                <!-- SECTION RIGHT -->
                <div class="col-lg-3 section-right">
                    <div class="mt-5">
                        <cfinclude template="/home/calendar.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions légales et confidentialité</a>
                <p class="m-0 pe-3 ps-3">N° de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" alt="bouton menu déroulant" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">

        <script type="text/javascript"> //Script gestion menu burger
            function openNav() {
                document.getElementById("navMobileItems").style.display= "block";
                document.getElementById("navMobileLabel").style.display= "none";
                const background = document.getElementById("bgBlur");
                background.className = "bg-blur";
                background.addEventListener("click", closeNav);
            }
            function closeNav() {
                document.getElementById("navMobileItems").style.display= "none";
                document.getElementById("bgBlur").classList.remove("bg-blur");
                document.getElementById("navMobileLabel").style.display= "block";
            }
            //The button onClick handler displays the message boxes. 
            function showMB(mbox)  { 
                ColdFusion.MessageBox.show(mbox); 
            }
        </script>
    </body>
</html>