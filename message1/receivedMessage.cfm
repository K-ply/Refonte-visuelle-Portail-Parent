﻿<cfquery name="message" datasource="f8w_test">
    Select * from message use index(id_client) 
    Where id_client='#lect_client.id#' 
    and EmisPar=0
    order by DateMessage DESC
</cfquery>

<div class="row mt-5 content">

    <button type="button" class="box-message-active col-3 p-3 d-flex justify-content-center">Messages re&ccedil;us</button>
    <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintSendMessage()">Messages envoy&eacute;s</button>
    <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintNewMessage()">Envoyer un nouveau message</button>

    <div class="p-2 p-lg-5 bg-white section small-scroll">
        
        <table class="table ">
                        
                <cfform name="pourdet">
                    <cfinput type="hidden" name="savderclic" value="#message.id#">
                            <cfoutput query="message">
                                <cfset ladate = #lsdateformat(message.DateMessage,"dddd")# & " " & #lsdateformat(message.DateMessage,"dd")# & " " & #lsdateformat(message.DateMessage,"mmmm")# & " " & #lsdateformat(message.DateMessage,"yyyy")#>
                                <cfset laheure = #lstimeformat(message.DateMessage,"HH")# & ":" & #lstimeformat(message.DateMessage,"mm")# & ":" & #lstimeformat(message.DateMessage,"ss")#>
                                    <cfif #url.id_message# eq #message.id#>
                                        <tr id="TR#message.id#" bgcolor="##CCCCCC">
                                    <cfelse>
                                        <tr id="TR#message.id#" bgcolor="##FFFFFF">
                                    </cfif>    
                                        <cfif #url.typemess# eq "in">
                                            <cfif #message.lu# neq 0>
                                                <td class="p-3 pt-3 pb-3 "><a class="text-grey link-blue lu_nonLu " href="/message1/affmess.cfm?id_message=#message.id#" target="frm_mess" id="B#message.id#">#message.Objet# </a></td>
                                            <cfelse>
                                                <td class="p-3 pt-3 pb-3 "><a class="bold text-grey link-blue lu_nonLu " onClick="LuNonLu('#message.id#')" href="/message1/affmess.cfm?id_message=#message.id#" target="frm_mess" id="B#message.id#">#message.Objet# </a></td>
                                            </cfif>
                                        <cfelse>
                                            <td class="p-3 pt-3 pb-3"><a href="/message1/affmess.cfm?id_message=#message.id#" target="frm_mess">#message.Objet#</a></td>
                                        </cfif>
                                            <cfif #message.lu# eq 0>
                                                <td class="p-3 pt-3 pb-3"> <img id="A#message.id#" src="/img/nonlu.gif" width="14" height="12" alt="Non lu" title="Non lu"/></td>
                                                <cfinput name="#message.id#" type="hidden" value="0">
                                            <cfelse>
                                                <cfset dtaff = lsdateformat(#message.datelu#,"dddd") & " "  & lsdateformat(#message.datelu#,"dd") & " " & lsdateformat(#message.datelu#,"MMMM") & " " & lsdateformat(#message.datelu#,"YYYY") & " à " & lstimeformat(#message.datelu#,"HH:MM:SS")>
                                                <td class="p-3 pt-3 pb-3"><img id="A#message.id#" src="/img/lu.gif" width="13" height="16" alt="Lu le #dtaff#" title="Lu le #dtaff#"  /></td>
                                                <cfinput name="#message.id#" type="hidden" value="1">

                                            </cfif>    
                                            <td class="pt-3 pb-3 text-grey">
                                                <cfif len(#message.piece_jointe#)>
                                                    <img width="14" height="13" alt="Ce message contient des pièces jointes" title="Ce message contient des pièces jointes" src="/img/icon/icon_paper_clip_grey.png">
                                                </cfif>
                                            </td>
                                        
                                        <td class="text-grey  pt-3 pb-3">#ladate#</td>
                                        <td class="text-grey  pt-3 pb-3">#laheure#</td>
                                    </tr>
                            </cfoutput>
                </cfform>
        </table>
    </div>
    
    <div class="p-2 p-lg-5 bg-white section small-scroll">
        <iframe src="/message1/affmess.cfm?id_mnu=<cfoutput>#url.id_mnu#</cfoutput>&id_message=<cfoutput>#url.id_message#</cfoutput>" style="background-color:#FFF" name="frm_mess" id="frm_mess" width="100%" height="200"></iframe></td>
    </div>
</div>