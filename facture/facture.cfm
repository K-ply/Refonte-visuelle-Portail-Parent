﻿<cfset msg = "">
<cfset msg1 = "">
<cfinclude template="/Query_Header.cfm">
<cfquery name="autreetabaveccb" datasource="f8w_test">
    Select * from application_client where siret like '#lect_etablissement.siret#%' 
    and (CBBanque>0 or CBTipi=1)
</cfquery>
<cfif #cgi.REMOTE_ADDR# neq "78.201.4.76" and #lect_etablissement.FactureParentCommeEncaissement# eq 0>
	<cfif #lect_etablissement.CBTipi# eq 1 and #lect_client.ModePaiement# neq 1 or #lect_etablissement.CBBanque# gt 0 and #lect_client.ModePaiement# neq 1 or #autreetabaveccb.recordcount#>
        <!--- CB TIPI --->
        <cflocation url="/facture/cbtipi/?id_mnu=144" addtoken="no">
    </cfif>
    <cfif #tipisite.CBTipi# eq 1 and #lect_client.ModePaiement# neq 1 or #tipisite.CBBanque# gt 0 and #lect_client.ModePaiement# neq 1 or #autreetabaveccb.recordcount# >
        <!--- CB TIPI --->
        <cflocation url="/facture/cbtipi/?id_mnu=144" addtoken="no">
    </cfif>
</cfif>

<div class="row d-flex justify-content-between mt-5 content">
    <div class="p-2 p-lg-5 bg-white section scroll">
        <cfset Solde = 0>
        <cfif #lect_etablissement.régierecette# neq 0>
            <!---<cfquery name="enc" datasource="f8w_test">
                Select sum(MontantPayé) as tot from encaissement use index(siret_NumClient) 
                Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#' 
                and NumE='#lect_etablissement.NumEtablissement#' and MoyenPaiement<>4
            </cfquery>--->
            <cfquery name="enc" datasource="f8w_test">
                Select sum(MontantPayé) as tot from encaissement use index(siret_NumClient) 
                Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#' 
                and NumE='#lect_etablissement.NumEtablissement#'
            </cfquery>
            <cfquery name="facture" datasource="f8w_test">
                Select * from facture use index(siret_numclient) 
                Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#'
                and NumE='#lect_etablissement.NumEtablissement#'
            </cfquery>
            <cfif #facture.recordcount#>
                <cfset tot = 0>
                <cfloop query="facture">
                    <cfquery name="role" datasource="f8w_test">
                        Select id from role use index(siret_NumRole) 
                        Where siret='#lect_user.siret#' and NumRole='#facture.NumRole#' 
                        and typerole<>2 and del=0
                    </cfquery>
                    <cfif #role.recordcount#>
                        <cfset tot = #tot# + #facture.TotalTTC#>
                    </cfif>
                </cfloop>
                <cfset solde = val(#tot#) - val(#enc.tot#)>
                <cfset msgsolde = "Solde de votre compte : " & #numberformat(solde,"9999.99")# & " € ">
                <cfif #solde# lt 0>
                    <cfset msgsolde = #msgsolde# & "à vous devoir.">
                    <cfset msgsolde = replace(#msgsolde#,"-","")>
                <cfelseif #solde# gt 0>
                    <cfset msgsolde = #msgsolde# & "à nous devoir.">
                </cfif>
            <cfelse>
                <cfset msgsolde = "">
            </cfif>	
        <cfelse>
            <cfset msgsolde = "">
        </cfif>

        <!--- uniquement du portefeuille ??? --->
        <cfif #lect_etablissement_miseadispo.recordcount#>
            <cfquery name="portefeuille1" datasource="f8w_test">
                select * from prestation use index(siret) where siret='#lect_client.siret#' and ticket=1 and del=0 
                or siret='#lect_etablissement_miseadispo.siret_miseadispo#' and ticket=1 and del=0
            </cfquery>
        <cfelse>
            <cfquery name="portefeuille1" datasource="f8w_test">
                select * from prestation use index(siret) where siret='#lect_client.siret#' and ticket=1 and del=0
            </cfquery>
        </cfif>
        <cfif #portefeuille1.recordcount# eq 0>
            <cfset msgsolde = "Solde de votre compte : " & #numberformat(#portfeuille.Montant#,"9999.99")# & " € ">
        </cfif>

        <cfform name="pourfacture" class="blue-select">
            <table class="table" name="listeFacture">
                <cfif #cgi.REMOTE_ADDR# eq "78.201.4.76" or #lect_etablissement.FactureParentCommeEncaissement# eq 1>
                    <!--- nouveau tableau facture et encaissement--->
                    <cfquery name="parent" datasource="f8w_test">
                        Select * from client use index(siret_numclient) where siret='#lect_user.siret#' 
                        and numclient='#lect_user.numclient#'
                    </cfquery>
                    <cfset url.idparent = #lect_client.id#>
                    <cfset url.id = 0>
                    <cfset url.n = 0>
                    <!--- declaration de l'objet module --->
                    <cfset CDF_Finance = createobject("component","commun.CDF_Finance")>
                    <cfset NewSolde = CDF_Finance.SoldeClient(#lect_user.siret#,#url.numetablissement#,val(#parent.id#))>
                    <cfquery name="lect_regies" datasource="f8w_test"><!--- lecture des établissements (plusieur régie) --->
                        Select * from application_client use index(siret) 
                        Where siret Like '#lect_etablissement.siret#%' order by NumEtablissement
                    </cfquery>
                    <cfif #lect_regies.recordcount# gt 1>
                        <cfselect name="pourregie" onChange="MM_jumpMenu('self',this,0)" class="mb-3">
                            <cfoutput query="lect_regies">
                                <cfif len(#lect_regies.NomR#)>
                                    <cfset nometab = #lect_regies.NomR#>
                                <cfelse>
                                    <cfset nometab = #lect_regies.NomE#>
                                </cfif>
                                <cfif #url.numetablissement# eq #lect_regies.NumEtablissement#>
                                    <option class="dropdown-item" value="/facture/?id_mnu=144&numetablissement=#lect_regies.numetablissement#" selected>#nometab#</option>
                                <cfelse>
                                    <option class="dropdown-item" value="/facture/?id_mnu=144&numetablissement=#lect_regies.numetablissement#">#nometab#</option>
                                </cfif>
                            </cfoutput>
                        </cfselect>
                    </cfif>
                    <cfinclude template="/EncaissementTableau.cfm"<!--- ICI "/commun/EncaissementTableau.cfm"--->>
                <cfelse>
                    <cfquery name="role" datasource="f8w_test">
                        Select * from role use index(siret_NumRole) 
                        Where siret = '#lect_etablissement.siret#' and afficher_facture=1 and TypeRole<>2 and del=0 order by NumRole Desc
                    </cfquery>
                    <cfif #lect_etablissement.PaiementCbInternet# eq 1>
                        <cfset format="applet">
                    <cfelse>
                        <cfset format="html">
                    </cfif>
                    <cfset format="html">

                    <tr>
                        <th class="pb-4">Date</th>
                        <th class="pb-4">Montant</th>
                        <th class="pb-4 hidden">Période</th>
                        <th class="pb-4">Etat</th>
                        <cfif #lect_etablissement.PaiementCbInternet# eq 1 and #lect_client.ModePaiement# eq 0>
                            <th class="pb-4">Règlement</th>
                        </cfif>
                        <th class="pb-4">Voir</th>
                    </tr>

                    <cfquery name="prestapasticket" datasource="f8w_test">
                        Select numprestation from prestation use index(siret)
                        Where siret = '#lect_etablissement.siret#' and Ticket=0 and del=0
                    </cfquery>
                    <cfset lstprestapasticket = "">

                    <cfloop query="prestapasticket">
                        <cfset lstprestapasticket = #lstprestapasticket# & #prestapasticket.numprestation# & ",">
                    </cfloop>

                    <cfloop query="role">           
                        <cfquery name="facture" datasource="f8w_test">
                            Select id,NumRole,NumFacture,NomPdf,FactureRéglée,TotalTTC 
                            from facture use index(siret_NumClient) 
                            Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                            and NumRole=#role.NumRole# and del=0
                        </cfquery>
                        <cfif #lect_etablissement.régierecette# neq 0>
                            <cfquery name="enc" datasource="f8w_test">
                                select * from encaissement use index(siret_NumClient_NumRole2)
                                Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                                and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' 
                                or NumRole22='#role.NumRole#' or NumRole23='#role.NumRole#')
                            </cfquery>
                            <cfif (#enc.recordcount# or #Solde# lte 0) and #enc.MoyenPaiement# eq 4>
                                <cfset comment = "Facture en CONTENTIEUX le " & #lsdateformat(enc.DatePaiement,"dd/MM/YYYY")#>
                            <cfelseif #enc.recordcount# or #Solde# lte 0>    
                                <cfset comment = "Facture soldée le " & #lsdateformat(enc.DatePaiement,"dd/MM/YYYY")#>
                            <cfelse>
                                <cfquery name="itemfacture" datasource="f8w_test">
                                    Select id from itemfacture use index(siret_numfacture)
                                    Where siret = '#lect_etablissement.siret#' and numfacture='#facture.numfacture#' 
                                    and find_in_set(numprestation,'#lstprestapasticket#')
                                </cfquery>
                                <cfif #itemfacture.recordcount#>
                                    <cfset comment = "Facture non soldée !!!">
                                <cfelse>
                                    <cfset comment = " ">
                                </cfif>
                            </cfif>
                        <cfelse>
                            <cfset comment = #role.STitreFacture#>
                        </cfif>

                        <cfif #facture.recordcount#><!--- facture --->
                            <cfquery name="classactive" datasource="classactive">
                                Select id from documents use index(siret_CodeAppli_Table_origine_Id_Table_Origine) 
                                Where siret = '#lect_etablissement.siret#' and codeappli='F8' and Table_origine='facture' 
                                and Id_Table_Origine='#facture.id#'
                            </cfquery>
                            <cfif #classactive.recordcount#><!--- classactive --->
                                <cfif #lect_etablissement.PaiementCbInternet# eq 1 and #lect_client.ModePaiement# eq 0>
                                    <cfif #facture.FactureRéglée# eq 0>
                                        <cfoutput>
                                            <tr>
                                                <td class="pt-3 pb-3">#role.datefacture#</td>
                                                <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                                <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                <td class="pt-3 pb-3">#comment#</td>
                                                <td class="pt-3 pb-3"><a class="link-red" href="/facture/?<cfoutput>#facture.id#&id_mnu=144&Purl=oui</cfoutput>">CLIQUEZ pour régler</a></td>
                                                <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                            </tr>
                                        </cfoutput>
                                    <cfelse>
                                        <cfoutput>
                                            <tr>
                                                <td class="pt-3 pb-3">#role.datefacture#</td>
                                                <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                                <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                <td class="pt-3 pb-3">#comment#</td>
                                                <td class="pt-3 pb-3"></td>
                                                <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                            </tr>
                                        </cfoutput>
                                    </cfif>
                                <cfelse>
                                    <cfoutput>
                                        <tr>
                                            <td class="pt-3 pb-3">#role.datefacture#</td>
                                            <td class="pt-3 pb-3 hidden">#facture.TotalTTC# €</td>
                                            <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                            <td class="pt-3 pb-3">#comment#</td>
                                            <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#classactive.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                        </tr>
                                    </cfoutput>
                                </cfif>
                            <cfelse><!--- pas classactive --->
                                <cfif len(#facture.NomPdf#) gt 1><!--- verif si facture produite --->
                                    <cfdirectory name="veriffact" directory="C:\inetpub\wwwroot\F8WEBcoldfu\facture\" filter="#facture.NomPdf#.pdf">
                                    <cfif #veriffact.recordcount# eq 1>                 
                                        <cffile variable="filefacture" action="readbinary" file="C:\inetpub\wwwroot\F8WEBcoldfu\facture\#facture.NomPdf#.pdf">
                                        <cfset b = tobase64(#filefacture#)>
                                        <cfset designation = #role.STitreFacture# & " " & #role.Z0#>
                                        <cfset recherche = #lect_client.nom# & " " & #lect_client.prénom# & " " & "facture" & " " & #role.DateFacture#>
                                        <cfquery name="add" datasource="classactive" result="Resultat">
                                            Insert into documents(siret,Id_Proprio,CodeAppli,Type_Proprio,Type_Doc,Table_origine,
                                            Id_Table_Origine,Désignation,Document,Recherche,extention) values('#role.siret#',
                                            '#lect_client.id#','F8','client','FACTURE','facture','#facture.id#',
                                            <cfqueryparam value="#designation#" cfsqltype="cf_sql_varchar">,
                                            <cfqueryparam value="#b#" cfsqltype="cf_sql_longvarchar">,
                                            <cfqueryparam value="#recherche#" cfsqltype="cf_sql_varchar">,'.pdf')
                                        </cfquery>
                                        <cfif #lect_etablissement.PaiementCbInternet# eq 1 and #lect_client.ModePaiement# eq 0>
                                            <cfif #facture.FactureRéglée# eq 0>
                                                <cfoutput>
                                                    <tr>
                                                        <td class="pt-3 pb-3">#role.datefacture#</td>
                                                        <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                                        <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                        <td class="pt-3 pb-3">#comment#</td>
                                                        <td class="pt-3 pb-3"><a class="link-red" href="/facture/?<cfoutput>#facture.id#&id_mnu=144&Purl=oui</cfoutput>">CLIQUEZ pour régler</a></td>
                                                        <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                                    </tr>
                                                </cfoutput>
                                            <cfelse>
                                                <cfoutput>
                                                    <tr>
                                                        <td class="pt-3 pb-3">#role.datefacture#</td>
                                                        <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                                        <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                        <td class="pt-3 pb-3">#comment#</td>
                                                        <td class="pt-3 pb-3"></td>
                                                        <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                                    </tr>
                                                </cfoutput>
                                            </cfif>
                                        <cfelse>
                                            <cfoutput>
                                                <tr>
                                                    <td class="pt-3 pb-3">#role.datefacture#</td>
                                                    <td class="pt-3 pb-3 hidden">#facture.TotalTTC# €</td>
                                                    <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                    <td class="pt-3 pb-3">#comment#</td>
                                                    <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#Resultat.GENERATED_KEY#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                                </tr>
                                            </cfoutput>
                                        </cfif>
                                    </cfif>
                                <cfelse>
                                    <cfoutput>
                                        <tr>
                                            <td class="pt-3 pb-3">#role.datefacture#</td>
                                            <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                            <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                            <td class="pt-3 pb-3">#comment#</td>
                                            <cfif #lect_etablissement.PaiementCbInternet# eq 1 and #lect_client.ModePaiement# eq 0>
                                                <td class="pt-3 pb-3"></td>
                                            </cfif>
                                            <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#&noclassactive=1&sess=#session.use_id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                        </tr>
                                    </cfoutput>
                                </cfif>
                            </cfif>
                        <cfelse><!--- pas facture, détail conso ?? --->
                            <cfquery name="classactive" datasource="classactive">
                                Select id from documents use index(siret_Id_Proprio_CodeAppli_Table_origine_Id_Table_Origine)
                                Where siret = '#lect_user.siret#' and id_Proprio='#lect_client.id#' and codeappli='F8' 
                                and Table_origine='role' and Id_Table_Origine='#role.id#'
                            </cfquery>
                            <cfif #classactive.recordcount# eq 1>
                                <cfif #lect_etablissement.PaiementCbInternet# eq 1 and #lect_client.ModePaiement# eq 0>
                                    <cfif #facture.FactureRéglée# eq 0>
                                        <cfoutput>
                                            <tr>
                                                <td class="pt-3 pb-3">#role.datefacture#</td>
                                                <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                                <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                <td class="pt-3 pb-3">#comment#</td>
                                                <td class="pt-3 pb-3"><a class="link-red" href="/facture/?<cfoutput>#facture.id#&id_mnu=144&Purl=oui</cfoutput>">CLIQUEZ pour régler</a></td>
                                                <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                            </tr>
                                        </cfoutput>
                                    <cfelse>
                                        <cfoutput>
                                            <tr>
                                                <td class="pt-3 pb-3">#role.datefacture#</td>
                                                <td class="pt-3 pb-3">#facture.TotalTTC# €</td>
                                                <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                                <td class="pt-3 pb-3">#comment#</td>
                                                <td class="pt-3 pb-3"></td>
                                                <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                            </tr>
                                        </cfoutput>
                                    </cfif>
                                <cfelse>
                                    <cfoutput>
                                        <tr>
                                            <td class="pt-3 pb-3">#role.datefacture#</td>
                                            <td class="pt-3 pb-3 hidden">#facture.TotalTTC# €</td>
                                            <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                            <td class="pt-3 pb-3">#comment#</td>
                                            <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#classactive.id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                        </tr>
                                    </cfoutput>
                                </cfif>
                            <cfelse>
                                <!---<cfgridrow data="#role.datefacture#,Du #role.DateDebPériode#  au #role.DateFinPériode#,#role.STitreFacture#,#role.id#&espaceparentconso=1&cli=#lect_client.NumClient#">--->
                            </cfif>
                        </cfif>
                    </cfloop>
                </cfif>
                <cfquery name="anfacture" datasource="f8w_test">
                    Select mid(NoRole,1,4) as an from facture use index(siret_numclient) 
                    where siret='#lect_etablissement.siret#' and numclient='#lect_client.numclient#' group by mid(NoRole,1,4)
                    order by mid(NoRole,1,4) desc
                </cfquery> 
            </table>
            
            <label class="pt-4 text-red bold" for="ddsfdsfds">Pour votre déclaration d'impots :</label>
            <cfselect name="ddsfdsfds" id="ddsfdsfds" onChange="MM_jumpMenu('self',this,0)">
                <option class="dropdown-item" value="">Attestation de facture pour l'année :</option>
                <cfoutput query="anfacture">
                    <cfif isdefined("url.an")>
                        <cfif #url.an# eq #anfacture.an#>
                            <option class="dropdown-item" selected value="?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#anfacture.an#">Attestation de facture pour l'année : #anfacture.an#</option>
                        <cfelse>
                            <option class="dropdown-item" value="?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#anfacture.an#">Attestation de facture pour l'année : #anfacture.an#</option>
                        </cfif>
                    <cfelse>
                        <option class="dropdown-item" value="?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#anfacture.an#">Attestation de facture pour l'année : #anfacture.an#</option>
                    </cfif>
                </cfoutput>
            </cfselect>
            <cfif isdefined("url.espaceparentfacture")>
                <cfoutput>
                    <a class="link-grey" href="https://gestion.cantine-de-france.fr/app/content/edition/rolefacture/attestationfacture.cfm?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#url.an#" target="_blank">Imprimer</a>
                </cfoutput>
            </cfif>
        </cfform>
    </div>
</div>