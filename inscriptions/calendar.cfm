﻿<cfset inscriptionamodifierpourheurearrive = 0>
<cfset Msgquota = 0>
<cfset affheureprestation = "">
<cfif isdefined("form.quelforfait")>
	<!--- choix du forfait --->
	<cfquery name="lectred" datasource="f8w_test">
		Select GroupeClient from reductions where id='#form.quelforfait#'
	</cfquery>
    <cfquery name="add" datasource="f8w_test">
    	Insert into clientgroupe(siret,NumGroupe,NumEnfant) value ('#lect_etablissement.siret#','#lectred.GroupeClient#',
        '#form.sav_numenfant#')
    </cfquery>
</cfif>
<cfif isdefined("form.reserve_aoupas")><!--- clic sur tableau --->
	<cfquery name="reserve_aoupas" datasource="f8w_test">
    	Select * from prestation_reserve_a where id='#form.sav_id#'
    </cfquery>
	<cfquery name="lect_prestation" datasource="f8w_test">
		Select * from prestation Where id='#form.sav_id_presta#'
	</cfquery>
    <cfquery name="lect_enfant" datasource="f8w_test">
    	select concat_ws(" ",Prénom,Nom) as lenfant
        from enfant where siret='#lect_etablissement.siret#' And NumEnfant='#form.sav_numenfant#'
    </cfquery>
	<cfset ouvertedu = "">
    <cfset ouvertedu1 = "">
    <cfset ouvertedustp = "">
	<cfif len(#reserve_aoupas.DebutPerioPrestaAn#)>
        <cfset premjouran1 = createdate(#reserve_aoupas.DebutPerioPrestaAn#,1,1)>
        <cfset ouvertedu = dateadd("d",#reserve_aoupas.DebutPerioPresta# -1,#premjouran1#)>
        <cfset ouvertedu1 = lsdateformat(#ouvertedu#,"dd/MM/YYYY")>
        <cfset ouvertedustp = lsdateformat(#ouvertedu#,"YYYYMMdd")>
    </cfif>
	<cfset ouverteau = "">
    <cfset ouverteau1 = "">
    <cfset ouverteaustp = "">
	<cfif len(#reserve_aoupas.FinPerioPrestaAn#)>
        <cfset premjouran1 = createdate(#reserve_aoupas.FinPerioPrestaAn#,1,1)>
        <cfset ouverteau = dateadd("d",#reserve_aoupas.FinPerioPresta# -1,#premjouran1#)>
        <cfset ouverteau1 = lsdateformat(#ouverteau#,"dd/MM/YYYY")>
        <cfset ouverteaustp = lsdateformat(#ouverteau#,"YYYYMMdd")>
    </cfif>
    <cfset nombredejour = datediff("d",#ouvertedu#,#ouverteau#) + 3>
    <cfset DateJourAtraiter = ouvertedu>
	<cfset DateJourAtraiterstr = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
    <cfset DateJourAtraiterstp = lsdateformat(#DateJourAtraiter#,"YYYYMMdd")>    
	<cfif isdefined("form.inscrit")><!--- inscription --->
        <cfquery name="enfantclasse" datasource="f8w_test">
            Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#lect_user.siret#' 
            and NumEnfant = '#form.sav_numenfant#'
        </cfquery>
        <cfset NumLieuPrestation = 0>
        <cfset NumClassePrestation = 0>
        <cfset NumLieuPrestationQuota = 0>
        <cfset NumIdReserveoupas = 0>
		<cfif #enfantclasse.recordcount# gt 0>         
            <cfloop query="enfantclasse">
                <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                    Select * from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
                    Where siret Like '%#lect_user.siret#%' and (NumClasse = '#enfantclasse.NumClasse#' or NumClasse=0) 
                    and NumPrestation = '#reserve_aoupas.NumPrestation#' 
                    and (id_reserve_a='#reserve_aoupas.id#' or id_reserve_a=0) limit 0,1
                </cfquery>
                <cfif #lieuprestationclasse.recordcount#>
                    <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                    <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                    <cfset NumClassePrestation = #lieuprestationclasse.NumClasse#>
                    <cfset NumIdReserveoupas = #lieuprestationclasse.id_reserve_a#>
                    <cfbreak>
                </cfif>
			</cfloop>
		</cfif>        
		<cfif #NumLieuPrestationQuota# gt 0>
        	<cfset Msgquota = Module.JourPrestationLieuQuota1(#lect_user.siret#,#ouvertedu1#,#reserve_aoupas.NumPrestation#,#NumLieuPrestation#,#NumClassePrestation#,#lect_prestation.siret#,#NumIdReserveoupas#)>
<!---            <cfif #cgi.REMOTE_ADDR# eq "78.201.4.76">
				<cfoutput>
                NumLieuPrestationQuota #NumLieuPrestationQuota#<br>
                Msgquota #Msgquota#<br>
                reserve_aoupas.NumPrestation #reserve_aoupas.NumPrestation#<br>
                NumClassePrestation #NumClassePrestation#<br>
                ouvertedu1 #ouvertedu1#<br>
                NumLieuPrestation #NumLieuPrestation#
                </cfoutput>
                <cfabort>
            </cfif>
--->
			<cfif #Msgquota# gte #NumLieuPrestationQuota#>                       	
                <cfset msg = "Désolé, vous ne pouvez pas inscrire " & #lect_enfant.lenfant# & " le à cette prestation.">
                <!---<cfset msg = #msg# & " Nombre de place maxi : " & #NumLieuPrestationQuota# & " , nombre d'inscrit : " & #Msgquota# & " !">--->
                <cfset msg = #msg# & " Le nombre de place maximum est atteint !">
                <cfset msg = urlencodedformat(#msg#)>
                <cfif #session.log_mairie# is true>
                    <cfset log_action_web = "Inscription par la mairie IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                <cfelse>
                    <cfset log_action_web = "Inscription IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
                </cfif>
                <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                <cfset log_action_web = #log_action_web# & "<br>Application le " & #url.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
                <cfset log_action_web = #log_action_web# & "<br>Quota : " & #NumLieuPrestationQuota# & ", nombre d'inscrit : " & #Msgquota# & ".">
                <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                <cfset dtapplication = #url.datejour#>
                <cfquery name="log" datasource="f8w_test">
                    Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
                    <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
                </cfquery>
                <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
            </cfif>
        </cfif>

		<cfif #session.log_mairie# is true>
            <cfset log_action_web = "Inscription par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
        <cfelse>
            <cfset log_action_web = "Inscription par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
        </cfif>
        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
        <cfset log_action_web = #log_action_web# & "<br>Période du " & #ouvertedu1# & " au " & #ouverteau1# >
        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
        <cfset dtapplication = #url.datejour#>
        <cfquery name="log" datasource="f8w_test">
            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
        </cfquery>
        
        <cfset NombreDeBoucle = 0>
        <cfloop from="1" to="#nombredejour#" index="idx">
 			<cfset a = Module.ResaJour(#lect_prestation.JoursExclus#,#DayOfWeek(DateJourAtraiter)#)>
            <cfif #a# neq 1>
                <cfif #NumLieuPrestationQuota# gt 0>
                	<cfset ouvertedu1test = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
					<!---<cfset Msgquota = Module.JourPrestationLieuQuota(#lect_user.siret#,#ouvertedu1test#,#reserve_aoupas.NumPrestation#,#NumLieuPrestation#)>--->
                    <cfset Msgquota = Module.JourPrestationLieuQuota1(#lect_user.siret#,#ouvertedu1test#,#reserve_aoupas.NumPrestation#,#NumLieuPrestation#,#NumClassePrestation#,#lect_prestation.siret#,#NumIdReserveoupas#)>
                    <cfif #Msgquota# gte #NumLieuPrestationQuota#>                       	
                        <cfset msg = "Désolé, vous ne pouvez pas inscrire " & #lect_enfant.lenfant# & " le à cette prestation.">
                        <!---<cfset msg = #msg# & " Nombre de place maxi : " & #NumLieuPrestationQuota# & " , nombre d'inscrit : " & #Msgquota# & " !">--->
                        <cfset msg = #msg# & " Le nombre maximal de place est atteint !">
						<cfset msg = urlencodedformat(#msg#)>
                        <cfif #session.log_mairie# is true>
                            <cfset log_action_web = "Inscription par la mairie IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                        <cfelse>
                            <cfset log_action_web = "Inscription IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
                        </cfif>
                        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                        <cfset log_action_web = #log_action_web# & "<br>Application le " & #url.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
                        <cfset log_action_web = #log_action_web# & "<br>Quota : " & #NumLieuPrestationQuota# & ", nombre d'inscrit : " & #Msgquota# & ".">
                        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfset dtapplication = #url.datejour#>
                        <cfquery name="log" datasource="f8w_test">
                            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
                            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
                        </cfquery>
                        <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                    </cfif>
                </cfif>
                <cfset NombreDeBoucle = #NombreDeBoucle# + 1 >
				<!--- gestion prepaiement sur inscription --->
                <cfif #lect_prestation.ticket# eq 1 and #NombreDeBoucle# eq 1>
                	<!--- prestation en prépaiement ont verifie si ticket dispo sinon ont re-dirige sur paiement --->
                	<cfquery name="prestation_prepaye" datasource="f8w_test">
                		Select * from prestation_prepaye use index(siret_NumPrestation_NumClient) 
                        Where siret='#lect_etablissement.siret#' and NumPrestation='#lect_prestation.numprestation#' 
                        and numclient='#lect_client.NumClient#' and Qte>0
                	</cfquery>
                    <cfif #prestation_prepaye.recordcount#>
                    	<cfset NewQte = #prestation_prepaye.Qte# - 1>
                        <cfquery name="maj" datasource="f8w_test">
                        	Update prestation_prepaye set Qte='#NewQte#' where id = '#prestation_prepaye.id#'
                        </cfquery>
                        <cfquery name="add" datasource="f8w_test">
                        	Insert into prestation_prepaye_histo (siret,numprestation,numclient,Qte_precedante,Qte_Ajouté,Qte_Nouvelle,
                            FaitPar) values ('#lect_etablissement.siret#','#lect_prestation.numprestation#','#lect_client.NumClient#',
                            '#prestation_prepaye.Qte#',-1,'#NewQte#','#lect_client.NumClient#')
                        </cfquery>
                    <cfelse>
                    	<!--- ont prepare le paiement et ont re-dirige --->
                        <cfset leuuid = createuuid()>
    					<cfset leuuid1 = replacenocase(#leuuid#,"-","","all")>
						<cfset session.uuidpanier = right(#leuuid1#,30)>
						<!--- ont determine le prix --->
                        <cfquery name="prixachat" datasource="f8w_test">
                            Select pu from prixprestation use index(siret_numprestation) Where siret='#lect_prestation.siret#' 
                            and numprestation='#lect_prestation.numprestation#' and CodePrix=1 and del=0
                        </cfquery> 
						<!--- IL FAUDRA VOIR A GERER LES REDUCTIONS....!!!! --->
                        <cfquery name="reductionachat" datasource="f8w_test">
                            Select * from reductions use index(siret_numprestation) 
                            where siret='#lect_prestation.siret#' 
                            and NumPrestation='#lect_prestation.numprestation#' 
                            and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                            and MontReduc<>0 and del=0 order by GroupeClient desc
                        </cfquery>
						<cfset reduc = 0>
                        <cfloop query="reductionachat">
                        	<cfif #reductionachat.GroupeClient# gt 0>
                        		<cfquery name="verclientgroupe" datasource="f8w_test">
                                	Select * from clientgroupe use index(siret_NumClient_NumGroupe) 
                                    where siret='#lect_prestation.siret#' and numClient='#lect_client.numclient#' 
                                    and numgroupe='#reductionachat.GroupeClient#'
                                </cfquery>
                                <cfif #verclientgroupe.recordcount#>
                                	<cfquery name="vergroupeclient" datasource="f8w_test">
                                    	select * from groupeclient use index(siret_NumGroupe)
                                        where siret='#lect_prestation.siret#' and numgroupe='#reductionachat.GroupeClient#' 
                                        and del=0
                                    </cfquery>
                                	<cfif #vergroupeclient.recordcount#>
                                    	<cfset reduc = #reductionachat.MontReduc#>
                                		<cfbreak>
                                    </cfif>
                                </cfif>
                        	<cfelse>
                            	<cfset reduc = #reductionachat.MontReduc#>
                                <cfbreak>
                            </cfif>
                        </cfloop>
						<cfif #reduc# neq 0>
                            <cfset PrixReduitAchat = #prixachat.PU# - #reduc#>
                            <cfset NewMontantAchat = 1 * #PrixReduitAchat#>
                            <cfset prixachat.pu = #PrixReduitAchat#>
                        <cfelse>
                            <cfset NewMontantAchat = 1 * #prixachat.PU#>
                        </cfif>
                        
                        <cfquery name="add" datasource="f8w_test" result="Resultat">
                            Insert into cbpanier (uuid,siret,NumClient,NumPrestation,Qte,pu,Montant,mail,Id_prestation_reserve_a,
                            NumEnfant,IdClient) 
                            values ('#session.uuidpanier#','#lect_prestation.siret#','#lect_client.numclient#',
                            '#lect_prestation.numprestation#',1,'#prixachat.pu#','#NewMontantAchat#',
                            '#lect_user.login#','#reserve_aoupas.id#','#form.sav_numenfant#','#lect_client.id#')
                        </cfquery>
                        <cflocation url="/AchatSess/?idpan=#Resultat.GENERATED_KEY#" addtoken="no">
                    
                    </cfif>
                </cfif>
                <cfquery name="verifinscription" datasource="f8w_test">
                    Select id,NumInscription from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                    Where siret='#lect_etablissement.siret#' and NumPrestation='#reserve_aoupas.NumPrestation#' and NumEnfant='#form.sav_numenfant#' 
                    and DateInscription='#DateJourAtraiterstr#'
                </cfquery>
                <cfif #verifinscription.recordcount# eq 0>
                    <cfset NumInscription = Module.NumSuivant6(#lect_user.siret#,"inscriptionplanning","NumInscription")>
                    <cfquery name="add" datasource="f8w_test">
                        Insert into inscriptionplanning (siret,NumInscription,numprestation,numenfant,dateinscription,Nature,
                        DateInscriptionSTP,id_reserve_a) Values ('#lect_user.siret#','#NumInscription#','#reserve_aoupas.NumPrestation#',
                        '#form.sav_numenfant#','#DateJourAtraiterstr#','0','#DateJourAtraiterstp#','#reserve_aoupas.id#')
                    </cfquery>
                    <cfif #lect_etablissement.FullWeb# neq 1>
                        <cfset req_sql = "Insert into inscriptionplanning (NumInscription,numprestation,numenfant,dateinscription,Nature">
                        <cfset req_sql = #req_sql# & ") Values (" & #NumInscription# & "," & #reserve_aoupas.NumPrestation# & "," & #form.sav_numenfant#>
                        <cfset req_sql = #req_sql# & ",'" & #DateJourAtraiterstr# & "',0)">
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                            '#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>
                    </cfif>
                    <cfif #lect_prestation.PrixParSession# eq 1><!--- ont inscrit une seule fois pour la session --->
                    	<cfbreak>
                    </cfif>
                <cfelse>
                    <cfquery name="maj" datasource="f8w_test">
                        Update inscriptionplanning set Nature='0', id_reserve_a='#reserve_aoupas.id#' Where id = '#verifinscription.id#'
                    </cfquery>
                    <cfif #lect_etablissement.FullWeb# neq 1>
                        <cfset req_sql = "Update inscriptionplanning set Nature=0 where Numinscription=" & #verifinscription.NumInscription#>
                        <cfset req_sql = #req_sql# & " and numprestation=" & #reserve_aoupas.NumPrestation# & " and numenfant=">
                        <cfset req_sql = #req_sql# & #form.sav_numenfant# >
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                            '#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>            
                    </cfif>
                    <cfif #lect_prestation.PrixParSession# eq 1><!--- ont inscrit une seule fois pour la session --->
                    	<cfbreak>
                    </cfif>                              
                </cfif>
			</cfif>
			<cfif #DateJourAtraiterstr# neq #ouverteau1#>    
				<cfset DateJourAtraiter = DateAdd("d",1,#DateJourAtraiter#)>
            	<cfset DateJourAtraiterstr = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
            	<cfset DateJourAtraiterstp = lsdateformat(#DateJourAtraiter#,"YYYYMMdd")>   
    		<cfelse>
            	<cfbreak>
            </cfif>
        </cfloop>
		<cfset msg1 = "Inscription enregistrée !">
        
    <cfelse><!--- desinscription --->
		<cfif #session.log_mairie# is true>
            <cfset log_action_web = "Désinscription par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
        <cfelse>
            <cfset log_action_web = "Désinscription par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
        </cfif>
        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
        <cfset log_action_web = #log_action_web# & "<br>Période du " & #ouvertedu1# & " au " & #ouverteau1# >
        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
        <cfset dtapplication = #url.datejour#>
        <cfquery name="log" datasource="f8w_test">
            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
        </cfquery>   
        <cfset NombreDeBoucle = 0> 
        <cfset EtaitInscrit = "non">
    	<cfloop from="1" to="#nombredejour#" index="idx">
 			<cfset NombreDeBoucle = #NombreDeBoucle# + 1>
			<cfif #lect_prestation.ticket# eq 1 and #NombreDeBoucle# eq 1 and #EtaitInscrit# eq "oui">
				<!--- prestation en prépaiement ont verifie si ticket dispo --->
                <cfquery name="prestation_prepaye" datasource="f8w_test">
                    Select * from prestation_prepaye use index(siret_NumPrestation_NumClient) 
                    Where siret='#lect_etablissement.siret#' and NumPrestation='#lect_prestation.numprestation#' 
                    and numclient='#lect_client.NumClient#'
                </cfquery>
				<cfif #prestation_prepaye.recordcount#>
                    <cfset NewQte = #prestation_prepaye.Qte# + 1>
                    <cfquery name="maj" datasource="f8w_test">
                        Update prestation_prepaye set Qte='#NewQte#' where id = '#prestation_prepaye.id#'
                    </cfquery>
                    <cfquery name="add" datasource="f8w_test">
                        Insert into prestation_prepaye_histo (siret,numprestation,numclient,Qte_precedante,Qte_Ajouté,Qte_Nouvelle,
                        FaitPar) values ('#lect_etablissement.siret#','#lect_prestation.numprestation#','#lect_client.NumClient#',
                        '#prestation_prepaye.Qte#',1,'#NewQte#','#lect_client.NumClient#')
                    </cfquery>
                <cfelse>			
        			<cfquery name="add" datasource="f8w_test">
        				insert into prestation_prepaye(siret,numprestation,numclient,Qte) Value 
                        ('#lect_etablissement.siret#','#lect_prestation.numprestation#','#lect_client.NumClient#',1)
        			</cfquery>
                    <cfquery name="add" datasource="f8w_test">
                        Insert into prestation_prepaye_histo (siret,numprestation,numclient,Qte_precedante,Qte_Ajouté,Qte_Nouvelle,
                        FaitPar) values ('#lect_etablissement.siret#','#lect_prestation.numprestation#','#lect_client.NumClient#',
                        0,1,1,'#lect_client.NumClient#')
                    </cfquery>                    
                </cfif>
            </cfif>
			<cfset a = Module.ResaJour(#lect_prestation.JoursExclus#,#DayOfWeek(DateJourAtraiter)#)>
            <cfif #a# neq 1> 			
                <cfquery name="verifinscription" datasource="f8w_test">
                    Select id,NumInscription from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                    Where siret='#lect_etablissement.siret#' 
                    and NumPrestation='#reserve_aoupas.NumPrestation#' 
                    and NumEnfant='#form.sav_numenfant#' and DateInscription='#DateJourAtraiterstr#'
                </cfquery>
                <cfif #verifinscription.recordcount# eq 0>
                    <cfset NumInscription = Module.NumSuivant6(#lect_user.siret#,"inscriptionplanning","NumInscription")>
                    <cfquery name="add" datasource="f8w_test">
                        Insert into inscriptionplanning (siret,NumInscription,numprestation,numenfant,dateinscription,Nature,
                        DateInscriptionSTP,id_reserve_a) Values ('#lect_user.siret#','#NumInscription#','#reserve_aoupas.NumPrestation#',
                        '#form.sav_numenfant#','#DateJourAtraiterstr#','1','#DateJourAtraiterstp#','#reserve_aoupas.id#')
                    </cfquery>
                    <cfif #lect_etablissement.FullWeb# neq 1>
                        <cfset req_sql = "Insert into inscriptionplanning (NumInscription,numprestation,numenfant,dateinscription,Nature">
                        <cfset req_sql = #req_sql# & ") Values (" & #NumInscription# & "," & #reserve_aoupas.NumPrestation# & "," & #form.sav_numenfant#>
                        <cfset req_sql = #req_sql# & ",'" & #DateJourAtraiterstr# & "',1)">
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                            '#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>
                    </cfif>
                <cfelse>
                	<cfif #EtaitInscrit# eq "non">
                		<cfset EtaitInscrit = "oui">
                    	<cfset NombreDeBoucle = 0>
                    </cfif>
                    <cfquery name="maj" datasource="f8w_test">
                        Update inscriptionplanning set Nature='1',id_reserve_a='#reserve_aoupas.id#' Where id = '#verifinscription.id#'
                    </cfquery>
                    <cfif #lect_etablissement.FullWeb# neq 1>
                        <cfset req_sql = "Update inscriptionplanning set Nature=1 where Numinscription=" & #verifinscription.NumInscription#>
                        <cfset req_sql = #req_sql# & " and numprestation=" & #reserve_aoupas.NumPrestation# & " and numenfant=">
                        <cfset req_sql = #req_sql# & #form.sav_numenfant# >
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                            '#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>            
                    </cfif>            
                </cfif>
			</cfif>
			<cfif #DateJourAtraiterstr# neq #ouverteau1#>    
				<cfset DateJourAtraiter = DateAdd("d",1,#DateJourAtraiter#)>
            	<cfset DateJourAtraiterstr = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
            	<cfset DateJourAtraiterstp = lsdateformat(#DateJourAtraiter#,"YYYYMMdd")>   
    		<cfelse>
            	<cfbreak>
            </cfif>
        </cfloop>	
		<cfset msg1 = "Désinscription enregistrée !">
	</cfif>
	<cfset msg1 = urlencodedformat(#msg1#)>
    <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg1=#msg1#&quotaconso=#Msgquota#" addtoken="no">
</cfif>
<cfif len(#url.nature#)><!--- clic sur calendrier --->
	<!--- verif si action possible --->
    <cfquery name="lect_prestation" datasource="f8w_test">
		Select * from prestation Where id = '#url.id_prestation#' and siret='#lect_user.siret#'
	</cfquery>
    <cfquery name="lect_enfant" datasource="f8w_test">
	    select * from enfant where id = '#url.id_enfant#' and siret='#lect_user.siret#'
    </cfquery>        
    <cfif #lect_prestation.recordcount# eq 0 or #lect_enfant.recordcount# eq 0 or #lect_prestation.numprestation# neq #url.numprestation# or #lect_enfant.numenfant# neq #url.numenfant#>
    	<cfabort>
    </cfif>

	<cfset datedeb = createdate(mid(#url.datejour#,7,4),mid(#url.datejour#,4,2),mid(#url.datejour#,1,2))>
    <cfset date_no_good = false>
	
	<!--- modif 07/11/2014 --->
    <cfset NumDaujourDhui = DayOfWeek(now())>
    <cfquery name="verifnewdelresaweb" datasource="f8w_test">
        Select * from resawebdelai use index(siret_NumPrestation) Where siret='#lect_prestation.siret#' 
        and NumPrestation='#lect_prestation.NumPrestation#' and LejourSemaine='#NumDaujourDhui#'
    </cfquery>
    <cfif #verifnewdelresaweb.recordcount# gt 0>
    	<cfset datedebtime = createdatetime(mid(#url.datejour#,7,4),mid(#url.datejour#,4,2),mid(#url.datejour#,1,2),hour(now()),minute(now()),second(now()))>
        
		<cfif lsdateformat(#datedebtime#,"dd/mm/yyyy") eq lsdateformat(now(),"dd/mm/yyyy")><!--- meme jour --->
        	<cfset NbrJourEcart = DateDiff('d',now(),#datedebtime#)>
        <cfelse>
        	<cfset NbrJourEcart = DateDiff('d',now(),#datedebtime#) + 1>
        </cfif>
        
    	<cfset HeureDaujourDhui = LsTimeFormat(now(),"HHmmss")>
        <cfset HeureLimite = replacenocase(#verifnewdelresaweb.JusquaH#,":","","all")>
        <cfif #HeureDaujourDhui# lte #HeureLimite#>
        	<cfset NbrJourEcartMin = #verifnewdelresaweb.DelaiJusqua#>
        <cfelse>    
        	<cfset NbrJourEcartMin = #verifnewdelresaweb.DelaiApres#>
            <cfset verifnewdelresaweb.DelaiDesincr = 0>
        </cfif>
        
		<cfif #NbrJourEcart# lt 0>
        	<cfset date_no_good = true>
        <cfelse>
            <!---<cfif #NbrJourEcart# lt #NbrJourEcartMin#>
            modif 29/01/2018 --->
            
			<!---<cfif #NbrJourEcart# lte #NbrJourEcartMin#> pour les petit gourmand mais marche pas pour st just malmont !!! --->
            <cfif #NbrJourEcart# lt #NbrJourEcartMin#>
            	<cfset date_no_good = true>
            </cfif>
        </cfif>
        <cfif #verifnewdelresaweb.DelaiDesincr# gt 0 and #url.action# eq "desinscr"><!--- delai différent pour la desinscription --->
        	<cfif #verifnewdelresaweb.DelaiDesincrLesJoursSuivant# eq 0>
				<cfif #NbrJourEcart# eq #verifnewdelresaweb.DelaiDesincr#>
                    <cfset date_no_good = false>
                </cfif>
			<cfelse>
				<cfif #NbrJourEcart# gte #verifnewdelresaweb.DelaiDesincr#>
                    <cfset date_no_good = false>
                </cfif>            
            </cfif>
        </cfif>
<!---        <cfoutput>
        	url.datejour	#url.datejour#<br>
            datedebtime		#datedebtime#<br>
            now()			#now()#<br>
        	NbrJourEcartMin #NbrJourEcartMin#<br>
        	NbrJourEcart	#NbrJourEcart#<br>
            date_no_good	#date_no_good#
        </cfoutput>--->   
    </cfif>
	<cfif #date_no_good# is false><!--- jour spécial sur jour --->
        <!--- <cfset h = lstimeformat(now(),"HHMM")> --->
        <cfset h = lstimeformat(now(),"HH")>
        <cfset dateapartirstp = lsdateformat(now(),"YYYYMMdd") & #h#>
    	<cfquery name="joursspecial" datasource="f8w_test">
        	Select id from ajustresaweb_date use index(siret) 
            Where siret='#lect_prestation.siret#' and (NumPrestation='#lect_prestation.NumPrestation#' or NumPrestation=0)
            and DateApartirSTP <= #dateapartirstp# and DatePour = '#url.datejour#' and JourOuPeriode = 0
        </cfquery>
        <cfif #joursspecial.recordcount#>
        	<cfset date_no_good = true>
        </cfif>
    </cfif>
    <cfif #date_no_good# is false and #session.log_mairie# is false><!--- reserve a (tap) --->
		<cfset reserve_a = Module.prestation_reserve_a1(#lect_client.siret#,#lect_prestation.NumPrestation#,#lect_client.NumClient#,#url.NumEnfant#,DayOfYear(#datedeb#),"clic",#url.datejour#,#lect_prestation.siret#)>
    	
		<cfif #reserve_a# neq "OK">
			<cfset msg = #reserve_a#>
        	<cfset msg = urlencodedformat(#msg#)>
        	<cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">        
        </cfif>
    </cfif>    
	<cfif #date_no_good# and #session.log_mairie# is false>
        <cfquery name="lect_enfant" datasource="f8w_test">
            select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#url.id_enfant#'
        </cfquery>        
            <!--- log --->
        <cfif #url.action# eq "desinscr">
        	<cfif #session.log_mairie# is true>
        		<cfset log_action_web = "Désinscription ponctuelle par la mairie IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
            <cfelse>
        		<cfset log_action_web = "Désinscription ponctuelle IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
        	</cfif>
        <cfelse>
        	<cfif #session.log_mairie# is true>
        		<cfset log_action_web = "Inscription ponctuelle par la mairie IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
        	<cfelse>
        		<cfset log_action_web = "Inscription ponctuelle IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
            </cfif>
        </cfif>
        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
        <cfset log_action_web = #log_action_web# & "<br>Application le " & #url.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
        <cfset dtapplication = #url.datejour#>
        <cfquery name="log" datasource="_test">
        	Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
        </cfquery>
    
    	<cfset msg = "Vous ne pouvez plus intervenir sur la journée du " & #url.datejour# & " !">
        <cfset msg = urlencodedformat(#msg#)>
        <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
    <cfelse>
    	<!--- ont a le droit de desinscrire ou d'inscrire --->
        <cfquery name="enfantclasse" datasource="f8w_test">
            Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#lect_user.siret#' 
            and NumEnfant = '#url.NumEnfant#'
        </cfquery>
        <cfset NumLieuPrestation = 0>
        <cfset NumLieuPrestationQuota = 0>
        <cfset NumLieuPrestationClasse = 0>
        <cfif #enfantclasse.recordcount# gt 0>         
            <cfloop query="enfantclasse">
                <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                    Select * from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
                    Where siret Like '%#lect_user.siret#%' 
                    and (NumClasse = '#enfantclasse.NumClasse#' or NumClasse=0) and NumPrestation = '#url.NumPrestation#' limit 0,1
                </cfquery>
                <cfif #lieuprestationclasse.recordcount#>
                    <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                    <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                    <cfset NumLieuPrestationClasse = #lieuprestationclasse.NumClasse#>
                    <cfbreak>
                </cfif>
			</cfloop>		
        <cfelse>
            <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                Select * from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
                Where siret Like '%#lect_user.siret#%' 
                and NumClasse=0 and NumPrestation = '#url.NumPrestation#' limit 0,1
            </cfquery>
            <cfif #lieuprestationclasse.recordcount#>
                <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                <cfset NumLieuPrestationClasse = #lieuprestationclasse.NumClasse#>
            </cfif>        
        </cfif>
		<cfquery name="testlieuprestationclassecummulee" datasource="f8w_test" maxrows="1"><!--- Quotat cumule pour cette presta ? --->
        	Select * from lieuprestationclassecummulee use index(siret) 
            Where siret = '#lect_user.siret#' and find_in_set('#url.NumPrestation#',NumPrestation)
        </cfquery>
        <cfif #testlieuprestationclassecummulee.recordcount#>
        	<cfloop query="enfantclasse">
                <cfquery name="testlieuprestationclassecummulee" datasource="f8w_test">
					<!--- Quotat cumule pour la classe de l'enfant ou toutes les classe et cette presta ? --->
                    Select * from lieuprestationclassecummulee use index(siret) 
                    Where siret = '#lect_user.siret#' and NumClasse='#enfantclasse.NumClasse#' 
                    and find_in_set('#url.NumPrestation#',NumPrestation) or 
                    siret = '#lect_user.siret#' and NumClasse='0' and find_in_set('#url.NumPrestation#',NumPrestation)
                </cfquery>
				<cfif #testlieuprestationclassecummulee.recordcount#>
                	<cfset NumLieuPrestationQuota = #testlieuprestationclassecummulee.Quota#>
                	<cfquery name="testlieuprestationclassecummulee" datasource="f8w_test"><!--- Quotat cumule pour cette presta --->
        				Select * from lieuprestationclassecummulee use index(siret) 
            			Where siret = '#lect_user.siret#' and find_in_set('#url.NumPrestation#',NumPrestation) 
                        and Quota='#NumLieuPrestationQuota#'
        			</cfquery>
                    <cfbreak>
                </cfif>            
            </cfloop>
        </cfif>
		<cfif #url.action# eq "inscr" and #url.nature# eq "0" or #url.action# eq "reinscr" and #url.nature# eq "2"><!--- sauf si limitation d'inscription/prestation/semaine --->		
			<!---<cfset msglimitation = Module.limitation(#url.id_enfant#,#url.id_prestation#,#url.datejour#)>
        	<cfif len(#msglimitation#)>
            	<cfset msg = urlencodedformat(#msglimitation#)>
                <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
            </cfif>--->
        	<!--- ou quota de place maxi sur prestation --->
			<cfif #NumLieuPrestationQuota# gt 0 or #testlieuprestationclassecummulee.recordcount#><!--- Quota --->
        		<cfif #testlieuprestationclassecummulee.recordcount# eq 0>
					<!---<cfset Msgquota = Module.JourPrestationLieuQuota(#lect_user.siret#,#url.datejour#,#url.NumPrestation#,#NumLieuPrestation#,#NumLieuPrestationClasse#)>--->
					<cfif find(",",#lieuprestationclasse.siret#) eq 0>
						<cfset Msgquota = Module.JourPrestationLieuQuota1(#lect_user.siret#,#url.datejour#,#url.NumPrestation#,#NumLieuPrestation#,#NumLieuPrestationClasse#,#lect_prestation.siret#)>
                	<cfelse>
                    	<cfset Msgquota = 0>
                        <cfloop list="#lieuprestationclasse.siret#" delimiters="," index="srt">
                        	<cfset Msgquota1 = Module.JourPrestationLieuQuota1(#srt#,#url.datejour#,#url.NumPrestation#,#NumLieuPrestation#,#NumLieuPrestationClasse#,#lect_prestation.siret#)>
                        	<cfset Msgquota = #Msgquota# + #Msgquota1#>
                        </cfloop>
                    </cfif>
                <cfelse>
                	<cfset Msgquota = 0>
                	<cfset MsgquotaCummulee = 0>
                	<cfloop query="testlieuprestationclassecummulee">
                        <cfloop list="#testlieuprestationclassecummulee.NumPrestation#" delimiters="," index="idx">							
							<cfset Msgquota = Module.JourPrestationLieuQuota1(#lect_user.siret#,#url.datejour#,#idx#,#testlieuprestationclassecummulee.NumLieu#,#testlieuprestationclassecummulee.NumClasse#,#lect_prestation.siret#)>
                            <!---<cfoutput>#Msgquota#<br>
							#lect_user.siret#,#url.datejour#,#idx#,#testlieuprestationclassecummulee.NumLieu#,#testlieuprestationclassecummulee.NumClasse#<br><br>
							</cfoutput>--->
							<cfset MsgquotaCummulee = #MsgquotaCummulee# + #Msgquota#>
							<cfif #MsgquotaCummulee# gte #NumLieuPrestationQuota#>
                                <cfset Msgquota = #MsgquotaCummulee#>
                                <cfbreak>
                            </cfif>
                        </cfloop>
					</cfloop> 
                    <!---<cfabort>--->         
                </cfif>
				<cfif #Msgquota# gte #NumLieuPrestationQuota#>
                    <cfquery name="lect_enfant" datasource="f8w_test">
                        select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#url.id_enfant#'
                    </cfquery>                        	
					<cfset msg = "Désolé, vous ne pouvez pas inscrire " & #lect_enfant.lenfant# & " le " & #url.datejour# & " à cette prestation.">
                    <!---<cfset msg = #msg# & " Nombre de place maxi : " & #NumLieuPrestationQuota# & " , nombre d'inscrit : " & #Msgquota# & " !">--->
					<cfset msg = #msg# & " Le nombrede place maximal est atteint !">
					<cfset msg = urlencodedformat(#msg#)>
					<cfif #session.log_mairie# is true>
                        <cfset log_action_web = "Inscription ponctuelle par la mairie IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                    <cfelse>
                        <cfset log_action_web = "Inscription ponctuelle IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
                    </cfif>
					<cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                    <cfset log_action_web = #log_action_web# & "<br>Application le " & #url.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
                    <cfset log_action_web = #log_action_web# & "<br>Quota : " & #NumLieuPrestationQuota# & ", nombre d'inscrit : " & #Msgquota# & ".">
					<cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                    <cfset dtapplication = #url.datejour#>
                    <cfquery name="log" datasource="f8w_test">
                        Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
                        <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
                    </cfquery>
                    <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                </cfif>
            </cfif>
        </cfif>
        
        <!--- creation ou suppression inscription --->
        <cfset ResultatTicketPortefeuille = QueryNew("MontantDisponible,MontantDu,IdPortefeuille","Decimal,Decimal,Integer")>
		<cfif #lect_prestation.ticket# eq 1>
            <cfset NombreDeJourDansLeFutur = datediff("d",now(),#datedeb#)>
            <cfquery name="prestation_prepaye_portefeuille_presta" datasource="f8w_test">
            	Select id_ent from prestation_prepaye_portefeuille_presta use index(siret_NumPrestation)
                Where siret='#lect_etablissement.siret#' and NumPrestation='#lect_prestation.NumPrestation#'
            </cfquery>
            <cfif #prestation_prepaye_portefeuille_presta.recordcount# eq 1>
            	<cfset identport = #prestation_prepaye_portefeuille_presta.id_ent#>
            <cfelse>
            	<cfset identport = 0>
            </cfif>
            <cfquery name="prestation_prepaye_portefeuille_ent" datasource="f8w_test">
            	Select * from prestation_prepaye_portefeuille_ent 
                Where id='#identport#' and del=0
            </cfquery>
            <cfif #prestation_prepaye_portefeuille_ent.recordcount# eq 1>
            	<cfquery name="prestation_prepaye_portefeuille" datasource="f8w_test">
            		Select id,Montant from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                    Where siret='#lect_etablissement.siret#' and id_ent_portefeuille='#prestation_prepaye_portefeuille_ent.id#' 
                    and NumClient='#lect_client.NumClient#'
            	</cfquery>
                <cfif (#prestation_prepaye_portefeuille.recordcount# eq 0 and #url.action# neq "desinscr" and #session.log_mairie# is false and (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0 or (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 1 and #NombreDeJourDansLeFutur# gt 15))) 
                or 
                (#prestation_prepaye_portefeuille.recordcount# neq 0 and #prestation_prepaye_portefeuille.Montant# lte 0 and #url.action# neq "desinscr" and #session.log_mairie# is false and (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0 or (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 1 and #NombreDeJourDansLeFutur# gt 15)))>
                	<cfset msglimitation = "Désolé, vous avez consommé la totalité de votre portefeuille, vous ne pouvez plus ajouter d'inscription !!">
                    <cfset msg = urlencodedformat(#msglimitation#)>
                    <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                <cfelse><!--- il reste assez d'argent ???? --->
            		<cfif #prestation_prepaye_portefeuille.recordcount# eq 0>
                    	<cfquery name="add" datasource="f8w_test">
                        	Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient) values (
                            '#lect_etablissement.siret#','#prestation_prepaye_portefeuille_ent.id#','#lect_client.NumClient#')
                        </cfquery>
                        <cfquery name="prestation_prepaye_portefeuille" datasource="f8w_test">
                            Select id,Montant from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                            Where siret='#lect_etablissement.siret#' 
                            and id_ent_portefeuille='#prestation_prepaye_portefeuille_ent.id#' 
                            and NumClient='#lect_client.NumClient#'
                        </cfquery>
                    </cfif>
					<cfset ResultatTicketPortefeuille = Module.ResultatTicketPortefeuille(#lect_user.siret#,#lect_client.NumClient#,#url.NumEnfant#,#url.NumPrestation#,#prestation_prepaye_portefeuille.id#,#url.datejour#,#url.action#,1)>
                    <cfif #val(ResultatTicketPortefeuille.MontantDu)# gt val(#ResultatTicketPortefeuille.MontantDisponible#) and #url.action# neq "desinscr" and #session.log_mairie# is false and #lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0 >
                    	<cfset msglimitation = "Désolé, votre portefeuille électronique présente un solde de " & #ResultatTicketPortefeuille.MontantDisponible# & "€ et cette inscription a un coût de " & #val(ResultatTicketPortefeuille.MontantDu)# & "€, vous ne pouvez plus ajouter d'inscription !!">
                    	<cfset msg = urlencodedformat(#msglimitation#)>
                    	<cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                    </cfif>
            	</cfif>
            <cfelse>
                <cfquery name="prestation_prepaye" datasource="f8w_test">
                    Select * from prestation_prepaye use index(siret_NumPrestation_NumClient) 
                    Where siret='#lect_etablissement.siret#' and NumPrestation='#lect_prestation.NumPrestation#' 
                    and NumClient='#lect_client.NumClient#' or siret='#lect_etablissement.siret#' and NumPrestation='0' 
                    and NumClient='#lect_client.NumClient#'
                </cfquery>
                <cfif (#prestation_prepaye.qte# lte 0 and #url.action# neq "desinscr" and #session.log_mairie# is false and (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0 or (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 1 and #NombreDeJourDansLeFutur# gt 15))) 
                or (#prestation_prepaye.recordcount# eq 0 and #url.action# neq "desinscr" and #session.log_mairie# is false and (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0 or (#lect_etablissement.ResaMemeSiPortefeuilleNul# eq 1 and #NombreDeJourDansLeFutur# gt 15)))>
                    <cfset msglimitation = "Désolé, vous avez consommé la totalité des réservations payées d'avance, vous ne pouvez plus ajouter d'inscription !!">
                    <cfset msg = urlencodedformat(#msglimitation#)>
                    <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                </cfif>	            
			</cfif>        
        </cfif>

        <cfif #url.action# eq "inscr" or #url.action# eq "reinscr"><!--- inscr ou reinscr --->
	        <!--- qte sur semaine DAMMARIE ou ST BRICE SUR VIENNE ??? --->
            <cfset QteMaxSemaine = 0>
            <cfset DesiForfait = "">
            <cfquery name="lect_enfant" datasource="f8w_test">
                select NumEnfant from enfant where id = '#url.id_enfant#'
            </cfquery>                        	    
            <cfquery name="verifforfaitsurreduction" datasource="f8w_test">
                Select * from reductions use index(siret_NumPrestation) 
                Where siret='#lect_etablissement.siret#' and Numprestation='#url.NumPrestation#' 
                and ReserverSurReduction=1
            </cfquery>
            <cfloop query="verifforfaitsurreduction">
                <cfquery name="veradhesion" datasource="f8w_test">
                    Select * from clientgroupe use index(siret_NumGroupe) 
                    Where siret='#verifforfaitsurreduction.siret#' 
                    and numGroupe='#verifforfaitsurreduction.GroupeClient#' 
                    and NumEnfant='#lect_enfant.NumEnfant#'
                </cfquery>
				<cfif #veradhesion.recordcount# gt 0>
					<cfset QteMaxSemaine = #verifforfaitsurreduction.NbrJourMaxSemaineReserverSurReduction#>
                    <cfset DesiForfait = #verifforfaitsurreduction.Désignation#>
                    <cfbreak>
                </cfif>
            </cfloop>
			<cfif #QteMaxSemaine# gt 0>
        		<!--- ont détermine le début et fin de semaine à vérifier en fonction de la date demandée en résa --->
                <cfset DateResaForfait = createdate(mid(#url.datejour#,7,4),mid(#url.datejour#,4,2),mid(#url.datejour#,1,2))>
                <cfset joursemaine = DayOfWeek(#DateResaForfait#)>
                <cfswitch expression="#joursemaine#">
                	<cfcase value="1"><!--- dimanche --->
                    	<cfset JourLundi = dateadd("d",-6,#DateResaForfait#)>
                    	<cfset JourDimanche = #DateResaForfait#>
                    </cfcase>
                	<cfcase value="2"><!--- Lundi --->
                    	<cfset JourDimanche = dateadd("d",6,#DateResaForfait#)>
                    	<cfset JourLundi = #DateResaForfait#>
                    </cfcase>
                	<cfcase value="3"><!--- mardi --->
                    	<cfset JourDimanche = dateadd("d",5,#DateResaForfait#)>
                    	<cfset JourLundi = dateadd("d",-1,#DateResaForfait#)>
                    </cfcase>
                	<cfcase value="4"><!--- mercredi --->
                    	<cfset JourDimanche = dateadd("d",4,#DateResaForfait#)>
                    	<cfset JourLundi = dateadd("d",-2,#DateResaForfait#)>
                    </cfcase>
                	<cfcase value="5"><!--- jeudi --->
                    	<cfset JourDimanche = dateadd("d",3,#DateResaForfait#)>
                    	<cfset JourLundi = dateadd("d",-3,#DateResaForfait#)>
                    </cfcase>
                	<cfcase value="6"><!--- vendredi --->
                    	<cfset JourDimanche = dateadd("d",2,#DateResaForfait#)>
                    	<cfset JourLundi = dateadd("d",-4,#DateResaForfait#)>
                    </cfcase>
                	<cfcase value="7"><!--- samedi --->
                    	<cfset JourDimanche = dateadd("d",1,#DateResaForfait#)>
                    	<cfset JourLundi = dateadd("d",-5,#DateResaForfait#)>
                    </cfcase>
        		</cfswitch>
                <!--- ont collect le nombre de jours réservé pour cette semaine par cet enfant pour cette prestation --->
                <cfset JourDimancheStp = lsdateformat(#JourDimanche#,"dd/MM/YYYY")>
				<cfset JourLundiStp = lsdateformat(#JourLundi#,"dd/MM/YYYY")>
				<cfset QteDejourReserve = Module.GetInscriptionV2(#lect_etablissement.siret#,0,#lect_enfant.NumEnfant#,#url.NumPrestation#,#JourLundiStp#,#JourDimancheStp#,0)>
                <cfset QteJour = 0>
                <cfloop query="#QteDejourReserve#">
                	<cfif #QteDejourReserve.inscrit# eq 1>
                    	<cfset QteJour = #QteJour# + 1>
                	</cfif>
                </cfloop>
                <cfif #QteJour# gte #QteMaxSemaine#>
                	<cfset msglimitation = "Désolé, votre forfait " & #DesiForfait# & " permet " & #QteMaxSemaine# & " réservation(s) par semaine !!">
					<cfset msg = urlencodedformat(#msglimitation#)>
                	<cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                </cfif>
        	</cfif>
        	
			<!--- une seule prestation par jour/enfant alsh sivsc ??? --->
            <cfquery name="profil" datasource="f8w_test">
            	Select NumProfil from profil use index(siret) 
                where siret='#lect_prestation.siret#' and InscriptionUneSeulePrestaJourEnfant=1 and del=0
            </cfquery>
            <cfloop query="profil">
            	<cfquery name="profilprestation" datasource="f8w_test">
                	Select * from profilprestation use index(siret_NumProfil_NumPrestation) 
                    where siret='#lect_prestation.siret#' and NumProfil='#profil.NumProfil#' 
                    and numprestation='#lect_prestation.Numprestation#'
                </cfquery>
                <cfif #profilprestation.recordcount#>
                    <cfquery name="profilprestationFin" datasource="f8w_test">
                        Select * from profilprestation use index(siret_NumProfil_NumPrestation) 
                        where siret='#lect_prestation.siret#' and NumProfil='#profil.NumProfil#' 
                        and numprestation<>'#lect_prestation.Numprestation#'
                    </cfquery>
                	<cfloop query="profilprestationFin">
                		<cfset QteDejourReserve = Module.GetInscriptionV2(#lect_etablissement.siret#,#lect_user.NumClient#,#url.NumEnfant#,#profilprestationFin.NumPrestation#,#url.DateJour#,#url.DateJour#,0,#lect_prestation.siret#)>
                        <cfif #QteDejourReserve.inscrit# neq 0>
                        	<cfquery name="prestadejareserv" datasource="f8w_test">
                            	Select désignation from prestation 
                                use index(siret_numprestation) where siret='#lect_prestation.siret#' 
                                and numprestation='#profilprestationFin.NumPrestation#'
                            </cfquery>
                        	<cfset msg = "Désolé, vous ne pouvez pas inscrire votre enfant le " & #url.datejour# & " à cette prestation,">
                            <cfset msg = #msg# & " il ou elle est déjà inscrit(e) à " & #prestadejareserv.désignation# & " ce jour là !">
                            <cfset msg = urlencodedformat(#msg#)>
                            <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#" addtoken="no">
                        </cfif>
                	</cfloop>
                </cfif>
            </cfloop>
        </cfif>
        
        
        
        <cfquery name="lect_inscriptionplanning" datasource="f8w_test">
        	Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
            Where siret = '#lect_user.siret#' 
            and NumPrestation = '#url.NumPrestation#' and NumEnfant = '#url.NumEnfant#' and DateInscription = '#url.datejour#'
        </cfquery>
        <cfset inscriptionamodifierpourheurearrive = #lect_inscriptionplanning.id#>
        <cfquery name="lect_enfant" datasource="f8w_test">
            select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#url.id_enfant#'
        </cfquery>        
    	<cfif #url.nature# eq 2><!--- ont supprime --->
        	<cfif #lect_inscriptionplanning.recordcount#><!--- pour bug 
delete from inscriptionplanning where Numinscription= and numprestation= and numenfant= --->
                <cfquery name="del" datasource="f8w_test">
                    Delete from inscriptionplanning where id = '#lect_inscriptionplanning.id#'
                </cfquery>
                <cfif #lect_prestation.ticket# eq 1>
					<cfif #ResultatTicketPortefeuille.recordcount# eq 1>
                    	
                        <!--- VU LE 28/02/2022 GROS BUG PLAISSAN A CAUSE DU CODE CI-DESSOUS EN DESINSCRIPTION CA ENLEVE DU CREDIT AU LIEU DEN AJOUTER !!--->
                        
                        <!--- maj 09/02/2022 pour rembourser ce qui a été payé (ajout PU stocké dans inscriptionplanning) (si modif tarif entre la résa et l'annulation) --->
                        <!---<cfif #lect_inscriptionplanning.Pu# eq 0>
                            <cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #ResultatTicketPortefeuille.MontantDu#>
						<cfelse>
                            <cfset inscriptionpunegatif = #lect_inscriptionplanning.Pu# - #lect_inscriptionplanning.Pu# - #lect_inscriptionplanning.Pu#>
                            <cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #inscriptionpunegatif#>
                            <cfset ResultatTicketPortefeuille.MontantDu = #inscriptionpunegatif#>    
                        </cfif>--->
                        
                        <cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #ResultatTicketPortefeuille.MontantDu#>
                            
                        <cfquery name="majportefeuille" datasource="f8w_test">
                        	Update prestation_prepaye_portefeuille 
                            set Montant='#NewMontant#' where id='#ResultatTicketPortefeuille.IdPortefeuille#'
                    	</cfquery>
                        <cfquery name="addhisto" datasource="f8w_test">
                            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                            ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                            '#ResultatTicketPortefeuille.MontantDisponible#','#ResultatTicketPortefeuille.MontantDu#',
                            '#NewMontant#','1','#NewMontant#','#ResultatTicketPortefeuille.IdPortefeuille#','#url.NumEnfant#',
                            '#url.datejour#',-1)
                        </cfquery>
                    <cfelse>
						<cfset NewQte = #prestation_prepaye.Qte# + 1>
                        <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                            Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                        </cfquery>            
                        <cfquery name="addhisto" datasource="f8w_test">
                            Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                            ('#lect_etablissement.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                            '#prestation_prepaye.Qte#','1','#NewQte#','1')
                        </cfquery>                
					</cfif>
                </cfif>				
				<cfif #lect_etablissement.FullWeb# neq 1>
					<cfset req_sql = "delete from inscriptionplanning where Numinscription=" & #lect_inscriptionplanning.NumInscription#>
                    <cfset req_sql = #req_sql# & " and numprestation=" & #lect_inscriptionplanning.numprestation# & " and numenfant=">
                    <cfset req_sql = #req_sql# & #lect_inscriptionplanning.numenfant# >
                    <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                    <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                    <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                    <cfquery name="SYNCHRO" datasource="services">
                        Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) 
                        Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                        '#lect_user.siret#','#datesoumission#','F8')
                    </cfquery>
				</cfif>
                <cfset msg1 = "Modification enregistrée !">
                <cfset msg1 = urlencodedformat(#msg1#)>
			</cfif>        
        <cfelse>
			<cfif #lect_inscriptionplanning.recordcount#>
            	<cfif #lect_inscriptionplanning.Nature# neq #url.nature#>
                    <cfquery name="maj" datasource="f8w_test">
                        Update inscriptionplanning set Nature='#url.nature#', NumLieu='#NumLieuPrestation#' 
                        Where id = '#lect_inscriptionplanning.id#'
                    </cfquery>
                    <cfif #lect_prestation.ticket# eq 1>
                        <cfif #url.nature# eq 1><!--- desinscr --->
                            <cfif #ResultatTicketPortefeuille.recordcount# eq 1>
                                
                                <!--- maj 09/02/2022 pour rembourser ce qui a été payé (ajout PU stocké dans inscriptionplanning) (si modif tarif entre la résa et l'annulation) --->
                                <!---<cfif #lect_inscriptionplanning.Pu# eq 0>
								    <cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #ResultatTicketPortefeuille.MontantDu#>
                                <cfelse>
                                    <cfset inscriptionpunegatif = #lect_inscriptionplanning.Pu# - #lect_inscriptionplanning.Pu# - #lect_inscriptionplanning.Pu#>
                                    <cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #inscriptionpunegatif#>
                                    <cfset ResultatTicketPortefeuille.MontantDu = #inscriptionpunegatif#>    
                                </cfif>--->
                                        
                                <cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #ResultatTicketPortefeuille.MontantDu#>        
                                
                                <cfquery name="majportefeuille" datasource="f8w_test">
                                    Update prestation_prepaye_portefeuille 
                                    set Montant='#NewMontant#' where id='#ResultatTicketPortefeuille.IdPortefeuille#'
                                </cfquery>
                                <cfquery name="addhisto" datasource="f8w_test">
                                    Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                                    Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                                    ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                    '#ResultatTicketPortefeuille.MontantDisponible#','#ResultatTicketPortefeuille.MontantDu#',
                                    '#NewMontant#','1','#NewMontant#','#ResultatTicketPortefeuille.IdPortefeuille#',
                                    '#url.NumEnfant#','#url.datejour#',-1)
                                </cfquery>
                            <cfelse>
								<cfset NewQte = #prestation_prepaye.Qte# + 1>
                                <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                    Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                </cfquery>            
                                <cfquery name="addhisto" datasource="f8w_test">
                                    Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                    ('#lect_etablissement.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                    '#prestation_prepaye.Qte#','1','#NewQte#','1')
                                </cfquery>                
							</cfif>
                        <cfelseif #url.nature# eq 2 or #url.nature# eq 0><!--- inscr ou reinscr --->
							<cfif #ResultatTicketPortefeuille.recordcount# eq 1>
								<cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# - #ResultatTicketPortefeuille.MontantDu#>
                                <cfquery name="majportefeuille" datasource="f8w_test">
                                    Update prestation_prepaye_portefeuille 
                                    set Montant='#NewMontant#' where id='#ResultatTicketPortefeuille.IdPortefeuille#'
                                </cfquery>
                                <cfquery name="addhisto" datasource="f8w_test">
                                    Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                                    Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                                    ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                    '#ResultatTicketPortefeuille.MontantDisponible#','-#ResultatTicketPortefeuille.MontantDu#',
                                    '#NewMontant#','1','#NewMontant#','#ResultatTicketPortefeuille.IdPortefeuille#',
                                    '#url.NumEnfant#','#url.datejour#',0)
                                </cfquery>
                            <cfelse>
                                <cfif #prestation_prepaye.recordcount#>
                                    <cfset NewQte = #prestation_prepaye.Qte# - 1>
                                    <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                        Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                    </cfquery>
                                    <cfquery name="addhisto" datasource="f8w_test">
                                        Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                        ('#lect_etablissement.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                        '#prestation_prepaye.Qte#','-1','#NewQte#','1')
                                    </cfquery>                    
                                <cfelse>
                                    <cfquery name="add" datasource="f8w_test">
                                        Insert into prestation_prepaye(siret,numprestation,numclient,idclient,Qte) values('#lect_etablissement.siret#',
                                        '#url.NumPrestation#','#lect_client.NumClient#','#lect_client.id#',-1)    
                                    </cfquery>
                                    <cfquery name="addhisto" datasource="f8w_test">
                                        Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                        ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                        '0','-1','-1','1')
                                    </cfquery>
                                </cfif>
                            </cfif>
                        </cfif>
                    </cfif>				
                    <cfif #lect_etablissement.FullWeb# neq 1>
                        <cfset req_sql = "Update inscriptionplanning set Nature=" & #url.nature# & " where Numinscription=" & #lect_inscriptionplanning.NumInscription#>
                        <cfset req_sql = #req_sql# & " and numprestation=" & #lect_inscriptionplanning.numprestation# & " and numenfant=">
                        <cfset req_sql = #req_sql# & #lect_inscriptionplanning.numenfant# >
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                            '#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>            
                    </cfif>
				</cfif>
                <cfset msg1 = "Modification enregistrée !">
        		<cfset msg1 = urlencodedformat(#msg1#)>
            <cfelse>
               	<cfset NumInscription = Module.NumSuivant6(#lect_user.siret#,"inscriptionplanning","NumInscription")>
				<cfset datedebstp = lsdateformat(#datedeb#,"YYYYMMdd")>
                <cfquery name="add" datasource="f8w_test" result="leresultat">
                	Insert into inscriptionplanning (siret,NumInscription,numprestation,numenfant,dateinscription,Nature,
                    DateInscriptionSTP,NumLieu) Values ('#lect_user.siret#','#NumInscription#','#url.NumPrestation#','#url.NumEnfant#','#url.datejour#',
                    '#url.nature#','#datedebstp#','#NumLieuPrestation#')
                </cfquery>
                <cfset inscriptionamodifierpourheurearrive = #leresultat.GENERATED_KEY#>
                <cfif #lect_prestation.ticket# eq 1>
                	<cfif #url.nature# eq 1><!--- desinscr --->
						<cfif #ResultatTicketPortefeuille.recordcount# eq 1>
							<cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# + #ResultatTicketPortefeuille.MontantDu#>
                            <cfquery name="majportefeuille" datasource="f8w_test">
                                Update prestation_prepaye_portefeuille 
                                set Montant='#NewMontant#' where id='#ResultatTicketPortefeuille.IdPortefeuille#'
                            </cfquery>
                            <cfquery name="addhisto" datasource="f8w_test">
                                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                                ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                '#ResultatTicketPortefeuille.MontantDisponible#','#ResultatTicketPortefeuille.MontantDu#',
                                '#NewMontant#','1','#NewMontant#','#ResultatTicketPortefeuille.IdPortefeuille#'
                                ,'#url.NumEnfant#','#url.datejour#',-1)
                            </cfquery>
                        <cfelse>
							<cfset NewQte = val(#prestation_prepaye.Qte#) + 1>
                            <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                            </cfquery>            
                            <cfquery name="addhisto" datasource="f8w_test">
                                Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                '#prestation_prepaye.Qte#','1','#NewQte#','1')
                            </cfquery>                
						</cfif>
					<cfelseif #url.nature# eq 2 or #url.nature# eq 0><!--- inscr ou reinscr --->
						<cfif #ResultatTicketPortefeuille.recordcount# eq 1>
							<cfset NewMontant = #ResultatTicketPortefeuille.MontantDisponible# - #ResultatTicketPortefeuille.MontantDu#>
                            <cfquery name="majportefeuille" datasource="f8w_test">
                                Update prestation_prepaye_portefeuille 
                                set Montant='#NewMontant#' where id='#ResultatTicketPortefeuille.IdPortefeuille#'
                            </cfquery>
                            <cfquery name="addhisto" datasource="f8w_test">
                                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                                ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                '#ResultatTicketPortefeuille.MontantDisponible#','-#ResultatTicketPortefeuille.MontantDu#',
                                '#NewMontant#','1','#NewMontant#','#ResultatTicketPortefeuille.IdPortefeuille#',
                                '#url.NumEnfant#','#url.datejour#',0)
                            </cfquery>
                            <!--- maj 09/02/2022 pour rembourser ce qui a été payé (ajout PU stocké dans inscriptionplanning) (si modif tarif entre la résa et l'annulation) --->
                            <cfquery name="maj" datasource="f8w_test">
                                Update inscriptionplanning set Pu='#ResultatTicketPortefeuille.MontantDu#' where id='#leresultat.GENERATED_KEY#'
                            </cfquery>
                        <cfelse>
							<cfset NewQte = val(#prestation_prepaye.Qte#) - 1>
                            <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                            </cfquery>
                            <cfquery name="addhisto" datasource="f8w_test">
                                Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                ('#lect_etablissement.siret#','#url.NumPrestation#','#lect_client.NumClient#',
                                '#prestation_prepaye.Qte#','-1','#NewQte#','1')
                            </cfquery>                    
						</cfif>
                    </cfif>
                </cfif>				                
                <cfif #lect_etablissement.FullWeb# neq 1>
					<cfset req_sql = "Insert into inscriptionplanning (NumInscription,numprestation,numenfant,dateinscription,Nature">
                    <cfset req_sql = #req_sql# & ") Values (" & #NumInscription# & "," & #url.NumPrestation# & "," & #url.NumEnfant#>
                    <cfset req_sql = #req_sql# & ",'" & #url.datejour# & "'," & #url.nature# & ")">
                    <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                    <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                    <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                    <cfquery name="SYNCHRO" datasource="services">
                        Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) 
                        Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                        '#lect_user.siret#','#datesoumission#','F8')
                    </cfquery>
				</cfif>
                <cfset msg1 = "Modification enregistrée !">
        		<cfset msg1 = urlencodedformat(#msg1#)>
            </cfif>    
        </cfif>
            <!--- log --->
        <cfif #url.action# eq "desinscr">
        	<cfif #session.log_mairie# is true>
        		<cfset log_action_web = "Désinscription ponctuelle par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
            <cfelse>
        		<cfset log_action_web = "Désinscription ponctuelle par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
        	</cfif>
        <cfelse>
        	<cfif #session.log_mairie# is true>
        		<cfset log_action_web = "Inscription ponctuelle par  la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
            <cfelse>
        		<cfset log_action_web = "Inscription ponctuelle par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
        	</cfif>
        </cfif>
        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
        <cfset log_action_web = #log_action_web# & "<br>Application le " & #url.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
        <cfset dtapplication = #url.datejour#>
        <cfquery name="log" datasource="f8w_test">
        	Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#','#session.use_id#',
            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#url.id_prestation#')
        </cfquery>
        <!--- modif 11/09/2013 après action, ont demande à l'utilisateur si appliquer récurence
        <cflocation url="/inscriptions/?id_mnu=1&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#&msg1=#msg1#" addtoken="no"> --->
    	<cfif #lect_prestation.PasDeRecurence# eq 0 and len(#url.action#)>
        	<cflocation url="/inscriptions/recurence.cfm?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#&msg1=#msg1#&numjour=#url.numjour#&datejour=#url.datejour#&numenfant=#url.numenfant#&numprestation=#url.numprestation#&action=#url.action#&nature=#url.nature#&NumLieuPrestation=#NumLieuPrestation#&NumLieuPrestationQuota=#NumLieuPrestationQuota#" addtoken="no">
    	<cfelseif #lect_prestation.SaisirHeure# eq 1 and (#url.action# eq "inscr" or #url.action# eq "reinscr")>
        	<cflocation url="/inscriptions/SaisieHeure.cfm?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg=#msg#&msg1=#msg1#&numjour=#url.numjour#&datejour=#url.datejour#&numenfant=#url.numenfant#&numprestation=#url.numprestation#&action=#url.action#&nature=#url.nature#&NumLieuPrestation=#NumLieuPrestation#&NumLieuPrestationQuota=#NumLieuPrestationQuota#&inscriptionamodifierpourheurearrive=#inscriptionamodifierpourheurearrive#" addtoken="no">
        <cfelse>
        	<cfset msg1 = "Modification enregistrée !">
        	<cfset msg1 = urlencodedformat(#msg1#)>
        	<cflocation url="/inscriptions/?id_mnu=15&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#&msg1=#msg1#" addtoken="no">
        </cfif>
    </cfif>
</cfif>
<cfquery name="docattentvalidationbloqueresa" dbtype="query">
	Select * from Documents where bloqueresa=1 and statut<>'Validé'
</cfquery>

<cfquery name="lect_enfant" datasource="f8w_test"><!--- lecture enfant --->
    Select id,NumEnfant,Prénom,concat_ws(" ",Prénom,nom) as lenom 
    from enfant use index(siret_NumClientI)
    Where siret = '#lect_user.siret#' and NumClientI = '#lect_client.Numclient#' 
    and parti=0 and del=0 Order by Prénom
</cfquery>
<cfif val(#url.id_enfant#) gt 0>
    <cfquery name="verid" dbtype="query">
        Select * from lect_enfant where id='#url.id_enfant#'
    </cfquery>
    <cfif #verid.recordcount# eq 0>
            <cflocation url="/inscriptions/?id_mnu=#url.id_mnu#">
    </cfif>
</cfif>
<cfif #lect_enfant.recordcount# eq 1>
    <cfset url.id_enfant = #lect_enfant.id#>
</cfif>

    <!--- prestation cotisation annuelle ??? --->
<cfquery name="CotiseAnnuelle" datasource="f8w_test">
    Select * from prestation use index(siret) 
    Where siret = '#lect_user.siret#' and PrestaCotisationAnnuelle=1 and del=0
</cfquery>
<cfif #CotiseAnnuelle.recordcount#>
    <cfloop query="CotiseAnnuelle">
        <cfset debperstpcotise = mid(#CotiseAnnuelle.PrestaCotisationAnnuelleEntreLe#,7,4) & mid(#CotiseAnnuelle.PrestaCotisationAnnuelleEntreLe#,4,2) & mid(#CotiseAnnuelle.PrestaCotisationAnnuelleEntreLe#,1,2)>
        <cfset finperstpcotise = mid(#CotiseAnnuelle.PrestaCotisationAnnuelleEtLe#,7,4) & mid(#CotiseAnnuelle.PrestaCotisationAnnuelleEtLe#,4,2) & mid(#CotiseAnnuelle.PrestaCotisationAnnuelleEtLe#,1,2)>
        <cfset dtnowstp = lsdateformat(now(),"YYYYMMdd")> 
        <cfif #dtnowstp# gte #debperstpcotise# and #dtnowstp# lte #finperstpcotise#>
            <cfset cotiseok = "non">
            <cfloop query="lect_enfant">
                <cfquery name="verinscrcotise" datasource="f8w_test">
                    Select id from inscriptionplanning use index 
                    (siret_NumEnfant_NumPrestation_DateInscriptionSTP) Where siret = '#lect_user.siret#' 
                    and NumEnfant='#lect_enfant.numEnfant#' and NumPrestation='#CotiseAnnuelle.NumPrestation#' 
                    and DateInscriptionSTP>='#debperstpcotise#' and DateInscriptionSTP<='#finperstpcotise#'
                </cfquery>
                <cfif #verinscrcotise.recordcount# eq 0>
                    <cfquery name="verinscrcotise" datasource="f8w_test" maxrows="1">
                        Select Date from présenceabsence use index
                        (siret_NumEnfant_NumPrestation_Date) Where siret = '#lect_user.siret#' 
                        and NumEnfant='#lect_enfant.numEnfant#' and NumPrestation='#CotiseAnnuelle.NumPrestation#' 
                        order by id desc limit 0,1
                    </cfquery>
                    <cfif #verinscrcotise.recordcount#>
                        <cfset dtstamppres = mid(#verinscrcotise.Date#,7,4) & mid(#verinscrcotise.Date#,4,2) & mid(#verinscrcotise.Date#,1,2)>
                        <cfif #dtstamppres# gte #debperstpcotise# and #dtstamppres# lte #finperstpcotise#>
                            <cfset cotiseok = "oui">
                            <cfbreak>
                        <cfelse>
                            <cfquery name="verinscrcotise" datasource="f8w_test">
                                Select id from présenceabsence where id=0
                            </cfquery>
                        </cfif>
                    </cfif>
                </cfif>
                <cfif #verinscrcotise.recordcount#>
                    <cfset cotiseok = "oui">
                    <cfbreak>
                </cfif>
            </cfloop>
            <cfif #cotiseok# eq "non" and #lect_enfant.recordcount#>
                <cfset datedeb = createdate(year(now()),month(now()),#CotiseAnnuelle.PrestaCotisationAnnuelleFactLeJourDuMoisCourant#)>
                <cfset datedebstp = lsdateformat(#datedeb#,"YYYYMMdd")> 
                <cfset datejourcotise = lsdateformat(#datedeb#,"dd/MM/YYYY")>            
                <cfloop query="lect_enfant">
                    <cfset NumInscription = Module.NumSuivant6(#lect_user.siret#,"inscriptionplanning","NumInscription")>                 
                    <cfquery name="add" datasource="f8w_test" result="leresultat">
                        Insert into inscriptionplanning (siret,NumInscription,numprestation,numenfant,dateinscription,
                        Nature,DateInscriptionSTP) Values ('#lect_user.siret#','#NumInscription#',
                        '#CotiseAnnuelle.NumPrestation#','#lect_enfant.NumEnfant#','#datejourcotise#',
                        '0','#datedebstp#')
                    </cfquery>                        	
                    <cfif #CotiseAnnuelle.PrestaCotisAnnuelleInscrAllEnfant# eq 0>
                        <cfbreak>
                    </cfif>
                </cfloop>
            </cfif>
        </cfif>
    </cfloop>
</cfif>

<div class="row d-flex justify-content-between mt-5 content">
    <div class="p-5 bg-white section section-table scroll">
        <!---<button class="open-button"onclick="openPopUp()">Inscrire mon enfant</button>--->

        <cfform name="eddinsc" method="post" preservedata="yes" onerror="showMB('err_login')" class="blue-select">

            <label for="NumEnfant">Choisissez l'enfant :</label>
            <!--- pour BloquerResaSiPasReglee --->
            <cfset OnBloque = false>
            <cfset OnAffiche = false>
            <cfquery name="BloquerResaSiPasReglee" datasource="f8w_test">
                Select NumPrestation from prestation use index(siret) Where siret='#lect_user.siret#' 
                and BloquerResaSiPasReglee=1 and del=0
            </cfquery>
            <cfset detpresta = "">
            <cfif #BloquerResaSiPasReglee.recordcount#>
                <cfset lstnumpresta = "">
                <cfloop query="BloquerResaSiPasReglee">
                    <cfset lstnumpresta = #lstnumpresta# & #BloquerResaSiPasReglee.numprestation# & ",">
                </cfloop>
                <cfquery name="itemfacture" datasource="f8w_test">
                    Select NumRole,libPrestation,DetailItem from itemfacture use index(siret_numclient) 
                    Where siret='#lect_user.siret#' and NumClient='#lect_user.numclient#' 
                    and find_in_set(NumPrestation,'#lstnumpresta#') group by NumRole,NumPrestation
                </cfquery>
                <cfloop query="itemfacture">
                    <cfquery name="role" datasource="f8w_test">
                        Select datepaiement from role use index(siret_numrole)
                        Where siret='#lect_user.siret#' and NumRole='#itemfacture.NumRole#' and del=0
                    </cfquery>
                    <cfif #role.recordcount#>
                        <cfset dtpaiestp = mid(#role.datepaiement#,7,4) & mid(#role.datepaiement#,4,2) & mid(#role.datepaiement#,1,2)>
                        <cfset nowstp = lsdateformat(now(),"YYYYMMdd")>
                        <cfif #nowstp# lte #dtpaiestp#>
                            <cfset OnAffiche = true>
                        <cfelse>
                            <cfset OnAffiche = false>
                        </cfif>
                        <cfquery name="regle" datasource="f8w_test">
                            Select * from encaissement use index(siret_NumClient_NumRole2) 
                            Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                            and NumRole2='#itemfacture.NumRole#'
                        </cfquery>
                        <cfif #regle.recordcount# eq 0 and #OnAffiche# is false>
                            <cfset OnBloque = True>
                            <cfset detpresta = #detpresta# & #itemfacture.LibPrestation# & " " & #itemfacture.DetailItem# & ",">
                            <cfbreak>
                        <cfelseif #regle.recordcount# gt 0 and #OnAffiche# is true >
                            <cfset OnAffiche = false>
                        </cfif>
                    </cfif>                                    
                </cfloop>
            </cfif>
            <cfif #OnBloque# is false and #lect_client.AccesMenuResa# eq 0>
                <cfset OnBloque = True>
            </cfif>
            <cfif #docattentvalidationbloqueresa.recordcount# neq 0>
                <cfset OnBloque = True>
            </cfif>
            <cfif #OnBloque# is false>
                <cfselect name="NumEnfant" onChange="MM_jumpMenu('self',this,0)">
                    <option class="dropdown-item" value=""></option>
                    <cfoutput query="lect_enfant">
                        <cfif #lect_enfant.id# eq #url.id_enfant# >
                            <option class="dropdown-item" title="#lect_enfant.NumEnfant#" selected value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#url.id_prestation#&id_enfant=#lect_enfant.id#">#lect_enfant.prénom#</option>
                        <cfelse>
                            <option class="dropdown-item" title="#lect_enfant.NumEnfant#" value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#url.id_prestation#&id_enfant=#lect_enfant.id#">#lect_enfant.prénom#</option>                                            
                        </cfif>
                    </cfoutput>
                </cfselect>
                <cfif #OnAffiche# is true>
                    <p class="text-red">Attention, vous devez régler une ou plusieurs factures !</p>
                </cfif>
            <cfelse>
                <cfselect disabled name="NumEnfant" onChange="MM_jumpMenu('self',this,0)">
                    <option class="dropdown-item" value=""></option>
                    <cfoutput query="lect_enfant">
                        <cfif #lect_enfant.id# eq #url.id_enfant#>
                            <option class="dropdown-item" title="#lect_enfant.NumEnfant#" selected value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#url.id_prestation#&id_enfant=#lect_enfant.id#">#lect_enfant.prénom#</option>
                        <cfelse>
                            <option class="dropdown-item" title="#lect_enfant.NumEnfant#" value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#url.id_prestation#&id_enfant=#lect_enfant.id#">#lect_enfant.prénom#</option>                                            
                        </cfif>
                    </cfoutput>
                </cfselect>
                <cfif #docattentvalidationbloqueresa.recordcount# neq 0>
                    <p class="text-red">Attention, vous avez un ou plusieur DOCUMENT OBLIGATOIRE a transmettre avant de pouvoir réserver !</font></p>
                <cfelseif #lect_client.AccesMenuResa# eq 0>
                    <p class="text-red">Attention, votre accès aux réservations a été désactivé !</font></p>                                                                     
                <cfelse>
                    <p class="text-red">Attention, vous devez régler une ou plusieurs factures avant de pouvoir accéder aux réservations (<cfoutput>#detpresta#</cfoutput>)</font></p>                                
                </cfif>
                <cfabort>
            </cfif>
            <cfif len(#url.id_enfant#) and #OnBloque# is false>
                <cfquery name="lect_numEnfant" datasource="f8w_test">
                    Select NumEnfant from enfant where id = '#url.id_enfant#'
                </cfquery>
                <cfquery name="lect_photo" datasource="f8w_test">
                    Select id,FilePhoto,FilePhotoWeb from photo use index(siret_NumEnfant) 
                    Where siret = '#lect_user.siret#' 
                    and NumEnfant = '#lect_numEnfant.NumEnfant#'
                </cfquery>
                <!---<cfif #lect_photo.recordcount#>
                    <cfif len(#lect_photo.FilePhoto#) gt 0 and len(#lect_photo.FilePhotoWeb#) eq 0>
                        <!--- traitement de la photo pour le web (mise a la taille)--->
                        <cfloop list="#lect_photo.FilePhoto#" delimiters="\" index="laphoto"></cfloop>
                        <cfset lasource = "C:\inetpub\wwwroot\F8WEBcoldfu\photo\" & #lect_user.siret# & "\" & #laphoto#>
                        <cfset ladesti = "C:\inetpub\wwwroot\F8WEBcoldfu\photo\" & #lect_user.siret# & "\" & #lect_photo.id# & ".jpg">
                        <cfimage action="resize" height="100" width="" source="#lasource#" destination="#ladesti#" overwrite="yes">
                        <cffile action="delete" file="#lasource#">
                        <cfquery name="maj_photo" datasource="f8w_test">
                            Update photo set FilePhotoWeb = <cfqueryparam value="#lect_photo.id#.jpg" cfsqltype="cf_sql_varchar">
                            Where id = '#lect_photo.id#'
                        </cfquery>
                        <cfset laphoto = #lect_photo.id# & ".jpg">
                    <cfelse>
                        <cfset laphoto = #lect_photo.FilePhotoWeb#>
                    </cfif>
                        &nbsp;&nbsp;<img width="100" height="100" src="/photo/<cfoutput>#lect_user.siret#/#laphoto#</cfoutput>">
                </cfif>--->
            </cfif>
            <cfif len(#url.id_enfant#)>
                <cfquery name="lect_numEnfant" datasource="f8w_test">
                    Select NumEnfant from enfant where id = '#url.id_enfant#'
                </cfquery>
                <cfif #session.log_mairie# is true>
                    <cfset topweb="">
                <cfelse>
                    <cfset topweb="and topweb=1">
                </cfif>
                <cfquery name="lect_prestation" datasource="f8w_test"><!--- lecture prestation --->
                    Select * from prestation use index(siret_NumPrestation) 
                    Where siret = '#lect_user.siret#' #topweb# and resaconstat=1 and del=0 order by désignation
                </cfquery>
                <cfif val(#url.id_prestation#) gt 0>
                    <cfquery name="verid" dbtype="query">
                        Select * from lect_prestation where id='#url.id_prestation#'
                    </cfquery>
                    <cfif #verid.recordcount# eq 0>
                        <cflocation url="/inscriptions/?id_mnu=#url.id_mnu#">
                    </cfif>
                </cfif>
                <cfif #lect_prestation.recordcount# eq 1>
                    <cfset url.id_prestation = #lect_prestation.id#>
                </cfif>
                <cfquery name="enfantclasse" datasource="f8w_test">
                    Select * From enfantclasse use index(siret_NumEnfant) Where siret = '#lect_user.siret#' 
                    and NumEnfant = '#lect_numEnfant.NumEnfant#'
                </cfquery>
                <cfif #enfantclasse.recordcount# eq 0>
                    <cfset enfantclasse.NumClasse = 0>
                </cfif>
                <cfset LibClasse = "">
                <cfset LibEcole = "">
                <cfloop query="enfantclasse">
                    <cfquery name="classe" datasource="f8w_test">
                        Select LibClasse, NomInstit, NumEcole 
                        from classe use index(siret_NumClasse) Where siret = '#lect_user.siret#' 
                        and NumClasse = '#enfantclasse.NumClasse#' and VisibleDansEspaceParent=1 and del=0
                    </cfquery>
                    <cfif #classe.recordcount#>
                        <cfset LibClasse = #classe.LibClasse#>
                        <cfquery name="ecole" datasource="f8w_test">
                            Select NomEcole from ecole use index(siret_NumEcole) Where siret = '#lect_user.siret#' 
                            and NumEcole = '#classe.NumEcole#'
                        </cfquery>
                        <cfset LibEcole = #ecole.NomEcole#>
                        <cfbreak>
                    </cfif>
                </cfloop>

                <cfif #LibEcole# neq "">
                    <p class="text-small"><cfif findnocase("ecole",#LibEcole#) eq 0>Ecole :&nbsp;</cfif>
                        <cfoutput>#LibEcole#</cfoutput>
                        <cfif #LibClasse# neq "">
                        &nbsp;&nbsp;Classe :&nbsp;<cfoutput>#LibClasse# <!---#classe.NomInstit#---></cfoutput>
                        </cfif>
                    </p>
                <cfelse>
                    <br/><br/>
                </cfif>
            
                <label for="NumPrestation">Choisissez la prestation :</label>
                <cfset affdesiprestation = "Prestation">
                <cfselect name="NumPrestation" onChange="MM_jumpMenu('self',this,0)">
                    <option class="dropdown-item" value=""></option>
                    <cfoutput query="lect_prestation">
                        <cfset Afficher = "oui">
                        <cfif #lect_prestation.TopWeb# eq 0 and #session.log_mairie# is false>
                            <cfset Afficher = "non">
                        </cfif>
                        <cfif #lect_prestation.SurCodePostal# eq 1><!--- sur code postal --->
                            <cfif len(#lect_prestation.CodePostalOui#)>
                                <cfif #lect_prestation.VilleSurCodePostal# eq #lect_client.ville#>
                                    <cfset Afficher = "oui">
                                    <cfset lect_prestation.désignation = #lect_prestation.DésiSurCodePostal#>
                                <cfelse>
                                    <cfset Afficher = "non">
                                </cfif>
                            <cfelseif len(#lect_prestation.CodePostalNon#)>
                                <cfif #lect_prestation.VilleSurCodePostal# neq #lect_client.ville#>
                                    <cfset Afficher = "oui">
                                    <cfset lect_prestation.désignation = #lect_prestation.DésiSurCodePostal#>
                                <cfelse>
                                    <cfset Afficher = "non">
                                </cfif>                                            
                            </cfif>
                        </cfif>
                        <cfif len(#lect_prestation.LstNumClientOui#)>
                            <cfif findnocase(#lect_client.NumClient#,#lect_prestation.LstNumClientOui#)>
                                <cfset Afficher = "oui">
                            <cfelse>
                                <cfset Afficher = "non">
                            </cfif>
                        </cfif>
                        <cfif len(#lect_prestation.LstNumClientNon#) and findnocase(#lect_client.NumClient#,#lect_prestation.LstNumClientNon#)>
                            <cfset Afficher = "non">
                        </cfif>
                        <cfif #lect_prestation.recordcount#>
                            <cfset reserve_a = Module.prestation_reserve_a1(#lect_client.siret#,#lect_prestation.NumPrestation#,#lect_client.NumClient#,#lect_numEnfant.NumEnfant#,0,"",#lect_prestation.siret#)>
                            <cfif #reserve_a# neq "OK" and #session.log_mairie# is false>
                                <cfset Afficher = "non">
                            </cfif>
                        </cfif>
                        <cfif #Afficher# eq "oui">                                            
                            <cfif #lect_prestation.id# eq #url.id_prestation#>
                                <option class="dropdown-item" selected value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#lect_prestation.id#&id_enfant=#url.id_enfant#">#lect_prestation.Désignation#</option>
                                <cfset affdesiprestation = #lect_prestation.Désignation#>
                                <cfif #lect_prestation.HeureDébut# neq "00:00:00">
                                    <cfset affheureprestation = "De " & mid(#lect_prestation.HeureDébut#,1,2) & "h" &  mid(#lect_prestation.HeureDébut#,4,2) & " à " & mid(#lect_prestation.HeureFin#,1,2) & "h" &  mid(#lect_prestation.HeureFin#,4,2)>
                                </cfif>
                            <cfelse>
                                <option class="dropdown-item" value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#lect_prestation.id#&id_enfant=#url.id_enfant#">#lect_prestation.Désignation#</option>
                            </cfif>
                        </cfif>
                    </cfoutput>
                    <!--- prestation mise à dispo ??? --->
                    <cfquery name="prestationmiseadispo" datasource="f8w_test">
                        Select * from prestation_miseadispo 
                        where find_in_set('#lect_user.siret#',lst_siret_dispose) and actif=1 
                    </cfquery>
                    <cfoutput query="prestationmiseadispo">
                        <cfquery name="prestadispose" datasource="f8w_test">
                            Select id,ResaPeriode,désignation,TopWeb from prestation use index(siret_numprestation)
                            where siret='#prestationmiseadispo.siret_miseadispo#' 
                            and numprestation='#prestationmiseadispo.NumPrestation_miseadispo#'
                        </cfquery>
                        <cfif #prestadispose.TopWeb# eq 0>
                            <cfset afficherdispo = "non">
                        <cfelse>
                            <cfset afficherdispo = "oui">
                        </cfif>
                        <cfif #prestadispose.ResaPeriode# eq 1>
                            <cfquery name="testreserve_aoupas" datasource="f8w_test" maxrows="1">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                where siret='#lect_client.siret#' and 
                                NumPrestation='#prestationmiseadispo.NumPrestation_miseadispo#' 
                                Limit 0,1
                            </cfquery>    
                            <cfif #testreserve_aoupas.recordcount# eq 0>
                                <cfset afficherdispo = "non">
                            </cfif>                                       
                        </cfif>
                        <cfif #afficherdispo# eq "oui">
                            <cfif #prestadispose.id# eq #url.id_prestation#>
                                <option class="dropdown-item" selected value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#prestadispose.id#&id_enfant=#url.id_enfant#">#prestadispose.Désignation#</option>
                            <cfelse>
                                <option class="dropdown-item" value="/inscriptions/?id_mnu=#url.id_mnu#&id_prestation=#prestadispose.id#&id_enfant=#url.id_enfant#">#prestadispose.Désignation#</option>
                            </cfif>
                        </cfif>
                    </cfoutput>
                </cfselect>
                &nbsp;<cfoutput>#affheureprestation#</cfoutput>

                <div class="pt-5">
                    <cfif len(#url.id_prestation#) and #url.id_prestation# gt 0>
                        <!--- ont a choisi la prestation --->
                        <cfquery name="lect_prestation" datasource="f8w_test">
                            Select * from prestation where id = '#url.id_prestation#'
                        </cfquery>
                        <cfif #lect_etablissement_miseadispo.recordcount#>
                            <cfquery name="document" datasource="classactive">
                                Select id,Désignation,Date_Ajout_Modif from documents 
                                use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
                                where (siret='#lect_etablissement.siret#' or siret='#lect_etablissement_miseadispo.siret_miseadispo#') 
                                and Id_Proprio='#lect_etablissement.id#' and CodeAppli='F8' and Type_Proprio='etablissement' 
                                and Type_Doc='REGLEMENT' order by Date_Ajout_Modif DESC
                            </cfquery>
                        <cfelse>
                            <cfquery name="document" datasource="classactive">
                                Select id,Désignation,Date_Ajout_Modif from documents 
                                use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
                                where siret='#lect_etablissement.siret#' and Id_Proprio='#lect_etablissement.id#' and CodeAppli='F8' 
                                and Type_Proprio='etablissement' 
                                and Type_Doc='REGLEMENT' order by Date_Ajout_Modif DESC
                            </cfquery>
                        </cfif>
                        <cfif #document.recordcount#>
                            <p class="text-red text-center">
                                La réservation à cette prestation vaut acceptation des conditions et engagements définit dans le(s) document(s) ci après : <cfoutput query="document"><a class="link-blue" href="/commun/VisuPdf.cfm?id=#document.id#" target="_blank">#document.désignation#</a>&nbsp;&nbsp;</cfoutput>
                            </p>                         
                        <cfelse>
                            <cfdirectory name="ReglementPresta" action="list" directory="C:\inetpub\wwwroot\F8WEBcoldfu\pj\#lect_prestation.siret#" filter="*.pdf">
                            <cfif #ReglementPresta.recordcount#>
                                <p class="text-red text-center">
                                La réservation à cette prestation vaut acceptation des conditions et engagements définit dans le(s) document(s) ci après : <cfoutput query="ReglementPresta"><a class="link-blue" href="/pj/#lect_prestation.siret#/#ReglementPresta.name#" target="_blank">#ReglementPresta.name#</a>&nbsp;&nbsp;</cfoutput>
                                </p>
                            </cfif>
                        </cfif>
                        <cfset explic_delai = "(Modifications possibles au plus tard ">
                        <cfif #lect_prestation.DélaiRésaWebj# eq 0>
                            <!---<cfset explic_delai = #explic_delai# & "le jour même jusqu'à " & #lect_prestation.DélaiRésaWebH# & ")">--->
                            <!--- modif du 18/10/2019 --->
                            <cfset explic_delai = "">
                        <cfelse>
                            <cfset explic_delai = #explic_delai# & #lect_prestation.DélaiRésaWebj# & " jour(s) avant jusqu'à " & #lect_prestation.DélaiRésaWebH# & ")">
                        </cfif>
                        <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                            Select NumLieu,Quota from lieuprestationclasse 
                            use index(siret_NumClasse_NumPrestation) Where siret = '#lect_user.siret#' 
                            and NumClasse = '#enfantclasse.NumClasse#' 
                            and NumPrestation = '#lect_prestation.Numprestation#' limit 0,1
                        </cfquery>

                        <cfif #lieuprestationclasse.recordcount#>
                            <cfquery name="lieuactivité" datasource="f8w_test">
                                Select NomLieu from lieuactivité use index(siret_NumLieu) Where siret = '#lect_user.siret#' 
                                and NumLieu = '#lieuprestationclasse.numLieu#'
                            </cfquery>
                            <cfif #lieuprestationclasse.Quota# gt 0>
                                <cfset lieuactivité.NomLieu = #lieuactivité.NomLieu# & " (" & #lieuprestationclasse.Quota# & " places maxi.)">
                            </cfif>
                            <p><cfoutput>#lieuactivité.NomLieu#</cfoutput></p>                  
                        </cfif>
                        <!---<cfset reserve_a1 = Module.prestation_reserve_a(#lect_client.siret#,#lect_prestation.NumPrestation#,#lect_client.NumClient#,#lect_numEnfant.NumEnfant#,#dayofyear(now())#)>--->
                        <cfset reserve_a1 = Module.prestation_reserve_a1(#lect_client.siret#,#lect_prestation.NumPrestation#,#lect_client.NumClient#,#lect_numEnfant.NumEnfant#,0,"",#dayofyear(now())#,#lect_prestation.siret#)>
                        <cfset NumDaujourDhui = DayOfWeek(now())>
                        <cfquery name="verifnewdelresaweb" datasource="f8w_test">
                            Select * from resawebdelai use index(siret_NumPrestation) Where siret='#lect_prestation.siret#' 
                            and NumPrestation='#lect_prestation.NumPrestation#' and LejourSemaine='#NumDaujourDhui#'
                        </cfquery>

                        <cfif #verifnewdelresaweb.recordcount# eq 1>
                            <cfif #lect_etablissement.siret# neq "21730085400015" or #lect_prestation.siret# neq #lect_etablissement.siret#>
                                <cfset premréserv = dateadd("d",#verifnewdelresaweb.DelaiJusqua#,now())>
                                <cfset suiteréserv = dateadd("d",#verifnewdelresaweb.Delaiapres#,now())>
                                <cfoutput>
                                    <p class="bold text-red text-center">
                                        <cfif #lect_prestation.ExplicationDelaiBlocage# eq "">
                                            Le #lsdateformat(now(),"dddd")# #lsdateformat(now(),"dd")# #lsdateformat(now(),"MMMM")# jusqu'à #verifnewdelresaweb.JusquaH# heure, le premier jour modifiable est le #lsdateformat(premréserv,"dddd")# #lsdateformat(premréserv,"dd")# #lsdateformat(premréserv,"MMMM")#. Après #verifnewdelresaweb.JusquaH# heure, le premier jour modifiable est le #lsdateformat(suiteréserv,"dddd")# #lsdateformat(suiteréserv,"dd")# #lsdateformat(suiteréserv,"MMMM")#
                                        <cfelse>
                                            #lect_prestation.ExplicationDelaiBlocage#    
                                        </cfif>
                                    </p>
                                </cfoutput>
                            <cfelse>
                                <p class="bold text-red text-center">(Modifications possibles au plus tard le mercredi 23h59 pour la semaine suivante)</p>
                            </cfif>
                        <cfelseif #lect_prestation.ResaPeriode# eq 0>
                            <p class="bold text-red text-center">
                                <cfif #reserve_a1# eq "OK">
                                    <cfoutput>#explic_delai#</cfoutput>
                                <cfelse>
                                    <cfif #reserve_a1# neq "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                                        <cfoutput>#reserve_a1#</cfoutput>
                                    </cfif>
                                </cfif>
                            </p>                              
                        </cfif>
                        <cfif len(#url.msg#)>
                            <p class="text-red bold"><cfoutput>#URLDecode(url.msg)#</cfoutput></p>
                        <cfelseif len(#url.msg1#)>
                            <p class="text-red bold"><cfoutput>#URLDecode(url.msg1)#</cfoutput></p>
                        </cfif>
                        
                        <cfquery name="lect_enfant" datasource="f8w_test">
                            Select Numenfant from enfant where id = '#url.id_enfant#'
                        </cfquery>
                        <!--- 26/06/2018 DAMMARIE verif si adhésion a un forfait (voir réduction)--->
                        <cfset txtpourforfait = "">
                        <cfquery name="verifforfaitsurreduction" datasource="f8w_test">
                            Select * from reductions use index(siret_NumPrestation) 
                            Where siret='#lect_etablissement.siret#' and Numprestation='#lect_prestation.NumPrestation#' 
                            and ReserverSurReduction=1 and del=0 order by Désignation ASC
                        </cfquery>
                        <cfset AdhesionFaite = False>
                        <cfloop query="verifforfaitsurreduction">
                            <cfquery name="veradhesion" datasource="f8w_test">
                                Select * from clientgroupe use index(siret_NumGroupe) 
                                Where siret='#verifforfaitsurreduction.siret#' 
                                and numGroupe='#verifforfaitsurreduction.GroupeClient#' 
                                and NumEnfant='#lect_numEnfant.NumEnfant#'
                            </cfquery>
                            <cfif #veradhesion.recordcount# gt 0>
                                <cfset AdhesionFaite = True>
                                <cfset txtpourforfait = #verifforfaitsurreduction.désignation#>
                                <cfbreak>
                            </cfif>
                        </cfloop>
                        <cfif #verifforfaitsurreduction.recordcount# gt 0 and #AdhesionFaite# is false>
                            <!--- ont affiche les "forfaits" (DAMMARIE)--->
                            <p>Choisir le forfait pour les réservations de votre enfant à la prestation <cfoutput>#affdesiprestation#</cfoutput>:</p>
                            <cfinput type="hidden" name="Sav_NumEnfant" value="#lect_numEnfant.NumEnfant#">
                            <cfoutput query="verifforfaitsurreduction">
                                <p><cfinput type="radio" name="quelforfait" value="#verifforfaitsurreduction.id#">&nbsp;#verifforfaitsurreduction.désignation#</p>
                            </cfoutput>

                            <p class="text-red">Attention, une fois choisi, vous ne pourrez pas changer de forfait, vous devrez contacter le responsable du périscolaire</p>
                            <cfinput class="btn btn-primary bold" name="btchoixforfait" type="submit" value="Choisir ce forfait">

                        <cfelseif #lect_prestation.ResaPeriode# eq 0><!--- ont affiche le calendrier --->
                            <cfif #reserve_a1# eq "OK">
                                <p>Cliquez dans les calendriers ci-dessous pour adapter ou créer la réservation de votre enfant à la prestation <cfoutput>#affdesiprestation#</cfoutput>:</p>
                                <cfif len(#txtpourforfait#)>
                                    <p class="text-red bold"><cfoutput>#txtpourforfait#</cfoutput> :</p>
                                </cfif>
                            </cfif>
                            <cfif #lect_prestation.ticket# eq 1>
                                <cfquery name="prestation_prepaye_portefeuille_presta" datasource="f8w_test">
                                    Select id_ent from prestation_prepaye_portefeuille_presta 
                                    use index(siret_NumPrestation)
                                    Where siret='#lect_prestation.siret#' 
                                    and NumPrestation='#lect_prestation.NumPrestation#'
                                </cfquery>
                                <cfif #prestation_prepaye_portefeuille_presta.recordcount# eq 1>
                                    <cfset identport = #prestation_prepaye_portefeuille_presta.id_ent#>
                                <cfelse>
                                    <cfset identport = 0>
                                </cfif>
                                <cfquery name="prestation_prepaye_portefeuille_ent" datasource="f8w_test">
                                    Select * from prestation_prepaye_portefeuille_ent 
                                    Where id='#identport#' and del=0
                                </cfquery>
                                <cfif #prestation_prepaye_portefeuille_ent.recordcount# eq 1>
                                    <cfquery name="prestation_prepaye_portefeuille" datasource="f8w_test">
                                        Select Montant from prestation_prepaye_portefeuille 
                                        use index(siret_id_ent_portefeuille_NumClient) 
                                        Where siret='#lect_etablissement.siret#' 
                                        and id_ent_portefeuille='#prestation_prepaye_portefeuille_ent.id#'
                                        and NumClient='#lect_client.NumClient#'
                                    </cfquery>
                                    <cfset MontantReste = #prestation_prepaye_portefeuille.Montant#>
                                <cfelse>
                                    <cfquery name="prestation_prepaye" datasource="f8w_test">
                                        Select Qte from prestation_prepaye use index(siret_NumPrestation_NumClient)
                                        Where siret='#lect_etablissement.siret#' 
                                        and NumPrestation='#lect_prestation.NumPrestation#' 
                                        and NumClient='#lect_client.NumClient#' 
                                        or siret='#lect_etablissement.siret#' and NumPrestation='0' 
                                        and NumClient='#lect_client.NumClient#'
                                    </cfquery>
                                    <cfif #prestation_prepaye.recordcount# gt 0>
                                        <cfset ticketreste = #prestation_prepaye.Qte#>
                                    <cfelse>
                                        <cfset ticketreste = 0>
                                    </cfif>
                                </cfif> 

                                <div class="text-center mb-4">
                                    <cfif isdefined("MontantReste")>
                                        <cfif #lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0>
                                            <p class="bold text-red text-large m-0">Attention, votre portefeuille électronique présente un solde de <cfoutput>#Val(MontantReste)#</cfoutput>€</p>
                                            <a class="link-blue text-large bold" href="##" onClick="javascript:ColdFusion.Window.show('histoticket')">(Cliquez pour consulter l'historique)</a>
                                        <cfelse>
                                            <cfif #Val(MontantReste)# gt 0>
                                                <p class="bold text-red text-large m-0">Attention, votre portefeuille électronique présente un solde de <cfoutput>#Val(MontantReste)#</cfoutput>€</p>
                                                <a class="link-blue text-large bold" href="##" onClick="javascript:ColdFusion.Window.show('histoticket')">(Cliquez pour consulter l'historique)</a>
                                            <cfelse>    
                                                <p class="text-red text-large m-0"><span class="bold">VOUS POUVEZ EXCEPTIONNELLEMENT réserver malgré votre solde de <cfoutput>#Val(MontantReste)#</cfoutput>€</span>
                                                    dans la limite de 15 jours dans le futur, vous vous engager à régulariser par un achat dès que possible !</p>
                                                <a class="link-blue text-large bold" href="##" onClick="javascript:ColdFusion.Window.show('histoticket')">(Cliquez pour consulter l'historique)</a>
                                            </cfif>
                                        </cfif>
                                    <cfelse>
                                        <cfif #lect_etablissement.ResaMemeSiPortefeuilleNul# eq 0>
                                            <p class="bold text-red text-large">Attention, vous pouvez encore réserver <cfoutput>#ticketreste#</cfoutput> jour(s)</p>
                                        <cfelse>
                                            <cfif #ticketreste# gt 0>
                                                <p class="bold text-red text-large">Attention, vous pouvez encore réserver <cfoutput>#ticketreste#</cfoutput> jour(s)</p>
                                            <cfelse>    
                                                <p class="text-red text-large"><span class="bold">VOUS POUVEZ EXCEPTIONNELLEMENT réserver malgré votre solde de<cfoutput>#ticketreste#</cfoutput> jour(s)</span>
                                                dans la limite de 15 jours dans le futur, vous vous engager à régulariser par un achat dès que possible !</p>
                                            </cfif>
                                        </cfif>
                                    </cfif>
                                    &nbsp;
                                    <cfif #lect_etablissement.CBBanque# eq 1 or #lect_etablissement.CBTipi# eq 1>
                                        <!--- pour st quentin la poterie des client en prelèvement et d'autre en prepaiement --->
                                        <cfif #lect_etablissement.siret# eq "11821135031090">
                                            <!--- client dans groupe client prépaiement ? --->
                                            <cfquery name="clientgroupe" datasource="f8w_test">
                                                Select * from clientgroupe use index(siret_NumClient_NumGroupe) 
                                                Where siret='#lect_etablissement.siret#' 
                                                and NumClient='#lect_client.numclient#' and NumGroupe='1000000000'
                                            </cfquery>
                                            <cfif #clientgroupe.recordcount# eq 1>
                                                <a class="link-blue" href="/achats/?id_mnu=150&init=1">Acheter</a>
                                            </cfif>
                                        <cfelse>
                                            <a class="link-blue" href="/achats/?id_mnu=150&init=1">Acheter</a>
                                        </cfif>
                                    </cfif>
                                </div>
                            </cfif>
                            <cfif len(#lect_prestation.LibLibre#) gt 0>
                                <p class="text-red bold"><cfoutput>#lect_prestation.LibLibre#</cfoutput></p>
                            </cfif>
                            <!---<tr bgcolor="#C6D580">
                                <td>&nbsp;</td>
                                <td colspan="10" align="center">&nbsp;</td>
                            </tr>
                            <cfoutput>
                                <tr bgcolor="##C6D580">
                                    <td>&nbsp;</td>
                                    <td colspan="10" align="center"><a title="Imprimer" href="/inscriptions/indexprt.cfm?id_mnu=22&id_prestation=#url.id_prestation#&id_enfant=#url.id_enfant#" target="_blank"><img src="/commun/imprimer.png"></a></td>
                                </tr>
                            </cfoutput>
                            <tr bgcolor="#C6D580">
                                <td>&nbsp;</td>
                                <td colspan="10" align="center">&nbsp;</td>
                            </tr>--->
                            <div class="row">
                                <div class="col-1 d-flex justify-content-center align-items-center">
                                </div>

                                <div class="col-10 row d-flex flex-wrap justify-content-between">
                                    <cfset Date_calend_dep = createdate(year(now()),month(now()),1)>
                                    <cfset Date_calend_dep = dateadd("M",-1,#Date_calend_dep#)>
                                    <cfset calendrier = Module.Calendrier1(#Date_calend_dep#,#lect_user.siret#,#lect_prestation.Numprestation#,#lect_enfant.Numenfant#,#lect_prestation.siret#)>
                                    <cfinclude template="calendrier.cfm">
                                    
                                    <cfset Date_calend_dep = createdate(year(now()),month(now()),1)>
                                    <cfset calendrier = Module.Calendrier1(#Date_calend_dep#,#lect_user.siret#,#lect_prestation.Numprestation#,#lect_enfant.Numenfant#,#lect_prestation.siret#)>
                                    <cfinclude template="calendrier.cfm">
                                        
                                    <cfset Date_calend_dep = createdate(year(now()),month(now()),1)>
                                    <cfset Date_calend_dep = dateadd("M",1,#Date_calend_dep#)>
                                    <cfset calendrier = Module.Calendrier1(#Date_calend_dep#,#lect_user.siret#,#lect_prestation.Numprestation#,#lect_enfant.Numenfant#,#lect_prestation.siret#)>
                                    <cfinclude template="calendrier.cfm">
                                </div>

                                <div class="col-1 d-flex justify-content-center align-items-center">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-1 d-flex justify-content-center align-items-center">
                                </div>

                                <div class="col-10 row d-flex flex-wrap justify-content-between">
                                    <cfset Date_calend_dep = createdate(year(now()),month(now()),1)>
                                    <cfset Date_calend_dep = dateadd("M",2,#Date_calend_dep#)>
                                    <cfset calendrier = Module.Calendrier1(#Date_calend_dep#,#lect_user.siret#,#lect_prestation.Numprestation#,#lect_enfant.Numenfant#,#lect_prestation.siret#)>
                                    <cfinclude template="calendrier.cfm">
                                    
                                    <cfset Date_calend_dep = createdate(year(now()),month(now()),1)>
                                    <cfset Date_calend_dep = dateadd("M",3,#Date_calend_dep#)>
                                    <cfset calendrier = Module.Calendrier1(#Date_calend_dep#,#lect_user.siret#,#lect_prestation.Numprestation#,#lect_enfant.Numenfant#,#lect_prestation.siret#)>
                                    <cfinclude template="calendrier.cfm">
                                        
                                    <cfset Date_calend_dep = createdate(year(now()),month(now()),1)>
                                    <cfset Date_calend_dep = dateadd("M",4,#Date_calend_dep#)>
                                    <cfset calendrier = Module.Calendrier1(#Date_calend_dep#,#lect_user.siret#,#lect_prestation.Numprestation#,#lect_enfant.Numenfant#,#lect_prestation.siret#)>
                                    <cfinclude template="calendrier.cfm">
                                </div>

                                <div class="col-1 d-flex justify-content-center align-items-center">
                                </div>
                            </div>

                            <table width="100%" cellspacing="1" class="mt-4">
                                <td width="10%" align="center" bgcolor="#B4F8A0"><font size="-1">Etait inscrit</font></td>
                                <td width="10%" align="center" bgcolor="#00FF00"><font size="-1">Sera présent</font></td>
                                <td width="10%" align="center" bgcolor="#FF0000"><font size="-1">Sera absent</font></td>
                                <td width="10%" align="center" bgcolor="#FFFFFF"><font size="-1">Prestation <cfoutput>#affdesiprestation#</cfoutput> Ouverte (& pas inscrit)</font></td>
                                <td width="10%" align="center" bgcolor="#666666"><font color="#FFFFFF" size="-1">Prestation <cfoutput>#affdesiprestation#</cfoutput> Fermée</font></td>
                                <td width="10%" align="center" bgcolor="#E9E29B"><font size="-1">Noté absent</font></td>
                                <td width="10%" align="center" bgcolor="#2D8DC3"><font size="-1">Noté présent</font></td>
                            </table>

                        <cfelseif #lect_prestation.ResaPeriode# eq 1><!--- resa sur période (forfait) ---> 
                            <!--- reserve_a ou pas ??? --->
                            <cfquery name="reserve_aoupas" datasource="f8w_test">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                where siret='#lect_client.siret#' and 
                                NumPrestation='#lect_prestation.NumPrestation#' 
                                order by DebutPerioPrestaAn ASC, DebutPerioPresta ASC
                            </cfquery>
                            
                            <cfif #reserve_aoupas.recordcount#><!--- période définie ou réservé a--->
                                <cfif #lect_prestation.ticket# eq 1>
                                    <cfquery name="prestation_prepaye_portefeuille_presta" datasource="f8w_test">
                                        Select id_ent from prestation_prepaye_portefeuille_presta 
                                        use index(siret_NumPrestation)
                                        Where siret='#lect_prestation.siret#' 
                                        and NumPrestation='#lect_prestation.NumPrestation#'
                                    </cfquery>
                                    <cfif #prestation_prepaye_portefeuille_presta.recordcount# eq 1>
                                        <cfset identport = #prestation_prepaye_portefeuille_presta.id_ent#>
                                    <cfelse>
                                        <cfset identport = 0>
                                    </cfif>
                                    <cfquery name="prestation_prepaye_portefeuille_ent" datasource="f8w_test">
                                        Select * from prestation_prepaye_portefeuille_ent 
                                        Where id='#identport#' and del=0
                                    </cfquery>
                                    <cfif #prestation_prepaye_portefeuille_ent.recordcount# eq 1>
                                        <cfquery name="prestation_prepaye_portefeuille" datasource="f8w_test">
                                            Select Montant from prestation_prepaye_portefeuille 
                                            use index(siret_id_ent_portefeuille_NumClient) 
                                            Where siret='#lect_etablissement.siret#' 
                                            and id_ent_portefeuille='#prestation_prepaye_portefeuille_ent.id#' 
                                            and NumClient='#lect_client.NumClient#'
                                        </cfquery>
                                        <cfset MontantReste = #prestation_prepaye_portefeuille.Montant#>
                                    <cfelse>  
                                        <cfquery name="prestation_prepaye" datasource="f8w_test">
                                            Select Qte from prestation_prepaye use index(siret_NumPrestation_NumClient) 
                                            Where siret='#lect_etablissement.siret#' 
                                            and NumPrestation='#lect_prestation.NumPrestation#' 
                                            and NumClient='#lect_client.NumClient#' 
                                            or siret='#lect_etablissement.siret#' and NumPrestation='0' 
                                            and NumClient='#lect_client.NumClient#'
                                        </cfquery>
                                        <cfif #prestation_prepaye.recordcount# gt 0>
                                            <cfset ticketreste = #prestation_prepaye.Qte#>
                                        <cfelse>
                                            <cfset ticketreste = 0>
                                        </cfif>
                                    </cfif>
                                    
                                    <cfif #lect_prestation.ResaPeriode# eq 0>
                                        <cfif isdefined("MontantReste")>
                                            <p class="text-red text-large bold">Attention, votre portefeuille électronique présente un solde de <cfoutput>#Val(MontantReste)#</cfoutput>€ pour <cfoutput>#prestation_prepaye_portefeuille_ent.designation#</cfoutput></p>
                                            <a class="link-red bold text-large" href="##" onClick="javascript:ColdFusion.Window.show('histoticket')">(Cliquez pour consulter l'historique)</a>
                                        <cfelse>
                                            <p class="text-red text-large bold">Attention, vous pouvez encore réserver <cfoutput>#ticketreste#</cfoutput> jour(s)</p>
                                        </cfif>
                                        &nbsp;
                                        <cfif #lect_etablissement.CBBanque# eq 1 or #lect_etablissement.CBTipi# eq 1>
                                            <a class="link-blue" href="/achats/?id_mnu=150&init=1">Acheter</a>
                                        </cfif>
                                    </cfif>
                                </cfif>
                                <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <th colspan="2" align="center">Réservation possible</th>
                                        <th colspan="2" align="center">Prestation ouverte</th>
                                        <td width="12%">&nbsp;</td>
                                        <td width="44%">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="11%" align="center">Du</td>
                                        <td width="11%" align="center">Au</td>
                                        <td width="11%" align="center">Du</td>
                                        <td width="11%" align="center">Au</td>
                                        <td align="center">Réservé</td>
                                        <td align="center">&nbsp;</td>
                                    </tr>

                                    <cfoutput query="reserve_aoupas">
                                        <cfset ont_affiche = "oui">
                                        <cfif #reserve_aoupas.NumGroupeClient# gt 0>
                                            <cfquery name="groupeclient" datasource="f8w_test">
                                                Select NumGroupe from clientgroupe use index(siret_NumClient) 
                                                Where siret='#lect_client.siret#' and NumClient='#lect_client.NumClient#' 
                                                and NumGroupe='#reserve_aoupas.NumGroupeClient#'
                                            </cfquery>
                                            <cfif #groupeclient.recordcount# eq 0><cfset ont_affiche = "non"></cfif>
                                        <cfelseif #reserve_aoupas.NumClasse# gt 0>
                                            <cfquery name="enfantclasse" datasource="f8w_test">
                                                Select NumClasse from enfantclasse use index(siret_NumEnfant) 
                                                Where siret='#lect_client.siret#' 
                                                and NumEnfant='#lect_numEnfant.NumEnfant#' 
                                                and NumClasse='#reserve_aoupas.NumClasse#'
                                            </cfquery>
                                            <cfif #enfantclasse.recordcount# eq 0><cfset ont_affiche = "non"></cfif>
                                        </cfif>
                                        <cfif #ont_affiche# eq "oui">
                                            <cfset ModifPossible = 1>
                                            <cfform name="reser#reserve_aoupas.id#" class="blue-select">
                                                <tr>
                                                    <cfset resadu = "">
                                                    <cfif len(#reserve_aoupas.ReservableDuAn#)>
                                                        <cfset premjouran = createdate(#reserve_aoupas.ReservableDuAn#,1,1)>
                                                        <cfset resadu = dateadd("d",#reserve_aoupas.ReservableDu# -1,#premjouran#)>
                                                        <cfset resadu1 = lsdateformat(#resadu#,"dd/MM/YYYY")>
                                                        <cfif #modifpossible# eq 1>
                                                            <cfif datediff("d",now(),#resadu#) gt 0>
                                                            <!---<cfif datediff("d",now(),#resadu#) gte 0>--->
                                                                <cfset ModifPossible = 0>
                                                            </cfif>
                                                        </cfif>
                                                    </cfif>
                                                    <td align="center">#resadu1#</td>
                                                    <cfset resaau = "">
                                                    <cfif len(#reserve_aoupas.ReservableAuAn#)>
                                                        <cfset premjouran1 = createdate(#reserve_aoupas.ReservableAuAn#,1,1)>
                                                        <cfset resaau = dateadd("d",#reserve_aoupas.ReservableAu# -1,#premjouran1#)>
                                                        <cfset resaau1 = lsdateformat(#resaau#,"dd/MM/YYYY")>
                                                        <cfif #modifpossible# eq 1>
                                                            <cfif datediff("d",now(),#resaau#) lt 0>
                                                            <!---<cfif datediff("d",now(),#resaau#) lte 0>--->
                                                                <cfset ModifPossible = 0>
                                                            </cfif>
                                                        </cfif>                                                                
                                                    </cfif>                                                            
                                                    <td align="center">#resaau1#</td>
                                                    <cfset ouvertedu = "">
                                                    <cfif len(#reserve_aoupas.DebutPerioPrestaAn#)>
                                                        <cfset premjouran1 = createdate(#reserve_aoupas.DebutPerioPrestaAn#,1,1)>
                                                        <cfset ouvertedu = dateadd("d",#reserve_aoupas.DebutPerioPresta# -1,#premjouran1#)>
                                                        <cfset ouvertedu1 = lsdateformat(#ouvertedu#,"dd/MM/YYYY")>
                                                        <cfset ouvertedustp = lsdateformat(#ouvertedu#,"YYYYMMdd")>
                                                    </cfif>                                                            
                                                    <td align="center">#ouvertedu1#</td>
                                                    <cfset ouverteau = "">
                                                    <cfif len(#reserve_aoupas.FinPerioPrestaAn#)>
                                                        <cfset premjouran1 = createdate(#reserve_aoupas.FinPerioPrestaAn#,1,1)>
                                                        <cfset ouverteau = dateadd("d",#reserve_aoupas.FinPerioPresta# -1,#premjouran1#)>
                                                        <cfset ouverteau1 = lsdateformat(#ouverteau#,"dd/MM/YYYY")>
                                                        <cfset ouverteaustp = lsdateformat(#ouverteau#,"YYYYMMdd")>
                                                    </cfif>                                                            
                                                    <td align="center">#ouverteau1#</td>
                                                    <!--- inscrit ou pas --->
                                                    <cfset Inscrit = 0>
                                                    <!--- inscription récurente ?? --->
                                                    <cfquery name="verinscription" datasource="f8w_test">
                                                        Select InscriptionImpaire,DatedebSTP 
                                                        from inscription 
                                                        use index(siret_Numenfant_NumPrestation) 
                                                        Where siret='#lect_client.siret#' 
                                                        and NumEnfant='#lect_numEnfant.NumEnfant#' 
                                                        and NumPrestation='#lect_prestation.NumPrestation#' 
                                                        order by DatedebSTP DESC
                                                    </cfquery>
                                                    <cfset stpjour = lsdateformat(now(),"YYYYMMdd")>
                                                    <cfloop query="verinscription">
                                                        <cfif #verinscription.DatedebSTP# lte #stpjour#>
                                                            <cfif #verinscription.InscriptionImpaire# gt 0>
                                                                <cfset Inscrit = 1>
                                                            </cfif>
                                                            <cfbreak>
                                                        </cfif>
                                                    </cfloop>
                                                    <cfif #Inscrit# eq 0><!--- inscription ponctuelle --->
                                                        <cfif len(#ouvertedu1#)><!--- borne sur depart --->
                                                            <cfif len(#ouverteau1#)><!--- borne sur fin --->
                                                                <cfquery name="inscrplan" datasource="f8w_test" maxrows="1">
                                                                    Select * from inscriptionplanning 
                                                                    use index(siret_NumPrestation_NumEnfant) 
                                                                    Where siret='#lect_client.siret#' 
                                                                    and NumPrestation='#lect_prestation.NumPrestation#' 
                                                                    and NumEnfant='#lect_numEnfant.NumEnfant#' 
                                                                    and nature=0 
                                                                    and DateInscriptionSTP>='#ouvertedustp#' 
                                                                    and DateInscriptionSTP<='#ouverteaustp#'
                                                                </cfquery>
                                                                <cfif #inscrplan.recordcount#>
                                                                    <cfset Inscrit = 1>
                                                                </cfif>
                                                            <cfelse>
                                                                <cfquery name="inscrplan" datasource="f8w_test" maxrows="1">
                                                                    Select * from inscriptionplanning 
                                                                    use index(siret_NumPrestation_NumEnfant) 
                                                                    Where siret='#lect_client.siret#' 
                                                                    and NumPrestation='#lect_prestation.NumPrestation#' 
                                                                    and NumEnfant='#lect_numEnfant.NumEnfant#' 
                                                                    and nature=0 
                                                                    and DateInscriptionSTP>='#ouvertedustp#'
                                                                </cfquery>
                                                                <cfif #inscrplan.recordcount#>
                                                                    <cfset Inscrit = 1>
                                                                </cfif>                                                                    
                                                            </cfif>
                                                        <cfelse>
                                                            <cfif len(#ouverteau1#)><!--- borne sur fin --->
                                                                <cfquery name="inscrplan" datasource="f8w_test" maxrows="1">
                                                                    Select * from inscriptionplanning 
                                                                    use index(siret_NumPrestation_NumEnfant) 
                                                                    Where siret='#lect_client.siret#' 
                                                                    and NumPrestation='#lect_prestation.NumPrestation#' 
                                                                    and NumEnfant='#lect_numEnfant.NumEnfant#' 
                                                                    and nature=0 
                                                                    and DateInscriptionSTP<='#ouverteaustp#'
                                                                </cfquery>
                                                                <cfif #inscrplan.recordcount#>
                                                                    <cfset Inscrit = 1>
                                                                </cfif>
                                                            </cfif>                                                                
                                                        </cfif>
                                                    </cfif>
                                                    <td>&nbsp;
                                                        <!--- 13/08/2018 QUOTA ??? --->
                                                        <cfset TxtQuota = "">
                                                        <cfquery name="enfantclasse" datasource="f8w_test">
                                                            Select NumClasse From enfantclasse use index(siret_NumEnfant) 
                                                            Where siret = '#lect_user.siret#' 
                                                            and NumEnfant = '#lect_enfant.numenfant#'
                                                        </cfquery>
                                                        <cfset NumLieuPrestation = 0>
                                                        <cfset NumClassePrestation = 0>
                                                        <cfset NumLieuPrestationQuota = 0>
                                                        <cfif #enfantclasse.recordcount# gt 0>         
                                                            <cfloop query="enfantclasse">
                                                                <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                                                                    Select NumLieu,NumClasse,Quota 
                                                                    from lieuprestationclasse 
                                                                    use index(siret_NumClasse_NumPrestation) 
                                                                    Where siret = '#lect_user.siret#' and (
                                                                    NumClasse = '#enfantclasse.NumClasse#' or NumClasse=0) 
                                                                    and NumPrestation = '#lect_prestation.NumPrestation#' 
                                                                    and (id_reserve_a='#reserve_aoupas.id#' or id_reserve_a=0) 
                                                                    limit 0,1
                                                                </cfquery>
                                                                <cfif #lieuprestationclasse.recordcount#>
                                                                    <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                                                                    <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                                                                    <cfset NumClassePrestation = #lieuprestationclasse.NumClasse#>
                                                                    <cfbreak>
                                                                </cfif>
                                                            </cfloop>
                                                        <cfelse>
                                                            <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                                                                Select NumLieu,NumClasse,Quota 
                                                                from lieuprestationclasse 
                                                                use index(siret_NumClasse_NumPrestation) 
                                                                Where siret = '#lect_user.siret#' and NumClasse=0
                                                                and NumPrestation = '#lect_prestation.NumPrestation#' 
                                                                and (id_reserve_a='#reserve_aoupas.id#' or id_reserve_a=0)
                                                                limit 0,1
                                                            </cfquery>
                                                            <cfif #lieuprestationclasse.recordcount#>
                                                                <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                                                                <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                                                                <cfset NumClassePrestation = #lieuprestationclasse.NumClasse#>
                                                            </cfif>                                                                
                                                        </cfif>        
                                                        <cfif #NumLieuPrestationQuota# gt 0>
                                                            <cfset ouvertedu = "">
                                                            <cfset ouvertedu1 = "">
                                                            <cfset ouvertedustp = "">
                                                            <cfif len(#reserve_aoupas.DebutPerioPrestaAn#)>
                                                                <cfset premjouran1 = createdate(#reserve_aoupas.DebutPerioPrestaAn#,1,1)>
                                                                <cfset ouvertedu = dateadd("d",#reserve_aoupas.DebutPerioPresta# -1,#premjouran1#)>
                                                                <cfset ouvertedu1 = lsdateformat(#ouvertedu#,"dd/MM/YYYY")>
                                                                <cfset ouvertedustp = lsdateformat(#ouvertedu#,"YYYYMMdd")>
                                                            </cfif>    
                                                            <cfset ouverteau = "">
                                                            <cfset ouverteau1 = "">
                                                            <cfset ouverteaustp = "">
                                                            <cfif len(#reserve_aoupas.FinPerioPrestaAn#)>
                                                                <cfset premjouran1 = createdate(#reserve_aoupas.FinPerioPrestaAn#,1,1)>
                                                                <cfset ouverteau = dateadd("d",#reserve_aoupas.FinPerioPresta# -1,#premjouran1#)>
                                                                <cfset ouverteau1 = lsdateformat(#ouverteau#,"dd/MM/YYYY")>
                                                                <cfset ouverteaustp = lsdateformat(#ouverteau#,"YYYYMMdd")>
                                                            </cfif>
                                                            <cfset nombredejour = datediff("d",#ouvertedu#,#ouverteau#) + 3>
                                                            <cfset DateJourAtraiter = ouvertedu>
                                                            <cfset DateJourAtraiterstr = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
                                                            <cfset DateJourAtraiterstp = lsdateformat(#DateJourAtraiter#,"YYYYMMdd")>
                                                            <cfloop from="1" to="#nombredejour#" index="idx">
                                                                <cfset a = Module.ResaJour(#lect_prestation.JoursExclus#,#DayOfWeek(DateJourAtraiter)#)>
                                                                <cfif #a# neq 1>
                                                                    <cfif #NumLieuPrestationQuota# gt 0>
                                                                        <cfset ouvertedu1test = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
                                                                        <!---<cfset Msgquota = Module.JourPrestationLieuQuota(#lect_user.siret#,#ouvertedu1test#,#reserve_aoupas.NumPrestation#,#NumLieuPrestation#)>--->
                                                                        <cfset Msgquota = Module.JourPrestationLieuQuota1(#lect_user.siret#,#ouvertedu1test#,#reserve_aoupas.NumPrestation#,#NumLieuPrestation#,#NumClassePrestation#,#lect_prestation.siret#,#reserve_aoupas.id#)>
                                                                        <cfif #Msgquota# gte #NumLieuPrestationQuota#>
                                                                            <cfif #Inscrit# eq 0>	
                                                                                <cfset ModifPossible = 0>
                                                                            </cfif>
                                                                            <cfset PlaceRestante = #NumLieuPrestationQuota# - #Msgquota#>
                                                                        
                                                                            <cfif #PlaceRestante# gt 0>
                                                                                <cfset TxtQuota = '<font color="##FF0000">' & #NumLieuPrestationQuota# & " places maxi. et il reste " & #PlaceRestante# & " places</font>">
                                                                            <cfelse>
                                                                                <cfset TxtQuota = '<font color="##FF0000">Le nombre de place maximal est atteint !</font>'>
                                                                            </cfif>
                                                                            <cfbreak>
                                                                        <cfelse>
                                                                            <cfset PlaceRestante = #NumLieuPrestationQuota# - #Msgquota#>
                                                                            <cfif #PlaceRestante# gt 0>
                                                                                <cfset TxtQuota = #NumLieuPrestationQuota# & " places maxi. et il reste " & #PlaceRestante# & " places">								
                                                                            <cfelse>
                                                                                <cfset TxtQuota = '<font color="##FF0000">Le nombre de place maximal est atteint !</font>'>
                                                                            </cfif>	
                                                                        </cfif>																		
                                                                    </cfif>
                                                                    <cfbreak>
                                                                </cfif>
                                                                <cfif #DateJourAtraiterstr# neq #ouverteau1#>    
                                                                    <cfset DateJourAtraiter = DateAdd("d",1,#DateJourAtraiter#)>
                                                                    <cfset DateJourAtraiterstr = lsdateformat(#DateJourAtraiter#,"dd/MM/YYYY")>
                                                                    <cfset DateJourAtraiterstp = lsdateformat(#DateJourAtraiter#,"YYYYMMdd")>   
                                                                <cfelse>
                                                                    <cfbreak>
                                                                </cfif>
                                                            </cfloop>
                <!---										<cfset Msgquota = Module.JourPrestationLieuQuota1(#lect_user.siret#,#ouvertedu1#,#lect_prestation.NumPrestation#,#NumLieuPrestation#,#NumClassePrestation#)>
                                                            <cfif #Msgquota# gte #NumLieuPrestationQuota#>                       	
                                                                <cfset ModifPossible = 0>
                                                            </cfif>
                                                            <cfset PlaceRestante = #NumLieuPrestationQuota# - #Msgquota#>
                                                            <cfset TxtQuota = #NumLieuPrestationQuota# & " places maxi. et il reste " & #PlaceRestante# & " places">																
                --->																
                                                        </cfif>
                                                        <cfset TxtAchatpossible = "">
                                                        <cfif isdefined("MontantReste")><!--- portefeuille --->
                                                            <cfquery name="tarif" datasource="f8w_test">
                                                                Select PU from prixprestation use index(siret_Numprestation)
                                                                where siret='#lect_prestation.siret#' 
                                                                and NumPrestation='#lect_prestation.NumPrestation#' and CodePrix=1
                                                            </cfquery>
                                                            <!--- COLLIAS REDUC SUR QF ET NBR ENF --->
                                                            <cfquery name="reduction" datasource="f8w_test">
                                                                Select * from reductions use index(siret_numprestation)
                                                                where siret='#lect_prestation.siret#' 
                                                                and NumPrestation='#lect_prestation.NumPrestation#' 
                                                                and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                                                                and del=0
                                                                order by ApartirDeNbrEnfant
                                                            </cfquery>
                                                            <cfset txttar = "Tarif : ">
                                                            <cfset prixreduit = 0>
                                                            <cfif #reduction.recordcount#>
                                                                <cfloop query="reduction">
                                                                    <cfset prixreduit = #tarif.PU# - #reduction.MontReduc#>
                                                                    <cfset txttar = #txttar# & #reduction.Désignation# & " : " & #prixreduit# & " €, ">
                                                                </cfloop>
                                                            <cfelse>
                                                                <cfset txttar = #txttar# & #tarif.PU# & " €">
                                                                <cfset prixreduit = #tarif.PU#>
                                                            </cfif>
                                                            <cfset Achatpossible = val(#MontantReste#) - val(#prixreduit#)>
                                                            <cfif #Achatpossible# lte 0>
                                                                <cfif #Inscrit# eq 0>	
                                                                    <cfset ModifPossible = 0>
                                                                </cfif>
                                                                <cfset TxtAchatpossible = "Vous devez augmenter le montant de votre portefeuille si vous souhaitez inscrire votre enfant !">
                                                            </cfif>
                                                        </cfif>
                                                        <!--- traitement du cas sivsc plusieur session identique --->
                                                        <cfquery name="verifsivsc" dbtype="query">
                                                            Select * from reserve_aoupas where id<>'#reserve_aoupas.id#' 
                                                            and NumGroupeClient='#reserve_aoupas.NUmGroupeClient#' 
                                                            and NumClasse='#reserve_aoupas.NumClasse#' 
                                                            and AnneeNaissance='#reserve_aoupas.AnneeNaissance#' 
                                                            and Age='#reserve_aoupas.Age#' 
                                                            and DebutPerioPresta='#reserve_aoupas.DebutPerioPresta#' 
                                                            and DebutPerioPrestaAn='#reserve_aoupas.DebutPerioPrestaAn#'
                                                            and FinPerioPresta='#reserve_aoupas.FinPerioPresta#'
                                                            and FinPerioPrestaAn='#reserve_aoupas.FinPerioPrestaAn#'
                                                        </cfquery>
                                                        <cfif #verifsivsc.recordcount#>
                                                            <cfset inscrialasession = #Inscrit#>
                                                            <cfset modifsessionpossible = #ModifPossible#>
                                                            <cfif #inscrplan.id_reserve_a# gt 0>
                                                                <cfif #inscrplan.id_reserve_a# neq #reserve_aoupas.id#>
                                                                    <cfset inscrialasession = 0>
                                                                    <cfset modifsessionpossible = 0>
                                                                </cfif>
                                                            <cfelse>
                                                                <cfquery name="verachat" datasource="f8w_test" maxrows="1">
                                                                    Select * from cbpanier use index(siret_numclient) 
                                                                    where siret='#lect_prestation.siret#' 
                                                                    and numclient='#lect_client.numclient#' 
                                                                    and idclient='#lect_client.id#'
                                                                    and id_prestation_reserve_a='#reserve_aoupas.id#'
                                                                    and numenfant='#lect_enfant.numenfant#'
                                                                    and numprestation='#lect_prestation.numprestation#' 
                                                                    and CodeReponse='P' limit 0,1	                                                                    	
                                                                </cfquery>
                                                                <cfif #verachat.recordcount# eq 0>
                                                                    <cfset inscrialasession = 0>
                                                                    <!---<cfset modifsessionpossible = 0>
                                                                    Modif 10/12/2018 --->
                                                                    <cfset modifsessionpossible = 1>
                                                                </cfif>
                                                            </cfif>
                                                            <cfif #inscrialasession# eq 0>
                                                                <cfinput type="checkbox" name="inscrit">
                                                            <cfelse>
                                                                <cfinput type="checkbox" checked="yes" name="inscrit">
                                                            </cfif>&nbsp;&nbsp;
                                                            <cfif #modifsessionpossible# eq 1 or #session.log_mairie# is true >
                                                                <cfif #lect_prestation.ticket# eq 1 and #inscrialasession# eq 0>
                                                                    <cfif #ModifPossible# neq 0>
                                                                        <cfinput type="submit" class="btn btn-primary" name="reserve_aoupas" value="Acheter">
                                                                    <cfelse>
                                                                        <cfinput disabled class="btn btn-primary" type="submit" name="reserve_aoupas" value="Acheter">
                                                                    </cfif>
                                                                <cfelse>
                                                                    <!--- modif 19/02/2020
                                                                    <cfinput type="submit" name="reserve_aoupas" value="Valider">--->
                                                                    <cfinput disabled type="submit" class="btn btn-primary" name="reserve_aoupas" value="Valider">
                                                                </cfif>
                                                            <cfelse>
                                                                <cfinput type="submit" disabled class="btn btn-primary" name="reserve_aoupas" value="Valider">
                                                            </cfif>
                                                        <cfelse>
                                                            <cfif #Inscrit# eq 0>
                                                                <cfinput type="checkbox" name="inscrit">
                                                            <cfelse>
                                                                <cfinput type="checkbox" checked="yes" name="inscrit">
                                                            </cfif>&nbsp;&nbsp;
                                                            <cfif #ModifPossible# eq 1 or #session.log_mairie# is true >
                                                                <cfif #lect_prestation.ticket# eq 1 and #Inscrit# eq 0>
                                                                    <cfinput type="submit" class="btn btn-primary" name="reserve_aoupas" value="Acheter">
                                                                <cfelseif #lect_prestation.ticket# eq 1>
                                                                    <!--- modif 19/02/2020 --->
                                                                    <cfinput disabled type="submit" class="btn btn-primary" name="reserve_aoupas" value="Valider">
                                                                <cfelse>
                                                                    <cfinput type="submit" class="btn btn-primary" name="reserve_aoupas" value="Valider">
                                                                </cfif>
                                                            <cfelse>
                                                                <cfinput type="submit" class="btn btn-primary" disabled name="reserve_aoupas" value="Valider">
                                                            </cfif>
                                                        </cfif>
                                                    </td>
                                                    <td>
                                                        <cfif len(#reserve_aoupas.Designation#)>
                                                            <p class="bold">#reserve_aoupas.Designation#</p><br>
                                                        </cfif>
                                                        #TxtQuota#
                                                        <cfif isdefined("txttar")>
                                                            <br>#txttar#
                                                        </cfif>
                                                        <cfif isdefined("TxtAchatpossible")>
                                                            <br>#TxtAchatpossible#
                                                        </cfif>
                                                    </td>
                                                </tr>
                                                <cfinput type="hidden" name="sav_id" value="#reserve_aoupas.id#">
                                                <cfinput type="hidden" name="sav_numenfant" value="#lect_numEnfant.NumEnfant#">
                                                <cfinput type="hidden" name="sav_id_presta" value="#url.id_prestation#">
                                            </cfform>
                                        </cfif>
                                    </cfoutput>
                                </table>
                            <cfelse><!---pas de période--->
                                <cfif #lect_prestation.ResaPeriode# eq 0>
                                    <p class="text-red"><cfoutput>#explic_delai#</cfoutput></p>
                                </cfif>
                            </cfif>
                        </cfif>
                    </cfif>
                </cfif>
            </div>
        </cfform>
    </div>
</div>