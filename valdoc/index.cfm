﻿<cfinclude template="/Query_Header.cfm">
<cfif isdefined("form.signedoc")>
	<cfquery name="maj" datasource="f8w_test">
    	Update client set ValidationReglement=#now()# where id='#lect_client.id#'
    </cfquery>
    <cflocation url="/home/?id_mnu=149" addtoken="no">
</cfif>



<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Menus cantine</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

<cfif len(#msg#)>
    <body onLoad="showMB('err_login')">
        <cfmessagebox name="err_login" type="alert" message="#msg#" icon="error" title="Attention, erreur de saisie"  />
<cfelseif len(#msg1#)>
    <body onLoad="showMB('err_login')">
        <cfmessagebox name="err_login" type="alert" message="#msg1#" icon="info" title="Confirmation"  />
<cfelseif len(#msg2#)>
    <body onLoad="showMB('err_login')">
        <cfmessagebox name="err_login" type="alert" message="#msg2#" icon="error" title="Attention, important"  />
<cfelse>
    <body>
</cfif>
        <div id="bgBlur">
            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>

            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-7 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p>
                            <h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>#lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3>
                        </p>
                        <p class="description ps-5 pe-5">Veuillez consulter et accepter les règlements ci-dessous.</p>
                    </div>

                    <!-- CONTENT -->
                    <div class="row d-flex justify-content-between mt-5 content">
                        <div class="p-2 p-lg-5 bg-white section scroll">
                            <cfform name="log" method="post">
                                <p class="text-red bold mb-1">De nouveaux règlements ont été publiés, vous pouvez les consulter en cliquant sur leur nom :</p>
                                <p class="text-red">(Vous devez les accepter pour pouvoir utiliser le service)</p>

                                
                                <cfoutput query="document">
                                    <a class="link-blue" href="/commun/VisuPdf.cfm?id=#document.id#" target="_blank">#document.désignation#</a>
                                    <p>Publié le #lsdateformat(document.Date_Ajout_Modif,"dd/MM/YYYY")# à #lstimeformat(document.Date_Ajout_Modif,"HH:MM")#</p>
                                </cfoutput>
                                <p class="text-red bold">
                                <cfinput type="checkbox" name="chkval" value="1" required="yes" message="Merci de cocher la case"> 
                                Je déclare avoir pris connaissance des documents ci-dessus et accepte les conditions générales d'utilisations et/ou les règlements intérieurs.</p>
                                <cfinput type="submit" name="signedoc" class="btn btn-primary" value="Signer"></td>
                            </cfform>
                        </div>
                    </div>
                </div>
                
                    <!-- SECTION RIGHT -->
                <div class="col-lg-3 section-right">
                    <div class="mt-5">
                        <cfinclude template="/home/calendar.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions l&eacute;gales et confidentialit&eacute;</a>
                <p class="m-0 pe-3 ps-3">N&deg; de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">

        <script type="text/javascript"> //Script gestion menu burger
            function openNav() {
                document.getElementById("navMobileItems").style.display= "block";
                document.getElementById("navMobileLabel").style.display= "none";
                const background = document.getElementById("bgBlur");
                background.className = "bg-blur";
                background.addEventListener("click", closeNav);
            }

            function closeNav() {
                document.getElementById("navMobileItems").style.display= "none";
                document.getElementById("bgBlur").classList.remove("bg-blur");
                document.getElementById("navMobileLabel").style.display= "block";
            }
            function showMB(mbox)  { 
                ColdFusion.MessageBox.show(mbox); 
            } 
        </script>
    </body>
</html>