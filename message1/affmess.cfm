﻿<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Messagerie</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

    <body>
        <cfif len(#url.id_message#) gt 0>
            <cfquery name="msg" datasource="f8w_test">
                Select * from message where id='#url.id_message#'
            </cfquery>

            <cfquery name="lect_user" datasource="services"><!--- lecture de l'utilisateur du site ---->
                Select * from login Where id = '#session.use_id#' and siret='#msg.siret#' and niveau=0
            </cfquery>

            <cfquery name="verif" datasource="f8w_test">
                Select id from client use index(siret_numclient) where siret='#lect_user.siret#' and numclient='#lect_user.numclient#'
            </cfquery>

            <cfif #verif.recordcount# eq 0>
                <cfabort>
            </cfif>

            <cfif #msg.lu# eq 0 and #msg.EmisPar# eq 0>
                <!--- message de la mairie a marquer comme lu --->
                <cfquery name="maj" datasource="f8w_test">
                    Update message set Lu='1', DateLu=#now()# where id='#url.id_message#'
                </cfquery>    
            </cfif>

            <cfoutput query="msg">
                <table bgcolor="##FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <cfif #msg.EmisPar# eq 0>
                        <cfform name="pourrepondre" action="" method="post" target="_parent"> 
                            <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="99%">
                                <cfif #msg.EmisPar# eq 0>
                                    <cfinput type="submit" name="repondreaumessage" value="Répondre"class="btn btn-primary bold">
                                </cfif>
                            </td>
                            </tr>
                            <cfinput type="hidden" name="sav_idmessage" value="#url.id_message#">
                        </cfform>
                    </cfif>
                    <tr>
                    <td>&nbsp;</td>
                    <td>
                        <cfif len(#msg.piece_jointe#)>
                            <font size="-1">Pièce(s) jointe(s) :
                            <cfloop list="#msg.piece_jointe#" delimiters=";" index="lefic">
                                <cfquery name="lect_classactive" datasource="classactive">
                                    Select id from documents use index(siret_CodeAppli_Table_Origine_Id_Table_Origine) 
                                    Where siret='#msg.siret#' and CodeAppli='F8' and Table_Origine='message_ent' 
                                    and Id_Table_Origine='#msg.id_ent#' and Nom_Fichier='#lefic#'
                                </cfquery>
                                <cfif #lect_classactive.recordcount#>
                                    <cfoutput><a href="/Commun/VisuPdf.cfm?id=#lect_classactive.id#" target="_blank">#lefic#</a>&nbsp;</cfoutput>
                                </cfif>
                            </cfloop>
                            </font>
                        </cfif>
                    </td>
                    </tr>
                    <tr>
                    <td>&nbsp;</td>
                    <td>#msg.text#</td>
                    </tr>
                </table>
            </cfoutput>
        </cfif>
    </body>
</html>