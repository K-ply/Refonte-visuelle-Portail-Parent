﻿<cfparam name="Attributes.url" default="https://particulier-test.api.gouv.fr/api/v2/">
<cfparam name="Attributes.Api" default="">
<cfparam name="Attributes.XAPIKey" default="3841b13fa8032ed3c31d160d3437a76a">
<cfparam name="Attributes.numeroAllocataire" default="">
<cfparam name="Attributes.codePostal" default="">

<cfset Retour = "">
<cfif len(#Attributes.Api#)>
	<cfswitch expression="#Attributes.Api#">
    	<cfcase value="composition-familiale">
            <cfset NumallocataireCorrige = "">
            <cfloop from="1" to="#len(Attributes.numeroAllocataire)#" index="idx">
                <cfset car = mid(#Attributes.numeroAllocataire#,#idx#,1)>
                <cfif isnumeric(#car#) and #car# neq " ">
                    <cfset NumallocataireCorrige = #NumallocataireCorrige# & #car#>
                </cfif>
            </cfloop>
        	<cfset Retour = querynew("reason,message,error,adresse_identite,adresse_numeroRue,adresse_complementIdentiteGeo,adresse_codePostalVille,adresse_pays,allocataires1_nomPrenom,allocataires1_dateDeNaissance,allocataires1_sexe,allocataires2_nomPrenom,allocataires2_dateDeNaissance,allocataires2_sexe,annee,enfants_nomPrenom,enfants_dateDeNaissance,enfants_sexe,mois,quotientFamilial,httpr")>
            <cfhttp method="get" url="#Attributes.url##Attributes.Api#?numeroAllocataire=#NumallocataireCorrige#&codePostal=#Attributes.codePostal#">
        		<cfhttpparam type="header" name="accept" value="application/json">
        		<cfhttpparam type="header" name="X-API-Key" value="#Attributes.XAPIKey#">
        	</cfhttp>
            <!---<cfset nfic = createuuid()>
            <cfdump var="#cfhttp#" output="c:\tempweb\particulierjson#nfic#.txt">--->
        	<cfset err = "non">
            <cftry>
				<cfset RetourJson = DeserializeJSON(#cfhttp.FileContent#,false)>
        		<cfcatch>
                	<cfset RetourJson.message = "La plateforme Api particulier ne répond pas, merci de ressayer plus tard.">
                    <!---<cfset fic = "Retour_API_PART_" & createuuid() & ".txt">
                    <cffile action="WRITE" file="c:\tempweb\#fic#" output="#cfhttp.FileContent#">--->
                    <cfset err = "oui">
                </cfcatch>
            </cftry>
            <cfif #err# eq "oui">
                <cfhttp method="get" url="#Attributes.url##Attributes.Api#?numeroAllocataire=#NumallocataireCorrige#&codePostal=#Attributes.codePostal#">
                    <cfhttpparam type="header" name="accept" value="application/json">
                    <cfhttpparam type="header" name="X-API-Key" value="#Attributes.XAPIKey#">
                </cfhttp>
                <!---<cfset nfic = createuuid()>
                <cfdump var="#cfhttp#" output="c:\tempweb\particulierjson#nfic#.txt">--->
                <cfset err = "non">
                <cftry>
                    <cfset RetourJson = DeserializeJSON(#cfhttp.FileContent#,false)>
                    <cfcatch>
                        <cfset RetourJson.message = "La plateforme Api particulier ne répond pas, merci de ressayer plus tard.">
                        <!---<cfset fic = "Retour_API_PART_" & createuuid() & ".txt">
                        <cffile action="WRITE" file="c:\tempweb\#fic#" output="#cfhttp.FileContent#">--->
                        <cfset err = "oui">
                    </cfcatch>
                </cftry>                    
            </cfif>
            <cfset r = queryaddrow(Retour)>
			<cfif #err# eq "oui">
                <cfset r = querysetcell(Retour,"message",#RetourJson.message#)>
            <cfelse>
            <!---<cfif isdefined("RetourJson.reason") or isdefined("RetourJson.message") or isdefined("RetourJson.error")>--->
                <cfif isdefined("RetourJson.reason")>
					<cfset r = querysetcell(Retour,"reason",#RetourJson.reason#)>
                </cfif>
                <cfif isdefined("RetourJson.message")>
					<cfset r = querysetcell(Retour,"message",#RetourJson.message#)>
                </cfif>
                <cfif isdefined("RetourJson.error")>
					<cfset r = querysetcell(Retour,"error",#RetourJson.error#)>
            	</cfif>
            <!---<cfelse>--->
            	<!--- traitement des allocataires (tous sur 1 enreg ) --->
				<cfset parent = querynew("id,nomPrenom,dateDeNaissance,sexe")>
                <cfif isdefined("RetourJson.allocataires")>
                    <cfloop from="1" to="#ArrayLen(RetourJson.allocataires)#" index="idx">
                        <cfset arr = ArraySlice(RetourJson.allocataires,#idx#,1)>
                        <!---<cfdump var="#arr#">--->
                        <cfscript>
                            queryaddrow(parent);
                            querysetcell(parent,"id",#idx#);
                            ArrayEach(arr,function(dt){
                                querysetcell(parent,"dateDeNaissance",dt.dateDeNaissance);
                                querysetcell(parent,"nomPrenom",dt.nomPrenom);
                                querysetcell(parent,"sexe",dt.sexe);			
                            }
                        );
                        </cfscript>
                    </cfloop>            
				</cfif>
            	<!--- traitement des enfants (génere plusieur enreg --->
            	<cfset enfants = querynew("id,nomPrenom,dateDeNaissance,sexe")>
         		<cfif isdefined("RetourJson.enfants")>
                    <cfloop from="1" to="#ArrayLen(RetourJson.enfants)#" index="idx">
                        <cfset arr = ArraySlice(RetourJson.enfants,#idx#,1)>
                        <!---<cfdump var="#arr#">--->
                        <cfscript>
                            queryaddrow(enfants);
                            querysetcell(enfants,"id",#idx#);
                            ArrayEach(arr,function(dt){
                                querysetcell(enfants,"dateDeNaissance",dt.dateDeNaissance);
                                querysetcell(enfants,"nomPrenom",dt.nomPrenom);
                                querysetcell(enfants,"sexe",dt.sexe);			
                            }
                        );
                        </cfscript>
                    </cfloop> 
				</cfif>
                <cfset cptrowenf = 0>    
            	<cfloop query="enfants">
                    <cfif #cptrowenf# gt 0>
            		    <cfset r = queryaddrow(Retour)>
            		</cfif>
                    <cfset r = querysetcell(Retour,"reason","")>
                	<cfset r = querysetcell(Retour,"message","")>
                	<cfset r = querysetcell(Retour,"error","OK")>
            		<cfif isdefined("RetourJson.adresse")>
                    	<cfif isdefined("RetourJson.adresse.identite")>
							<cfset r = querysetcell(Retour,"adresse_identite",#RetourJson.adresse.identite#)>
                    	</cfif>
                        <cfif isdefined("RetourJson.adresse.numeroRue")>
							<cfset r = querysetcell(Retour,"adresse_numeroRue",#RetourJson.adresse.numeroRue#)>
            			</cfif>
                        <cfif isdefined("RetourJson.adresse.complementIdentiteGeo")>
							<cfset r = querysetcell(Retour,"adresse_complementIdentiteGeo",#RetourJson.adresse.complementIdentiteGeo#)>
                    	</cfif>
                        <cfif isdefined("RetourJson.adresse.codePostalVille")>
							<cfset r = querysetcell(Retour,"adresse_codePostalVille",#RetourJson.adresse.codePostalVille#)>
                    	</cfif>
                        <cfif isdefined("RetourJson.adresse.pays")>
							<cfset r = querysetcell(Retour,"adresse_pays",#RetourJson.adresse.pays#)>
                    	</cfif>
                    </cfif>
					<cfset cpt = 0>
                    <cfloop query="parent">
                    	<cfset cpt = #cpt# + 1>
                        <cfif #cpt# eq 1>
                        	<cfset r = querysetcell(Retour,"allocataires1_nomPrenom",#parent.nomPrenom#)>
                            <cfset r = querysetcell(Retour,"allocataires1_dateDeNaissance",#parent.dateDeNaissance#)>
                            <cfset r = querysetcell(Retour,"allocataires1_sexe",#parent.sexe#)>
                        <cfelse>
                        	<cfset r = querysetcell(Retour,"allocataires2_nomPrenom",#parent.nomPrenom#)>
                            <cfset r = querysetcell(Retour,"allocataires2_dateDeNaissance",#parent.dateDeNaissance#)>
                            <cfset r = querysetcell(Retour,"allocataires2_sexe",#parent.sexe#)>
                        </cfif>
                    </cfloop>
                    <cfif isdefined("RetourJson.annee")>
            			<cfset r = querysetcell(Retour,"annee",#RetourJson.annee#)>
                	<cfelse>
                    	<cfset r = querysetcell(Retour,"annee","")>
                    </cfif>
					<cfset r = querysetcell(Retour,"enfants_nomPrenom",#enfants.nomPrenom#)>
                    <cfset r = querysetcell(Retour,"enfants_dateDeNaissance",#enfants.dateDeNaissance#)>
                    <cfset r = querysetcell(Retour,"enfants_sexe",#enfants.sexe#)>
                	<cfif isdefined("RetourJson.mois")>
						<cfif len(#RetourJson.mois#) eq 1>
                            <cfset lemois = "0" & #RetourJson.mois#>
                        <cfelse>
                            <cfset lemois = #RetourJson.mois#>
                        </cfif>
					<cfelse>
                    	<cfset lemois = "">
                    </cfif>
					<cfset r = querysetcell(Retour,"mois",#lemois#)>
                    <cfif isdefined("RetourJson.quotientFamilial")>
						<cfset r = querysetcell(Retour,"quotientFamilial",#RetourJson.quotientFamilial#)>
                	<cfelse>
                    	<cfset r = querysetcell(Retour,"quotientFamilial","")>
                    </cfif>
                    <cfset cptrowenf = #cptrowenf# + 1>
                </cfloop>
                <cfif #enfants.recordcount# eq 0>
                    <cfset cpt = 0>
                    <cfloop query="parent">
                    	<cfset cpt = #cpt# + 1>
                        <cfif #cpt# eq 1>
                        	<cfset r = querysetcell(Retour,"allocataires1_nomPrenom",#parent.nomPrenom#)>
                            <cfset r = querysetcell(Retour,"allocataires1_dateDeNaissance",#parent.dateDeNaissance#)>
                            <cfset r = querysetcell(Retour,"allocataires1_sexe",#parent.sexe#)>
                        <cfelse>
                        	<cfset r = querysetcell(Retour,"allocataires2_nomPrenom",#parent.nomPrenom#)>
                            <cfset r = querysetcell(Retour,"allocataires2_dateDeNaissance",#parent.dateDeNaissance#)>
                            <cfset r = querysetcell(Retour,"allocataires2_sexe",#parent.sexe#)>
                        </cfif>
                    </cfloop>
                    <cfif isdefined("RetourJson.annee")>
            			<cfset r = querysetcell(Retour,"annee",#RetourJson.annee#)>
                	<cfelse>
                    	<cfset r = querysetcell(Retour,"annee","")>
                    </cfif>
                    <cfif isdefined("RetourJson.mois")>
						<cfif len(#RetourJson.mois#) eq 1>
                            <cfset lemois = "0" & #RetourJson.mois#>
                        <cfelse>
                            <cfset lemois = #RetourJson.mois#>
                        </cfif>
					<cfelse>
                    	<cfset lemois = "">
                    </cfif>
					<cfset r = querysetcell(Retour,"mois",#lemois#)>
                    <cfif isdefined("RetourJson.quotientFamilial")>
						<cfset r = querysetcell(Retour,"quotientFamilial",#RetourJson.quotientFamilial#)>
                	<cfelse>
                    	<cfset r = querysetcell(Retour,"quotientFamilial","")>
                    </cfif>    
                </cfif>
            </cfif>
            <cfset r = querysetcell(Retour,"httpr",#cfhttp.FileContent#)>
        </cfcase>
    
    	<cfdefaultcase></cfdefaultcase>
	</cfswitch>
</cfif>
<cfset Caller.cf_Api_particulier.Retour = #Retour#>