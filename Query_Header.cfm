﻿<cfset Module = createobject("component","module")><!--- declaration de l'objet module --->
<cfset ModuleCDF = createobject("component","commun.ModuleCDF")>
<cfparam name="url.id_mnu" default="0">
<cfparam name="url.id_Smnu" default="0">
<cfparam name="url.log" default="">
<cfparam name="url.id_inscription" default="">
<cfparam name="url.id_prestation" default="">
<cfparam name="url.id_prestation1" default="">
<cfparam name="url.id_enfant" default="">
<cfparam name="url.NumEnfant" default="">
<cfparam name="url.Numprestation" default="">
<cfparam name="url.NUMLIEUPRESTATION" default="0">
<cfparam name="url.msg" default="">
<cfparam name="url.msg1" default="">
<cfparam name="url.msg2" default="">
<cfparam name="url.numjour" default="">
<cfparam name="url.datejour" default="">
<cfparam name="url.action" default="">
<cfparam name="url.nature" default="">
<cfparam name="url.id_message" default="">
<cfparam name="url.NumClasse" default="">
<cfparam name="url.pointage" default="">
<cfparam name="url.id_presentabsent" default="">
<cfparam name="url.txtaction" default="">
<cfparam name="url.id_fact" default="">
<cfparam name="url.prod_fact" default="">
<cfparam name="url.lst_classe_sel" default="">
<cfparam name="url.datedeb" default="#lsdateformat(now(),'dd/MM/YYYY')#">
<cfparam name="url.datefin" default="#lsdateformat(now(),'dd/MM/YYYY')#">
<cfparam name="url.datedeb1" default="">
<cfparam name="url.datefin1" default="">
<cfparam name="url.recp" default="">
<cfparam name="url.id_limitation_resa_ent" default="0">
<cfparam name="url.id_client_sel" default="">
<cfparam name="url.id_enfant_sel" default="">
<cfparam name="url.id_limitation_resa_Unsel" default="">
<cfparam name="url.id_reduction" default="">
<cfparam name="url.UpldInscr" default="0">
<cfparam name="url.qte" default="0">
<cfparam name="url.typemess" default="in">
<cfparam name="url.numetablissement" default="1">
<cfparam name="url.type_doc" default="">
<cfif #url.log# eq 0><!--- si clic sur deconnection --->
<!---    <cfquery name="connect" datasource="services" maxrows="1">
        Select id,login,pw,premiereconnexion,siret,niveau from login Where id='#session.use_id#'
    </cfquery>
    <!--- log --->
    <cfquery name="lect_client" datasource="f8w_test">
        Select * from client use index(siret_email) Where siret = '#connect.siret#' and email = '#connect.login#'
    </cfquery>            
    <cfset log_action_web = "Déconnexion du site par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
    <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
    <cfquery name="log" datasource="f8w_test">
        Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#connect.siret#','#session.use_id#',
        <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtaction#','0')
    </cfquery>
--->
	<cfset session.use_id = 0>
    <cfset session.mdp = "">
</cfif>
<cfquery name="lect_user" datasource="services"><!--- lecture de l'utilisateur du site ---->
	Select * from login Where id = '#session.use_id#' and niveau=0
</cfquery><!---
<cfquery name="lect_niveau" datasource="f8w_test">
    Select lst_menu from application_menu_niveau where id=0
</cfquery>--->
<cfif #lect_user.recordcount#>
    <cfquery name="lect_niveau" datasource="f8w_test">
        Select lst_menu from application_menu_niveau use index(siret_niveau) Where siret = '#lect_user.siret#' and niveau = '#lect_user.niveau#'
    </cfquery>
    <cfif #lect_niveau.recordcount# eq 0>
        <cfquery name="lect_niveau" datasource="f8w_test">
            Select lst_menu from application_menu_niveau use index(siret_niveau) Where siret = 'defaut' and niveau = '#lect_user.niveau#'
        </cfquery>
	</cfif>

<cfelseif len(#url.recp#) eq 0>
    <cfset session.use_id = 0>
    <cfset session.mdp = "">
    <cflocation url="/">
    <cfquery name="lect_niveau" datasource="f8w_test">
        Select lst_menu from application_menu_niveau Where niveau = '-10'
    </cfquery>
</cfif>
<cfquery name="lect_menu" datasource="f8w_test"><!--- lecture des menus de l'utilisateur --->
    Select * from application_menu Where <!--- find_in_set(id,'#lect_niveau.lst_menu#') and ---> version=2 and Wurl= "parenttest.cantine-de-france.fr"
    and parent = 0 and actif = 1 and validé=1 order by ordre
</cfquery>
<cfquery name="lect_menu1" datasource="f8w_test"><!--- lecture des menus de l'utilisateur --->
    Select * from application_menu Where id='#url.id_mnu#' and version=2 and Wurl= "parenttest.cantine-de-france.fr"
</cfquery>
<cfquery name="lect_etablissement" datasource="f8w_test"><!--- lecture de l'environement (etablissement) --->
    Select * from application_client use index(siret) Where siret = '#lect_user.siret#'
</cfquery>
<cfif val(lstimeformat(now(),"HH")) lt 5><!--- pour maintenance journaliere --->
	<cfset session.use_id = 0>
</cfif>
<cfif #session.use_id# eq 0 and len(#url.recp#) eq 0 or #lect_etablissement.maintenance# eq 1>
    <cfif findnocase("id_mnu=6",#cgi.QUERY_STRING#) eq 0 and #session.mdp# neq "370321web" and findnocase("inscription_parent",#cgi.PATH_TRANSLATED#) eq 0><!--- session vide et pas sur page login, ont va a l'acceuil --->
		<cfif #lect_etablissement.maintenance# eq 1>
			<cfset msg1 = "Désolé, veuillez réessayer ulterieurement.">
        	<cfset msg1 = urlencodedformat(#msg1#,"utf-8")>
        <cfelse>
        	<cfset msg1 = "">
        </cfif>
        <cflocation url="/login/?id_mnu=6&log=0&msg1=#msg1#" addtoken="no">
        <cfquery name="lect_menu" datasource="f8w_test"><!--- session vide donc menu Me connecter --->
            Select * from application_menu Where id = 6 and version=2 and Wurl= "parenttest.cantine-de-france.fr"
        </cfquery>
    </cfif>
</cfif>
<cfquery name="lect_client" datasource="f8w_test">
	Select * from client use index(siret_NumClient) Where siret = '#lect_user.siret#' and NumClient = '#lect_user.NumClient#'
</cfquery>
<cfset dtpourreinscr = lsdateformat(now(),"YYYYMMdd")>
<cfquery name="reinscription_ent" datasource="f8w_test">
    Select * from reinscription_ent use index(id_etablissement) 
    where id_etablissement='#lect_etablissement.id#' and DateDebCampagne<=#dtpourreinscr# and DateFinCampagne>=#dtpourreinscr# 
    and active=1
</cfquery>
<cfquery name="lect_etablissement_miseadispo" datasource="f8w_test">
	select siret_miseadispo from application_client_miseadispo 
	where find_in_set('#lect_etablissement.siret#',lst_siret_dispose)
</cfquery>
<cfif #lect_etablissement_miseadispo.recordcount# neq 0>
    <cfquery name="tipisite" datasource="f8w_test">
        Select * from application_client where siret='#lect_etablissement_miseadispo.siret_miseadispo#'
    </cfquery>
<cfelse>
    <cfquery name="tipisite" datasource="f8w_test">
        Select * from application_client where siret='#lect_etablissement.siret#'
    </cfquery>
</cfif>
<cfif #lect_etablissement_miseadispo.recordcount#>
    <cfquery name="document" datasource="classactive">
        Select id,Désignation,Date_Ajout_Modif from documents use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
        where (siret='#lect_etablissement.siret#' or siret='#lect_etablissement_miseadispo.siret_miseadispo#') 
        and Id_Proprio='#lect_etablissement.id#' and CodeAppli='F8' and Type_Proprio='etablissement' 
        and Type_Doc='REGLEMENT' and Date_Ajout_Modif > '#lect_client.ValidationReglement#'
    </cfquery>
<cfelse>
    <cftry>
        <cfquery name="document" datasource="classactive">
            Select id,Désignation,Date_Ajout_Modif from documents use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
            where siret='#lect_etablissement.siret#' and Id_Proprio='#lect_etablissement.id#' and CodeAppli='F8' 
            and Type_Proprio='etablissement' 
            and Type_Doc='REGLEMENT' and Date_Ajout_Modif > '#lect_client.ValidationReglement#'
        </cfquery>
		<cfcatch>
            <cfquery name="document" datasource="classactive">
                Select id,Désignation,Date_Ajout_Modif from documents where id=0
            </cfquery>
        </cfcatch>
    </cftry>
</cfif>
<cfif #document.recordcount# and #url.id_mnu# neq 0 and #session.log_mairie# is false <!---and #session.accesdepuisgestion# is false---> >
	<cflocation url="/valdoc/?id_mnu=0" addtoken="no">
</cfif>
<cfquery name="menucantine" datasource="classactive">
    Select id,Désignation,Date_Ajout_Modif from documents use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
    where siret='#lect_etablissement.siret#' and Id_Proprio='#lect_etablissement.id#' and CodeAppli='F8'
    and Type_Proprio='etablissement'
    and Type_Doc='MENU' order by Date_Ajout_Modif DESC
</cfquery>
<cfquery name="aumoinsunenfant" datasource="f8w_test" maxrows="1">
	Select id from enfant use index(siret_numclientI) where siret='#lect_etablissement.siret#' 
    and NumClientI='#lect_client.numclient#' and parti=0 and del=0 limit 0,1
</cfquery>

<cfset Documents = ModuleCDF.DocumentDemande(#lect_etablissement.siret#,#lect_client.id#)>