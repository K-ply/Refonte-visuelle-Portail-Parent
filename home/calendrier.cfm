﻿<cfset stringMonth=monthAsString(month(date))>  
<cfset firstOfTheMonth=createDate(year(date),month(date),1)><!--représente le premier jour du mois-->
<cfset dow=dayOfWeek(firstOfTheMonth, "iso")>
<cfset pad=dow-1><!--pad correspond au jour de la semaine-2-->

<table class="table responsive-sm calendar" id="calendar">
    <tr>
        <td colspan="8" align="center">
            <h4 class="date-title">  
                <b><cfoutput>
                        <script type="text/javascript">
                            var month='#stringMonth#';
                            document.write(month.charAt().toUpperCase() + month.substring(1).toLowerCase());                
                        </script>
                    </cfoutput>&nbsp;
                </b>
            <cfoutput>#year(date)#</cfoutput>
            </h4>  
        </td>
        <tr>
            <cfloop index="i" from="2" to="8">
                <cfoutput>
                    <th>#left(dayOfWeekAsString(dayOfWeek(i)),3)#</th>
                </cfoutput>
            </cfloop>   
        </tr>
        <cfoutput>
            <tr>
        </cfoutput>

    <cfif pad gt 0>
            <cfset lastMonth = dateAdd("M", -1, #firstOfTheMonth#)>
            <cfset daysInMonth = daysInMonth(lastMonth)>
            <cfloop index="i" from= "1" to="#pad#">
                <cfset lastDay = #daysInMonth#-(#pad#-#i#)>
                <cfoutput><td class="day-clear-grey">#lastDay#</td></cfoutput>
            </cfloop>
    </cfif>

    <cfset days = daysInMonth(date)>
    <cfset counter = pad + 1>
    <cfloop index= "i" from= "1" to= "#days#">
        <cfif i is day(today) and month(date) is month(today) and year(date) is year(today)>
            <cfif counter is 6 or counter is 7>
                <cfoutput><td class="d-flex dayNow day-clear-grey"></cfoutput>
            <cfelse>
                <cfoutput><td class="d-flex dayNow"></cfoutput>
            </cfif>
        <cfelse>
            <cfif counter is 6 or counter is 7>
                <cfoutput><td class="day-clear-grey"></cfoutput>
            <cfelse>
                <cfoutput><td></cfoutput>
            </cfif>
        </cfif>
    
        <cfoutput>
            #i# </td >
        </cfoutput>
    
        <cfset counter = counter + 1>
        <cfif counter is 8 >
            <cfoutput> 
                </tr>
            </cfoutput>
            <cfif i lt days> 
                <cfset counter = 1>
                <cfoutput>
                <tr>
                </cfoutput>
            </cfif> 
        </cfif> 

        <cfif i is #days# and counter is not 8>
            <cfset nextDay = 1>
            <cfloop index="i" from="#counter#" to="7">
                <cfoutput>
                    <td class="day-clear-grey">#nextDay#</td>
                </cfoutput>
                <cfset nextDay = nextDay+1>
            </cfloop>
        </cfif>
    </cfloop>
</table>