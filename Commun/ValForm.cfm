﻿<cfparam name="Attributes.BaseDeDonnee" default="">
<cfparam name="Attributes.Table" default="">
<cfparam name="Attributes.Form" default="">
<cfparam name="Attributes.Siret" default="">
<cfparam name="Attributes.Champs" default="">
<cfparam name="Attributes.libelle" default="">
<cfset Caller.TitreMessage = "">
<cfset Caller.ContenuMessage = "">
<cfset Caller.Obligatoire = False>
<cfset Caller.Modifiable = True>
<cfif len(#Attributes.BaseDeDonnee#) eq 0>
	<cfset Caller.TitreMessage = "Attention...!">
	<cfset Caller.ContenuMessage = "Une base de donnée doit être passée en paramètres...!">
<cfelseif len(#Attributes.Table#) eq 0>
	<cfset Caller.TitreMessage = "Attention...!">
	<cfset Caller.ContenuMessage = "Une liste de table doit être passée en paramètres...!">
<cfelse>
	<cfif len(#Attributes.Champs#)><!--- ont traite un champs --->
        <cfset tableobli = #Attributes.Table# & "_obli">
        <cftry>
            <cfquery name="obli" datasource="#Attributes.BaseDeDonnee#" maxrows="1"><!--- verif table xxx_obli exist --->
                Select siret,#Attributes.Champs# as LaColonne from #tableobli# use index(siret) 
                Where (siret='#Attributes.Siret#' or siret='' or siret is Null) order by siret ASC Limit 0,1
            </cfquery>
            <cfcatch>
                <cfset Caller.TitreMessage = "Attention...!">
                <cfset Caller.ContenuMessage = "La table " & #tableobli# & " n'existe pas...!">
            </cfcatch>
        </cftry>
    	<cfloop query="obli">
			<cfif #obli.LaColonne# eq 1>
        		<cfset Caller.Obligatoire = True>
                <cfif len(#obli.siret#) eq 0>
                	<cfset Caller.Modifiable = False>
                	<cfbreak>
                </cfif>
        	</cfif>
		</cfloop>
		<cfif #Caller.Obligatoire# is true>
           	<cfoutput><font color="##FF0000"><i>* #Attributes.libelle# :&nbsp;</i></font></cfoutput>
        <cfelse>
           	<cfoutput>#Attributes.libelle# :&nbsp;</cfoutput>
        </cfif>
	<cfelseif isstruct(#Attributes.Form#)><!--- ont traite une form --->
		<cfloop list="#Attributes.Table#" delimiters="," index="LaTAble"><!--- ont boucle sur la liste des table passée en paramètre --->
			<cfset tableobli = #LaTAble# & "_obli">
            <cftry>
                <cfquery name="obli" datasource="#Attributes.BaseDeDonnee#" maxrows="1"><!--- verif table xxx_obli exist --->
                    Select * from #tableobli# use index(siret) 
                    Where (siret='#Attributes.Siret#' or siret='' or siret is Null) order by siret DESC Limit 0,1
                </cfquery>
                <cfcatch>
                    <cfset Caller.TitreMessage = "Attention...!">
                    <cfset Caller.ContenuMessage = "La table " & #tableobli# & " n'existe pas...!">
                    <cfbreak>
                </cfcatch>
            </cftry>
            <cfif len(#Caller.TitreMessage#) eq 0>	
        		<cfdbinfo datasource="#Attributes.BaseDeDonnee#" name="StructTable" type="columns" table="#tableobli#"></cfdbinfo>
        		<cfloop list="#Attributes.Form.FIELDNAMES#" delimiters="," index="LeChamps"><!--- boucle sur les champs de la form --->
                	<cfset err = "non">
                    <cftry>
                        <cfquery name="oblioupas" dbtype="query">
                            Select #LeChamps# as col from obli
                        </cfquery>
                        <cfcatch>
                            <!--- ont est pas sur le bon couple table / champs --->
                            <cfset err = "oui">
                        </cfcatch>
                    </cftry>
                    <cfif #err# eq "non">
                    	<cfset LeChamps = lcase(#LeChamps#)>
                    	<cfquery name="StructChamps" dbtype="query">
                        	Select * from StructTable Where lower(COLUMN_NAME)='#LeChamps#'
                        </cfquery>
                        <cfset aResults = StructFind(#Attributes.Form#, #LeChamps#)>
                        <cfif #oblioupas.col# eq 1><!--- obligatoire ou pas --->
                            <cfset Caller.Obligatoire = True>
							<cfif #aResults# eq "">
                                <cfset Caller.TitreMessage = "Attention...!">
                                <cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & "...!">
                                <cfbreak>
                            </cfif>
                        </cfif>
                        <cfif len(#Caller.TitreMessage#) eq 0 and #aResults# neq ""><!--- validation donnée saisie --->
                        	<cfif findnocase("char",#StructChamps.type_name#)><!--- char ou varchar --->
                            	<cfif findnocase("date",#LeChamps#)><!--- date --->
                                	<cfif len(#aResults#) neq 10 or findnocase("/",#aResults#) neq 3 or findnocase("/",#aResults#,4) neq 6>
                                    	<cfset Caller.TitreMessage = "Attention...!">
                                		<cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & " au format JJ/MM/AAAA...!">
                                		<cfbreak>
                                	<cfelse>
                                    	<cfset err = "non">
                                        <cftry>
                                            <cfset ladate = createdate(#mid(aResults,7,4)#,#mid(aResults,4,2)#,#mid(aResults,1,2)#)>
                                            <cfcatch><cfset err = "oui"></cfcatch>
                                        </cftry>
                                        <cfif #err# eq "oui">
                                            <cfset Caller.TitreMessage = "Attention...!">
                                            <cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & " au format JJ/MM/AAAA...!">
                                            <cfbreak>
                                        </cfif>
                                    </cfif>
                                </cfif>
                            	<cfif len(#Caller.TitreMessage#) eq 0 and findnocase("tel",#LeChamps#)><!--- date --->
                            		<cfif len(#aResults#) neq 10>
                                    	<cfset Caller.TitreMessage = "Attention...!">
                                		<cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & " au format 9999999999...!">
                                		<cfbreak>
                                    </cfif>
                                    <cfif len(#Caller.TitreMessage#) eq 0>
                                    	<cfloop from="1" to="10" index="idx">
                                    		<cfif isnumeric(#idx#) is false>	
                                    			<cfset Caller.TitreMessage = "Attention...!">
                                				<cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & " au format 9999999999...!">
                                    			<cfbreak>
                                            </cfif>
                                    	</cfloop>
                                        <cfif len(#Caller.TitreMessage#) neq 0>
                                        	<cfbreak>
                                        </cfif>
                                    </cfif>
                            	</cfif>
                                <cfif len(#Caller.TitreMessage#) eq 0 and findnocase("email",#LeChamps#)><!--- email --->
                                	<cfset arobase = findnocase("@",#aResults#)>
                                	<cfif #arobase# eq 0>
                                    	<cfset Caller.TitreMessage = "Attention...!">
                                		<cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & " au format abcd@abcd.ab ...!">
                                    	<cfbreak>
                                    <cfelse>
                                    	<cfset point = findnocase(".",#aResults#,#arobase#)>
                                        <cfif #point# eq 0>
                                    		<cfset Caller.TitreMessage = "Attention...!">
                                			<cfset Caller.ContenuMessage = "Merci de saisir " & #StructChamps.REMARKS# & " au format abcd@abcd.ab ...!">
                                    		<cfbreak>
                                        </cfif>
                                    </cfif>
                                </cfif>
                            </cfif>
                        </cfif>
                    </cfif>
                </cfloop>
        	</cfif>
        </cfloop>
    <cfelse>
    	<cfset Caller.TitreMessage = "Attention...!">
		<cfset Caller.ContenuMessage = "Une form ou un champs doivent être passés en paramêtres...!">
    </cfif>
</cfif>