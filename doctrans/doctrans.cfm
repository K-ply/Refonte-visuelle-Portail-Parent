﻿<cfparam name="msg" default="">
<cfif isdefined("form.numenfant")>
	<cfset nompdf = createuuid() & ".pdf">
    <cfset chemin = "C:\TempWeb\">
    <cftry>
    	<cffile action="upload" filefield="fichier" destination="#chemin##nompdf#">
    	<cfcatch>
        	<cfset msg = "Impossible de transmettre le fichier !!">
        </cfcatch>
    </cftry>
    <cfif #msg# eq "">
        <cfset extention = #cffile.serverFileExt#>
        <cfquery name="typedoc" datasource="classactive">
        	Select * from type_doc use index(type_doc_CodeAppli) where type_doc='#form.type_doc#' and CodeAppli='F8'
        </cfquery>  
    	<cfif #form.numenfant# gt 0>
        	<cfquery name="enfant" datasource="f8w_test">
            	Select id,nom,prénom from enfant use index(siret_numenfant) where siret='#lect_client.siret#' 
                and numenfant='#form.numenfant#'
            </cfquery>
            <cfset idproprio = #enfant.id#>
            <cfset libdoc = #typedoc.désignation# & " (" & #enfant.prénom# & " " & #enfant.nom# & ")">
        <cfelse>
        	<cfset idproprio = #lect_client.id#>
            <cfset libdoc = #typedoc.désignation#>
        </cfif>
        <cfquery name="docencour" dbtype="query">
        	select * from Documents where type_doc='#form.type_doc#'
        </cfquery>
        <cfif #docencour.validation# eq 1>
        	<cfset valide = 0>
        <cfelse>
        	<cfset valide = 1>
        </cfif>
        <cftry>
        	<cffile variable="filefichier" action="readbinary" file="#chemin##nompdf#">
        	<cfcatch>
        		<cfset msg = "Impossible de transmettre le fichier !!">
        	</cfcatch>
		</cftry>
		<cfif #msg# eq "">
        	<cftry>
				<cfset b = tobase64(#filefichier#)>
            	<cfcatch>
        			<cfset msg = "Impossible de transmettre le fichier !!">
        		</cfcatch>
            </cftry>
            <cfif #msg# eq "">
            	<cftry>
                    <cfquery name="add" datasource="classactive">
                        Insert into documents(siret,Id_Proprio,CodeAppli,Type_Proprio,Type_Doc,Table_origine,
                        Id_Table_Origine,Désignation,Document,Recherche,extention,Validé) values('#lect_client.siret#',
                        '#idproprio#','F8','#typedoc.type_proprio#','#form.type_doc#','','0',
                        <cfqueryparam value="#libdoc#" cfsqltype="cf_sql_varchar">,
                        <cfqueryparam value="#b#" cfsqltype="cf_sql_longvarchar">,'','#extention#','#valide#')
                    </cfquery>
					<cfcatch>
        				<cfset msg = "Impossible de transmettre le fichier !!">
        			</cfcatch>
            	</cftry>
			</cfif>
            <cfif #msg# eq "">
				<!--- l'envoi  de mail d'alerte à gestionnaire se fera via robot--->
                <cfset url.type_doc = "">
                <cfset url.numenfant = "">
                <cfset Documents = ModuleCDF.DocumentDemande(#lect_etablissement.siret#,#lect_client.id#)>
                <cfset msg = "Votre document a bien été transmis">
			</cfif>
		</cfif>
    </cfif>
</cfif>

<cfquery name="docObli" dbtype="query">
    <!---Select * from Documents where (Obligatoire=1 or bloqueresa=1 or bloqueachat=1) and statut<>'Validé'--->
    Select * from Documents where (Obligatoire=1 or bloqueresa=1 or bloqueachat=1) and statut='A transmettre'
</cfquery>
<div class="row d-flex justify-content-center mt-5 content">
    <div class="p-2 p-lg-5 bg-white section scroll mobile-no-scroll">
        <div class="mb-5 text-md-center">
            <p>Vous pouvez transmettre votre document au format pdf (ou jpg pour les photos) ci-dessous en sélectionnant le document concerné puis en cliquant sur parcourir pour le récupérer et enfin sur transmettre le fichier :</p>
        </div>
        <cfif #docObli.recordcount# gt 0>
            <p class="text-red bold text-md-center">Vous avez un ou plusieur document OBLIGATOIRE à transmettre !!</p>
        </cfif>

        <cfform class="pb-4" name="docdemande" enctype="multipart/form-data">
            <table class="table w-100 text-grey blue-select">
                <tr class="d-flex justify-content-center align-items-center">
                    <td class="border-none">
                        <label for="type_doc1">Sélectionnez le document </label>
                    </td>
                    
                    <td class="border-none">


                        <cfselect name="type_doc1" id="type_doc1" class="dropdown-border text-grey" onchange="MM_jumpMenu('self',this,0)" required="yes" message="Veuillez choisir un document a transmettre">
                            <cfif #Documents.recordcount# eq 0>
                                <option class="dropdown-item text-grey" value="">Aucune demande de document</option>
                            <cfelse>
                                <option class="dropdown-item text-grey" value="/doctrans/?id_mnu=#url.id_mnu#"></option>
                            </cfif>
                            <cfoutput query="Documents">
                                <cfif #Documents.statut# eq "A transmettre" or #Documents.type_doc# eq "JUSTABS">
                                    <cfif #Documents.statut# eq "A transmettre" and #Documents.type_doc# neq "JUSTABS">
                                        <cfset des = #documents.désignation# & " demandé le " & lsdateformat(#documents.date#,"dd/MM/YYYY")>
                                    <cfelseif #Documents.type_doc# eq "JUSTABS">
                                        <cfset des = #documents.désignation#>
                                    <cfelse>
                                        <cfset des = #documents.désignation# & " (" & #Documents.statut# & ")">
                                    </cfif>
                                    <cfif #url.type_doc# eq #Documents.type_doc#>
                                        <cfif #Documents.numenfant# eq 0>
                                            <option class="dropdown-item text-grey" selected value="/doctrans/?id_mnu=#url.id_mnu#&type_doc=#Documents.type_doc#&numenfant=#Documents.numenfant#">#des#</option>
                                        <cfelse>
                                            <cfif #Documents.numenfant# eq #url.numenfant#>
                                                <option class="dropdown-item text-grey" selected value="/doctrans/?id_mnu=#url.id_mnu#&type_doc=#Documents.type_doc#&numenfant=#Documents.numenfant#">#des#</option>
                                            <cfelse>
                                                <option class="dropdown-item text-grey" value="/doctrans/?id_mnu=#url.id_mnu#&type_doc=#Documents.type_doc#&numenfant=#Documents.numenfant#">#des#</option>
                                            </cfif>
                                        </cfif>
                                    <cfelse>
                                        <option class="dropdown-item text-grey" value="/doctrans/?id_mnu=#url.id_mnu#&type_doc=#Documents.type_doc#&numenfant=#Documents.numenfant#">#des#</option>
                                    </cfif>
                                </cfif>
                            </cfoutput>
                        </cfselect>
                    </td>
                </tr>
                <cfif len(#url.type_doc#)>
                    <tr class="d-flex justify-content-center">
                        <td class="border-none">
                            <label for="document_trans">Sélectionnez un fichier à transmettre </label>
                        </td>
                        <td class="border-none">
                            <cfinput required="yes" message="Sélectionnez un pdf ou jpg svp" type="file" name="fichier" accept="image/*;capture=camera">
                        </td>
                    </tr>

                    <tr class="d-flex justify-content-center">
                        <td class="border-none">
                            <cfinput type="hidden" name="numenfant" value="#url.numenfant#">
                            <cfinput class="btn btn-primary" name="pubdoc" type="submit" value="Transmettre le fichier">
                        </td>
                    </tr>
                </cfif>
            </table>
            <cfinput type="hidden" name="type_doc" value="#url.type_doc#">
        </cfform>

        <div class="text-grey">
            <cfif len(#msg#)>
                <div class="p-margin-none">
                    <p><cfoutput>#msg#</cfoutput></p>
                </div>
            </cfif>	
            <div class="p-margin-none pb-4">
                <p class="bold pb-2">Ou le communiquer à :</p>
                <cfoutput>
                    <p>#lect_etablissement.nome#</p>
                    <p>#lect_etablissement.adr1e#</p>
                    <p>#lect_etablissement.adr2e#</p>
                    <p>#lect_etablissement.cpe# #lect_etablissement.villee#</p>
                    <p>#lect_etablissement.tel#</p>
                    <p><a class="link-grey" href="mailto:#lect_etablissement.EmailE#">#lect_etablissement.EmailE#</a></p>
                </cfoutput>
            </div>
            <div class="p-margin-none">
                <p class="bold pb-2">Vos documents :</p>
                <cfset err = "non">

                <cftry>
                    <cfquery name="docaff" dbtype="query">
                        Select * from Documents where Documents.id_doc<>'0' order by id_doc desc
                    </cfquery>                         
                    <cfcatch>
                        <cfset err = "oui">
                    </cfcatch>
                </cftry>
                <cfif #err# eq "non">
                    <cfoutput query="docaff">
                        <p><a class="link-grey" href="/commun/VisuPdf.cfm?id=#docaff.id_doc#" target="_blank">#docaff.désignation#</a>
                        <span class="text-small"> - #docaff.statut# #lsdateformat(docaff.Date,"dd/MM/YYYY")# #lstimeformat(docaff.Date,"HH:MM:SS")#</span></p>
                    </cfoutput>
                </cfif>
                <cfquery name="document" datasource="classactive">
                    Select id,Désignation from documents 
                    use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
                    where siret='#lect_etablissement.siret#' 
                    and Id_Proprio='#lect_etablissement.id#' 
                    and CodeAppli='F8' and Type_Proprio='etablissement' 
                    and Type_Doc='REGLEMENT'
                </cfquery>    
                <cfoutput query="document">
                    <p><a class="link-grey" href="/commun/VisuPdf.cfm?id=#document.id#" target="_blank">#document.désignation#</a></p>
                </cfoutput>
            </div>
        </div>
    </div>
</div>