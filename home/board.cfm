﻿<cfset msg = "">
<cfset msg1 = "">
<!---<cfquery name="fin_annee" datasource="f8w_test">
    Select * from fin_annee_ent Where Active=1 order by id desc limit 0,1
</cfquery>
<cfquery name="verifenfantpresent" datasource="f8w_test">
    Select * from enfant use index(siret_numclientI) Where siret='#lect_client.siret#' 
    and numclientI='#lect_client.numclient#' and parti=0
</cfquery>
<cfif (#fin_annee.id# eq #lect_client.id_fin_annee_ent# and #lect_client.parti# eq 1) or #verifenfantpresent.recordcount# eq 0><!--- procedure fin d'année --->
	<cflocation url="/reinscription/" addtoken="no">
</cfif>
--->

<cfquery name="prestation" datasource="f8w_test"><!--- lecture prestation --->
    Select * from prestation use index(siret_NumPrestation) Where siret = '#lect_user.siret#' and TopWeb=1 and del=0 Order by ordre
</cfquery>       

<!---cfquery name="lect_enfant" datasource="f8w_test_test"><!--- lecture enfant --->
	Select id,NumEnfant,Prénom,sexe,concat_ws(" ",Prénom,nom) as lenom from enfant use index(siret_NumClientI) 
	Where siret = '#lect_user.siret#' and NumClientI = '#lect_client.Numclient#' and parti=0 and del=0 Order by Prénom
</cfquery--->
                                               
<cfset dtnowstamp = lsdateformat(now(),"YYYYMMdd")>

<cfquery name="lect_enfant" datasource="f8w_test">
    Select * from enfant use index(siret_numclientI) 
    where siret='#lect_etablissement.siret#' and numclientI='#lect_client.numclient#' 
    and parti=0 and del=0 order by nom,prénom
</cfquery>



<!--- A implémenter dans un blocs infos --->


<cfif #reinscription_ent.recordcount# gt 0>
    <cfquery name="Newreinscription" datasource="f8w_test" maxrows="1">
        Select * from reinscription use index(id_reinscription_ent_id_client) 
        Where id_reinscription_ent='#reinscription_ent.id#' and id_client='#lect_client.id#'
    </cfquery>
    <cfif #Newreinscription.recordcount# eq 0>
        <cfset ann = year(now())>
          <cfset annplus = #ann# + 1>     
        <cfset dateclot = mid(#reinscription_ent.DateRazCliEnfInscr#,7,2) & "/" & mid(#reinscription_ent.DateRazCliEnfInscr#,5,2) & "/" & mid(#reinscription_ent.DateRazCliEnfInscr#,1,4)>                       
          <tr bgcolor="#C6D580">
            <td colspan="5" align="center">&nbsp;</td>
          </tr>
          <tr bgcolor="#C6D580">
            <td colspan="5" align="center"><font color="#FF0000"><b>Pensez à gérer la rentée <cfoutput>#ann# - #annplus#</cfoutput> ! <i>(Menu Rentrée scolaire)</i></b></font></td>
          </tr>
          <tr bgcolor="#C6D580">
            <td colspan="5" align="center"><font color="#FF0000">Sans action de votre part, vos enfant seront désinscrit à compter du <cfoutput>#dateclot#</cfoutput> ...!</font></td>
          </tr>
          <tr bgcolor="#C6D580">
            <td colspan="5" align="center">&nbsp;</td>
          </tr>
    </cfif>
</cfif>               



<div class="row d-flex flex-wrap justify-content-center justify-content-lg-around mt-4 mt-lg-5 content scroll">
    <cfquery name="message" datasource="f8w_test">
        Select id,Objet,Lu,DateMessage from message use index(id_client_Lu) 
        Where id_client='#lect_client.id#' and Lu=0 order by DateMessage DESC limit 10
    </cfquery>
    <cfif #message.recordcount#>
        <div class="col-9 order-1 order-lg-1 col-lg-5 p-3 bg-white section mb-4 scroll">
            <p class="bold text-grey">Nouveaux messages</p>
            <ul>
                <cfoutput query="message">
                    <li class="text-grey pb-2">#lsdateformat(DateMessage,"dd/MM/YYYY")# <a class="link-blue" href="../message1/?id_mnu=148&id_message=#id#">#Objet#</a></li>
                </cfoutput>
            </ul>
        </div>
    </cfif>
    <div class="scroll col-9 order-1 order-lg-1 col-lg-5 p-3 bg-white section mb-4">
        <div class"recap-header col-9 order-1 order-lg-1 col-lg-5 p-3 bg-white section mb-4">
            <p class="bold text-grey d-flex align-items-center">
                <img src="../img/icon/icon_bell_grey.png" alt="icon de cloche" class="pe-2"><span>Vos réservations &agrave; venir</span>
            </p>
            <cfform name="choixenfantrecap" method="post" class="blue-select">
                <div class="bloc-recap-home col-lg-10 ">
                    <cfselect name="NumEnfanta" onChange="MM_jumpMenu('self',this,0)">
                        <option class="dropdown-item" value=""></option>
                        <cfoutput query="lect_enfant">
                            <cfif #lect_enfant.id# eq #url.id_enfant#>
                                <option class="dropdown-item" title="#lect_enfant.NumEnfant#" selected value="?id_mnu=#url.id_mnu#&id_prestation=#url.id_prestation#&id_enfant=#lect_enfant.id#">#lect_enfant.prénom# #lect_enfant.nom#</option>
                            <cfelse>
                                <option class="dropdown-item" title="#lect_enfant.NumEnfant#" value="?id_mnu=#url.id_mnu#&id_prestation=#url.id_prestation#&id_enfant=#lect_enfant.id#">#lect_enfant.prénom# #lect_enfant.nom#</option>   
                            </cfif>
                        </cfoutput>
                    </cfselect>
                       
                    <cfif #url.id_enfant# neq 0>
                        <cfquery name="enfant" datasource="f8w_test">
                            Select * from enfant where id='#url.id_enfant#' and numclienti='#lect_user.numclient#'
                        </cfquery>
                        <cfif #enfant.recordcount#>
                            <cfif #enfant.sexe #eq "F">
                                <cfset sexenfant = "e">
                            <cfelse>
                                <cfset sexenfant = "">
                            </cfif>
                           
                            
                            <cfloop query="prestation"><!--- boucle sur prestation --->
                                <cfif findnocase("cantine",#prestation.désignation#)>
                                    <cfset iconpresta = "icon_cantine_blue.png">
                                <cfelseif findnocase("garderie",#prestation.désignation#)>
                                    <cfset iconpresta = "icon_children_safe.png">
                                <cfelse>
                                    <cfset iconpresta = "icon_loisir_blue.png">
                                </cfif>
                                <cfset datealister = now()>
                                <cfset Enreg_Tableau = ''>
                                <cfset Enreg_TableauPonctu = "">
                            
    
                                <cfloop from="1" to="15" index="numjour">
                                    
                                   
                                    <cfset a = Module.resaponctuelle1(#datealister#,#lect_user.siret#,#prestation.numprestation#,#enfant.numenfant#,#prestation.JoursExclus#,#prestation.DélaiRésaWebJ#,#prestation.DélaiRésaWebH#,#prestation.id#,#enfant.id#)>
                                    <cfif #a.bg# neq "##FF0000"<!--- rouge ---> and #a.bg# neq "##666666"<!--- gris ---> and #a.bg# neq "##FFFFFF"<!--- blanc ---> >
                                    <!--- inscrit a la presta --->    
                                    <cfset iconToggle="icon_toggle_on.png">
                                        <cfif len(#Enreg_TableauPonctu#) eq 0>
                                          
                                            <!---<cfset Enreg_Tableau = #Enreg_Tableau# & "&nbsp;&nbsp;&nbsp;&nbsp;- est inscrit#sexenfant# :<br>">--->
                                            <cfset Enreg_TableauPonctu = "Dans les 15 jours qui viennent, " & #enfant.Prénom# & " sera présent#sexenfant#: <br>le " & #lsdateformat(datealister,"dddd")# & " " & #lsdateformat(datealister,"dd")# & " " & #lsdateformat(datealister,"mmmm")# & " ,">
                                        <cfelse>
                                            <cfset Enreg_TableauPonctu = #Enreg_TableauPonctu# & " le " & #lsdateformat(datealister,"dddd")# & " " & #lsdateformat(datealister,"dd")# & " " & #lsdateformat(datealister,"mmmm")# & " ,">
                                            
                                        </cfif>
                                    </cfif>
                                    <cfset datealister = DateAdd("d",1,#datealister#)>
                                </cfloop>
                                <cfif len(#Enreg_TableauPonctu#)>
                                  
                                    <cfset Enreg_Tableau = #Enreg_Tableau# & #Enreg_TableauPonctu# & "</i>">
                                
                                <cfelse>
                                    <cfset iconToggle="icon_toggle_off.png">
                                    <cfset Enreg_Tableau = #Enreg_Tableau# & "#enfant.Prénom# n'est pas inscrit#sexenfant# dans les 15 prochains jours.">
                                </cfif>
                                <p class="separation-bar"></p>    
                                                         
                                <span class="title-recap-home text-grey"><cfoutput>#prestation.Désignation#</cfoutput></span>
                                <div class="bloc-recap-toggle">   
                                    <img class="img-recap" src="..\img\icon\<cfoutput>#iconpresta#</cfoutput>">
                                    <span class="p-recap-home text-grey p-3">
                                    <cfoutput>#Enreg_Tableau#</cfoutput></span>
                                    <!---button type="hidden" onclick ="window.location.href = 'http://parenttest.cantine-de-france.fr/inscriptions/?id_mnu=147'" class="toggle" id="toggle" --->
                                        <a href="http://parenttest.cantine-de-france.fr/inscriptions/?id_mnu=147"> <img class="img-recap" width="80%" src="..\img\icon\<cfoutput>#iconToggle#</cfoutput>"></a>
                                </div>
                            
                            </cfloop> 
                        </cfif>
                    </cfif>        
                </div>           
            </cfform> 
        </div>
    </div>
</div>
    <!---
    <div class="col-9 order-2 order-lg-1 col-lg-3 bg-white d-flex flex-column pb-4">
        <div class="section-center mb-4">
            <p class="bold text-grey"><span class="text-blue">2 enfants</span> inscrits</p>
            <a href="/inscriptions/?id_mnu=15" class="link-blue">Inscrire un nouvel enfant</a>
        </div>
        <div class="section-center mb-4">
            <p class="bold text-grey">Solde actuel: <span class="text-blue">100,00&euro;</span></p>
            <a href="/achats/?id_mnu=122&init=1" class="link-blue">Recharger</a>
        </div>
        <div class="section-center mb-4">
            <p class="bold text-grey"><span class="text-blue">1 facture</span> disponible</p>
            <a href="/facture/cbtipi/?id_mnu=12" class="link-blue">Voir les factures</a>
        </div>
    --->
  