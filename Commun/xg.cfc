﻿<cfcomponent>
<!--- '***************************************************************     --->
	<cffunction name="getstatuszip" access="remote">
        <cfset str = StructNew()>
        <cfset str.message = #session.messageprogressbar#>
        <cfif NOT IsDefined("session.STATUS")>
            <cfset session.STATUS = 0.01>
            <cfscript>
                Sleep(5);
            </cfscript>
        <!---<cfelseif session.STATUS LTE 0.99>--->
        <cfelseif session.STATUS LTE 0.99>
            <!---<cfset session.STATUS=session.STATUS + .01>--->
            <cfscript>
                Sleep(5);
            </cfscript>
        <cfelse>
            <cfset str.message = "Zip produit avec succès...">
            <cfset session.STATUS="1.0">
        </cfif>
        <cfset str.status = session.STATUS>
        <cfreturn str>
    </cffunction>
    <cffunction name="limitation" access="public" returntype="any">
    	<cfargument name="id_enfant" type="numeric" required="yes">
        <cfargument name="id_prestation" type="numeric" required="yes">
        <cfargument name="datejour" type="string" required="yes">
    	<cfset Msglimitation = "">
		<cfquery name="verif_verification" datasource="f8w"><!--- des limitation pour cet enfant sur cette prestation ??? --->
			Select id,id_ent from limitation_resa use index(id_enfant_id_prestation) Where id_enfant='#Arguments.id_enfant#' 
            and id_prestation='#Arguments.id_prestation#'
		</cfquery>
		<cfif #verif_verification.recordcount# gt 0>
        	<cfquery name="enfantlimitation" datasource="f8w">
            	Select id,siret,NumEnfant,prénom from enfant where id='#Arguments.id_enfant#'
            </cfquery>
            <cfquery name="prestationlimitation" datasource="f8w">
            	Select id,numprestation,désignation,JoursExclus,DélaiRésaWebJ,DélaiRésaWebH from prestation where id='#Arguments.id_prestation#'
            </cfquery>
            <cfquery name="limitation_resa_ent" datasource="f8w"><!--- recup du Nbr de jour maxi par semaine --->
        		Select designation,NbrJour from limitation_resa_ent Where id='#verif_verification.id_ent#'
        	</cfquery>
        	<cfset DateDemandelimitation = createdate(#mid(Arguments.datejour,7,4)#,#mid(Arguments.datejour,4,2)#,#mid(Arguments.datejour,1,2)#)>
        	<cfswitch expression="#dayofweek(DateDemandelimitation)#">
        		<cfcase value="1"><!--- dimanche --->
                	<cfset DateDeplimitation = Dateadd("d",-6,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="2"><!--- lundi --->
                	<cfset DateDeplimitation = #DateDemandelimitation#>
                </cfcase>
        		<cfcase value="3"><!--- mardi --->
                	<cfset DateDeplimitation = dateadd("d",-1,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="4"><!--- mercredi --->
                	<cfset DateDeplimitation = dateadd("d",-2,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="5"><!--- jeudi --->
                	<cfset DateDeplimitation = dateadd("d",-3,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="6"><!--- vendredi --->
                	<cfset DateDeplimitation = dateadd("d",-4,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="7"><!--- samedi --->
                	<cfset DateDeplimitation = dateadd("d",-5,#DateDemandelimitation#)>
                </cfcase>
        	</cfswitch>
        	<cfset NombreDeResaCetteSemaine = 1><!--- 0 + celle demandé --->    
            <cfloop from="1" to="7" index="nbrJourlimitation">
        		<cfset DateCherchelimitation = dateadd("d",#nbrJourlimitation# - 1,#DateDeplimitation#)>
                <cfset aaa = resaponctuelle(#DateCherchelimitation#,#enfantlimitation.siret#,#prestationlimitation.numprestation#,#enfantlimitation.numenfant#,#prestationlimitation.JoursExclus#,#prestationlimitation.DélaiRésaWebJ#,#prestationlimitation.DélaiRésaWebH#,#prestationlimitation.id#,#enfantlimitation.id#)>
                <cfif #aaa.bg# neq "##FF0000"<!--- rouge ---> and #aaa.bg# neq "##666666"<!--- gris ---> and #aaa.bg# neq "##FFFFFF"<!--- blanc ---> >
					<!--- inscrit a la presta --->
                    <cfset NombreDeResaCetteSemaine = #NombreDeResaCetteSemaine# + 1>
				</cfif>
            </cfloop>
            <cfif #NombreDeResaCetteSemaine# gt #limitation_resa_ent.NbrJour#>
            	<cfset Msglimitation = "Désolé, inscription impossible, vous ne pouvez inscrire " & #enfantlimitation.prénom# & " que " & #limitation_resa_ent.NbrJour# & " jour(s) ">
                <cfset Msglimitation = #Msglimitation# & "par semaine à la " & #prestationlimitation.désignation# & " ! Limitation : " & #limitation_resa_ent.designation#>
            </cfif>
        </cfif>
    	<cfreturn Msglimitation>    
    </cffunction>
<!--- '***************************************************************     --->
    <cffunction name="DateMiniResaNew" access="public" returntype="any">
    	<cfargument name="JoursExclus" type="numeric" required="yes">
        <cfargument name="DelaiResaWebj" type="numeric" required="yes"><!--- mini en jour pour agir --->
    	<cfargument name="DelaiResaWebH" type="numeric" required="yes"><!--- heure butoir pour j + X --->
        <cfargument name="DateAtester" type="date" required="yes"><!--- ont veut agire sur cette datetime --->
        <cfset Retour = "">
        <cfset Moment_de_laction = now()>
        <!--- Ont est avant ou après l'heure butoir ? --->
			<cfset Pour_heure_butoir = Createdatetime(year(now()),month(now()),day(now()),#mid(arguments.DelaiResaWebH,1,2)#,#mid(arguments.DelaiResaWebH,4,2)#,#mid(arguments.DelaiResaWebH,7,2)#)>
            <cfif DateCompare(#Moment_de_laction#,#Pour_heure_butoir#) eq 1 and #arguments.DelaiResaWebj# lte 1><!--- ont a dépassé l'heure --->
            	<cfset arguments.DelaiResaWebj = #arguments.DelaiResaWebj# + 1><!--- ont fait + 1 sur jour --->
            </cfif>
        <!--- --->
		<!---
		Ont produit DateMiniResa1 qui est la date la plus proche possible d'aujourdui en fonction des
		parametres DelaiResaWebH ou DelaiResaWebj de la prestation --->
		<cfif #Arguments.DelaiResaWebj# neq 0>
			<!--- creation de la date suivant resawebj et resawebh --->
			<cfset DateMiniResa = DateAdd("d",#Arguments.DelaiResaWebj#,now())>
            <cfset DateMiniResa1 = createdatetime(year(#DateMiniResa#),month(#DateMiniResa#),day(#DateMiniResa#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
        <cfelse>
        	<cfset DateMiniResa = now()>
            <cfset DateMiniResa1 = createdatetime(year(#DateMiniResa#),month(#DateMiniResa#),day(#DateMiniResa#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
            <cfif DateCompare(#DateMiniResa1#,now()) eq -1><!--- l'heure maxi pour resa est dépassé --->
            	<cfset DateMiniResa1 = DateAdd("d",1,#DateMiniResa1#)>
            </cfif>
        </cfif>
        <!--- --->
        <!---
		Ont verifie que DateMiniResa1 n'est pas sur un jour exclus
		Si oui ont fait j + 1 jusqu'a ne pas etre sur un jour exclus --->
        <cfset a = 1>
        <cfloop condition="#a# eq 1">
            <cfset a = Resajour(#Arguments.JoursExclus#,#DayOfWeek(DateMiniResa1)#)>
            <cfif #a# eq 1>
                <cfset DateMiniResa1 = DateAdd("d",1,#DateMiniResa1#)>
            <cfelse>
                <cfbreak>
            </cfif>
        </cfloop>
        <!--- --->
        <!---
		Ont compare la date demandé par l'utilisateur (Arguments.DateAtester) à DateMiniResa1
        Il faut dabord traiter la date demandé :
		1 - c'est la date du jour, l'heure de la demande doit etre reel
		2 - c'est la meme date que miniresa mais dans le futur
		3 - c'est une date dans le future ont touche pas (25/02/2100 00:00:00)
		--->
		<cfif lsdateformat(now(),"dd/MM/YYYY") eq lsdateformat(#Arguments.DateAtester#,"dd/MM/YYYY")><!--- ont demande date du jour --->
        	<cfset Arguments.DateAtester = createdateTime(year(#Arguments.DateAtester#),month(#Arguments.DateAtester#),day(#Arguments.DateAtester#),hour(now()),minute(now()),second(now()))>
        <cfelseif lsdateformat(#DateMiniResa1#,"dd/MM/YYYY") eq lsdateformat(#Arguments.DateAtester#,"dd/MM/YYYY")>
			<!--- ont demande meme date que mini resa mais dans le futur --->
        	<cfset Arguments.DateAtester = createdateTime(year(#Arguments.DateAtester#),month(#Arguments.DateAtester#),day(#Arguments.DateAtester#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
        </cfif>
        <!--- --->
        <cfif DateCompare(#Arguments.DateAtester#,#DateMiniResa1#) eq -1><!--- Arguments.DateAtester est antérieure à DateMiniResa1 --->
            <!--- j'ai demandé résa pour le 20 juillet et mini est 25 juillet --->
            
			<cfset Retour = #DateMiniResa1#><!--- ont retourne la date mini --->
			<!--- <cfset Retour = "OK"> --->
		<cfelseif DateCompare(#Arguments.DateAtester#,#DateMiniResa1#) eq 1><!--- Arguments.DateAtester est postérieure à DateMiniResa1 --->
            <!--- j'ai demandé résa pour le 30 juillet et mini est 25 juillet --->
			<!--- avant de retourner date demandé, ont verifie jour exclus --->
            <cfset a = 1>
            <cfset modif_sur_jour_exclus = "non">
            <cfloop condition="#a# eq 1">
                <cfset a = Resajour(#Arguments.JoursExclus#,#DayOfWeek(Arguments.DateAtester)#)>
                <cfif #a# eq 1>
                    <cfset Arguments.DateAtester = DateAdd("d",1,#Arguments.DateAtester#)>
                    <cfset modif_sur_jour_exclus = "oui">
                <cfelse>
                    <cfbreak>
                </cfif>
            </cfloop>
            <cfif #modif_sur_jour_exclus# eq "non">
            	<cfset Retour = "OK">
            <cfelse>
            	<cfset Retour = #Arguments.DateAtester#>
            </cfif>
        <cfelse><!--- Arguments.DateAtester est égale à DateMiniResa1 --->
            <!--- <cfset Retour = #Arguments.DateAtester#><!--- ont retourne date demandé ---> --->
            <cfset Retour = "OK"> 
        </cfif>
        
        
        <cfreturn Retour>
    </cffunction>        
	<!--- --->    
    <cffunction name="NumSuivant6" access="public" returntype="any">
    	<cfargument name="siret" type="string" required="yes">
        <cfargument name="table" type="string" required="yes">
        <cfargument name="champs" type="string" required="yes">
        <cfquery name="lect" datasource="f8w">
        	Select id,nval from parametres use index(siret_tabl_champs) Where `siret` = '#Arguments.siret#' and `tabl` = '#Arguments.table#' 
            and `champs` = '#Arguments.champs#'
        </cfquery>
        <cfif #lect.recordcount#>
        	<cfset NumSuivant6 = #lect.nval# + 1>
            <cfquery name="maj" datasource="f8w">
            	Update parametres set nval = '#NumSuivant6#' Where id = '#lect.id#'
            </cfquery>
        <cfelse>
        	<cfset NumSuivant6 = 1000000000>
            <cfquery name="add" datasource="f8w">
            	Insert into parametres (`siret`,`tabl`,`champs`,`nval`) Values ('#Arguments.siret#','#Arguments.table#','#Arguments.champs#','#NumSuivant6#')
            </cfquery>
        </cfif>
        <cfreturn NumSuivant6>
    </cffunction>	
    <cffunction name="cléRIB" access="public" returntype="string">
		<cfargument name="cb$" type="string" required="yes">
		<cfargument name="cg$" type="string" required="yes">
    	<cfargument name="compte$" type="string" required="yes">
        <cfargument name="cle$" type="string" required="yes">
        <cfset retour = "">
		<!---'***************************************************
        '* La function retourne la valeur de la clé RIB
        '* en fonction du code banque cb$, du code guichet
        '* cg$ et du N° de compte (compte$)
        '***************************************************
        --->
		<cfIf Len(#arguments.cb$#) neq 5><cfset retour="KO Le code banque doit être sur 5 chiffres !"><cfreturn retour></cfIf>
		<cfIf Len(#arguments.cg$#) neq 5><cfset retour="KO Le code agence doit être sur 5 chiffres !"><cfreturn retour></cfIf>
		<cfIf Len(#arguments.compte$#) neq 11><cfset retour="KO Le N° de compte doit être sur 11 caractères !"><cfreturn retour></cfIf>
        <cfIf Len(#arguments.cle$#) neq 2><cfset retour="KO La clé doit être sur 2 chiffres !"><cfreturn retour></cfIf>
		<!--- '*-------------------------------------------------- --->
		<cfset cpte$ = UCase(#arguments.compte$#)> <!--- 'élévation en majuscule --->
		<cfloop from="1" to="11" index="i"><!--- 'on transforme les lettres possibles en chiffre --->
    		<cfset v = Asc(Mid(#cpte$#, i, 1))>
    		<cfif #v# gte 65 and #v# lte 73><!--- 'A à I --->
        		<!---<cfset Mid(#cpte$#, i, 1) = Chr(#v# - 16)> <!--- '1 à 9 ---> --->
                <cfset chaine = Mid(#cpte$#, 1, i - 1) & Chr(#v# - 16) & Mid(#cpte$#, i, len(#cpte$#))>
                <cfset cpte$ = #chaine#>
   			<cfelseif #v# gte 74 and #v# lte 82><!--- 'J à R --->
        		<!--- <cfset Mid(#cpte$#, i, 1) = Chr(#v# - 25)> <!--- '1 à 9 ---> --->
                <cfset chaine = Mid(#cpte$#, 1, i - 1) & Chr(#v# - 25) & Mid(#cpte$#, i, len(#cpte$#))>
                <cfset cpte$ = #chaine#>                
    		<cfelseif #v# gte 83 and #v# lte 90><!--- 'S à Z --->
        		<!--- <cfset Mid(#cpte$#, i, 1) = Chr(#v# - 33)><!--- '2 à 9 ---> --->
                <cfset chaine = Mid(#cpte$#, 1, i - 1) & Chr(#v# - 33) & Mid(#cpte$#, i, len(#cpte$#))>
                <cfset cpte$ = #chaine#>                                
    		<cfelseif #v# gte 48 and #v# lte 57><!--- '0 à 9 --->
        		<!--- ' on ne change pas les caractères chiffres inscrits--->
    		<cfelse>
        		<cfset retour="Attention, impossible de vérifier la clé du rib !"><cfreturn retour>
            </cfif>
		</cfloop>
		<!--- '*---------------------------------------------------- --->
		<cfset rib$ = #arguments.cb$# & #arguments.cg$# & #cpte$# & "00"><!--- '23 caractères --->
		<cfset dividande$ = Mid(#rib$#, 1, 3)><!--- 'on prend les 3 premiers chiffres du rib (on aurait pu en prendre plus ou moins)--->
		<cfloop from="4" to="23" index="i">
    		<cfset dividande$ = #dividande$# & Mid(#rib$#, i, 1)><!--- 'on ajoute un chiffre au nombre string --->
    		<cfset dividande$ = (Val(#dividande$#)) Mod 97><!--- 'le reste de la division entière est repassé dans le dividande--->
		</cfloop><!---'le dernier dividance est le reste de la division entière du nombre à 21 chiffres --->
		<cfset cléRIB = 97 - Fix(Val(#dividande$#))>
		<cfif #cléRIB# neq #arguments.cle$#><cfset retour="Attention, la clé du rib devrait être " & #cléRIB# & " !"></cfIf>
		<cfreturn retour>
    </cffunction>
    <cffunction name="ConversionRibIban" access="public" returntype="string">
    	<cfargument name="rib$" type="string" required="yes">
            <!---'************************************************************
            '* La conversion RIB IBAN ne concerne que les RIB français.
            '* le rib$ doit contenir 23 car. La clé RIB est comprise.
            '************************************************************ --->
		<cfset retour = "">
		<cfif Len(#arguments.rib$#) eq 23>
    		<cfset ribinit$ = #arguments.rib$#>
<!---            '*-------------------------------------------------------------
            '* remplacement des caractères alpha du rib français par les numériques
            '*------------------------------------------------------------- --->
    		<cfset xrib$ = #arguments.rib$#>
    		<cfset yrib$ = "">
            <cfloop from="1" to="21" index="i"> <!---'la clé rib est toujours numérique--->
        		<cfset Case = Mid(#xrib$#, i, 1)>
        		<cfif #Case# eq  " "><!--- 'blanc interdit--->
            		<cfbreak><!---ConversionRibIban = "": Exit Function--->
                </cfif>
        		<cfif isnumeric(#Case#)>
                
                <cfelse>
        			<!---Case "A" To "Z"--->
     		       <cfset yrib$ = Mid(#xrib$#, 1, i - 1) & Asc(Mid(xrib$, i, 1)) - 55 & Mid(#xrib$#, i + 1, Len(#xrib$#) - i)>
				</cfif>
            </cfloop>
<!---            '*------------------------------------------------------------------------------------------------------
            '* calcul de la clé IBAN sur la base du RIB tout numérique augmenté des numériques équivalents à FR00
            '*------------------------------------------------------------------------------------------------------ --->
    		<cfset iban$ = #yrib$# & "152700">

    		<cfset dividande$ = Mid(#iban$#, 1, 3)><!--- 'on prend les 3 premiers chiffres de l'iban (on aurait pu en prendre plus ou moins) --->
    		<cfloop from="4" to="#Len(iban$)#" index="i">
        		<cfset dividande$ = #dividande$# & Mid(#iban$#, i, 1)><!--- 'on ajoute un chiffre au nombre string --->
        		<cfset dividande$ = Val(#dividande$#) Mod 97><!--- 'le reste de la division entière est repassé dans le dividande --->
    		</cfloop> <!--- 'le dernier dividande est le reste de la division entière du nombre à 21 chiffres --->
    		<cfset cleiban$ = 98 - Fix(Val(dividande$))>
    		<cfset retour = "FR" & cleiban$ & ribinit$>
		</cfif>
		<cfreturn retour>
	</cffunction>
<!---	'***************************************************************
        '* iv est un entier codifiant les jours réservés ou non
        '* d'une semaine sous la forme
        '* Dimanche*1 + lundi*2 + mardi*4 + mercredi*8 +
        '* jeudi*16 + vendredi*32 + samedi*64,
        '* chaque jour prenant la valeur 1 (exclus) ou 0(permis)
        '* La function retourne 1 ou zéro pour le jour noj
        '* si une résa est faite.
        '***************************************************************    --->
    <cffunction name="Resajour" access="public" returntype="any">
		<cfargument name="iv" type="numeric" required="yes">
		<cfargument name="noj" type="numeric" required="yes">
        <cfset r = #arguments.iv#>
        <cfloop from="6" to="0" step="-1" index="k">
        	<cfset q = #r# \ (2 ^ k)>
            <cfIf k + 1 eq #arguments.noj#>
            	<cfbreak>
            </cfIf>
            <cfset r = #r# Mod (2 ^ #k#)>
        </cfloop>
		<cfset Retour = #q# >
		<cfreturn Retour>
	</cffunction>    
    <cffunction name="resaponctuelle1" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
    	<cfargument name="JoursExclus" type="string" required="no" default="0">
        <cfargument name="DélaiRésaWebJ" type="string" required="no" default="">
        <cfargument name="DélaiRésaWebH" type="string" required="no" default="">
        <cfargument name="idprestation" type="string" required="no" default="">
        <cfargument name="idenfant" type="string" required="no" default="">
        <cfargument name="siretdispo" type="string" required="no" default="">
        <cfif len(#arguments.siretdispo#) eq 0>
			<cfset arguments.siretdispo = #arguments.siret#>
		</cfif>
    	<cfset aa = querynew("bg,cont")>
    	<cfset NumJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd")>
        <cfset NumJourI = DayOfWeek(#Arguments.Date_calend_dep#)>
        <cfset DateJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd/MM/YYYY")>
        <cfset DateJourStp = LsDateFormat(#Arguments.Date_calend_dep#,"YYYYMMdd")>
		<cfset CasTraiTe = false><!--- pour eviter case noir cas non traité --->
		<!--- Blanc : Prestation ouverte, pas d'inscription recurente ni ponctuelle, si clic : inscription ponctuelle --->
		<!--- FUTUR : --->
		<cfset FondBlanc = "##FFFFFF">
        <cfset ContenuUrlBlanc = '<a title=""  href="/admin/?id_mnu=24&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#&id_enfant_sel=#Arguments.idenfant#&id_client_sel=0'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&action=inscr&nature=urlnature">#NumJour#</a>'>
        <!--- PASSE : --->
		<cfset msg = "Votre enfant n'était pas inscrit à la prestation le " & #DateJour# & " (sous réserve de constat de présence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlBlancPASS = '<a title="#msg#"  href="/admin/?id_mnu=24&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#&id_enfant_sel=#Arguments.idenfant#&id_client_sel=0'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&msg1=#msg1#">#NumJour#</a>'>        
		<!--- --->
        <!--- Vert : Prestation ouverte, inscrit à la prestation, si clic : ont désinscrit --->
        <cfset FondVert = "##00FF00">
        <cfset ContenuUrlVert = '<a title="" href="/admin/?id_mnu=24&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#&id_enfant_sel=#Arguments.idenfant#&id_client_sel=0'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&action=desinscr&nature=urlnature">#NumJour#</a>'>
        <!--- --->
        <!--- Rouge : Prestation ouverte, désinscrit à la prestation, si clic : ont réinscrit --->
        <cfset FondRouge = "##FF0000">
        <cfset ContenuUrlRouge = '<a title="" href="/admin/?id_mnu=24&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#&id_enfant_sel=#Arguments.idenfant#&id_client_sel=0'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&action=reinscr&nature=urlnature"><font color="##FFFFFF">#NumJour#</font></a>'>
        <!--- --->
        <!--- Gris : Prestation fermé --->
        <cfset FondGris = "##666666">
        <cfset ContenuUrlGris = '<font color="##FFFFFF">#NumJour#</font>'>
		<!--- --->        
        <!--- Vert clair : Etait inscrit --->
        <cfset FondVertClair = "##B4F8A0">
        <cfset msg = "Votre enfant était inscrit à la prestation le " & #DateJour# & " (sous réserve de constat d'absence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlVertClair = '<a title="#msg#"  href="/admin/?id_mnu=24&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#&id_enfant_sel=#Arguments.idenfant#&id_client_sel=0'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&msg1=#msg1#">#NumJour#</a>'>
		<!--- --->
        <!--- 1 : Jour exclus pour la prestation ? --->
        <cfquery name="ptv" datasource="f8w"><!--- pour calendier type vacance si besoint --->
        	Select typevacance,PasDeVacance from prestation use index(siret_numprestation) 
            Where siret = '#Arguments.siret#' and NumPrestation = '#Arguments.numprestation#'
        </cfquery>
        <cfquery name="etabpourzone" datasource="f8w">
        	Select Zone from application_client use index(siret) Where siret = '#Arguments.siret#'
        </cfquery>
        <cfquery name="jourexclus" datasource="f8w">
        	Select id,NumClasse from jourseclus#ptv.typevacance# use index(siret_Numprestation) 
            Where siret = '#Arguments.siret#' and NumPrestation = '#Arguments.numprestation#'
            and DateJour = '#DateJour#' 
            or siret = '' and NumPrestation = 0 and DateJour = '#DateJour#' and zone = '#etabpourzone.Zone#'
        </cfquery>
        <cfquery name="jourNONexclus" datasource="f8w">
        	Select id from joursNONeclus#ptv.typevacance# use index(siret_Numprestation) 
            Where siret = '#Arguments.siret#' and NumPrestation = '#Arguments.numprestation#' 
            and DateJour = '#DateJour#'
        </cfquery>  
        <cfset fermé = false>
        <cfloop query="jourexclus">
            <cfif #jourexclus.NumClasse# eq 0>
                <cfset fermé = true>
                <cfbreak>        
            <cfelse>
                <cfquery name="enfantclasse" datasource="f8w">
                    Select id from enfantclasse use index(siret_NumEnfant_NumClasse) Where siret = '#Arguments.siretdispo#' 
                    and NumEnfant='#Arguments.NumEnfant#' and NumClasse='#jourexclus.NumClasse#'
                </cfquery>
                <cfif #enfantclasse.recordcount#>
                    <cfquery name="classe" datasource="f8w">
                        Select id from classe use index(Siret_NumClasse) Where siret = '#Arguments.siretdispo#' 
                        and NumClasse='#jourexclus.NumClasse#' and del=0
                    </cfquery>
                    <cfif #classe.recordcount#>
                        <cfset fermé = true>
                        <cfbreak>
                    </cfif>
                </cfif>
            </cfif>
        </cfloop>	      
        <cfif (#fermé# eq true or Resajour(#Arguments.JoursExclus#,#NumJourI#) eq 1) and #jourNONexclus.recordcount# eq 0 and #ptv.PasDeVacance# eq 0><!--- jour exclus --->
			<cfset newRow = QueryAddRow(aa)>
        	<cfset temp = QuerySetCell(aa, "bg",#FondGris#)>
			<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlGris#)>
            <cfset CasTraiTe = true>
        <cfelse>
        	<!--- 2 : Inscription Hebdo ??? --->
            <cfquery name="lect_inscription" datasource="f8w" maxrows="1">
            	Select id,InscriptionImpaire,DateDebSTP from inscription use index(siret_Numenfant) Where siret = '#Arguments.siretdispo#' 
                and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#' 
                and DateDebSTP <= '#DateJourStp#' order by DateDebSTP DESC,id DESC limit 0,1
            </cfquery>
 			<!--- maj numlieu --->
            <cfquery name="enfantclasse" datasource="f8w">
                Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#Arguments.siretdispo#' 
                and NumEnfant = '#Arguments.numenfant#'
            </cfquery>
            <cfset NumLieuPrestation = 0>
			<cfif #enfantclasse.recordcount# gt 0>         
                <cfquery name="lieuprestationclasse" datasource="f8w" maxrows="1">
                    Select NumLieu from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
                    Where siret = '#Arguments.siretdispo#' 
                    and NumClasse = '#enfantclasse.NumClasse#' and NumPrestation = '#Arguments.numprestation#' limit 0,1
                </cfquery>
                <cfif #lieuprestationclasse.recordcount#>
                    <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                </cfif>
            </cfif>
            <cfif #lect_inscription.recordcount#>
            	<cfquery name="maj" datasource="f8w">
                	Update inscription set NumLieu='#NumLieuPrestation#' Where id='#lect_inscription.id#'
                </cfquery>
            </cfif>
            <!--- --->
            <!--- pour traiter init --->
            <cfquery name="lect_inscription_une_avant" datasource="f8w" maxrows="1">
            	Select id from inscription use index(siret_Numenfant) Where siret = '#Arguments.siretdispo#' 
                and Numenfant = '#Arguments.numenfant#' 
                and Numprestation = '#Arguments.numprestation#' and DateDebSTP < '#lect_inscription.DateDebSTP#' 
                and InscriptionImpaire > 0 order by DateDebSTP DESC,id DESC limit 0,1
            </cfquery>
            <!--- 2.1 : Inscrit ponctuel --->
            <cfquery name="lect_inscriptionplanning" datasource="f8w">
            	Select id,nature from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                Where siret = '#Arguments.siretdispo#' 
                and Numprestation = '#Arguments.numprestation#' and Numenfant = '#Arguments.numenfant#' and DateInscription = '#DateJour#'
            </cfquery>
			<!--- maj numlieu --->
            <cfif #lect_inscriptionplanning.recordcount#>
            	<cfquery name="maj" datasource="f8w">
                	Update inscriptionplanning set NumLieu='#NumLieuPrestation#' Where id='#lect_inscriptionplanning.id#'
                </cfquery>
            </cfif>
            <!--- --->
            <cfif #lect_inscription.recordcount#>
            	<cfif #lect_inscription.InscriptionImpaire# gt 0>
                	<cfif Resajour(#lect_inscription.InscriptionImpaire#,#NumJourI#) eq 1><!--- inscrit hebdo --->
						<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
							<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                                <cfset CasTraiTe = true>                                               						
                        	<cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                                               
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                            	<!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                <!--- donc inscrit --->
                            	<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>
                            </cfif>
                        <cfelse>
                        	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                                              
							<cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)> 
                                <cfset CasTraiTe = true>                                                      
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                            	<!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                <!--- donc inscrit --->
                            	<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>  
                                <cfset CasTraiTe = true>                          
                            </cfif>
						</cfif>
                    <cfelse><!--- pas inscrit hebdo --->
                    	<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
							<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                            	<cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            	<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                                <cfset CasTraiTe = true>                       
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                                <cfset CasTraiTe = true>                                               						
                            </cfif>
                        <cfelse>
                        	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                                                                   
							<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
								<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                          
                            </cfif>
						</cfif>
                    </cfif>
                <cfelse><!--- desinscrit --->
					<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
						<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
							<cfif #lect_inscription_une_avant.recordcount# eq 1><!--- pour traiter init --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                      
	                       <cfelse><!--- pour traiter init --->
                                <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                                          
                            </cfif>
						<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
							<cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                            <cfset CasTraiTe = true>                                              						
                        <cfelse>
							<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>

                            <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                            <cfset msg1 = UrlEncodedFormat(#msg#)>
                            <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                            <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                            <cfset CasTraiTe = true>                                                  
                        </cfif>
					<cfelse>
                    	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
							<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                            <cfset CasTraiTe = true>                 
						<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
							<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                            <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                            <cfset CasTraiTe = true>                                                                      
                        <cfelseif #lect_inscriptionplanning.nature# eq 1><!--- ILLOGIQUE !!! --->
                        
                        
                        </cfif>
                    </cfif>
                </cfif>
            <cfelse><!--- pas inscription hebdo --->
				<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
                    <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
						<cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>    
                        <cfset CasTraiTe = true>                    
					<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
						<cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                        <cfset CasTraiTe = true>                                               						
                    </cfif>
                <cfelse>
                	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
						<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                        <cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
                        <cfset CasTraiTe = true>                                                                 
					<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
						<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                        <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                        <cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)> 
                        <cfset CasTraiTe = true>                                       
                    </cfif>
                </cfif>
            </cfif>
        </cfif>
        <cfif #CasTraiTe# is false>
			<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
            <cfset newRow = QueryAddRow(aa)> 
            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
            <!---<cfset temp = QuerySetCell(aa, "cont","CAS PAS TRAITE")>--->
            <cfset CasTraiTe = true>                                                                       
        </cfif>
        <cfreturn aa>
    </cffunction>    
    <cffunction name="Calendrier1" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
		<cfset calendrier = querynew("bg1,cont1,bg2,cont2,bg3,cont3,bg4,cont4,bg5,cont5,bg6,cont6,bg7,cont7")>
        <cfset sav_Arguments.Date_calend_dep = #Arguments.Date_calend_dep#>
        <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
        	<cfquery name="lect_prestation" datasource="f8w">
            	Select id,JoursExclus,DélaiRésaWebJ,DélaiRésaWebH from prestation use index(siret_numprestation)
                Where siret = '#arguments.siret#' and numprestation = '#Arguments.numprestation#'
            </cfquery>
            <cfquery name="lect_enfant" datasource="f8w">
            	select id from enfant use index(siret_numenfant) Where siret = '#arguments.siret#' and numenfant = '#Arguments.numenfant#'
            </cfquery>
        </cfif>
        <cfloop from="1" to="6" index="lignetableau">	
			<cfset newRow = QueryAddRow(calendrier)> 
            <cfloop from="1" to="7" index="cpt">
                <cfswitch expression="#cpt#">
                    <cfcase value="1"><!--- lundi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 2 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg1",#a.bg#)>
								<cfset temp = QuerySetCell(calendrier, "cont1",#a.cont#)>
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg1","##00FF00")>
								<cfset temp = QuerySetCell(calendrier, "cont1",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                        	</cfif>
                            <cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg1","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont1","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="2"><!--- mardi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 3 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg2",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg2","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg2","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont2","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="3"><!--- mercredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 4 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg3",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg3","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg3","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont3","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="4"><!--- jeudi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 5 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                            	<cfset temp = QuerySetCell(calendrier, "bg4",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",#a.cont#)>                            
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg4","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg4","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont4","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="5"><!--- vendredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 6 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg5",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg5","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg5","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont5","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="6"><!--- samedi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 7 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg6",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",#a.cont#)>                            
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg6","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg6","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont6","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="7"><!--- dimanche --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 1 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg7",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",#a.cont#)>                                
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg7","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg7","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont7","&nbsp;")>
                        </cfif>                
                    </cfcase>            
                </cfswitch>
            </cfloop>
		</cfloop>        
    	<cfreturn calendrier>
    </cffunction>
	<cffunction name="URLdecode1" returntype="string">
    	<cfargument name="Text" type="string">
     	<!--- <cfset Hex = "0123456789ABCDEFGHIJQLNMOPQRSTUVWXYZabcdefghijklnmopqrstuvwxyz">--->
    	<cfset fin = 0>
        <cfset urltxt = #arguments.Text#>
    	<cfloop condition="#fin# neq 1">
        	<cfset DebutPourentage = findnocase("%",#urltxt#)>
        	<cfif #DebutPourentage# gt 0>
        		<cfset FinPourentage = findnocase("%",#urltxt#,#DebutPourentage# + 1)>
               	<cfif #FinPourentage# gt 0>
                	<cfset TexteAtraiter = mid(#urltxt#,#DebutPourentage# + 1, (#FinPourentage# - #DebutPourentage#) - 1 )>
                	<cfset TexteAtraiterChar = chr(#TexteAtraiter#)>
                    <cfset cherche = "%" & #TexteAtraiter# & "%">
                    <cfset urltxt = replace(#urltxt#,#cherche#,#TexteAtraiterChar#,"all")>
                <cfelse>
					<cfset fin = 1>
                    <cfbreak>                
                </cfif>
        	<cfelse>
            	<cfset fin = 1>
                <cfbreak>
            </cfif>
        </cfloop>
        <cfreturn urltxt>
    </cffunction>
	<!---<cfFunction name="ConversionRibIban" returntype="string">
		<cfargument name="rib$" type="string">
        <!---
        '************************************************************
        '* La conversion RIB IBAN ne concerne que les RIB français.
        '* le rib$ doit contenir 23 car. La clé RIB est comprise.
        '************************************************************ --->
		<cfIf Len(#rib$#) = 23>
    		<cfset ribinit$ = #rib$#>
            <!---'*-------------------------------------------------------------
            '* remplacement des caractères alpha du rib français par les numériques
            '*---------------------------------------------------------------->
    		<cfset xrib$ = #rib$#>
    		<cfloop from="1" to="21" index="i"><!--- 'la clé rib est toujours numérique --->
                <cfswitch expression="#mid(xrib$, i, 1)#">
        			<cfCase value=" "><!--- 'blanc interdit--->
            			<cfset yrib$ = "">
                        <cfbreak>
                    </cfCase>
        			<cfdefaultcase>
                    	<cfif isnumeric(#mid(xrib$, i, 1)#) is false>
                        	<cfset yrib$ = Mid$(#xrib$#, 1, i - 1)>
                        	<cfset yrib$ = #yrib$# & #lsnumberformat(Asc(Mid$(xrib$, i, 1)) - 55,"99")# & #Mid$(xrib$, i + 1, Len(xrib$) - i)#>
                        </cfif>
                    </cfdefaultcase>
				</cfswitch>
			</cfloop>
            <!---'*------------------------------------------------------------------------------------------------------
            '* calcul de la clé IBAN sur la base du RIB tout numérique augmenté des numériques équivalents à FR00
            '*------------------------------------------------------------------------------------------------------ --->
    		<cfif len(#yrib$#)>
				<cfset IBAN$ = #yrib$# & "152700">
    			<cfset dividande$ = Mid$(#IBAN$#, 1, 3)> <!--- 'on prend les 3 premiers chiffres de l'iban (on aurait pu en prendre plus ou moins) --->
    			<cfloop from="#i#" to="#Len(IBAN$)#" index="idx">
        			<cfset dividande$ = #dividande$# & #Mid$(IBAN$, i, 1)#> <!--- 'on ajoute un chiffre au nombre string --->
        			<cfset dividande$ = lsnumberFormat((Val(#dividande$#)) Mod 97, "00")> <!--- 'le reste de la division entière est repassé dans le dividande--->
    			</cfloop> <!---'le dernier dividande est le reste de la division entière du nombre à 21 chiffres --->
    			<cfset cleiban$ = lsnumberFormat(98 - Fix(Val(dividande$)), "00")>
    			<!---'*------------------------------------------------------------------------------------ --->
    			<cfset ConversionRibIban = "FR" & cleiban$ & ribinit$>
			<cfElse>
    			<cfset ConversionRibIban = "">
			</cfif>
		<cfelse>
        	<cfset ConversionRibIban = "">
        </cfIf>
        <cfreturn ConversionRibIban>
    </cffunction>--->
	<cfFunction name="BICissuRIB" returntype="string">
		<cfargument name="CodeBanque" type="string">
        <cfargument name="CodeGuichet" type="string">
		<!---
		'********************************************************************************
		'* La function retourne le BIC complément de l'IBAN en fonction des
		'* CodeBanque et du CodeGuichet du RIB, au travers de la table RéférentielSEPA/pes.mdb
		'******************************************************************************** --->
		<cfquery name="sepa" datasource="eurosyl">
        	Select bic from référentielsepa use index(cbque_cgui) WHERE cbque='#arguments.CodeBanque#' and cgui='#arguments.CodeGuichet#'
        </cfquery>
		<cfif #sepa.recordcount#>
        	<cfset BICissuRIB = #sepa.bic#>
        <cfelse>    
    		<cfset BICissuRIB = "XXXXXXXXXXX">
		</cfif>
		<cfreturn BICissuRIB>
	</cfFunction>
    <!---<cffunction name="prestation_reserve_a" returntype="string">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="NumPrestation" type="numeric" required="no" default="0">
        <cfargument name="NumClient" type="numeric" required="no" default="0">
       	<cfargument name="NumEnfant" type="numeric" required="no" default="0">
        <cfargument name="NumJourPerioPresta" type="numeric" required="no" default="0">
        <cfset retour = "OK"><!--- par defaut pas de blocage --->
    	<cfquery name="groupeclient" datasource="f8w">
        	Select NumGroupe from clientgroupe use index(siret_NumClient) Where siret='#arguments.siret#' and NumClient='#arguments.NumClient#'
        </cfquery>
        <cfquery name="enfant" datasource="f8w">
        	Select DateNaissance from enfant use index(siret_NumEnfant) Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#'
        </cfquery>
        <cfquery name="enfantclasse" datasource="f8w">
        	Select NumClasse from enfantclasse use index(siret_NumEnfant) Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#'
        </cfquery>
        
        <!--- reservé a un groupe ou a une classe ou a .... --->
        <cfquery name="reserve" datasource="f8w">
    		Select * from prestation_reserve_a use index(siret_NumPrestation) 
            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumGroupeClient>0 
    	</cfquery> 
        <cfif #reserve.recordcount#>
            <cfquery name="reserve" datasource="f8w">
                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumGroupeClient='#groupeclient.NumGroupe#'
            </cfquery>
        	<cfif #reserve.recordcount# eq 0>
            	<cfset retour = "Réservé à des groupes clients auquel n'appartient pas ce client">
            <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
            	<cfif #reserve.ReservableDu# gt 0 and DayOfYear(now()) gte #reserve.ReservableDu#>
                	<cfif #reserve.ReservableAu# gt 0 and DayOfYear(now()) gt #reserve.ReservableAu#>
                    	<cfset retour = "Vous n'êtes pas dans la période de réservation !">
                    </cfif>
                <cfelse>	
                    <cfset retour = "Vous n'êtes pas dans la période de réservation !">
                </cfif>
            	<cfif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #reserve.DebutPerioPresta#>
                	<cfif #reserve.FinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #reserve.FinPerioPresta#>
                    	<cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                    </cfif>
                <cfelse>	
                    <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                </cfif>
            </cfif>    
    	</cfif>
        <cfif retour eq "OK">
        
        
        
        </cfif>
    	<cfreturn retour>
    </cffunction>--->
</cfcomponent>