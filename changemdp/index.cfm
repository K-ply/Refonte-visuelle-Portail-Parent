﻿<cfset msg = "">
<cfset msg1 = "">
<cfset msg2 = "">
<cfif isdefined("url.UidRecPsw")>
    <cfquery name="lect_user" datasource="services"><!--- lecture de l'utilisateur du site ---->
        Select * from login use index(UidRecPsw) Where UidRecPsw = '#url.UidRecPsw#'
    </cfquery>
    <cfif #lect_user.recordcount# eq 0>
    	<cflocation url="https://parent.cantine-de-france.fr/recupmdp/?recp=1">
    </cfif>
    <cfquery name="lect_menu1" datasource="f8w_test"><!--- lecture des menus de l'utilisateur --->
    	Select * from application_menu Where id='#url.id_mnu#'
	</cfquery>
    <cfquery name="lect_etablissement" datasource="f8w_test"><!--- lecture de l'environement (etablissement) --->
        Select * from application_client use index(siret) Where siret = '#lect_user.siret#'
    </cfquery>
<cfelse>
	<cfinclude template="/Query_Header.cfm">
</cfif>
<cfif isdefined("form.modnotif")><!--- notif et sécurité --->
	<cfif #form.mdpver# eq #session.mdp#>
		<cfquery name="maj" datasource="services">
        	Update login set DblAuth='#form.DblAuth#',NotifConnect='#form.NotifConnect#' where id='#lect_user.id#'
        </cfquery>
        <cfset msg2 = "Modifications effectuées avec succès !">
        <cfquery name="lect_user" datasource="services"><!--- lecture de l'utilisateur du site ---->
			Select * from login Where id = '#session.use_id#'
		</cfquery>
	<cfelse>
		<cfset msg2 = "Mot de passe incorrect !">
	</cfif>
</cfif>
<cfif isdefined("form.modifpsw")><!--- modif mot de passe --->
	<cfset nombreChiffre = 0>
    <cfloop from="1" to="#len(form.mdp)#" index="pos">
    	<cfset car = mid(#form.mdp#,#pos#,1)>
        <cfif isnumeric(#car#)>
        	<cfset nombreChiffre = #nombreChiffre# + 1>
        </cfif>
    </cfloop>
	<cf_encrdecr action="e" donnee="#form.oldmdp#">
	<cfif len(#form.mdp#) lt 8>
    	<cfset msg = "Le mot de passe doit faire 8 caractères MINI !">
	<cfelseif (#form.oldmdp# neq #lect_user.pw# and #lect_user.EncryptedPw# eq 0 and isdefined("url.UidRecPsw") is false) or 
	(#cf_encrdecr.donnee# neq #lect_user.pw# and #lect_user.EncryptedPw# eq 1 and isdefined("url.UidRecPsw") is false)>
    	<cfset msg = "Ancien mot de passe incorrect !">
    <cfelseif #form.mdp# neq #form.mdpconf#>
    	<cfset msg = "Nouveau mot de passe différent de la confirmation !">
    <cfelseif #nombreChiffre# eq 0 or #nombreChiffre# eq len(#form.mdp#)>
    	<cfif #nombreChiffre# eq 0>
        	<cfset msg = "Votre mot de passe doit contenir au moins un chiffre !">
    	</cfif>
    	<cfif #nombreChiffre# eq len(#form.mdp#)>
        	<cfset msg = "Votre mot de passe doit contenir au moins une lettre !">
    	</cfif>        
    <cfelse>
    	<cf_encrdecr action="e" donnee="#form.mdp#">
    	<cfquery name="verif" datasource="services" maxrows="1">
        	Select id from login use index(pw) 
            Where pw='#form.mdp#' and login='#lect_user.login#' and EncryptedPw=0 or
            pw='#cf_encrdecr.donnee#' and login='#lect_user.login#' and EncryptedPw=1 Limit 0,1
        </cfquery>
        <cfif #verif.recordcount# eq 0>
            <cfquery name="majlogin" datasource="services">
                Update login set pw=<cfqueryparam value="#form.mdp#" cfsqltype="cf_sql_char">,UidRecPsw='',EncryptedPw=0 
                Where siret='#lect_user.siret#' and login='#lect_user.login#' and NumClient = '#lect_user.NumClient#'
            </cfquery>
            <cfquery name="majclient" datasource="f8w_test">
                Update client set password=<cfqueryparam value="#form.mdp#" cfsqltype="cf_sql_char"> 
                Where siret='#lect_user.siret#' and NumClient = '#lect_user.NumClient#'
            </cfquery>
            <cfif #lect_etablissement.fullweb# eq 0>
				<cfset req = "Update client set password='" & #form.mdp# & "' Where email='" & #lect_user.login# & "'">
                <cfset req1 = replacenocase(#req#,"'","lolocotlolo","all")>
                <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                <cfset dtsoum = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                <cfquery name="synchro" datasource="services">
                    Insert into synchro (ReqSql,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_char">,
                    '#lect_user.siret#','#dtsoum#','F8')
                </cfquery>
			</cfif>            
            <cfset msg1 = "Votre mot de passe a bien été modifié !">
            <!---<cfif isdefined("form.sendmail")>
				<!--- modif 20/01/2016 --->
                <cfquery name="cptmailserv" datasource="mission">
                    Select * from cpt_appel_msg where pour='MSG'
                </cfquery>
                <!--- --->              
                <cfset txt_body = "Bonjour,#chr(13)#Votre mot de passe pour #lect_etablissement.PourEditionMotdepasse# est : #form.mdp# #chr(13)# #chr(13)#Cordialement #chr(13)#L'équipe Cantine-de-France">
                <cfset html_body = "Bonjour,<br>Votre mot de passe pour #lect_etablissement.PourEditionMotdepasse# est : #form.mdp# <br><br>Cordialement <br>L'équipe Cantine-de-France">
                <cfquery name="addspool" datasource="services">
                    Insert into spool_mail (de,pour,sujet,html_body,txt_body,date_crea,siret,leserveur,
                        utilisateur,mailutilisateur,motdepasse) 
                    values('#lect_etablissement.EmailE#','#lect_user.login#',
                    <cfqueryparam value="Votre nouveau mot de passe" cfsqltype="cf_sql_varchar">,
                    <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
                    <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,#now()#,'#lect_etablissement.siret#',
                        '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
                </cfquery>   
            </cfif>--->
		<cfelse>
        	<cfset msg = "Votre nouveau mot de passe est déjà utilisé, modification impossible !">
        </cfif>
    </cfif>
</cfif>
<!---<cfif isdefined("form.modifgsm")>
	<cfif #form.oldmdp# neq #lect_user.pw#>
    	<cfset msg = "Mot de passe incorrect !">
    <cfelseif #form.gsm# neq #form.gsmconf#>
    	<cfset msg = "Nouveau N° de mobile différent de la confirmation !">
    <cfelseif mid(#form.gsm#,1,2) neq "06" and mid(#form.gsm#,1,2) neq "07">
    	<cfset msg = "Nouveau N° de mobile incorrect !">
    <cfelse>
    	<cfquery name="majclient" datasource="f8w_test">
        	Update client set telportable=<cfqueryparam value="#form.gsm#" cfsqltype="cf_sql_char"> 
            Where siret='#lect_user.siret#' and NumClient = '#lect_user.NumClient#'
        </cfquery>
        <cfset req = "Update client set telportable='" & #form.gsm# & "' Where NumClient=" & #lect_user.NumClient#>
        <cfset req1 = replacenocase(#req#,"'","lolocotlolo","all")>
		<cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
		<cfset dtsoum = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
        <cfquery name="synchro" datasource="services">
        	Insert into synchro (ReqSql,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_char">,
            '#lect_user.siret#','#dtsoum#','F8')
        </cfquery>
		<cfset msg1 = "Votre N° de mobile a bien été modifié !">
        
		<cfset txt_body = "Bonjour, " & #lect_client.nom# & " " & #lect_client.prénom# & " a modifié sont N° de mobile, nouveau N° : " & #form.gsm# & #chr(13)#>
        <cfset txt_body = #txt_body# & "Nous vous conseillons vivement d'appeler ce numéro afin de vous assurer que vous joignez bien " & #lect_client.nom# & " " & #lect_client.prénom# & #chr(13)#>
        <cfset txt_body = #txt_body# & "Cordialament, l'équipe Cantine-de-France">
        
		<cfset html_body = "Bonjour, " & #lect_client.nom# & " " & #lect_client.prénom# & " a modifié sont N° de mobile, nouveau N° : " & #form.gsm# & "<br>">
        <cfset html_body = #html_body# & "Nous vous conseillons vivement d'appeler ce numéro afin de vous assurer que vous joignez bien " & #lect_client.nom# & " " & #lect_client.prénom# & "<br>">
        <cfset html_body = #html_body# & "Cordialament, l'équipe Cantine-de-France">        
        <!--- modif 20/01/2016 --->
        <cfquery name="cptmailserv" datasource="mission">
            Select * from cpt_appel_msg where pour='MSG'
        </cfquery>
        <!--- ---> 
        <cfquery name="addspool" datasource="services">
            Insert into spool_mail (de,pour,sujet,html_body,txt_body,date_crea,siret,leserveur,
                        utilisateur,mailutilisateur,motdepasse) 
            values('PasDeReponse@cantine-de-france.fr','#lect_etablissement.EmailE#',
            <cfqueryparam value="Modification de numéro de mobile par un parent !" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,#now()#,'#lect_etablissement.siret#',
                        '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
        </cfquery>        
	</cfif>
</cfif>--->

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Mot de passe & sécurité</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>
    <cfif isdefined("url.UidRecPsw")>
        <body>
            <div class="bg-img"></div>
            <main>
                <div class="row d-flex justify-content-center">
                    <div class="card connexion col-9 col-md-5 col-lg-3 position-absolute">
                        <cfif len(#msg1#)>
                            <a href="/login/?id_mnu=6" class="icon-img mt-4">
                                <img src="/img/message_submit.png" alt="icon message envoyé">
                            </a>
                            <p class="text-red pt-5"><cfoutput>#msg1#</cfoutput></p>
                            <a href="/login/?id_mnu=6" class="link-grey text-center pt-2 pb-3">Retour à la page de connexion</a>
                        <cfelse>
                            <a href="/login/?id_mnu=6" class="icon-img mt-4">
                                <img src="/img/connexion.png" alt="icon de connexion">
                            </a>
                            <p class="text-center text-grey pt-5"><span class="bold">Créer un nouveau mot de passe</span></p>
                            <p class="text-center text-grey mb-1">Veuillez saisir et confirmer votre nouveau mot de passe.</p>
                            <p class="text-center text-red">Le mot de passe doit contenir au moins huit caractères avec au moins un chiffre et une lettre.</p>
                            
                            <cfform name="modpsw" method="post" preservedata="yes">
                                <cfinput type="hidden" name="UidRecPsw" value="#url.UidRecPsw#"><cfinput type="hidden" name="oldmdp" value="">
                                <div class="mb-3">
                                    <label for="mdp" class="form-label">Mon nouveau mot de passe* :</label>
                                    <div class="d-flex">
                                        <cfinput name="mdp" class="form-line" id="mdp" type="text" required="yes" message="Veuillez renseigner votre nouveau mot de passe">
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label for="mdpconf" class="form-label">Confirmation nouveau mot de passe* :</label>
                                    <div class="d-flex">
                                        <cfinput name="mdpconf" class="form-line" id="mdpconf" type="text" required="yes" message="Veuillez confirmer votre nouveau mot de passe">
                                    </div>
                                </div>

                                <cfif len(#msg#)>
                                    <div class="d-flex justify-content-center">
                                        <p class="text-red"><cfoutput>#msg#</cfoutput></p>
                                    </div>
                                <cfelseif len(#msg1#)>
                                    <div class="d-flex justify-content-center">
                                        <p class="text-red"><cfoutput>#msg1#</cfoutput></p>
                                    </div>
                                </cfif>
        
                                <cfif #session.log_demo#>
                                    <div class="d-flex justify-content-center">
                                        <cfinput disabled name="modifpsw" id="modifpsw" type="submit" value="Valider" class="btn btn-primary">
                                    </div>
                                <cfelse>
                                    <div class="d-flex justify-content-center">
                                        <cfinput name="modifpsw" id="modifpsw" type="submit" value="Valider" class="btn btn-primary">
                                    </div>
                                </cfif>
                            </cfform>
                        </cfif>
                        <div class="row text-small">
                            <a class="text-center pe-3 ps-3 link-grey" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                            <button class="text-center d-flex justify-content-center btn-mentionslegales link-grey" onclick="openPopUp()">Mentions légales et confidentialité</button>
                        </div>
                    </div>
                </div>
                <cfinclude template="/login/popupMentionsLegales.cfm">
            </main>
    
            <script type="text/javascript">
                function openPopUp() {
                    document.getElementById("popupMentionsLegales").style.display = "block";
                }
                function closePopUp() {
                    document.getElementById("popupMentionsLegales").style.display = "none";
                }
            </script>
            <cfif isdefined ("Form.go")>
                <cfif !isValid("email", form.email)> 
                    <cfoutput>
                        <script type="text/javascript">
                            alert("Veuillez saisir une adresse mail valide.");
                        </script>
                    </cfoutput>
                </cfif>
            </cfif>
    
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        </body>
    <cfelse>
        <body>
            <div id="bgBlur">
                <!--BANNIERE-->
                <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                    <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                        <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                            <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                        </cfif>
                        <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                    </header>
                <cfelse>
                    <header>
                        <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                    </header>
                </cfif>

                <main class="lg-row">
                    <!-- NAV -->
                    <cfif isdefined("url.UidRecPsw") is false>
                        <cfinclude template="/menu.cfm">
                    </cfif>

                    <div class="col-lg-7 main">
                        <!-- NAME AND DESCRIPTION -->
                        <div>
                            <p><h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>
                                #lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3></p>
                            <p class="description ps-5 pe-5">Vous pouvez modifier ci-dessous votre mot de passe et modifier vos options de notifications.</p>
                        </div>
                        <!-- CONTENT -->
                        <cfinclude template="/changemdp/changemdp.cfm">
                    </div>

                        <!-- SECTION RIGHT -->
                    <div class="col-lg-3 section-right">
                        <div class="mt-5">
                            <cfinclude template="/home/calendar.cfm">
                        </div>
                    </div>
                </main>
                <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                    <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                    <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions légales et confidentialité</a>
                    <p class="m-0 pe-3 ps-3">N&deg; de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
                </footer>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
            
            <!-- POP-UP MENU BURGER -->
            <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" onclick="openNav()" alt="bouton menu déroulant"></button>
            <cfinclude template="/menuBurger.cfm">

            <script type="text/javascript"> //Script gestion menu burger
                function openNav() {
                    document.getElementById("navMobileItems").style.display= "block";
                    document.getElementById("navMobileLabel").style.display= "none";
                    const background = document.getElementById("bgBlur");
                    background.className = "bg-blur";
                    background.addEventListener("click", closeNav);
                }
                function closeNav() {
                    document.getElementById("navMobileItems").style.display= "none";
                    document.getElementById("bgBlur").classList.remove("bg-blur");
                    document.getElementById("navMobileLabel").style.display= "block";
                }
                function MM_jumpMenu(targ,selObj,restore) { 
                    eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
                    if (restore) selObj.selectedIndex=1;
                }
                function val() {
                    document.getElementById("pubdoc").disabled = true;
                    document.docdemande.submit();
                }
            </script>
        </body>

    </cfif>
</html>