﻿<cfquery name="enfant" datasource="f8w">
	Select Siret,NumClientI from enfant where id='#url.idenfant#'
</cfquery>
<cfquery name="prestation" datasource="f8w">
	Select NumPrestation from prestation where id='#url.id_prestation#'
</cfquery>
<cfquery name="prestation_prepaye_portefeuille_presta" datasource="f8w">
	Select id_ent from prestation_prepaye_portefeuille_presta use index(siret_NumPrestation) 
    where siret='#enfant.siret#' and NuMprestation='#prestation.NumPrestation#'
</cfquery>
<cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
	Select id from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_Numclient) 
    where siret='#enfant.siret#' and id_ent_portefeuille='#prestation_prepaye_portefeuille_presta.id_ent#' 
    and numclient='#enfant.numclienti#'
</cfquery>
<cfset Req = querynew("date,pourle,enfant,action,QP,QA,QN,fp,prest")>
<cfquery name="prestation_prepaye_histo_portefeuille" datasource="f8w">
	Select * from prestation_prepaye_histo_portefeuille use index(siret_numclient) 
    where siret='#enfant.siret#' and numclient='#enfant.numclientI#' 
    and (id_portefeuille='#prestation_prepaye_portefeuille_presta.id_ent#' or id_portefeuille='#prestation_prepaye_portefeuille.id#')
    order by id Desc
</cfquery>
<cfif #prestation_prepaye_histo_portefeuille.recordcount#><!--- portefeuille --->
	<cfloop query="prestation_prepaye_histo_portefeuille">
    	<cfset r = queryaddrow(Req)>
        <cfquery name="enfant1" datasource="f8w">
        	Select prénom from enfant use index(siret_numenfant) 
            where siret='#enfant.siret#' and numenfant='#prestation_prepaye_histo_portefeuille.numenfant#'
    	</cfquery>
    	<cfset ladate = lsdateformat(#prestation_prepaye_histo_portefeuille.maj#,"dd/MM/YYYY") & " " & lstimeformat(#prestation_prepaye_histo_portefeuille.maj#,"HH:MM:SS")>
    	<cfset r = querysetcell(Req,"date",#ladate#)>
    	<cfset r = querysetcell(Req,"pourle",#prestation_prepaye_histo_portefeuille.pourle#)>
    	<cfset r = querysetcell(Req,"enfant",#enfant1.prénom#)>
    	<cfset action = "">
		<cfif #prestation_prepaye_histo_portefeuille.NumEncaissement# gt 0>
        	<cfset action = "Achat">
    	<cfelse>
        	<cfswitch expression="#prestation_prepaye_histo_portefeuille.PresentAbsent#">
        		<cfcase value="0">
					<cfif #prestation_prepaye_histo_portefeuille.Qte_Ajouté# lte 0>
						<cfset action = "Inscr.">
                    <cfelse>
                    	<cfset action = "Désinscr.">
                    </cfif>
                </cfcase>
                <cfcase value="-1">
                	<cfset action = "Non inscrit">
                </cfcase>
        		<cfcase value="1">
                	<cfset action = "Abs. Just.">
                </cfcase>
        		<cfcase value="2">
                	<cfset action = "Abs. Injust.">
                </cfcase>
        		<cfcase value="3">
                	<cfset action = "Pres. Just.">
                </cfcase>
        		<cfcase value="4">
                	<cfset action = "Pres. Injust.">
                </cfcase>
                <cfdefaultcase>
                	<cfset action = #prestation_prepaye_histo_portefeuille.PresentAbsent#>
                </cfdefaultcase>
        	</cfswitch>
        </cfif>
        <!---<cfif #prestation_prepaye_histo_portefeuille.numprestation# eq 0 and #prestation_prepaye_histo_portefeuille.Qte_Ajouté# lt 0>
        	<cfset action = "Remboursement">
		<cfelseif len(#prestation_prepaye_histo_portefeuille.libAction#) gt 0>
        	<cfset action = #prestation_prepaye_histo_portefeuille.libAction#>
        </cfif>--->
        <cfif len(#prestation_prepaye_histo_portefeuille.libAction#) gt 0>
        	<cfset action = #prestation_prepaye_histo_portefeuille.libAction#>
        </cfif>
        <cfset r = querysetcell(Req,"action",#action#)>
        <cfset r = querysetcell(Req,"QP",#prestation_prepaye_histo_portefeuille.Qte_Precedante#)>
        <cfset r = querysetcell(Req,"QA",#prestation_prepaye_histo_portefeuille.Qte_Ajouté#)>
        <cfset r = querysetcell(Req,"QN",#prestation_prepaye_histo_portefeuille.Qte_Nouvelle#)>
        <cfif #prestation_prepaye_histo_portefeuille.FaitPar# eq 0>
        	<cfset r = querysetcell(Req,"fp","Mairie")>
        <cfelse>
        	<cfset r = querysetcell(Req,"fp","Parent")>
        </cfif>
        <cfquery name="prest" datasource="f8w">
        	Select désignation from prestation use index(siret_numprestation) where siret='#enfant.siret#' 
            and numprestation='#prestation_prepaye_histo_portefeuille.numprestation#'
        </cfquery>
        <cfset r = querysetcell(Req,"prest",#prest.désignation#)> 
    </cfloop>
<cfelse><!--- ticket --->

</cfif>
<table width="860" border="0" cellspacing="1" cellpadding="0">
  <cfset td = "##CCCCCC">
  <cfoutput query="Req">
      <cfif #td# eq "##CCCCCC">
      	<cfset td = "##999999">
      <cfelse>
      	<cfset td = "##CCCCCC">
      </cfif>
      <tr>
        <td bgcolor="#td#" width="112"><font size="-1">#Req.date#</font></td>
        <td bgcolor="#td#" width="205"><font size="-1">#Req.prest#</font></td>
        <td bgcolor="#td#" width="70" align="center"><font size="-1">#Req.Pourle#</font></td>
        <td bgcolor="#td#" width="200"><font size="-1">#Req.Enfant#</font></td>
        <td bgcolor="#td#" width="60"><font size="-1">#Action#</font></td>
        <td bgcolor="#td#" width="49" align="right"><font size="-1">#QP#</font>&nbsp;&nbsp;</td>
        <td bgcolor="#td#" width="49" align="right"><font size="-1">#QA#</font>&nbsp;&nbsp;</td>
        <td bgcolor="#td#" width="49" align="right"><font size="-1">#QN#</font>&nbsp;&nbsp;</td>
        <td bgcolor="#td#" width="56" align="center">#fp#</td>
      </tr>
	</cfoutput>
</table>