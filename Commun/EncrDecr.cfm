﻿<cfparam name="Attributes.Action" default="E">
<cfparam name="Attributes.Donnee" default="">
<cfset Caller.cf_EncrDecr.Message = "">
<cfset Caller.cf_EncrDecr.Donnee = "">
<cfif len(#Attributes.Donnee#) eq 0>
	<cfif #Attributes.Action# eq "E" or #Attributes.Action# eq "e">
		<cfset Caller.cf_EncrDecr.Message = "Aucune donnée à encrypter !!">
	<cfelse>
		<cfset Caller.cf_EncrDecr.Message = "Aucune donnée à décrypter !!">    
    </cfif>
<cfelse>
	<cfset key = "OHAV5rPqAmYJCGqRnH7zPw==">
	<cfif #Attributes.Action# eq "E" or #Attributes.Action# eq "e">
    	<cftry>
			<cfset DonneeEncr = encrypt(#Attributes.Donnee#, #key#, "AES","Base64")>
    		<cfset Caller.cf_EncrDecr.Donnee = trim(#DonneeEncr#)>
            <cfcatch><cfset Caller.cf_EncrDecr.Message = "KO Erreur au cryptage !"></cfcatch>
    	</cftry>
        <cfif len(#Caller.cf_EncrDecr.Message#) eq 0>
        	<cfset Caller.cf_EncrDecr.Message = "OK">
        </cfif>
    <cfelse>
    	<cftry>
			<cfset DonneeDecr = decrypt(#Attributes.Donnee#, #key#, "AES","Base64")>
    		<cfset Caller.cf_EncrDecr.Donnee = trim(#DonneeDecr#)>
            <cfcatch><cfset Caller.cf_EncrDecr.Message = "KO Erreur au décryptage !"></cfcatch>
    	</cftry>
        <cfif len(#Caller.cf_EncrDecr.Message#) eq 0>
        	<cfset Caller.cf_EncrDecr.Message = "OK">
        </cfif>
    </cfif>
</cfif>