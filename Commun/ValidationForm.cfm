﻿<cfparam name="Attributes.BaseDeDonnee" default=""><cfparam name="Attributes.Table" default=""><cfparam name="Attributes.Message" default=""><cfparam name="Attributes.form" default="">
<cfset Caller.cf_ValidationForm.Message = "">
<cfif len(#Attributes.BaseDeDonnee#) eq 0>
	<cfset Caller.cf_ValidationForm.Message = "Une base de donnée doit être passée en paramètres...!">
<cfelseif len(#Attributes.Table#) eq 0>
	<cfset Caller.cf_ValidationForm.Message = "Une table doit être passée en paramètres...!">
<cfelseif isdefined("Attributes.Form") is false >
	<cfset Caller.cf_ValidationForm.Message = "Une Form doit être passée en paramètres...!">
<cfelse>
    <cfset ficdump = "C:\TempWeb\" & createuuid() & ".txt">
    <cfdump var="#Attributes.form#" output="#ficdump#">
    <cfdbinfo datasource="#Attributes.BaseDeDonnee#" name="tableselect" type="columns" table="#Attributes.Table#"></cfdbinfo>
	<cffile action="read" file="#ficdump#" variable="dumpform">
	<cfloop list="#dumpform#" delimiters="#chr(10)##chr(13)#" index="dumpformligne">
    	<cfif len(#Caller.cf_ValidationForm.Message#) eq 0>
			<cfif findnocase("FIELDNAMES:",#dumpformligne#)>
            <cfelseif findnocase(":",#dumpformligne#)>
                <cfset Colonne = mid(#dumpformligne#,1,#findnocase(":",dumpformligne)# - 1)>
                <cfset Donnée = mid(#dumpformligne#,#findnocase(":",dumpformligne)# +1,len(#dumpformligne#))>
                <cfset Donnée = trim(#Donnée#)>
                <cfquery dbtype="query" name="InteroStructure"><!--- recup info sql de la colonne --->
                    Select * from tableselect Where UPPER(column_name)='#Colonne#'
                </cfquery>
                <cfif #InteroStructure.recordcount#>
                    <cfswitch expression="#InteroStructure.type_name#">
                        <cfcase value="VARCHAR,CHAR" delimiters=",">
                        	<cfif #Donnée# eq "[empty string]">
                            	<cfif #InteroStructure.is_nullable# eq "NO">
                                	<cfset Caller.cf_ValidationForm.Message = 'Les champs avec une <font color="##FF0000">*</font> sont obligatoires...!'>
                                <cfelse>
                                	<cfset "Attributes.form.#InteroStructure.column_name#" = "">
                                </cfif>
                            </cfif>
                        </cfcase>
                        <cfcase value="INT">
                            <cfif #Donnée# eq "">
                                <cfif #InteroStructure.is_nullable# eq "NO">
                                	<cfset Caller.cf_ValidationForm.Message = 'Les champs avec une <font color="##FF0000">*</font> sont obligatoires...!'>
                                <cfelse>
									<cfset "Attributes.form.#InteroStructure.column_name#" = 0>
                            	</cfif>
                            <cfelseif isnumeric(#Donnée#) is false>
                            	<cfif #InteroStructure.is_nullable# eq "NO">
                            		<cfset Caller.cf_ValidationForm.Message = "La donnée saisie pour " & #InteroStructure.column_name# & " doit être numérique...!">
                                <cfelse>
                                	<cfset "Attributes.form.#InteroStructure.column_name#" = 0>
                                </cfif>    
                            </cfif>    
                        </cfcase>
                        <cfcase value="DECIMAL">
                            <cfif #Donnée# eq "">
                                <cfif #InteroStructure.is_nullable# eq "NO">
                                	<cfset Caller.cf_ValidationForm.Message = 'Les champs avec une <font color="##FF0000">*</font> sont obligatoires...!'>
                                <cfelse>
									<cfset "Attributes.form.#InteroStructure.column_name#" = 0>
                            	</cfif>
							<cfelseif findnocase(",",#Donnée#)>
                                <cfset "Attributes.form.#InteroStructure.column_name#" = replace(#Donnée#,",",".","all")>
                            <cfelseif isnumeric(#Donnée#) is false>
                            	<cfset Caller.cf_ValidationForm.Message = "La donnée saisie pour " & #InteroStructure.column_name# & " doit être numérique...!">
                            </cfif>
                        </cfcase>
                        <cfcase value="DATETIME">
                        	<cfif #Donnée# eq "[empty string]">
                            	<cfset Caller.cf_ValidationForm.Message = 'Saisir une date svp...!'>
							<cfelseif findnocase("/",#Donnée#)>
                                <cfset err = "non">
                                <cftry>
                                	<cfset ladate = createdate(#mid(Donnée,7,4)#,#mid(Donnée,4,2)#,#mid(Donnée,1,2)#)>
                                	<cfcatch><cfset err = "oui"></cfcatch>
                            	</cftry>
                                <cfif #err# eq "non">
                                	<cfset "Attributes.form.#InteroStructure.column_name#" = #ladate#>
                                <cfelse>
                                	<cfset Caller.cf_ValidationForm.Message = 'Saisir une date svp valide...!'>
                                </cfif>
                            </cfif>
                        </cfcase>  
                        <cfdefaultcase></cfdefaultcase>
                    </cfswitch>                
                </cfif>
            </cfif>
		<cfelse>
        	<cfbreak>
        </cfif>
    </cfloop>
	<cffile action="delete" file="#ficdump#">
	<cfif len(#Caller.cf_ValidationForm.Message#) eq 0>
    	<cfset Caller.cf_ValidationForm.Message = "OK">
    </cfif>
	<cfset Caller.cf_ValidationForm.Form = #Attributes.Form#>
</cfif>