﻿<cfparam name="Attributes.TxtDate">
<cfset cpt = 0>
<cfset Caller.cf_datetxtennumerique.retour = "">
<cfset journum = "">
<cfset moisnum = "">
<cfloop list="#Attributes.TxtDate#" delimiters=" " index="Data">
	<cfset cpt = #cpt# + 1>
	<cfswitch expression="#cpt#">
		<cfcase value="1"><!--- jour txt---></cfcase>
		<cfcase value="2"><!--- jour num --->
			<cfset journum = #data#>
		</cfcase>
        <cfcase value="3"><!--- mois --->
			<cfswitch expression="#Data#">
    			<cfcase value="janvier"><cfset moisnum = "01"></cfcase>
    			<cfcase value="février"><cfset moisnum = "02"></cfcase>
                <cfcase value="mars"><cfset moisnum = "03"></cfcase>
                <cfcase value="avril"><cfset moisnum = "04"></cfcase>
                <cfcase value="mai"><cfset moisnum = "05"></cfcase>
                <cfcase value="juin"><cfset moisnum = "06"></cfcase>
                <cfcase value="juillet"><cfset moisnum = "07"></cfcase>
                <cfcase value="août"><cfset moisnum = "08"></cfcase>
                <cfcase value="septembre"><cfset moisnum = "09"></cfcase>
                <cfcase value="octobre"><cfset moisnum = "10"></cfcase>
                <cfcase value="novembre"><cfset moisnum = "11"></cfcase>
                <cfcase value="décembre"><cfset moisnum = "12"></cfcase>
    		</cfswitch>
    	</cfcase>
        <cfdefaultcase></cfdefaultcase>
    </cfswitch>
</cfloop>
<cfset Caller.cf_datetxtennumerique.retour =  #journum# & "/" & #moisnum# & "/">