﻿<cfparam name="Attributes.texte">
<cfset Retour = "">
<cfloop from="1" to="#len(Attributes.texte)#" index="idx">
    <cfset car = mid(#Attributes.texte#,#idx#,1)>
    <cfif #car# eq ",">
        <cfset car = " ">
    </cfif>
    <cfswitch expression="#car#">  
        <cfcase delimiters="," value="ä,à,â">
            <cfset Retour = #Retour# & "a">    
        </cfcase>    
        <cfcase delimiters="," value="é,è,ê,ë">
            <cfset Retour = #Retour# & "e">    
        </cfcase>
        <cfcase delimiters="," value="î,ï">
            <cfset Retour = #Retour# & "i">    
        </cfcase>
        <cfcase delimiters="," value="û">
            <cfset Retour = #Retour# & "u">    
        </cfcase> 
        <cfcase delimiters="," value="ô,ö">
            <cfset Retour = #Retour# & "o">    
        </cfcase>     
        <cfcase delimiters="," value="(,),?,+,*,@,;,!,'">
            <cfset Retour = #Retour# & " ">    
        </cfcase>
        <!---<cfcase delimiters="," value="0,1,2,3,4,5,6,7,8,9">
            <cfset Retour = #Retour# & #car#>    
        </cfcase>            
        <cfcase delimiters="," value="b,c,d,f,g,h,j,k,l,m,n,p,q,r,s,t,v,w,x,y,z">
            <cfset Retour = #Retour# & #car#>    
        </cfcase>
        <cfcase value=" ">
            <cfset Retour = #Retour# & #car#>    
        </cfcase>
        <cfdefaultcase><cfset Retour = #Retour# & " "></cfdefaultcase>--->
        <cfdefaultcase><cfset Retour = #Retour# & #car#></cfdefaultcase>    
    </cfswitch>
</cfloop>
<cfset Caller.cf_ValideTexteSms.Retour = #Retour#>    