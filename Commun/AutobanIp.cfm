﻿<cfparam name="Attributes.ip" default="">
<cfparam name="Attributes.url" default="">
<cfparam name="Attributes.action" default="">
<cfparam name="Attributes.login" default="">
<cfif #Attributes.action# eq "LoginOK">
	<cfquery name="majautobanip" datasource="mission">
		Update ipautoban set nombrecx=0, login='#Attributes.login#' where ip='#Attributes.ip#' and url='#Attributes.url#'
	</cfquery>
<cfelseif #Attributes.action# eq "LoginKO">
    <cfquery name="autobanip" datasource="mission">
        Select * from ipautoban where ip='#Attributes.ip#' and url='#Attributes.url#'
    </cfquery>
    <cfif #autobanip.recordcount#>
    	<cfset NewNombreCx = #autobanip.NombreCx# + 1>
        <cfquery name="maj" datasource="mission">
        	Update ipautoban set NombreCx='#NewNombreCx#', login='#Attributes.login#' 
            where ip='#Attributes.ip#' and url='#Attributes.url#'
        </cfquery>
    <cfelse>
		<cfquery name="add" datasource="mission">
        	Insert into ipautoban (ip,url,login,NombreCx) Values ('#Attributes.ip#','#Attributes.url#','#Attributes.login#',1)
        </cfquery>
	</cfif>
<cfelseif #Attributes.action# eq "LoginPossibleOuPas">
	<cfquery name="autobanip" datasource="mission">
        Select * from ipautoban where ip='#Attributes.ip#' and url='#Attributes.url#'
    </cfquery>
	<cfif #autobanip.NombreCx# gte 6><!--- 6 tentative ou plus en erreur --->
		<cfif datediff("h",#autobanip.Dateheure#,now()) lte 1><!--- blocage pendant 1 heure --->
            <cfset reste = 60 - datediff("n",#autobanip.Dateheure#,now())>
			<cfset msg1 = "Suite à trop de tentative de connexion en erreur, votre accès est bloqué pendant encore " & #reste#  & " minutes. Après ce délai, cliquez sur j'ai oublier mon mot de passe.">
			<cfset msg1 = urlencodedformat(#msg1#,"utf-8")>
    		<cfif #Attributes.url# eq "parent.cantine-de-france.fr">
            	<cflocation url="https://parent.cantine-de-france.fr/login/?id_mnu=6&msg1=#msg1#" addtoken="no"> 
            </cfif>
			<cfoutput>Merci de vous rapprocher de votre gestionnaire Cantine de france (Mairie ou Association) svp....!</cfoutput>
            <cfabort>
        <cfelse>
        	<cfquery name="majautobanip" datasource="mission">
                Update ipautoban set nombrecx=0 where ip='#Attributes.ip#' and url='#Attributes.url#'
            </cfquery>
        </cfif>
	</cfif>
</cfif>