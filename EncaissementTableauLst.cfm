<link href="../../css/cdf.css" rel="stylesheet" type="text/css">
<cfif isdefined("numclient")>
	<cfset CDF_Finance = createobject("component","commun.CDF_Finance")><!--- declaration de l'objet module --->
	<cfset NewSolde = CDF_Finance.SoldeClient(#siret#,#numetablissement#,val(#idparent#))>
	<cfset lstencaissement = querynew("dtstamp,date,numero,facture,paye,info,idencaissement,numrole,numrole1,numrole2,numrole3,idfacture","varchar,varchar,varchar,decimal,decimal,varchar,integer,integer,integer,integer,integer,integer")>
<!---    <cfquery name="encaissement" datasource="f8w">
		Select * from encaissement use index(siret_NumClient) 
        Where siret='#siret#' and NumClient='#NumClient#' and MoyenPaiement<>3 and nume='#numetablissement#'
	</cfquery>
--->	

	<!--- si il y a des presta prepaye ont ne poura pas considéré qu'un solde à 0 ou négatif veux dire que toutes les factures sont soldées --->
    <cfquery name="prestationprepaye" datasource="f8w" maxrows="1">
    	Select id from prestation use index(siret) Where siret='#siret#' and Ticket=1 and del=0 limit 0,1
    </cfquery>
    <cfquery name="lect_etablissement_en_cour" datasource="f8w">
    	Select * from application_client use index(siret) where siret like '#siret#%' and NumEtablissement='#numetablissement#'
    </cfquery>
    <cfset CbTipi = "non">
    <cfquery name="lect_client" datasource="f8w">
    	Select * from client where id='#idparent#'
    </cfquery>

    <!---<cfquery name="autreetabaveccb" datasource="f8w">
        Select * from application_client where siret like '#lect_etablissement_en_cour.siret#%' 
        and (CBBanque>0 or CBTipi=1)
    </cfquery>--->
    
	<cfif #lect_etablissement_en_cour.CBTipi# eq 1 and #lect_client.ModePaiement# neq 1 or #lect_etablissement_en_cour.CBBanque# gt 0 and #lect_client.ModePaiement# neq 1 <!---or #autreetabaveccb.recordcount#---> >
        <!--- CB TIPI --->
        <cfset CbTipi = "oui">
    </cfif>

    <!---<cfif #tipisite.CBTipi# eq 1 and #lect_client.ModePaiement# neq 1 or #tipisite.CBBanque# gt 0 and #lect_client.ModePaiement# neq 1 or #autreetabaveccb.recordcount# >
        <!--- CB TIPI --->
        <cflocation url="/facture/cbtipi/?id_mnu=12" addtoken="no">
    </cfif>--->

	<cfif #siret# neq #siretparent# and len(#siretparent#)>
        <cfquery name="encaissement" datasource="f8w">
            Select * from encaissement use index(siret_IdClient) 
            Where siret='#siret#' and IdClient='#idparent#' and nume='#numetablissement#'
        </cfquery>    
        <cfloop query="encaissement">
            <cfset r = queryaddrow(lstencaissement)>
            <cfset dtstamp = lsdateformat(#encaissement.DatePaiement#,"YYYYMMdd") & 0>
            <cfset r = querysetcell(lstencaissement,"dtstamp",#dtstamp#)>
            <cfset date = lsdateformat(#encaissement.DatePaiement#,"dd/MM/YYYY")>
            <cfset r = querysetcell(lstencaissement,"date",#date#)>
            <cfset r = querysetcell(lstencaissement,"numero",#encaissement.numencaissement#)>
            <cfset r = querysetcell(lstencaissement,"facture","")>
            <cfset r = querysetcell(lstencaissement,"paye",#encaissement.montantpayé#)>
            <cfquery name="moyenpaiement" datasource="f8w">
                Select * from moyenpaiement use index(siret_NoMoyen) Where siret='#siret#' and NoMoyen='#encaissement.moyenpaiement#'
            </cfquery>
            <cfset info = #moyenpaiement.désignation# & " " & #encaissement.banquepièce# & " " & #encaissement.nopièce# >
            <cfif #encaissement.numprestation# gt 0>
                <cfquery name="presta" datasource="f8w">
                    Select désignation from prestation use index(siret_numprestation) Where siret='#siret#' 
                    and numprestation = '#encaissement.numprestation#'
                </cfquery>
                <cfset info = #info# & " ticket " & #presta.désignation#>    
            <cfelseif #encaissement.id_prestation_prepaye_portefeuille_ent# gt 0>
                <cfquery name="presta" datasource="f8w">
                    Select designation from prestation_prepaye_portefeuille_ent 
                    Where id='#encaissement.id_prestation_prepaye_portefeuille_ent#'
                </cfquery>
                <cfset info = #info# & " ticket " & #presta.designation#>            
            </cfif>
            <cfset r = querysetcell(lstencaissement,"info",#info#)>
            <cfset r = querysetcell(lstencaissement,"idencaissement",#encaissement.id#)>
            <cfset r = querysetcell(lstencaissement,"idfacture",0)>
            <cfset r = querysetcell(lstencaissement,"numrole",#encaissement.numrole2#)>
            <cfset r = querysetcell(lstencaissement,"numrole1",#encaissement.numrole21#)>
            <cfset r = querysetcell(lstencaissement,"numrole2",#encaissement.numrole22#)>
            <cfset r = querysetcell(lstencaissement,"numrole3",#encaissement.numrole23#)>
        </cfloop>    
    <cfelse>
        <cfquery name="encaissement" datasource="f8w">
            Select * from encaissement use index(siret_NumClient) 
            Where siret='#siret#' and NumClient='#NumClient#' and nume='#numetablissement#'
        </cfquery>    
        <cfloop query="encaissement">
            <cfset r = queryaddrow(lstencaissement)>
            <cfset dtstamp = lsdateformat(#encaissement.DatePaiement#,"YYYYMMdd") & 0>
            <cfset r = querysetcell(lstencaissement,"dtstamp",#dtstamp#)>
            <cfset date = lsdateformat(#encaissement.DatePaiement#,"dd/MM/YYYY")>
            <cfset r = querysetcell(lstencaissement,"date",#date#)>
            <cfset r = querysetcell(lstencaissement,"numero",#encaissement.numencaissement#)>
            <cfset r = querysetcell(lstencaissement,"facture","")>
            <cfset r = querysetcell(lstencaissement,"paye",#encaissement.montantpayé#)>
            <cfquery name="moyenpaiement" datasource="f8w">
                Select * from moyenpaiement use index(siret_NoMoyen) 
                Where siret='#siret#' and NoMoyen='#encaissement.moyenpaiement#'
            </cfquery>
            <cfset info = #moyenpaiement.désignation# & " " & #encaissement.banquepièce# & " " & #encaissement.nopièce# >
            <cfif #encaissement.numprestation# gt 0>
                <cfquery name="presta" datasource="f8w">
                    Select désignation from prestation use index(siret_numprestation) Where siret='#siret#' 
                    and numprestation = '#encaissement.numprestation#'
                </cfquery>
                <cfset info = #info# & " ticket " & #presta.désignation#>    
            <cfelseif #encaissement.id_prestation_prepaye_portefeuille_ent# gt 0>
                <cfquery name="presta" datasource="f8w">
                    Select designation from prestation_prepaye_portefeuille_ent 
                    Where id='#encaissement.id_prestation_prepaye_portefeuille_ent#'
                </cfquery>
                <cfset info = #info# & " ticket " & #presta.designation#>            
            </cfif>
            <cfset r = querysetcell(lstencaissement,"info",#info#)>
            <cfset r = querysetcell(lstencaissement,"idencaissement",#encaissement.id#)>
            <cfset r = querysetcell(lstencaissement,"idfacture",0)>
            <cfset r = querysetcell(lstencaissement,"numrole",#encaissement.numrole2#)>
            <cfset r = querysetcell(lstencaissement,"numrole1",#encaissement.numrole21#)>
            <cfset r = querysetcell(lstencaissement,"numrole2",#encaissement.numrole22#)>
            <cfset r = querysetcell(lstencaissement,"numrole3",#encaissement.numrole23#)>
        </cfloop>
	</cfif>	
    <!---<cfquery name="facture" datasource="f8w">
    	Select * from facture use index(siret_numclient) 
        Where siret='#siret#' and NumClient='#NumClient#' and modepaiement=0 and nume='#numetablissement#'
    </cfquery>--->
    <cfif #siret# neq #siretparent# and len(#siretparent#)>
        <cfquery name="facture" datasource="f8w">
            Select * from facture use index(siret_IdClient) 
            Where siret='#siret#' and IdClient='#idparent#' and nume='#numetablissement#' and del=0
        </cfquery>
        <cfloop query="facture">
    <!---		<cfquery name="role" datasource="f8w">
                Select stitrefacture,datefacture from role use index(siret_NumRole) Where siret='#siret#' 
                and NumRole='#facture.NumRole#' and TypeRole<>2
            </cfquery>
    --->	<cfquery name="role" datasource="f8w">
                Select stitrefacture,datefacture from role use index(siret_NumRole) Where siret='#siret#' 
                and NumRole='#facture.NumRole#' and typerole<>2 and del=0
            </cfquery>        
            <cfif #role.recordcount#>
                <cfset r = queryaddrow(lstencaissement)>
                <cfset datestp1 = mid(#role.datefacture#,7,4) & mid(#role.datefacture#,4,2) & mid(#role.datefacture#,1,2) & 1>
                <cfset r = querysetcell(lstencaissement,"dtstamp",#datestp1#)>
                <cfset r = querysetcell(lstencaissement,"date",#role.datefacture#)>
                <cfset numero = mid(#facture.norole#,1,4) & "/" & mid(#facture.norole#,6,2) & " - " & #facture.nofacture#>
                <cfset r = querysetcell(lstencaissement,"numero",#numero#)>
                <cfset r = querysetcell(lstencaissement,"facture",#facture.totalttc#)>
                <cfset r = querysetcell(lstencaissement,"paye","")>
                <cfset r = querysetcell(lstencaissement,"info",#role.stitrefacture#)>
                <cfset r = querysetcell(lstencaissement,"idencaissement",0)>  
                <cfset r = querysetcell(lstencaissement,"idfacture",#facture.id#)>
                <cfset r = querysetcell(lstencaissement,"numrole",#facture.numrole#)>
                <cfset r = querysetcell(lstencaissement,"numrole1",0)>
            	<cfset r = querysetcell(lstencaissement,"numrole2",0)>
            	<cfset r = querysetcell(lstencaissement,"numrole3",0)>  
            </cfif>
        </cfloop>    
    <cfelse>
        <cfquery name="facture" datasource="f8w">
            Select * from facture use index(siret_numclient) 
            Where siret='#siret#' and NumClient='#NumClient#' and nume='#numetablissement#' and del=0
        </cfquery>
        <cfloop query="facture">
    <!---		<cfquery name="role" datasource="f8w">
                Select stitrefacture,datefacture from role use index(siret_NumRole) Where siret='#siret#' 
                and NumRole='#facture.NumRole#' and TypeRole<>2
            </cfquery>
    --->	<cfquery name="role" datasource="f8w">
                Select stitrefacture,datefacture from role use index(siret_NumRole) Where siret='#siret#' 
                and NumRole='#facture.NumRole#' and typerole<>2 and del=0
            </cfquery>        
            <cfif #role.recordcount#>
                <cfset r = queryaddrow(lstencaissement)>
                <cfset datestp1 = mid(#role.datefacture#,7,4) & mid(#role.datefacture#,4,2) & mid(#role.datefacture#,1,2) & 1>
                <cfset r = querysetcell(lstencaissement,"dtstamp",#datestp1#)>
                <cfset r = querysetcell(lstencaissement,"date",#role.datefacture#)>
                <cfset numero = mid(#facture.norole#,1,4) & "/" & mid(#facture.norole#,6,2) & " - " & #facture.nofacture#>
                <cfset r = querysetcell(lstencaissement,"numero",#numero#)>
                <cfset r = querysetcell(lstencaissement,"facture",#facture.totalttc#)>
                <cfset r = querysetcell(lstencaissement,"paye","")>
                <cfset r = querysetcell(lstencaissement,"info",#role.stitrefacture#)>
                <cfset r = querysetcell(lstencaissement,"idencaissement",0)>  
                <cfset r = querysetcell(lstencaissement,"idfacture",#facture.id#)>
                <cfset r = querysetcell(lstencaissement,"numrole",#facture.numrole#)>
                <cfset r = querysetcell(lstencaissement,"numrole1",0)>
            	<cfset r = querysetcell(lstencaissement,"numrole2",0)>
            	<cfset r = querysetcell(lstencaissement,"numrole3",0)>
            </cfif>
        </cfloop>
	</cfif>
    <cfquery name="lst" dbtype="query">
    	select * from lstencaissement order by dtstamp DESC
    </cfquery>
    <cfif findnocase("gestion.",#cgi.SERVER_NAME#)><!--- espace gestion --->
    	<cfset espace = "gestion">
    <cfelse><!--- espace parent --->
    	<cfset espace = "parent">
    </cfif>
    
    <script>
        function changeCouleur(ligne)
        {
            ligne.bgColor = '#dbf1fd';
        }
        
        function remetCouleur(ligne)
        {
            ligne.bgColor = '#FFFFFF';
        }
    </script>

    <cfif #lst.recordcount# eq 0>
        <cfloop from="1" to="6" index="idx">
            <tr bgcolor="#FFFFFF">
                <td width="74" class="libelle">&nbsp;</td>
                <td width="98" class="libelle"></td>
                <td width="70"></td>
                <td width="126"></td>
                <td width="492" class="libellemin"></td>
            </tr>
        </cfloop>
    <cfelse>
        <cfif #NewSolde.Solde# gt 0>
            <cfset SoldeRestant = #NewSolde.Solde#>
        <cfelse>
            <cfset SoldeRestant = 0>
        </cfif>

        <cfoutput query="lst">
            <cfset n1 = #lst.currentrow# - 6><!--- pour positionner la ligne séléctionné au réaffichage --->








            <cfif #espace# eq "gestion"><!--- espace gestion ICI Faire condition ---> 
                <cfif #lst.idencaissement# gt 0>
                    <cfif #lst.idencaissement# neq #id#>
                        <tr bgcolor="##08A2EC">
                            <td width="74" class="libelle"><a name="n#lst.currentrow#"></a><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent"><b>#lst.date#</b></a></td>
                            <td width="98" class="libelle"><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent"><b>#lst.numero#</b></a></td>
                            <td width="100"></td>
                            <td width="61"><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent"><b>#lsnumberformat(lst.paye,"9999.99")#</b></a></td>
                            <td width="527" class="libellemin"><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent"><b>#lst.info#</b></a></td>
                        </tr>                    
                    <cfelse>
                        <tr bgcolor="##FFFFFF" onMouseOver="changeCouleur(this);" onMouseOut="remetCouleur(this);">
                            <td width="74" class="libelle"><a name="n#lst.currentrow#"></a><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent">#lst.date#</a></td>
                            <td width="98" class="libelle"><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent">#lst.numero#</a></td>
                            <td width="100">2222222222222222222222222</td>
                            <td width="61"><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent">#lsnumberformat(lst.paye,"9999.99")#</a></td>
                            <td width="527" class="libellemin"><a style="text-decoration:none" href="/app/?idmnu=109&id=#lst.idencaissement#&numetablissement=#numetablissement#&siret=#siret#&idparent=#idparent#&numparent=#numparent#&n=#n1#" target="_parent">#lst.info#</a><a name="n#lst.idencaissement#"></a></td>
                        </tr>
                    </cfif>
                <cfelse>
                    <cfquery name="totreglepourfact" dbtype="query">
                        Select sum(paye) as totpayepourfact from lst 
                        where numrole=#lst.numrole# or numrole1=#lst.numrole# or numrole2=#lst.numrole# or numrole3=#lst.numrole#
                    </cfquery>
                    <cfif #totreglepourfact.totpayepourfact# gte #lst.facture# or (#NewSolde.Solde# lte 0 and #prestationprepaye.recordcount# eq 0) or #SoldeRestant# lte 0>
                        <cfset inf = '<img src="/img/icon/lu.gif" alt="Facture soldée" title="Facture soldée" />'>
                        <cfset txtinf = "">
                    <cfelse>
                        <cfset inf = '<img src="/img/icon/nonlu.gif" alt="Facture non soldée !" title="Facture non soldée !" />'>
                        <cfset txtinf = "A REGLER">
                        <cfset SoldeRestant = #SoldeRestant# - #lst.facture#>
                    </cfif>
                    <tr bgcolor="##FFFFFF">
                        <td width="74" class="libelle"><a name="n#lst.currentrow#">#lst.date#</td>
                        <td width="98" class="libelle">#lst.numero#</td>
                        <td width="70">#inf# #lst.facture#</td>
                        <td width="126"><font color="##FF0000">#txtinf#</font></td>
                        <td width="492" class="libellemin">#lst.info#</td>
                    </tr>            
                </cfif>










            <cfelse><!--- espace parent --->
                <cfif #lst.idencaissement# gt 0>
                    <tr bgcolor="##FFFFFF" class="text-grey">
                        <td width="74" class="libelle">#lst.date#</td>
                        <td width="98" class="libelle">#lst.numero#</td>
                        <td width="100"></td>
                        <td width="61">#lsnumberformat(lst.paye,"9999.99")#</td>
                        <td width="527" class="libellemin">#lst.info#</td>
                    </tr>
                <cfelse>
                    <cfquery name="totreglepourfact" dbtype="query">
                        Select sum(paye) as totpayepourfact from lst 
                        where numrole=#lst.numrole# or numrole1=#lst.numrole# or numrole2=#lst.numrole# or numrole3=#lst.numrole#
                    </cfquery>
                    <cfquery name="itemprepaye" datasource="f8w" maxrows="1">
                        Select NumPrestation from itemfacture use index(siret_NumRole) where siret='#siret#' 
                        and NumRole='#lst.numrole#' and NumClient='#NumClient#' and NumPrestation>0
                    </cfquery>
                    <cfquery name="presta" datasource="f8w">
                        Select id from prestation use index(siret_numprestation) where siret='#siret#' 
                        and numprestation='#itemprepaye.numprestation#' and ticket=1
                    </cfquery>
                    <cfif #lect_etablissement_en_cour.RégieRecette# eq 0>
                        <cfset inf = "">
                        <cfset txtinf = "">
                    <cfelseif #totreglepourfact.totpayepourfact# gte #lst.facture# or (#NewSolde.Solde# lte 0 and #prestationprepaye.recordcount# eq 0) or #SoldeRestant# lte 0 or #presta.recordcount# eq 1>
                        <cfset inf = '<img src="/img/icon/lu.gif" alt="Facture soldée" title="Facture soldée" />'>
                        <cfset txtinf = "">
                    <cfelse>
                        <cfset inf = '<img src="/img/icon/nonlu.gif" alt="Facture non soldée !" title="Facture non soldée !" />'>
                        <cfif #CbTipi# eq "non">
                            <cfset txtinf = "A REGLER">
                        <cfelse>
                            <cfset txtinf = '<a href="/facture/cbtipi/?CFGRIDKEY=#lst.idfacture#" target="_parent" class="link-red">A REGLER <img class="ms-1" src="/img/icon/icon_cb_blue.png" /></a>'>
                        </cfif>
                        <cfset SoldeRestant = #SoldeRestant# - #lst.facture#>
                    </cfif>
                    <tr bgcolor="##FFFFFF" onMouseOver="changeCouleur(this);" onMouseOut="remetCouleur(this);">
                        <td width="74" class="libelle"><a class="link-grey" href="/NewEdFact/afffacture.cfm?id=#lst.idfacture#" target="_blank">#lst.date#</a></td>
                        <td width="98" class="libelle"><a class="link-grey" href="/commun/VisuPdf.cfm?CFGRIDKEY=#lst.idfacture#&noclassactive=1&sess=#sess#" target="_blank">#lst.numero#</a></td>
                        <td width="70"><a class="link-grey" href="/commun/VisuPdf.cfm?CFGRIDKEY=#lst.idfacture#&noclassactive=1&sess=#sess#" target="_blank">#inf# #lst.facture#</a></td>
                        <td width="126"><font color="##FF0000">#txtinf#</font></td>
                        <td width="492" class="libellemin"><a class="link-grey" href="/commun/VisuPdf.cfm?CFGRIDKEY=#lst.idfacture#&noclassactive=1&sess=#sess#" target="_blank">#lst.info#</a></td>
                    </tr>
                </cfif>
            </cfif>
        </cfoutput>

        <cfset restligne = 6 - #lst.recordcount#>
        <cfif #restligne# gt 0>
            <cfloop from="1" to="#restligne#" index="idx">
                <tr bgcolor="#FFFFFF" >
                    <td class="libelle">&nbsp;</td>
                    <td class="libelle"></td>
                    <td></td>
                    <td></td>
                    <td class="libellemin"></td>
                </tr>
            </cfloop>
        </cfif>
    </cfif>
</cfif>