﻿<cfcomponent>
	<!--- determine les docs demandé à la famille et les bloquage éventuel --->
    <cffunction name="DocumentDemande" access="public" returntype="query">
		<cfargument name="siret" type="any" required="yes">
        <cfargument name="idclient" type="any" required="yes">
    	<cfset Retour = QueryNew("siret,type_doc,désignation,obligatoire,validation,bloqueresa,bloqueachat,datedemande,numclient,numenfant,statut,Date,id_doc,ExpireLe,UnSeulDoc")>
    	<cfquery name="application_client_document" datasource="f8w">
        	Select * from application_client_document use index(siret) where siret='#arguments.siret#'
        </cfquery>
        <cfquery name="client" datasource="f8w">
        	Select * from client where id='#arguments.idclient#'
        </cfquery>
        <cfset DemanderJustifQf = "non">
        <cfif #client.QuotientFamilial# gt 0>
        	<cfquery name="reductions" datasource="f8w" maxrows="1">
            	Select id from reductions use index(siret) where siret='#arguments.siret#' 
                and QfDe>0 and Qfa>'#client.QuotientFamilial#' and del=0 Limit 0,1
            </cfquery>
            <cfif #reductions.recordcount#>
            	<!--- le Qf de cette famille est supérieur à zero et est dans une borne qf de réduction --->
            	<cfset DemanderJustifQf = "oui">
            </cfif>
        </cfif>
        <cfloop query="application_client_document">
        	<cfquery name="type_doc" datasource="classactive">
            	Select * from type_doc use index(type_doc_CodeAppli) Where type_doc='#application_client_document.type_doc#' 
                and CodeAppli='F8'
            </cfquery>
            <cfif #type_doc.type_proprio# eq "parent">
            	<cfquery name="documents" datasource="classactive" maxrows="1">
        			Select * from documents use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
                    where siret='#arguments.siret#' and Id_Proprio='#arguments.idclient#' and CodeAppli='F8' 
                    and Type_Proprio='#type_doc.type_proprio#' and type_doc='#application_client_document.type_doc#' 
                    and Date_Ajout_Modif > '#application_client_document.DateDemande#' order by id desc Limit 0,1
        		</cfquery>
				<cfif #application_client_document.type_doc# neq "JUSTQF" or (#application_client_document.type_doc# eq "JUSTQF" and #DemanderJustifQf# eq "oui")>
					<cfset r = queryaddrow(Retour)>
                    <cfset r = querysetcell(Retour,'siret',#application_client_document.siret#)>
                    <cfset r = querysetcell(Retour,'type_doc',#application_client_document.type_doc#)>
                    <cfif #application_client_document.obligatoire# eq 0>
                    	<cfset r = querysetcell(Retour,'désignation',#type_doc.désignation#)>
                    <cfelse>
                    	<cfset des = #type_doc.désignation# & " (OBLIGATOIRE)">
                        <cfset r = querysetcell(Retour,'désignation',#des#)>
                    </cfif>
					<cfset r = querysetcell(Retour,'obligatoire',#application_client_document.obligatoire#)>
                    <cfset r = querysetcell(Retour,'validation',#application_client_document.validation#)>
                    <cfset r = querysetcell(Retour,'bloqueresa',#application_client_document.bloqueresa#)>
                    <cfset r = querysetcell(Retour,'bloqueachat',#application_client_document.bloqueachat#)>
                    <cfset r = querysetcell(Retour,'datedemande',#application_client_document.datedemande#)>
                    <cfset r = querysetcell(Retour,'numclient',#client.numclient#)>
                    <cfset r = querysetcell(Retour,'numenfant',0)>
                    <cfif #documents.recordcount# eq 0>
                    	<cfset r = querysetcell(Retour,'statut',"A transmettre")>
                        <cfset r = querysetcell(Retour,'Date',#application_client_document.DateDemande#)>
                        <cfset r = querysetcell(Retour,'id_doc',0)>
                	<cfelseif #documents.validé# eq 0>
                    	<cfset r = querysetcell(Retour,'statut',"En cours de validation")>
                        <cfset r = querysetcell(Retour,'Date',#documents.Date_Ajout_Modif#)>
                        <cfset r = querysetcell(Retour,'id_doc',#documents.id#)>
                    <cfelse>
                    	<cfset r = querysetcell(Retour,'statut',"Validé")>
                        <cfset r = querysetcell(Retour,'Date',#documents.Date_Ajout_Modif#)>
                        <cfset r = querysetcell(Retour,'id_doc',#documents.id#)>
                    </cfif>
                    <cfset r = querysetcell(Retour,'ExpireLe',#documents.ExpireLe#)>
                    <cfset r = querysetcell(Retour,'UnSeulDoc',#type_doc.UnSeulDoc#)>
                </cfif>        		
			<cfelseif #type_doc.type_proprio# eq "enfant">
            	<cfquery name="enfant" datasource="f8w">
                	Select * from enfant use index(siret_numclientI) where siret='#arguments.siret#' 
                    and NumClientI='#client.numclient#' and parti=0 and del=0
                </cfquery>
                <cfloop query="enfant">
                    <cfquery name="enfantinfo" datasource="f8w">
                        select * from enfantinfo use index(siret_numenfant) where siret='#arguments.siret#' 
                        and numenfant='#enfant.numenfant#' and pai=1
                    </cfquery>                
                	<cfquery name="documents" datasource="classactive" maxrows="1">
                        Select * from documents use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
                        where siret='#arguments.siret#' and Id_Proprio='#enfant.id#' and CodeAppli='F8' 
                        and Type_Proprio='#type_doc.type_proprio#' and type_doc='#application_client_document.type_doc#' 
                        and Date_Ajout_Modif > '#application_client_document.DateDemande#' order by id desc Limit 0,1
                    </cfquery>
					<cfif #application_client_document.type_doc# neq "ORDONANCEPAI" or (#application_client_document.type_doc# eq "ORDONANCEPAI" and #enfantinfo.recordcount# eq 1)>
						<cfset r = queryaddrow(Retour)>
                        <cfset r = querysetcell(Retour,'siret',#application_client_document.siret#)>
                        <cfset r = querysetcell(Retour,'type_doc',#application_client_document.type_doc#)>
                        <cfif #application_client_document.obligatoire# eq 0>
                        	<cfset des = #type_doc.désignation# & " (" & #enfant.prénom# & " " & #enfant.nom# & ")">
                        <cfelse>
                        	<cfset des = #type_doc.désignation# & " OBLIGATOIRE (" & #enfant.prénom# & " " & #enfant.nom# & ")">
                        </cfif>
						<cfset r = querysetcell(Retour,'désignation',#des#)>
                        <cfset r = querysetcell(Retour,'obligatoire',#application_client_document.obligatoire#)>
                        <cfset r = querysetcell(Retour,'validation',#application_client_document.validation#)>
                        <cfset r = querysetcell(Retour,'bloqueresa',#application_client_document.bloqueresa#)>
                        <cfset r = querysetcell(Retour,'bloqueachat',#application_client_document.bloqueachat#)>
                        <cfset r = querysetcell(Retour,'datedemande',#application_client_document.datedemande#)>
                        <cfset r = querysetcell(Retour,'numclient',#client.numclient#)>
                        <cfset r = querysetcell(Retour,'numenfant',#enfant.numenfant#)>     
                        <cfif #documents.recordcount# eq 0>
                            <cfset r = querysetcell(Retour,'statut',"A transmettre")>
                            <cfset r = querysetcell(Retour,'Date',#application_client_document.DateDemande#)>
                            <cfset r = querysetcell(Retour,'id_doc',0)>
                        <cfelseif #documents.validé# eq 0>
                            <cfset r = querysetcell(Retour,'statut',"En cours de validation")>
                            <cfset r = querysetcell(Retour,'Date',#documents.Date_Ajout_Modif#)>
                            <cfset r = querysetcell(Retour,'id_doc',#documents.id#)>
                        <cfelse>
                            <cfset r = querysetcell(Retour,'statut',"Validé")>
                            <cfset r = querysetcell(Retour,'Date',#documents.Date_Ajout_Modif#)>
                            <cfset r = querysetcell(Retour,'id_doc',#documents.id#)>
                        </cfif> 
                        <cfset r = querysetcell(Retour,'ExpireLe',#documents.ExpireLe#)>
                        <cfset r = querysetcell(Retour,'UnSeulDoc',#type_doc.UnSeulDoc#)>
                    </cfif>
                </cfloop>
            </cfif>
        </cfloop>
        <cfquery name="RetourFin" dbtype="query">
        	Select * from Retour order by désignation
        </cfquery>
        <cfreturn RetourFin>
    </cffunction>
	<!--- détermine le prix d'une prestation Ticket --->
    <cffunction name="PrixReduitTicket" access="public" returntype="query">
    	<cfargument name="siret" type="any" required="yes">
        <cfargument name="NumClient" type="numeric" required="yes">
    	<cfargument name="numEnfant" type="numeric" required="yes">
        <cfargument name="NumPrestation" type="numeric" required="yes">
    	<cfargument name="PourLeJour" type="string" required="yes">
        <cfargument name="CodePrix" type="numeric" required="no" default="1">
        <cfargument name="ModeAffichage" type="numeric" required="no" default="0">
        <cfargument name="action" type="string" required="no" default="inscr">
        <cfargument name="ModeTestNbrEnfant" type="numeric" required="no" default="0">
        <cfargument name="siretdispo" type="string" required="no" default="">
        <cfif len(#arguments.siretdispo#) eq 0>
        	<cfset arguments.siretdispo = #arguments.siret#>
        </cfif>
        <cfset Retour = QueryNew("NbrEnf,Désignation,PU")>
        <!--- récupération du QF du client --->
        <cfquery name="lect_client" datasource="f8w">
        	Select QuotientFamilial from client use index(siret_numclient) Where siret='#arguments.siret#' 
            and numclient='#arguments.numclient#'
        </cfquery>
		<!--- récupération des enfants du client --->
        <cfquery name="enfant" datasource="f8w">
        	Select NumEnfant from enfant use index(siret_NumClientI) Where siret='#arguments.siret#' 
            and NumClientI='#arguments.NumClient#' and parti=0 and del=0
        </cfquery>
		<!--- recupération du prix non réduit --->
        <cfquery name="prixprestation" datasource="f8w" maxrows="1">
    		Select * from prixprestation use index(siret_numprestation) Where siret='#arguments.siretdispo#' 
            and numprestation='#arguments.numprestation#' and CodePrix='#arguments.CodePrix#' 
            and (NbEnfant=0 or NbEnfant=1) and del=0 Limit 0,1
    	</cfquery>
        <!--- recupération des réductions --->
        <cfquery name="reductions" datasource="f8w">
        	Select * from reductions use index(siret_numprestation) 
            where siret='#arguments.siretdispo#' 
            and (NumPrestation='#arguments.NumPrestation#' or NumPrestation=0)
            and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
            and del=0 order by ApartirDeNbrEnfant DESC
        </cfquery>
        <cfif #prixprestation.recordcount#>
			<cfif #prixprestation.NonRéductionBarème# eq 1 or #reductions.recordcount# eq 0>
				<!--- pas de réduction --->
                <cfset r = QueryAddRow(Retour)>
                <cfset r = QuerySetCell(Retour,"NbrEnf",0)>
            	<cfset r = QuerySetCell(Retour,"Désignation",#prixprestation.Désignation#)>
            	<cfset r = QuerySetCell(Retour,"PU",#prixprestation.PU#)> 
			<cfelse>
				<!--- Si réduction sur nbr enfant ont vérifie si une réduc existe pour 0 ou un enfant sinon on ajoute le tarif normal --->
                <cfloop query="reductions">
                	<cfset Desi = #prixprestation.Désignation# & " " >
					<cfset Reduire = "oui">
					<cfif #reductions.GroupeClient# gt 0><!--- reduction sur groupe --->
						<cfquery name="groupeclient" datasource="f8w">
							Select id from groupeclient use index(siret_NumGroupe) 
                             where siret='#arguments.siretdispo#' and NumGroupe='#reductions.GroupeClient#' and numprofil=0 and del=0
                        </cfquery>
                        <cfif #groupeclient.recordcount#>
							<!--- reduc pour l'enfant ? --->
                            <cfquery name="GroupeDelenfant" datasource="f8w">
                                Select NumGroupe from clientgroupe use index(siret_NumEnfant) 
                                where siret='#arguments.siretdispo#' and numenfant='#arguments.NumEnfant#' 
                                and NumGroupe='#reductions.GroupeClient#'
                            </cfquery>                            
							<cfif #GroupeDelenfant.recordcount# eq 0 or #arguments.numenfant# eq 0>
                                <!--- reduc pour le client ? --->
                                <cfquery name="GroupeDuClient" datasource="f8w">
                                    Select NumGroupe from clientgroupe use index(siret_NumClient) 
                                    where siret='#arguments.siretdispo#' and numClient='#arguments.NumClient#' 
                                    and NumGroupe='#reductions.GroupeClient#'
                                </cfquery>
                                <cfif #GroupeDuClient.recordcount# eq 0>
                                    <!--- ni l'enfant ni le client ne font partis du groupe pour cette reduc --->
                                    <cfset Reduire = "non">
                                </cfif>
                            </cfif>
                        <cfelse>                	
                    		<cfset Reduire = "non">
                    	</cfif>
                    </cfif>
                    
                    <cfif (#Reduire# eq "oui" and #Arguments.ModeAffichage# eq 1) or (#Reduire# eq "oui" and #Retour.recordcount# eq 0)>
						<!--- plusieur enreg possible en mode affichage ou un seul sinon --->
                        <cfif #reductions.ApartirDeNbrEnfant# gt 0>
                        	<!--- sur nombre d'enfant --->
							<cfif #arguments.action# neq "desinscr"> 
                                <cfset NbrEnfantCeJour = 1><!--- demande d'inscription (ont ajoute celui qui va etre inscrit)--->
                            <cfelse>
                                <cfset NbrEnfantCeJour = 0><!--- demande desinscription --->
                            </cfif>
                            <cfif #Arguments.ModeTestNbrEnfant# gt 0>
                            	<cfset NbrEnfantCeJour = #Arguments.ModeTestNbrEnfant#>
                            <cfelse>
                                <cfloop query="enfant">
                                    <cfquery name="lect_inscriptionplanning" datasource="f8w">
                                        Select id from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                                        Where siret = '#arguments.siret#' 
                                        and NumPrestation = '#arguments.NumPrestation#' and NumEnfant = '#enfant.NumEnfant#' 
                                        and DateInscription = '#arguments.PourLeJour#' and Nature=0
                                    </cfquery>
                                    <cfset NbrEnfantCeJour = #NbrEnfantCeJour# + #lect_inscriptionplanning.recordcount#>
    <!---                                <cfif #arguments.AvecPresenceAbsence# eq 1>
    --->                                    
                                        <cfquery name="ver" datasource="f8w">
                                            Select PrésentAbsent from présenceabsence use index(siret_NumEnfant_NumPrestation_Date) 
                                            Where siret='#arguments.siret#' and NumEnfant='#enfant.NumEnfant#' 
                                            and NumPrestation='#arguments.NumPrestation#' and Date='#arguments.PourLeJour#'
                                        </cfquery>
                                        <cfif #ver.PrésentAbsent# eq 3 or #ver.PrésentAbsent# eq 4>
                                            <!--- Présent justifié ou Présent injustifié --->
                                            <cfset NbrEnfantCeJour = #NbrEnfantCeJour# + 1>
                                        <cfelseif #ver.PrésentAbsent# eq 1 or #ver.PrésentAbsent# eq 2>
                                            <!--- Absent justifié ou Absent injustifié--->  
                                            <cfset NbrEnfantCeJour = #NbrEnfantCeJour# - 1>
                                        </cfif>
    <!---                                </cfif>
    --->                            
								</cfloop>
                            </cfif>                        
							<cfif #NbrEnfantCeJour# neq #reductions.ApartirDeNbrEnfant# and #Arguments.ModeAffichage# eq 0>
                            	<cfset Reduire = "non">
                            </cfif>
                        </cfif>
                        <cfif #Reduire# eq "oui">
							<cfset DesiReduit = #Desi# & "(" & #reductions.Désignation# & ")">
                            <!---<cfset red1 = numberformat(#reductions.MontReduc#,"999.99")>
                            <cfset pu1 = numberformat(#prixprestation.PU#,"999.99")>--->
                            <!---<cfset PrixReduit1 = #prixprestation.PU# - #reductions.MontReduc#>--->
                            <!---<cfset PrixReduit = #prixprestation.PU# - #red1#>--->
                            <!---<cfset PrixReduit = #pu1# - #red1#>--->
                            <cfset PrixReduit = #prixprestation.PU# - #reductions.MontReduc#>
							<cfset r = QueryAddRow(Retour)>
                            <cfset r = QuerySetCell(Retour,"NbrEnf",#reductions.ApartirDeNbrEnfant#)>
                            <cfset r = QuerySetCell(Retour,"Désignation",#DesiReduit#)>
                            <cfset r = QuerySetCell(Retour,"PU",#PrixReduit#)>
                            <cfbreak>
                        </cfif>
                	</cfif>
				</cfloop>
				<cfif #Retour.recordcount# eq 0>
                	<!--- pas de réduction à appliquer --->
					<cfset r = QueryAddRow(Retour)>
                    <cfset r = QuerySetCell(Retour,"NbrEnf",0)>
                    <cfset r = QuerySetCell(Retour,"Désignation",#prixprestation.Désignation#)>
                    <cfset r = QuerySetCell(Retour,"PU",#prixprestation.PU#)>                     
                </cfif>
            </cfif>
        <cfelse>
			<cfset r = QueryAddRow(Retour)>
            <cfset r = QuerySetCell(Retour,"NbrEnf",0)>
            <cfset r = QuerySetCell(Retour,"Désignation","Aucun prix trouvé !!")>
            <cfset r = QuerySetCell(Retour,"PU",0)>        
        </cfif>
        <cfquery name="RetourFin" dbtype="query">
        	Select * from Retour order by NbrEnf,Désignation
        </cfquery>
        <!--- Si réduction sur nbr enfant ont vérifie si une réduc existe pour 0 ou un enfant sinon on ajoute le tarif normal si ont est en mode affichage --->
        <cfquery name="vernbr" dbtype="query">
        	Select * from Retour Where NbrEnf>0
        </cfquery>
        <cfif #vernbr.Recordcount#>
            <cfquery name="vernbrzeroouun" dbtype="query">
                Select * from Retour Where NbrEnf=0 or NbrEnf=1
            </cfquery>
        	<cfif #vernbrzeroouun.recordcount# eq 0 and #Arguments.ModeAffichage# eq 1>
        		<cfset r = QueryAddRow(Retour)>
				<cfset r = QuerySetCell(Retour,"NbrEnf",0)>
                <cfset r = QuerySetCell(Retour,"Désignation",#prixprestation.Désignation#)>
                <cfset r = QuerySetCell(Retour,"PU",#prixprestation.PU#)>
                <cfquery name="RetourFin" dbtype="query">
                    Select * from Retour order by NbrEnf,Désignation
                </cfquery>        
        	</cfif>
        </cfif>
    	<cfreturn RetourFin>
    </cffunction>
    <!--- --->

</cfcomponent>