﻿<cfquery name="message" datasource="f8w_test">
    Select * from message use index(id_client) 
    Where id_client='#lect_client.id#' 
    and EmisPar='#lect_client.id#'
    order by DateMessage DESC
</cfquery>



<div class="row mt-5 content">

    <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintReceivedMessage()">Messages re&ccedil;us</button>
    <button type="button" class="box-message-active col-3 p-3 d-flex justify-content-center">Messages envoy&eacute;s</button>
    <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintNewMessage()">Envoyer un nouveau message</button>



    <div class="p-2 p-lg-5 bg-white section small-scroll">

        <cfform name="pourdet">
            <cfinput type="hidden" name="savderclic" value="#message.id#">
                <table  class="table">
                    <cfoutput query="message">
                        <cfset ladate = #lsdateformat(message.DateMessage,"dddd")# & " " & #lsdateformat(message.DateMessage,"dd")# & " " & #lsdateformat(message.DateMessage,"mmmm")# & " " & #lsdateformat(message.DateMessage,"yyyy")#>
                        <cfset laheure = #lstimeformat(message.DateMessage,"HH")# & ":" & #lstimeformat(message.DateMessage,"mm")# & ":" & #lstimeformat(message.DateMessage,"ss")#>
                        <cfif #url.id_message# eq #message.id#>
                            <tr id="TR#message.id#">
                        <cfelse>
                            <tr id="TR#message.id#">
                        </cfif>    
                            <td class "pt-3 pb-3">&nbsp;
                                <a class="text-grey link-blue lu_nonLu" href="/message1/affmess.cfm?id_message=#message.id#" target="frm_mess">#message.Objet#</a>
                            </td>
                        
                            <td class="pt-3 pb-3">
                                <cfif len(#message.piece_jointe#)>
                                    <img width="14" height="13" alt="Ce message contient des pièces jointes" title="Ce message contient des pièces jointes" src="/mariedemeter/img/icon/icon_paper_clip_grey.png">
                                </cfif>
                            </td>

                            <td class="text-grey pt-3 pb-3">#ladate#</td>
                            <td class="text-grey pt-3 pb-3">#laheure#</td>
                        </tr>
                    </cfoutput>
                </table>
        </cfform>
    </div>

    <div class="p-2 p-lg-5 bg-white section small-scroll">
        <iframe src="/message1/affmess.cfm?id_mnu=<cfoutput>#url.id_mnu#</cfoutput>&id_message=<cfoutput>#url.id_message#</cfoutput>" style="background-color:#FFF" name="frm_mess" id="frm_mess" width="100%" height="200"></iframe></td>
    </div>
</div>
