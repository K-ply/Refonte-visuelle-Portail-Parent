﻿<cfif #url.siret# eq "DEMOLaurent">
	<cffile action="write" file="c:\tempWeb\Logredpres.txt" output="PresAbsTicketPortefeuilleNew.cfm">
</cfif>
<cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
	Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
	where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
</cfquery>
<cfquery name="enfantclient" datasource="f8w">
	Select * from enfant use index(siret_numclienti) where siret='#url.siret#' and NumClienti='#enfant.NumClientI#' 
	and parti=0 and del=0
</cfquery>

<!--- ont a besoin de connaitre éventuellement le prix inscrit et présent --->
<cfquery name="prixinscr" datasource="f8w">
	Select * from prixprestation use index(siret_NumPrestation) 
	Where siret='#url.siret#' and NumPrestation='#url.prestation#' and CodePrix='1' and del=0
</cfquery>
<cfset Leprixinscr.pu = #prixinscr.pu#>
<cfif #prixinscr.NonRéductionBarème# eq 0>
	<cfquery name="reductionachat" datasource="f8w">
		Select * from reductions use index(siret_numprestation) 
		where siret='#url.siret#' 
		and NumPrestation='#url.prestation#' 
		and QfDe<='#client.QuotientFamilial#' and QfA>='#client.QuotientFamilial#' 
		and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
	</cfquery>
	<cfloop query="reductionachat">
		<cfif #reductionachat.GroupeClient# gt 0>
			<cfquery name="verifgroupe" datasource="f8w">
				Select id from groupeclient use index(siret_numgroupe)
				Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' and del=0
			</cfquery>
			<cfif #verifgroupe.recordcount#>
				<cfquery name="verifclient" datasource="f8w">
					Select * from clientgroupe use index(siret_numgroupe) 
					Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
					and NumClient='#client.numclient#'
				</cfquery>
				<cfif #verifclient.recordcount#>
					<cfset Leprixinscr.pu = #Leprixinscr.pu# - #reductionachat.MontReduc#>
					<cfbreak>
				<cfelse>
					<cfquery name="verifenfant" datasource="f8w">
						Select * from clientgroupe use index(siret_numgroupe) 
						Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
						and NumEnfant='#enfant.numenfant#'
					</cfquery>
					<cfif #verifenfant.recordcount#>
						<cfset Leprixinscr.pu = #Leprixinscr.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					</cfif>
				</cfif>
			</cfif>                
		<cfelse>
			<cfif #reductionachat.ApartirDeNbrEnfant# eq 0 and #reductionachat.EnfantSuivantSiConsoMemeJour# eq 0 >
				<cfif #url.siret# eq "DEMOLaurent">
					<cfset txt = "Ligne 57 reduction " & #reductionachat.désignation# & " pu " & #Leprixinscr.pu# & " - " & #reductionachat.MontReduc# & #chr(13)#>
					<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
				</cfif>				
				<cfset Leprixinscr.pu = #Leprixinscr.pu# - #reductionachat.MontReduc#>
				<cfbreak>
			<cfelseif #reductionachat.ApartirDeNbrEnfant# neq 0 >
				<cfset nbrinscrit=0>
				<cfloop query="enfantclient">
					<cfquery name="inscriptionplanning" datasource="f8w">
						Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
						and DateInscription='#url.Date#' and nature=0
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
				</cfloop>
				<cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
					<cfquery name="verver" datasource="f8w">
						Select * from présenceabsence use index(siret_NumPrestation_Date) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
						and (PrésentAbsent=1 or PrésentAbsent=2)
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
					<cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
						<cfset Leprixinscr.pu = #Leprixinscr.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					</cfif>
				</cfif>
			<cfelseif #reductionachat.EnfantSuivantSiConsoMemeJour# neq 0 >
				<cfset nbrinscrit=0>
				<cfloop query="enfantclient">
					<cfquery name="inscriptionplanning" datasource="f8w">
						Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
						and DateInscription='#url.Date#' and nature=0
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
				</cfloop>
				<cfif #nbrinscrit# gte #reductionachat.EnfantSuivantSiConsoMemeJour#>
					<cfquery name="verver" datasource="f8w">
						Select * from présenceabsence use index(siret_NumPrestation_Date) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
						and (PrésentAbsent=1 or PrésentAbsent=2)
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
					<cfif #nbrinscrit# gte #reductionachat.EnfantSuivantSiConsoMemeJour#>
						<cfset Leprixinscr.pu = #Leprixinscr.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					</cfif>
				</cfif>			
            </cfif>
		</cfif>
	</cfloop>
</cfif>

<cfif #ver.recordcount#><!--- il y a déjà une presenceabsence                                                                --->
	<cfif #url.siret# eq "DEMOLaurent">
		<cfset txt = "Ligne 91 il y a déjà une presabs !" & #chr(13)#>
		<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
	</cfif>        
	<!--- ont annule l'anciene action sur le portefeuille --->
	<cfset Codeprix = #ver.PrésentAbsent# + 1>          
	<cfquery name="prix" datasource="f8w">
		Select * from prixprestation use index(siret_NumPrestation) 
		Where siret='#url.siret#' and NumPrestation='#url.prestation#' and CodePrix='#Codeprix#' and del=0
	</cfquery>
	<cfset Leprix.pu = #prix.pu#> 
	<cfif #url.siret# eq "DEMOLaurent">
		<cfset txt = "Ligne 102 ancien prix " & #prix.désignation# & " " & #prix.pu# & #chr(13)#>
		<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
	</cfif>        
	<cfif #prix.NonRéductionBarème# eq 0>
		<cfquery name="reductionachat" datasource="f8w">
			Select * from reductions use index(siret_numprestation) 
			where siret='#url.siret#' 
			and NumPrestation='#url.prestation#' 
			and QfDe<='#client.QuotientFamilial#' and QfA>='#client.QuotientFamilial#' 
			and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
		</cfquery>
		<cfloop query="reductionachat">
			<cfif #reductionachat.GroupeClient# gt 0>
				<cfquery name="verifgroupe" datasource="f8w">
					Select id from groupeclient use index(siret_numgroupe)
					Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' and del=0
				</cfquery>
				<cfif #verifgroupe.recordcount#>
					<cfquery name="verifclient" datasource="f8w">
						Select * from clientgroupe use index(siret_numgroupe) 
						Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
						and NumClient='#client.numclient#'
					</cfquery>
					<cfif #verifclient.recordcount#>
						<cfset Leprix.pu = #Leprix.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					<cfelse>
						<cfquery name="verifenfant" datasource="f8w">
							Select * from clientgroupe use index(siret_numgroupe) 
							Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
							and NumEnfant='#enfant.numenfant#'
						</cfquery>
						<cfif #verifenfant.recordcount#>
							<cfset Leprix.pu = #Leprix.pu# - #reductionachat.MontReduc#>
							<cfbreak>
						</cfif>
					</cfif>
				</cfif>                
			<cfelse>
				<cfif #reductionachat.ApartirDeNbrEnfant# eq 0 and #reductionachat.EnfantSuivantSiConsoMemeJour# eq 0 >
					<cfif #url.siret# eq "DEMOLaurent">
						<cfset txt = "Ligne 143 reduction " & #reductionachat.désignation# & " pu " & #Leprix.pu# & " - " & #reductionachat.MontReduc# & #chr(13)#>
						<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
					</cfif>				
					<cfset Leprix.pu = #Leprix.pu# - #reductionachat.MontReduc#>
					<cfbreak>
				<cfelseif #reductionachat.ApartirDeNbrEnfant# neq 0 >
					<cfset nbrinscrit=0>
					<cfloop query="enfantclient">
						<cfquery name="inscriptionplanning" datasource="f8w">
							Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
							Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
							and DateInscription='#url.Date#' and nature=0
						</cfquery>
						<cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
					</cfloop>
					<cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
						<cfquery name="verver" datasource="f8w">
							Select * from présenceabsence use index(siret_NumPrestation_Date) 
							Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
							and (PrésentAbsent=1 or PrésentAbsent=2)
						</cfquery>
						<cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
						<cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
							<cfset Leprix.pu = #Leprix.pu# - #reductionachat.MontReduc#>
							<cfbreak>
						</cfif>
					</cfif>
				<cfelseif #reductionachat.EnfantSuivantSiConsoMemeJour# neq 0 >
					<cfset nbrinscrit=0>
					<cfloop query="enfantclient">
						<cfquery name="inscriptionplanning" datasource="f8w">
							Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
							Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
							and DateInscription='#url.Date#' and nature=0
						</cfquery>
						<cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
					</cfloop>
					<cfif #nbrinscrit# gte #reductionachat.EnfantSuivantSiConsoMemeJour#>
						<cfquery name="verver" datasource="f8w">
							Select * from présenceabsence use index(siret_NumPrestation_Date) 
							Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
							and (PrésentAbsent=1 or PrésentAbsent=2)
						</cfquery>
						<cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
						<cfif #nbrinscrit# gte #reductionachat.EnfantSuivantSiConsoMemeJour#>
							<cfset Leprix.pu = #Leprix.pu# - #reductionachat.MontReduc#>
							<cfbreak>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
	</cfif>
	<cfif #ver.PrésentAbsent# eq 0>
		<cfset PresAbs = - 1>
	<cfelse>
		<cfset PresAbs = #ver.PrésentAbsent#>
	</cfif>
	<!--- c'était Absent justifié ou Absent injustifié--->
    <!--- ont a donc éventuellement re-créditer le portefeuille , ont le debite --->
	<cfif #ver.PrésentAbsent# eq 1 or #ver.PrésentAbsent# eq 2>
        <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
            Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
            where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
        </cfquery>    
		<cfif #ver.PrésentAbsent# eq 1>
			<cfset TxtHisto = "Annul. Abs. Just.">
        <cfelse>
        	<cfset TxtHisto = "Annul. Abs. Injust.">
        </cfif>
        <!---<cfset PrixAapplique = #Leprix.pu# - (#Leprix.pu# * 2)>--->
        <cfset PrixAapplique = #Leprix.pu#>
		<cfif #prestation_prepaye_portefeuille.recordcount#>
        	<cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#>
        	<cfif #url.siret# eq "DEMOLaurent">
				<cfset txt = "Ligne 191 " & #TxtHisto# & " NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>
            <cfquery name="majportefeuille" datasource="f8w">
                Update prestation_prepaye_portefeuille 
                set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
            </cfquery>
            <cfquery name="addhisto" datasource="f8w">
                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
                ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
                '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
                '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                '#url.NumEnfant#','#url.date#',#ver.PrésentAbsent#,'#TxtHisto#')
            </cfquery>
            <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
                Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
            </cfquery>
        <cfelse>
			<cfset NewMontant = 0 + #PrixAapplique#>
			<cfif #url.siret# eq "DEMOLaurent">
				<cfset txt = "Ligne 209 " & #TxtHisto# & " NewMontant = 0 + #PrixAapplique#" & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>
            <cfquery name="add" datasource="f8w" result="Resultat">
                Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
            </cfquery>
            <cfquery name="majhisto" datasource="f8w">
                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
                ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                '0','#PrixAapplique#',
                '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                '#url.NumEnfant#','#url.date#',#ver.PrésentAbsent#,'#TxtHisto#')
            </cfquery>
            <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
                Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
            </cfquery>
		</cfif>
	</cfif>
    <!--- c'était présent justifié ou présent injustifié--->
    <!--- ont a donc débiter le portefeuille, ont le crédite --->
	<cfif #ver.PrésentAbsent# eq 3 or #ver.PrésentAbsent# eq 4>
        <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
            Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
            where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
        </cfquery>    
    	<cfif #ver.PrésentAbsent# eq 3>
			<cfset TxtHisto = "Annul. Pres. Just.">
        <cfelse>
        	<cfset TxtHisto = "Annul. Pres. Injust.">
        </cfif>
		<cfset PrixAapplique = #Leprix.pu#>
        <cfif #prestation_prepaye_portefeuille.recordcount#>
        	<cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#>
        	<cfif #url.siret# eq "DEMOLaurent">
				<cfset txt = "Ligne 238 " & #TxtHisto# & " NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>
            <cfquery name="majportefeuille" datasource="f8w">
                Update prestation_prepaye_portefeuille 
                set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
            </cfquery>
            <cfquery name="addhisto" datasource="f8w">
                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
                ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
                '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
                '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                '#url.NumEnfant#','#url.date#',#ver.PrésentAbsent#,'#TxtHisto#')
            </cfquery>
            <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
                Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
            </cfquery>
        <cfelse>
			<cfset NewMontant = 0 + #PrixAapplique#>
			<cfif #url.siret# eq "DEMOLaurent">
				<cfset txt = "Ligne 256 " & #TxtHisto# & " NewMontant = 0 + #PrixAapplique#" & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>
            <cfquery name="add" datasource="f8w" result="Resultat">
                Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
            </cfquery>
            <cfquery name="majhisto" datasource="f8w">
                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
                ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                '0','#PrixAapplique#',
                '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                '#url.NumEnfant#','#url.date#',#ver.PrésentAbsent#,'#TxtHisto#')
            </cfquery>
            <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
                Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
            </cfquery>
		</cfif>
	</cfif>
</cfif>
<!--- fin traitement anciene presenceabsence --->
<cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
	Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
	where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
</cfquery>
<cfset NewCodePrix = #url.PresentAbsent# + 1>
<cfquery name="nouveauprix" datasource="f8w">
	Select pu,NonRéductionBarème from prixprestation use index(siret_NumPrestation) 
	Where siret='#url.siret#' and NumPrestation='#url.prestation#' and CodePrix='#NewCodePrix#' and del=0
</cfquery>
<cfset Lenouveauprix.pu = #nouveauprix.pu#>
<cfif #nouveauprix.NonRéductionBarème# eq 0>
	<cfquery name="reductionachat" datasource="f8w">
		Select * from reductions use index(siret_numprestation) 
		where siret='#url.siret#' 
		and NumPrestation='#url.prestation#' 
		and QfDe<='#client.QuotientFamilial#' and QfA>='#client.QuotientFamilial#' 
		and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
	</cfquery>
	<cfloop query="reductionachat">
		<cfif #reductionachat.GroupeClient# gt 0>
			<cfquery name="verifgroupe" datasource="f8w">
				Select id from groupeclient use index(siret_numgroupe)
				Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' and del=0
			</cfquery>
			<cfif #verifgroupe.recordcount#>
				<cfquery name="verifclient" datasource="f8w">
					Select * from clientgroupe use index(siret_numgroupe) 
					Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
					and NumClient='#client.numclient#'
				</cfquery>
				<cfif #verifclient.recordcount#>
					<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
					<cfbreak>
				<cfelse>
					<cfquery name="verifenfant" datasource="f8w">
						Select * from clientgroupe use index(siret_numgroupe) 
						Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
						and NumEnfant='#enfant.numenfant#'
					</cfquery>
					<cfif #verifenfant.recordcount#>
						<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					</cfif>
				</cfif>
			</cfif>                
		<cfelse>
			<cfif #reductionachat.ApartirDeNbrEnfant# eq 0 and #reductionachat.EnfantSuivantSiConsoMemeJour# eq 0 >
				<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
				<cfbreak>
			<cfelseif #reductionachat.ApartirDeNbrEnfant# neq 0 >
				<cfset nbrinscrit=0>
				<cfloop query="enfantclient">
					<cfquery name="inscriptionplanning" datasource="f8w">
						Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
						and DateInscription='#url.Date#' and nature=0
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
				</cfloop>
				<cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
					<cfquery name="verver" datasource="f8w">
						Select * from présenceabsence use index(siret_NumPrestation_Date) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
						and (PrésentAbsent=1 or PrésentAbsent=2)
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
					<cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
						<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					</cfif>
				</cfif>
			<cfelseif #reductionachat.EnfantSuivantSiConsoMemeJour# neq 0 >
				<cfset nbrinscrit=0>
				<cfloop query="enfantclient">
					<cfquery name="inscriptionplanning" datasource="f8w">
						Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
						and DateInscription='#url.Date#' and nature=0
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
				</cfloop>
				<cfif #nbrinscrit# gte #reductionachat.EnfantSuivantSiConsoMemeJour#>
					<cfquery name="verver" datasource="f8w">
						Select * from présenceabsence use index(siret_NumPrestation_Date) 
						Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
						and (PrésentAbsent=1 or PrésentAbsent=2)
					</cfquery>
					<cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
					<cfif #nbrinscrit# gte #reductionachat.EnfantSuivantSiConsoMemeJour#>
						<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
						<cfbreak>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
	</cfloop>
</cfif>
<cfif #url.PresentAbsent# eq 0><!--- ont demande inscrit et present --->
	<cfset PresAbs = - 1>
<cfelse>
    <cfset PresAbs = #url.PresentAbsent#>
</cfif>
<!--- c'est inscrit et présent que l'ont redemande --->
<!--- le portefeuille doit être à nouveau débité du prix inscrit et présent --->
<cfif #url.PresentAbsent# eq 0 and isdefined("url.pasincrit") is false>
    <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
        Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
        where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
    </cfquery>
    <cfset PrixAapplique = #Leprixinscr.pu# - (2 * #Leprixinscr.pu#)>
    <cfset TxtHisto = "Passé à Inscr. et Pres.">
	<cfif #prestation_prepaye_portefeuille.recordcount#>
        <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#>
        <cfif #url.siret# eq "DEMOLaurent">
        	<cfset txt = "Ligne 360 " & #TxtHisto# & " NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>
        <cfquery name="majportefeuille" datasource="f8w">
            Update prestation_prepaye_portefeuille 
            set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
        </cfquery>
        <cfquery name="addhisto" datasource="f8w">
            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
            ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
            '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
            '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
            '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,'#TxtHisto#')
        </cfquery>
    <cfelse>
        <cfset NewMontant = 0 + #PrixAapplique#>
        <cfif #url.siret# eq "DEMOLaurent">
            <cfset txt = "Ligne 378 " & #TxtHisto# & " NewMontant = 0 + #PrixAapplique#" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>
        <cfquery name="add" datasource="f8w" result="Resultat">
            Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
            values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
        </cfquery>
        <cfquery name="majhisto" datasource="f8w">
            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
            ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
            '0','#PrixAapplique#',
            '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
            '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,'#TxtHisto#')
        </cfquery>
    </cfif>
</cfif>
<!--- c'est Absent justifié ou Absent injustifié--->
<!--- le portefeuille à été débité par la réservation du prix inscrit et présent --->
<cfif #url.PresentAbsent# eq 1 or #url.PresentAbsent# eq 2>
    <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
        Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
        where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
    </cfquery>
    <!--- ont recrédite dabord le prix de la réservation ??? --->
    <cfif #ver.recordcount# eq 0>
    	<!--- Uniquement si il n'existait pas d'action présence absence --->
        <cfset PrixAapplique = #Leprixinscr.pu#>
        <cfset TxtHisto = "Anul. Inscr. et Pres.">
		<cfif #prestation_prepaye_portefeuille.recordcount#>
            <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#>
            <cfif #url.siret# eq "DEMOLaurent">
				<cfset txt = "Ligne 407 " & #TxtHisto# & " NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>
            <cfquery name="majportefeuille" datasource="f8w">
                Update prestation_prepaye_portefeuille 
                set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
            </cfquery>
            <cfquery name="addhisto" datasource="f8w">
                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
                ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
                '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
                '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,'#TxtHisto#')
            </cfquery>
            <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
                Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
            </cfquery>
        <cfelse>
            <cfset NewMontant = 0 + #PrixAapplique#>
            <cfif #url.siret# eq "DEMOLaurent">
                <cfset txt = "Ligne 425 " & #TxtHisto# & " NewMontant = 0 + #PrixAapplique#" & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>
            <cfquery name="add" datasource="f8w" result="Resultat">
                Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
            </cfquery>
            <cfquery name="majhisto" datasource="f8w">
                Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
                ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                '0','#PrixAapplique#',
                '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,'#TxtHisto#')
            </cfquery>
            <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
                Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
                where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
            </cfquery>
        </cfif>
    </cfif>
	<cfif #url.PresentAbsent# eq 1>
		<cfset TxtHisto = "Passé à Abs. Just.">
    <cfelse>
        <cfset TxtHisto = "Passé à Abs. Injust.">
    </cfif>
    <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
        Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
        where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
    </cfquery>    
	<cfset PrixAapplique = #Lenouveauprix.pu# - (2 * #Lenouveauprix.pu#)>
	<cfif #prestation_prepaye_portefeuille.recordcount#>
		<cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#>
        <cfif #url.siret# eq "DEMOLaurent">
			<cfset txt = "Ligne 452 " & #TxtHisto# & " NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>
        <cfquery name="majportefeuille" datasource="f8w">
            Update prestation_prepaye_portefeuille 
            set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
        </cfquery>
        <cfquery name="addhisto" datasource="f8w">
            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
            ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
            '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
            '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
            '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,'#TxtHisto#')
        </cfquery>
    <cfelse>
        <cfset NewMontant = 0 + #PrixAapplique#>
        <cfif #url.siret# eq "DEMOLaurent">
            <cfset txt = "Ligne 471 " & #TxtHisto# & " NewMontant = 0 + #PrixAapplique#" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>
        <cfquery name="add" datasource="f8w" result="Resultat">
            Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
            values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
        </cfquery>
        <cfquery name="majhisto" datasource="f8w">
            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
            ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
            '0','#PrixAapplique#',
            '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
            '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,'#TxtHisto#')
        </cfquery>
    </cfif>
</cfif>
<!--- c'est présent justifié ou présent injustifié--->
<!--- ont débite du prix --->
<cfif #url.PresentAbsent# eq 3 or #url.PresentAbsent# eq 4>
    <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
        Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
        where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
    </cfquery>
	<cfif #url.PresentAbsent# eq 3>
		<cfset TxtHisto = "Passé à Pres. Just.">
    <cfelse>
        <cfset TxtHisto = "Passé à Pres. Injust.">
    </cfif>
    <cfif val(#Lenouveauprix.pu#) eq 0>
    	<!--- erreur !!! --->
        <cfset mess = "ATTENTION nouveau prix non trouvé dans PresAbsTicketPortefeuilleNew.cfm, voir ligne 613">
        <cfset mess = #mess# & " Siret " & #url.siret# & " Prestation " & #url.Prestation# & " Enfant " & #url.NumEnfant#>
        <cfset mess = #mess# & " Date " & #url.date# & " Présentabsent " & #url.PresentAbsent#>
        <cfmail server="smtp1.neocim.fr" username="support@neocim.fr" password="SpEs3011*" from="err_cold@neocim.fr" to="laurentmurat25@gmail.com" subject="ERREUR WEB !!!">
            #mess#
        </cfmail>    
    </cfif>
	<cfset PrixAapplique = #Lenouveauprix.pu# - (2 * #Lenouveauprix.pu#)>
	<cfif #prestation_prepaye_portefeuille.recordcount#>
		<cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#>
        <cfif #url.siret# eq "DEMOLaurent">
			<cfset txt = "Ligne 500 " & #TxtHisto# & " NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>
        <cfquery name="majportefeuille" datasource="f8w">
            Update prestation_prepaye_portefeuille 
            set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
        </cfquery>
        <cfquery name="addhisto" datasource="f8w">
            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
            ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
            '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
            '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
            '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,<cfqueryparam value="#TxtHisto#" cfsqltype="cf_sql_varchar">)
        </cfquery>
    <cfelse>
        <cfset NewMontant = 0 + #PrixAapplique#>
        <cfif #url.siret# eq "DEMOLaurent">
            <cfset txt = "Ligne 518 " & #TxtHisto# & " NewMontant = 0 + #PrixAapplique#" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>
        <cfquery name="add" datasource="f8w" result="Resultat">
            Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
            values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
        </cfquery>
        <cfquery name="majhisto" datasource="f8w">
            Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
            Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent,LibAction) values 
            ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
            '0','#PrixAapplique#',
            '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
            '#url.NumEnfant#','#url.date#',#url.PresentAbsent#,<cfqueryparam value="#TxtHisto#" cfsqltype="cf_sql_varchar">)
        </cfquery>
    </cfif>
</cfif>