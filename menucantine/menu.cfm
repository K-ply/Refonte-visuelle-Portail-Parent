﻿<div class="row d-flex justify-content-between mt-5 content">
    <div class="p-2 p-lg-5 bg-white section scroll">
        <table class="table">
            <cfif len(#lect_etablissement.UrlMenu#) gt 0>
                <tr>
                    <th class="pb-4">Accèder aux menus</th>
                </tr>
                <tr>
                    <cfoutput><a href="#lect_etablissement.UrlMenu#" target="_blank">#lect_etablissement.UrlMenu#</a></cfoutput>
                </tr>
            <cfelse>
                <tr>
                    <th class="pb-4">Date</th>
                    <th class="pb-4 hidden">Nom du menu</th>
                    <th class="pb-4">Voir</th>
                </tr>
                <cfloop query="menucantine">
                    <tr>
                        <td class="pt-3 pb-3">Publié le <cfoutput>#lsdateformat(menucantine.Date_Ajout_Modif,"dd/MM/YYYY")#</cfoutput></td>
                        <td class="pt-3 pb-3 hidden"><cfoutput>#menucantine.désignation#</cfoutput></td>
                        <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm?id=<cfoutput>#menucantine.id#</cfoutput>" target="_blank"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                    </tr>
                </cfloop>
            </cfif>
        </table>
    </div>
</div>