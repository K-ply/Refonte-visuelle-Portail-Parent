﻿

<cfif affichemodifenfant eq "oui">
    <div class="row mt-5 content">
        <button type="button" class="box-message-active col-3 p-3 d-flex justify-content-center">Enfants</button>
        <cfif affichemodifparent eq "oui">
            <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintParentsInformations()">Parents</button>
        </cfif>
        <cfif afficheajoutenfant eq "oui">
            <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintAddChild()">Ajouter un enfant</button>
        </cfif>
<cfelseif affichemodifparent eq "oui">
    <div class="row mt-5 content">
        <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center">Parents</button>
        <cfif afficheajoutenfant eq "oui">
            <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintAddChild()">Ajouter un enfant</button>
        </cfif>
<cfelse>
    <div class="row mt-5 content">
        <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center">Ajouter un enfant</button>
</cfif>
        <div class="p-2 p-lg-4 bg-white section scroll">
            <cfform name="editajoutparent">
                <div class="row d-flex justify-content-around change-infos">
                    <div class="p-3 m-2 bg-white section infos-parent">
                        <cfif len(#msg1#)>
                            <p class="text-red"><cfoutput>#msg1#</cfoutput></p>
                        </cfif>
                        <h5>
                            <cfselect name="khjhkj" onChange="MM_jumpMenu('self',this,0)" class="p-2 dropdown-toggle ">
                                <cfoutput query="toutenfant">
                                    <cfif #toutenfant.id# eq #url.id#>
                                        <option class="dropdown-item" selected value="/ModPar/?id_mnu=151&id=#toutenfant.id#">#toutenfant.nom# #toutenfant.prénom#</option>
                                    <cfelse>
                                        <option class="dropdown-item" value="/ModPar/?id_mnu=151&id=#toutenfant.id#">#toutenfant.nom# #toutenfant.prénom#</option>
                                    </cfif>
                                </cfoutput>
                            </cfselect>
                        </h5>

                        <div>
                            <p class="border-new-section text-grey bold">Données personnelles </p>
                            <ul>
                                <cfif #lect_etablissement.ChoixDesClasses# eq 1>  
                                    <cfquery name="classe" datasource="f8w_test">
                                    	select * from classe use index(siret) where siret='#lect_etablissement.siret#' 
                                        and VisibleDansEspaceParent=1 and del=0 order by LibClasse
                                    </cfquery>
                                    <cfif #classe.recordcount#>
                                        <li>
                                            Classe : 
                                            <cfselect name="enfantclasse" id="enfantclasse" class="dropdown-toggle">
                                                <option value="0"></option>
                                                <cfoutput query="classe">
                                                    <cfquery name="ecole" datasource="f8w_test">
                                                        select * from ecole use index(siret_NumEcole) 
                                                        where siret='#lect_etablissement.siret#' and 
                                                        NumEcole = '#classe.numecole#'
                                                    </cfquery>
                                                    <cfquery name="enfantclasse" datasource="f8w_test">
                                                        select * from enfantclasse use index(siret_NumEnfant_NumClasse) 
                                                        where siret='#lect_etablissement.siret#' and Numenfant='#enfant.NumEnfant#' 
                                                        and NumClasse='#classe.numclasse#'
                                                    </cfquery>
                                                    <cfif #enfantclasse.recordcount#>
                                                        <option class="dropdown-item" selected value="#classe.numclasse#">#ecole.NomEcole# #classe.libclasse# - #classe.NomInstit#</option>
                                                    <cfelse>
                                                        <option class="dropdown-item"  value="#classe.numclasse#">#ecole.NomEcole# #classe.libclasse# - #classe.NomInstit#</option>
                                                    </cfif>
                                                </cfoutput>
                                            </cfselect>
                                        </li>
									</cfif>
								</cfif>
                                <li>
                                    Date de naissance : 
                                    <cfinput name="datenaissance" type="text" value="#enfant.datenaissance#" id="datenaissance" size="10" onkeyup="mask_onValueChanged();" onfocus="mask_onSetFocus(this, '99/99/9999');" onblur="mask_onKillFocus();"> 
                                    Sexe 
                                    <cfselect name="sexe" id="sexe" class="dropdown-toggle" >
                                        <cfif #enfant.sexe# eq "F">
                                        	<option class="dropdown-item" value="M">Garçon</option>
                                        	<option class="dropdown-item" selected value="F">Fille</option>                                        
                                        <cfelse>
                                        	<option class="dropdown-item" selected="selected" value="M">Garçon</option>
                                        	<option class="dropdown-item" value="F">Fille</option>
                                		</cfif>
                                	</cfselect>
                                </li>
                                <li>
                                    <cfquery name="allergie" datasource="f8w_test">
                                        Select * from mention use index(siret) where siret='#lect_etablissement.siret#' order by id
                                    </cfquery> 
                                    <cfif #allergie.recordcount# eq 0>
                                        <cfquery name="allergie" datasource="f8w_test">
                                            Select * from mentions order by id
                                        </cfquery>                                
                                    </cfif>
                                    <cfselect class="dropdown-toggle me-2" name="allergie" onChange="AlerteAllergie()" tooltip="Sélectionner une information sanitaire dans la liste.">
                                    	<cfif #enfant.allergie# eq 0>
                                        	<option class="dropdown-item" selected value="0">Pas de contre-indication</option>
                                        <cfelse>
                                        	<option class="dropdown-item" value="0">Pas de contre-indication</option>
										</cfif>
										<cfoutput query="allergie">
                                        	<cfif #enfant.allergie# eq #allergie.id#>
                                            	<option class="dropdown-item" selected value="#allergie.id#">#allergie.desi#</option>
                                            <cfelse>
                                        		<option class="dropdown-item" value="#allergie.id#">#allergie.desi#</option>
                                        	</cfif>
										</cfoutput>        
                                    </cfselect> 
                                    <cfinput value="#enfant.mention#" maxlength="255" tooltip="Saisir une information sanitaire complémentaire." name="mention" id="mention">
                                </li>
                            </ul>
                        </div>
                        <cfif #lect_etablissement.saisiesanitairecomplementonline# eq 1>
                            <div>
                                <p class="border-new-section text-grey bold">Personnes (hors parent) autorisées à prendre en charge l'enfant</p>
                                <ul>
                                    <li>Nom : 
                                        <cfinput name="nomcharge1" id="nomcharge1" type="text"  value="#enfantinfo.nomcharge1#" maxlength="50">
                                    </li>
                                    <li>Prénom : 
                                        <cfinput name="prenomcharge1" id="prenomcharge1" type="text" value="#enfantinfo.prenomcharge1#" maxlength="50">
                                    </li>
                                    <li>Tel fixe : 
                                        <cfinput name="telcharge1" id="telcharge1" type="text" value="#enfantinfo.telcharge1#" size="8">
                                    </li>
                                    <li>Tel portable : 
                                        <cfinput name="gsmcharge1" id="gsmcharge1" type="text" value="#enfantinfo.gsmcharge1#" size="8">
                                    </li>
                                    <li>Lien avec l'enfant : 
                                        <cfinput name="liencharge1" id="liencharge1" type="text" value="#enfantinfo.liencharge1#" maxlength="255">
                                    </li>
                                </ul>
                                <ul>
                                    <li>Nom : 
                                        <cfinput name="nomcharge2" id="nomcharge2" type="text" value="#enfantinfo.nomcharge2#" maxlength="50">
                                    </li>
                                    <li>Prénom : 
                                        <cfinput name="prenomcharge2" id="prenomcharge2" type="text" value="#enfantinfo.prenomcharge2#" maxlength="50">
                                    </li>
                                    <li>Tel fixe : 
                                        <cfinput name="telcharge2" id="telcharge2" type="text" value="#enfantinfo.telcharge2#" size="8">
                                    </li>
                                    <li>Tel portable : 
                                        <cfinput name="gsmcharge2" id="gsmcharge2" type="text" value="#enfantinfo.gsmcharge2#" size="8">
                                    </li>
                                    <li>Lien avec l'enfant : 
                                        <cfinput name="liencharge2" id="liencharge2" type="text" value="#enfantinfo.liencharge2#" maxlength="255">
                                    </li>
                                </ul>
                                <ul>
                                    <li>Nom : 
                                        <cfinput name="nomcharge3" id="nomcharge3" type="text" value="#enfantinfo.nomcharge3#" maxlength="50">
                                    </li>
                                    <li>Prénom : 
                                        <cfinput name="prenomcharge3" id="prenomcharge3" type="text" value="#enfantinfo.prenomcharge3#" maxlength="50">
                                    </li>
                                    <li>Tel fixe : 
                                        <cfinput name="telcharge3" id="telcharge3" type="text" value="#enfantinfo.telcharge3#" size="8">
                                    </li>
                                    <li>Tel portable : 
                                        <cfinput name="gsmcharge3" id="gsmcharge3" type="text" value="#enfantinfo.gsmcharge3#" size="8">
                                    </li>
                                    <li>Lien avec l'enfant : 
                                        <cfinput name="liencharge3" id="liencharge3" type="text" value="#enfantinfo.liencharge3#" maxlength="255">
                                    </li>
                                </ul>
                                <ul>
                                    <li>Nom : 
                                        <cfinput name="nomcharge4" id="nomcharge4" type="text" value="#enfantinfo.nomcharge4#" maxlength="50">
                                    </li>
                                    <li>Prénom : 
                                        <cfinput name="prenomcharge4" id="prenomcharge4" type="text" value="#enfantinfo.prenomcharge4#" maxlength="50">
                                    </li>
                                    <li>Tel fixe : 
                                        <cfinput name="telcharge4" id="telcharge4" type="text" value="#enfantinfo.telcharge4#" size="8">
                                    </li>
                                    <li>Tel portable : 
                                        <cfinput name="gsmcharge4" id="gsmcharge4" type="text" value="#enfantinfo.gsmcharge4#" size="8">
                                    </li>
                                    <li>Lien avec l'enfant : 
                                        <cfinput name="liencharge4" id="liencharge4" type="text" value="#enfantinfo.liencharge4#" maxlength="255">
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <p class="border-new-section text-grey bold">Personnes (hors parents) à prévenir en cas d'urgence</p>
                                <ul>
                                    <li>Nom : 
                                        <cfinput name="nomurgence1" id="nomurgence1" type="text" maxlength="50" value="#enfantinfo.nomurgence1#">
                                    </li>
                                    <li>Prénom : 
                                        <cfinput name="prenomurgence1" id="prenomurgence1" type="text" maxlength="50" value="#enfantinfo.prenomurgence1#">
                                    </li>
                                    <li>Tel fixe : 
                                        <cfinput name="telurgence1" id="telurgence1" type="text" size="8" value="#enfantinfo.telurgence1#">
                                    </li>
                                    <li>Tel portable : 
                                        <cfinput name="gsmurgence1" id="gsmurgence1" type="text" size="8" value="#enfantinfo.gsmurgence1#">
                                    </li>
                                    <li>Lien avec l'enfant : 
                                        <cfinput name="lienurgence1" id="lienurgence1" type="text" maxlength="255" value="#enfantinfo.lienurgence1#">
                                    </li>
                                </ul>
                                <ul>
                                    <li>Nom : 
                                        <cfinput name="nomurgence2" id="nomurgence2" type="text" maxlength="50" value="#enfantinfo.nomurgence2#">
                                    </li>
                                    <li>Prénom : 
                                        <cfinput name="prenomurgence2" id="prenomurgence2" type="text" maxlength="50" value="#enfantinfo.prenomurgence2#">
                                    </li>
                                    <li>Tel fixe : 
                                        <cfinput name="telurgence2" id="telurgence2" type="text" size="8" value="#enfantinfo.telurgence2#">
                                    </li>
                                    <li>Tel portable : 
                                        <cfinput name="gsmurgence2" id="gsmurgence2" type="text" size="8" value="#enfantinfo.gsmurgence2#">
                                    </li>
                                    <li>Lien avec l'enfant : 
                                        <cfinput name="lienurgence2" id="lienurgence2" type="text" maxlength="255" value="#enfantinfo.lienurgence2#">
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <p class="border-new-section text-grey bold">Autorisation et assurance</p>
                                <ul>
                                    <li>
                                        En cas d'urgence, les parents 
                                        <cfif #enfantinfo.autorisemesure# eq 0 or #enfantinfo.recordcount# eq 0>
                                            <cfinput type="radio" name="autorisemesure" value="1"> autorisent 
                                            <cfinput type="radio" name="autorisemesure" value="0" checked="yes"> n'autorisent pas 
                                        <cfelse>
                                            <cfinput type="radio" name="autorisemesure" value="1" checked="yes"> autorisent 
                                            <cfinput type="radio" name="autorisemesure" value="0"> n'autorisent pas 
                                        </cfif>
                                         le responsable à prendre les mesures nécessaires.
                                    </li>
                                    <li>
                                        Lieu d'hospitalisation si nécessaire : 
                                        <cfinput name="lieuhospital" id="lieuhospital" type="text" maxlength="255" value="#enfantinfo.lieuhospital#">
                                    </li>
                                    <li>
                                        Les parents 
                                        <cfif #enfantinfo.autorisephoto# eq 0 or #enfantinfo.recordcount# eq 0 >
                                            <cfinput type="radio" name="autorisephoto" value="1"> autorisent 
                                            <cfinput type="radio" name="autorisephoto" value="0" checked="yes"> n'autorisent pas
                                        <cfelse>
                                            <cfinput type="radio" name="autorisephoto" value="1" checked="yes"> autorisent 
                                            <cfinput type="radio" name="autorisephoto" value="0"> n'autorisent pas
                                        </cfif>
                                         la diffusion et publication de photo ou de film concernant leur enfant.
                                    </li>
                                    <li>
                                        Les parents 
                                        <cfif #enfantinfo.autorisequiter# eq 0 or #enfantinfo.recordcount# eq 0 >
                                            <cfinput type="radio" name="autorisequiter" value="1"> autorisent 
                                            <cfinput type="radio" name="autorisequiter" value="0" checked="yes"> n'autorisent pas 
                                        <cfelse>
                                            <cfinput type="radio" name="autorisequiter" value="1" checked="yes"> autorisent 
                                            <cfinput type="radio" name="autorisequiter" value="0"> n'autorisent pas 
                                        </cfif>
                                         leur enfant à quitter seul l'accueil périscolaire à 
                                         <cfinput name="heurequiter" value="#enfantinfo.heurequiter#" size="1"> heure.
                                    </li>
                                    <li>
                                        mode de sortie 
                                        <cfselect class="dropdown-toggle" name="modesortie">
                                            <cfswitch expression="#enfantinfo.modesortie#">
                                                <cfcase value="">
                                                    <option class="dropdown-item" value="" selected="selected"></option>
                                                    <option class="dropdown-item" value="Car">Car</option>
                                                    <option class="dropdown-item" value="Garderie">Garderie</option>
                                                    <option class="dropdown-item" value="Tiers">Tiers</option>
                                                    <option class="dropdown-item" value="Seul">Seul</option>                
                                                </cfcase>
                                                <cfcase value="Car">
                                                    <option class="dropdown-item" value=""></option>
                                                    <option class="dropdown-item" selected="selected" value="Car">Car</option>
                                                    <option class="dropdown-item" value="Garderie">Garderie</option>
                                                    <option class="dropdown-item" value="Tiers">Tiers</option>
                                                    <option class="dropdown-item" value="Seul">Seul</option>                
                                                </cfcase>
                                                <cfcase value="Garderie">
                                                    <option class="dropdown-item" value=""></option>
                                                    <option class="dropdown-item" value="Car">Car</option>
                                                    <option class="dropdown-item" selected="selected" value="Garderie">Garderie</option>
                                                    <option class="dropdown-item" value="Tiers">Tiers</option>
                                                    <option class="dropdown-item" value="Seul">Seul</option>                
                                                </cfcase>
                                                <cfcase value="Tiers">
                                                    <option class="dropdown-item" value=""></option>
                                                    <option class="dropdown-item" value="Car">Car</option>
                                                    <option class="dropdown-item" value="Garderie">Garderie</option>
                                                    <option class="dropdown-item" selected="selected" value="Tiers">Tiers</option>
                                                    <option class="dropdown-item" value="Seul">Seul</option>                
                                                </cfcase>
                                                <cfcase value="Seul">
                                                    <option class="dropdown-item" value=""></option>
                                                    <option class="dropdown-item" value="Car">Car</option>
                                                    <option class="dropdown-item" value="Garderie">Garderie</option>
                                                    <option class="dropdown-item" value="Tiers">Tiers</option>
                                                    <option class="dropdown-item" selected="selected" value="Seul">Seul</option>                
                                                </cfcase>
                                            </cfswitch>
                                        </cfselect>
                                    </li>
                                    <cfif #lect_user.siret# neq "21380341400019">
                                        <li>
                                            Les parents 
                                            <cfif #enfantinfo.autorisetransport# eq 0 or #enfantinfo.recordcount# eq 0 >
                                                <cfinput type="radio" name="autorisetransport" value="1"> autorisent 
                                                <cfinput type="radio" name="autorisetransport" value="0" checked="yes"> n'autorisent pas 
                                            <cfelse>
                                                <cfinput type="radio" name="autorisetransport" value="1" checked="yes"> autorisent 
                                                <cfinput type="radio" name="autorisetransport" value="0"> n'autorisent pas 
                                            </cfif>
                                             leur enfant à être véhiculé par le responsable périscolaire ou extrascolaire au cours de la prestation d'accueil.
                                        </li>
                                    <cfelse>
                                        <cfinput type="hidden" name="autorisetransport" value="0">
                                    </cfif>
                                    <li>
                                        L'enfant est couvert par une ou des assurances 
                                        <cfif #enfantinfo.responsabilitecivil# eq 1>
                                            <cfinput type="checkbox" name="responsabilitecivil" checked="yes">
                                        <cfelse>
                                            <cfinput type="checkbox" name="responsabilitecivil" checked="no">
                                        </cfif> responsablilité civile, 

                                        <cfif #enfantinfo.individuelaccident# eq 1>
                                            <cfinput type="checkbox" name="individuelaccident" checked="yes">
                                        <cfelse>
                                            <cfinput type="checkbox" name="individuelaccident" checked="no">
                                        </cfif>
                                         individuelle accident. 
                                    </li>
                                    <li>
                                        Compagnie d'assurance 
                                        <cfinput maxlength="255" name="compagnieassurance" value="#enfantinfo.compagnieassurance#"> N° de contrat 
                                        <cfinput name="numcontrat" maxlength="50" value="#enfantinfo.numcontrat#"> 
                                    </li>
                                    <li>
                                        <cfif #enfantinfo.pai# eq 1>
                                            <cfinput type="checkbox" name="pai" checked="yes">
                                        <cfelse>
                                            <cfinput type="checkbox" name="pai">
                                        </cfif>
                                        L'enfant bénéficie d'un projet d'accueil individualisé (PAI).
                                    </li>
                                    <li>
                                        Médecin traitant : nom: 
                                        <cfinput maxlength="50" name="nommedecin" value="#enfantinfo.nommedecin#"> prénom: 
                                        <cfinput maxlength="50" name="prenommedecin" value="#enfantinfo.prenommedecin#"> tel: 
                                        <cfinput size="8" name="telmedecin" value="#enfantinfo.telmedecin#">
                                    </li>
                                    <li>
                                        Notes médicales - recommandations des parents 
                                        <cfinput name="recommandation" id="recommandation" type="text" maxlength="255" value="#enfantinfo.recommandation#">            
                                    </li>
                                    <li>
                                        Vaccin 1 : <cfinput name="vaccin1" id="vaccin1" type="text" value="#enfantinfo.vaccin1#"> 
                                        Vaccin 2 : <cfinput name="vaccin2" id="vaccin2" type="text" value="#enfantinfo.vaccin2#"> 
                                        Vaccin 3 : <cfinput name="vaccin3" id="vaccin3" type="text" value="#enfantinfo.vaccin3#">
                                    </li>
                                </ul>
                            </div>
                        </cfif>
                        <div>
                            <ul>
                                <li class="text-red text-small">En cliquant sur Modifier vous déclarez sur l'honneur l'exactitude des informations saisies
                                    Je déclare avoir pris connaissance des documents ci-après et accepte les conditions générales d'utilisations et/ou les règlements intérieurs : 
                                    <a class="link-grey" href="">règlement intérieur 2022 2023</a>
                                </li>                                
                                <cfif len(#msg1#)>
                                    <li class="text-red">
                                        <cfoutput>#msg1#</cfoutput>                   
                                    </li>
                                </cfif>
                                <li class="d-flex justify-content-center">
                                    <cfinput name="modenfant" id="modenfant" type="submit" value="Modifier" class="btn btn-primary">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <cfinput type="hidden" name="id" value="#enfantinfo.id#">
                <cfinput type="hidden" name="Numenfant" value="#enfant.numenfant#">
                <cfinput type="hidden" name="AllergieAlimentaire" value="#enfant.AllergieAlimentaire#">
            </cfform>
        </div>
    </div>