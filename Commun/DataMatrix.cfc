﻿<cfcomponent>
	<cffunction name="CreationDataMatrix" access="public" returntype="any">
    	<cfargument name="siret" type="string" required="no" default="">
		<cfargument name="NumFacture" type="numeric" required="no" default="0">
		<cfargument name="RolCol" type="string" required="no" default="">
        <cfargument name="RolPer" type="string" required="no" default="">
        <cfargument name="Rolrec" type="string" required="no" default="">
        <cfargument name="Exerc" type="string" required="no" default="">
        <cfargument name="CodeEmetteur" type="string" required="no" default="">
        <cfargument name="IdPost" type="string" required="no" default="">
        
        <cfset Resultat = RepeatString(" ",64)>
		
		<cfif #Arguments.NumFacture# eq 0><!--- en mode WebService --->
            
		<cfelse><!--- depuis gestion.cantine-de-france.fr --->
			<cfquery name="facture" datasource="f8w">
            	Select * from facture use index(siret_numfacture) where siret='#arguments.siret#' 
                and numfacture='#arguments.numfacture#'
        	</cfquery>
            <cfquery name="etablissement" datasource="f8w">
            	Select * from application_client where siret='#arguments.siret#'
            </cfquery>
            
			<!---<cfset Resultat = #Resultat# & #etablissement.CodeCol#><!--- RolCol (CodeEtablissement) 3 caractères position 65 - 67 --->
            <cfset LaPos6567 = #etablissement.CodeCol#>--->
            
			<cfset Resultat = #Resultat# & #etablissement.DataMatrixCodeEtablissement#><!--- RolCol (CodeEtablissement) 3 caractères position 65 - 67 --->
            <cfset LaPos6567 = #etablissement.DataMatrixCodeEtablissement#>
            
            
			<cfset Resultat = #Resultat# & #etablissement.Per#><!--- RolPer (Balise PER du BlocPiece) 1 caractères position 68 --->
        	<cfset LaPos68 = #etablissement.Per#>
            
			<!--- voir doc !!! demande les 2 1er mais est sur 3 position ????? --->
			<cfset Resultat = #Resultat# & #etablissement.DataMatrixCodeProduit#><!--- RolRec (CodProdLoc) 2 premier caractères position 69 - 71 --->
            <cfset LaPos6971 = #etablissement.DataMatrixCodeProduit#>
        	
			<cfset Resultat = #Resultat# & "00"><!--- 2 caractères position 72 - 73 --->
            <cfset LaPos7273 = "00">
            
			<cfset Resultat = #Resultat# & mid(#facture.NoRole#,3,2)><!--- Exerc (Exercice) 2 dernier caractères position 74 - 75 --->
        	<cfset LaPos7475 = mid(#facture.NoRole#,3,2)>
        
        	<!--- Cle5 sur 1 caractère --->
			<cfset NumFormule = #etablissement.DataMatrixCodeEtablissement# & #etablissement.Per# & #etablissement.DataMatrixCodeProduit# & "00" & mid(#facture.NoRole#,3,2)>
            
			<cfset NumFormuleDiv11 = #NumFormule# / 11>
            
			<cfset Reste = Fix(#NumFormuleDiv11#)>
            
            <cfset ResteMultiplie11 = #Reste# * 11>
			
			<cfset Cle5 = 11 - (#NumFormule# - #ResteMultiplie11#)>
            <cfif len(#Cle5#) gt 1>
				<cfset Cle5 = right(#Cle5#,1)>
            </cfif>
            
			<cfset Resultat = #Resultat# & #Cle5#><!--- Clé5 1 caractère position 76 --->
        	<cfset LaPos76 = #Cle5#>
        
        	<cfset Resultat = #Resultat# & #etablissement.DatamatrixCodeEmetteur#><!--- CodeEmetteur (6 caractères position 77 - 82 --->
            <cfset LaPos7782 = #etablissement.DatamatrixCodeEmetteur#>
			
			<cfset Resultat = #Resultat# & "0001"><!--- CodeEtablissement (4 caractères figé position 83 - 86 --->
        	<cfset LaPos8386 = "0001">
			
        	<!--- Cle3 sur 2 car --->
            <cfset txtDataMatrix = #etablissement.DatamatrixCodeEmetteur# & "0001">
            <cfset Rang = 0>
            <cfset valDatamatrix = 0>
            <cfloop from="#Len(txtDataMatrix)#" to="1" step="-1" index="i">
            	<cfset Rang = #Rang# + 1>
            	<cfset valDatamatrix = #valDatamatrix# + (Val(Mid(#txtDataMatrix#, #i#, 1)) * #Rang#)>
            </cfloop>
            <cfset Cle3 = #valDatamatrix# Mod 100>
            <cfif len(#Cle3#) eq 1>
            	<cfset Cle3 = "0" & #Cle3#>
            </cfif>
			<cfset Resultat = #Resultat# & #Cle3#><!--- Clé3 2 caractère position 87 - 88 --->
            <cfset LaPos8788 = #Cle3#>
            
            <cfset Resultat = #Resultat# & " "><!--- 1 caractère position 89 --->
            <cfset LaPos89 = " " >
            
            <!---<cfset ReferenceOperation = "1" & "00">
            <cfset Completelongueurnofacture = 8 - len(#facture.NoFacture#)>
            <cfset ReferenceOperation = #ReferenceOperation# & RepeatString("0",#Completelongueurnofacture#) & #facture.NoFacture#>
            <!--- ReferenceOperation pos 92 - 115 --->
            <cfset txtDataMatrix = #ReferenceOperation# & 9>   
            <!--- cle 2  pos 90 - 91 --->
            <cfset Rang = 0>
            <cfset valDatamatrix = 0>
            <cfloop from="#Len(txtDataMatrix)#" to="1" step="-1" index="i">
            	<cfset Rang = #Rang# + 1>
            	<cfset valDatamatrix = #valDatamatrix# + (Val(Mid(#txtDataMatrix#, #i#, 1)) * #Rang#)>
            </cfloop>
            <cfset Cle2 = #valDatamatrix# Mod 100>
            <cfif len(#Cle2#) eq 1>
            	<cfset Cle2 = "0" & #Cle2#>
            </cfif>
            
            <cfset Resultat = #Resultat# & #Cle2# & #ReferenceOperation# & 9>--->
            
            
			<!--- detail de la ref opération de droite à gauche --->
            <!--- 1 car (code application à 4)
			6 car codique du poste
			15 car NoFacture complété à gauche par des 0
			2 car Cle2 --->
			
			<cfset Completelongueurnofacture = 15 - len(#facture.NoFacture#)>
            <cfset NoFactureComplete = RepeatString("0",#Completelongueurnofacture#) & #facture.NoFacture#>
            
			<cfset ReferenceOperation = "4">
            <cfset ReferenceOperation = #etablissement.codiqueduposte# & #ReferenceOperation#>
            <cfset ReferenceOperation = #NoFactureComplete# & #ReferenceOperation#>
			<cfset ReferenceOperation = #facture.Cle2DataMatrix# & #ReferenceOperation#>
                        
            <cfset txtDataMatrix = #ReferenceOperation# & 9>            
			<!--- cle 2  pos 90 - 91 --->
            <cfset Rang = 0>
            <cfset valDatamatrix = 0>
            <cfloop from="#Len(txtDataMatrix)#" to="1" step="-1" index="i">
            	<cfset Rang = #Rang# + 1>
            	<cfset valDatamatrix = #valDatamatrix# + (Val(Mid(#txtDataMatrix#, #i#, 1)) * #Rang#)>
            </cfloop>
            <cfset Cle2 = #valDatamatrix# Mod 100>
            <cfif len(#Cle2#) eq 1>
            	<cfset Cle2 = "0" & #Cle2#>
            </cfif>
			
            
			<cfset Resultat = #Resultat# & #Cle2# & #ReferenceOperation# & 9>
            
            
            
            
            <cfset LaPos9091 = #Cle2#>
            <cfset LaPos92115 = #ReferenceOperation#>
            <cfset LaPos116 = "9">
            
            
            <!--- cle1 pos 117 - 118 --->
            
            <cfset ToTalTtcSansVirgule = #facture.TotalTTC# * 100>
            <cfset complete = 8 - len(#ToTalTtcSansVirgule#)>
            <cfset ToTalTtcSansVirgule = RepeatString(" ",#complete#) & #ToTalTtcSansVirgule#>
            <cfset Espace1 = "8" & "06" & #ToTalTtcSansVirgule#>
            
            <cfset txtDataMatrix = #Espace1#>   
            <!--- cle 1  pos 117 - 118 --->
            <cfset Rang = 0>
            <cfset valDatamatrix = 0>
            <cfloop from="#Len(txtDataMatrix)#" to="1" step="-1" index="i">
            	<cfset Rang = #Rang# + 1>
            	<cfset valDatamatrix = #valDatamatrix# + (Val(Mid(#txtDataMatrix#, #i#, 1)) * #Rang#)>
            </cfloop>
            <cfset Cle1 = #valDatamatrix# Mod 100>
            <cfif len(#Cle1#) eq 1>
            	<cfset Cle1 = "0" & #Cle1#>
            </cfif>
            
            <cfset Resultat = #Resultat# & #Cle1# & "8" & "06" & " " & #ToTalTtcSansVirgule#>
            <cfset LaPos117118 = #Cle1#>
            <cfset LaPos119 = "8">
            <cfset LaPos120121 = "06">
			<cfset LaPos122 = " ">
            <cfset LaPos123130 = #ToTalTtcSansVirgule#>
                        
            <cfquery name="majfact" datasource="f8w">
            	update facture set Pos6567='#LaPos6567#', Pos68='#LaPos68#', Pos6971='#LaPos6971#', Pos7273='#LaPos7273#', 
                Pos7475='#LaPos7475#', Pos76='#LaPos76#', Pos7782='#LaPos7782#', Pos8386='#LaPos8386#', 
                Pos8788='#LaPos8788#', Pos89='#LaPos89#', Pos9091='#LaPos9091#', Pos92115='#LaPos92115#', 
                Pos116='#LaPos116#', Pos117118='#LaPos117118#', Pos119='#LaPos119#', Pos120121='#LaPos120121#', 
                Pos122='#LaPos122#', Pos123130='#LaPos123130#' where id='#facture.id#'
            </cfquery>
           
        </cfif>
        <cfreturn Resultat>
	</cffunction>
    <cffunction name="ProductionDataMatrix" access="public" returntype="any">
    	<cfargument name="NomFichier" type="string">
        <cfargument name="value" type="string">
		
        <cfset imgdatamatrix = "c:\TempWeb\" & #arguments.NomFichier#>
        <cfset resoluton = 25>
        <cfset shape = "SQUARE">
        
        <cfobject action="create" name="Reader" type="java" class="org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean">
        <cfobject action="create" name="sharpner" type="java" class="org.krysalis.barcode4j.impl.datamatrix.SymbolShapeHint">
        <cfset out = CreateObject("java","java.io.FileOutputStream").Init(CreateObject("java","java.io.File").Init('#imgdatamatrix#')) />
        
        <!--- JPG file format --->
        <cfset BufferedImage = CreateObject('java',"java.awt.image.BufferedImage")>
        <cfobject action="create" name="Objcanvas" type="java" class="org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider">
        <cfset canvas = Objcanvas.init(out, "image/jpeg", JavaCast('double',resoluton), BufferedImage.TYPE_BYTE_BINARY, false, 0)>
        <cfset ctype = "image/jpeg">
        
        <cfset Reader.setModuleWidth(JavaCast('int',1))>
        
        <!--- set the shape of the barcode --->
        <cfswitch expression="#shape#">
            <cfcase value="SQUARE">
              <cfset Reader.setShape(sharpner.FORCE_SQUARE)>
            </cfcase>
            <cfcase value="RECTANGLE">
              <cfset Reader.setShape(sharpner.FORCE_RECTANGLE)>
            </cfcase>
            <cfdefaultcase>
              <cfset Reader.setShape(sharpner.FORCE_NONE)>
            </cfdefaultcase>
        </cfswitch>
        
        <cfset Reader.generateBarcode(canvas, JavaCast('string',value))>
        <cfset canvas.finish()>
        <cfset out.close()>
        
        <cfset Resultat = "Ok">
        
        <cfreturn Resultat>    
    
    
    </cffunction>
    <!---<cffunction name="Cle5" access="public" returntype="any">
    	<cfargument name="txtDataMatrix" type="string" required="yes">
        <cfargument name="Modulo" type="numeric" required="yes">
        <cfset Cle5 = 0>
        <cfset ValDataMatrix = val(#Arguments.txtDataMatrix)>
        <cfset Cle5 = #valDatamatrix# - Fix(#valDatamatrix# / #Arguments.Modulo#) * #Arguments.Modulo#>
    Function Clé(txtDataMatrix, Modulo) As Integer
    Dim valDatamatrix As Double
    Clé5 = 0
    If IsNumeric(txtDataMatrix) = True Then
        valDatamatrix = CDbl(txtDataMatrix)
        Clé = valDatamatrix - Fix(valDatamatrix / Modulo) * Modulo
    Else
        Call MsgBox("La clé modulo " & Modulo & " pose des problèmes", vbCritical, "DataMatrix")
    End If
End Function
    </cffunction>--->
    
</cfcomponent>