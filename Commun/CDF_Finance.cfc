﻿<cfcomponent>
	<cffunction name="SoldeClient" access="public" returntype="any">
		<cfargument name="EtablissementSiret" required="yes">
        <cfargument name="EtablissementNum" required="yes">
        <cfargument name="IdClient" required="yes">
        <cfargument name="Adate" required="no" default="">
        <cfset Qsolde = querynew("TotFact,TotEnc,Solde")>
        <cfif len(#Arguments.Adate#) gt 0>
        	<cfset bornedate = createdate(mid(#Arguments.Adate#,7,4),mid(#Arguments.Adate#,4,2),mid(#Arguments.Adate#,1,2))>
		<cfelse>
        	<cfset bornedate = "">
        </cfif>
        <cfset tot = 0>
        <cfset totenc = 0>
        <cfset solde = 0>
		<!--- lecture famille --->
        <cfquery name="parent" datasource="f8w">
        	Select siret,NumClient from client where id='#Arguments.IdClient#'
        </cfquery>
        <cfif #parent.recordcount#>
			<!--- lecture facture --->
            <cfif #parent.siret# neq #Arguments.EtablissementSiret#>
                <cfquery name="facture" datasource="f8w">
                    Select NumRole,NumFacture,TotalTTC from facture use index(siret_IdClient) 
                    Where siret='#Arguments.EtablissementSiret#' and IdClient='#Arguments.IdClient#' 
                    and NumE='#Arguments.EtablissementNum#' and del=0
                </cfquery>
            <cfelse>    
                <cfquery name="facture" datasource="f8w">
                    Select NumRole,NumFacture,TotalTTC from facture use index(siret_numclient) 
                    Where siret='#Arguments.EtablissementSiret#' and NumClient='#parent.NumClient#'
                    and NumE='#Arguments.EtablissementNum#' and del=0
                </cfquery>		
            </cfif>	
            <cfloop query="facture">
            	<!--- controle si facture concerne prépaiement ou non --->
                <cfset ConcernePrepaiement = "non">
                <cfquery name="prestafacture" datasource="f8w">
                	Select NumPrestation from itemfacture use index(siret_numfacture) Where siret='#Arguments.EtablissementSiret#' 
                    and numfacture='#facture.numfacture#' group by NumPrestation
                </cfquery>
                <cfloop query="prestafacture">
                	<cfquery name="presta" datasource="f8w">
                    	Select Ticket from prestation use index(siret_numprestation) Where siret='#Arguments.EtablissementSiret#' 
                        and numprestation='#prestafacture.numprestation#'
                    </cfquery>
                    <cfif #presta.Ticket# eq 1>
                    	<cfset ConcernePrepaiement = "oui">
                        <cfbreak>
                    </cfif>
                </cfloop>
                <cfif #ConcernePrepaiement# eq "non">
                    <cfquery name="role" datasource="f8w">
                        Select id,DatePaiement from role use index(siret_NumRole) 
                        Where siret='#Arguments.EtablissementSiret#' and NumRole='#facture.NumRole#' and typerole<>2 and del=0
                    </cfquery>
                    <cfif #role.recordcount#>
                        <cfset datepairole = createdate(mid(#role.datepaiement#,7,4),mid(#role.datepaiement#,4,2),mid(#role.datepaiement#,1,2))>  
                        <cfset datevalide = "oui">
                        <cfif len(#Arguments.Adate#) gt 0>
                            <cfif datecompare(#datepairole#,#bornedate#) neq -1>
                                <cfset datevalide = "non">
                            </cfif>
                        </cfif>
                        <cfif #role.recordcount# and #datevalide# eq "oui">
                            <cfset tot = #tot# + #facture.TotalTTC#>
                        </cfif>
                    </cfif>
				</cfif>
            </cfloop>
            <!--- lecture encaissement --->
            <!---<cfif #parent.siret# neq #Arguments.EtablissementSiret#>
                <cfquery name="enc" datasource="f8w">
                    Select * from encaissement use index(siret_IdClient) 
                    Where siret='#Arguments.EtablissementSiret#' and IdClient='#Arguments.IdClient#' 
                    and NumE='#Arguments.EtablissementNum#'
                </cfquery>
           <cfelse>
                <cfquery name="enc" datasource="f8w">
                    Select * from encaissement use index(siret_NumClient) 
                    Where siret='#Arguments.EtablissementSiret#' and NumClient='#parent.NumClient#' 
                    and NumE='#Arguments.EtablissementNum#'
                </cfquery>       
           </cfif>--->
           <!--- modif 24/11/2020 ont prend pas les encaissements prépaiement --->
           <cfif #parent.siret# neq #Arguments.EtablissementSiret#>
                <cfquery name="enc" datasource="f8w">
                    Select * from encaissement use index(siret_IdClient) 
                    Where siret='#Arguments.EtablissementSiret#' and IdClient='#Arguments.IdClient#' 
                    and NumE='#Arguments.EtablissementNum#' and NumPrestation=0 and id_prestation_prepaye_portefeuille_ent=0
                </cfquery>
           <cfelse>
                <cfquery name="enc" datasource="f8w">
                    Select * from encaissement use index(siret_NumClient) 
                    Where siret='#Arguments.EtablissementSiret#' and NumClient='#parent.NumClient#' 
                    and NumE='#Arguments.EtablissementNum#' and NumPrestation=0 and id_prestation_prepaye_portefeuille_ent=0
                </cfquery>       
           </cfif> 
            <cfloop query="enc">
                <cfset datevalide = "oui">
                <!---<cfif len(#Arguments.Adate#) gt 0>
                    <cfif datecompare(#enc.datepaiement#,#bornedate#) neq -1>
                        <cfset datevalide = "non">
                    </cfif>
                </cfif>--->
                <cfif #enc.MoyenPaiement# eq 4><!--- encaissement contentieux, ont verifie si facture direct ont prend en compte dans le total sinon non car un encaissement prelevement existe --->
                    <cfquery name="facture" datasource="f8w">
                        Select id from facture use index(siret_idclient) 
                        where siret='#Arguments.EtablissementSiret#' and IdClient='#Arguments.IdClient#' 
                        and NumRole='#enc.numrole2#' and modepaiement=1 and del=0
                    </cfquery>
                    <cfif #facture.recordcount# eq 0 and #datevalide# eq "oui">
                        <cfset totenc = #totenc# + #enc.MontantPayé#>
                    </cfif>
				<cfelseif #datevalide# eq "oui">
                    <cfset totenc = #totenc# + #enc.MontantPayé#>
                </cfif>
                    
            </cfloop>
            <cfset solde = val(#tot#) - val(#totenc#)>	
		<cfelse>
        	<cfset solde = 0>
        </cfif>
        <cfset r = queryaddrow(Qsolde)>
        <cfset r = querysetcell(Qsolde,"TotFact",val(#tot#))>
        <cfset r = querysetcell(Qsolde,"TotEnc",val(#totenc#))>
        <cfset r = querysetcell(Qsolde,"Solde",#solde#)>
    	<cfreturn Qsolde>
	</cffunction>
</cfcomponent>