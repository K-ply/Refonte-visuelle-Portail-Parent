﻿<cfif isdefined("form.go")>
	<cfset mod_txt = "">
	<cfset mod_html = "">
	<cfset mod_txt1 = "">
	<cfset mod_html1 = "">
	<cfif #form.NoRue# neq #lect_client.NoRue#>
		<cfset mod_txt = #mod_txt# & "Ancien N° de rue : " & #lect_client.norue# & ", Nouveau N° : " & #form.norue# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien N° de rue : " & #lect_client.norue# & ", Nouveau N° : " & #form.norue# & "</p>">
    </cfif>
	<cfif #form.adr1# neq #lect_client.adr1#>
		<cfset mod_txt = #mod_txt# & "Anciene adresse : " & #lect_client.adr1# & ", Nouvelle : " & #form.adr1# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Anciene adresse : " & #lect_client.adr1# & ", Nouvelle : " & #form.adr1# & "</p>">
    </cfif>
	<cfif #form.adr2# neq #lect_client.adr2#>
		<cfset mod_txt = #mod_txt# & "Ancien complément d'adresse : " & #lect_client.adr2# & ", Nouveau : " & #form.adr2# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien complément d'adresse : " & #lect_client.adr2# & ", Nouveau : " & #form.adr2# & "</p>">
    </cfif>
	<cfif #form.cp# neq #lect_client.cp#>
		<cfset mod_txt = #mod_txt# & "Ancien code postal : " & #lect_client.cp# & ", Nouveau : " & #form.cp# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien code postal : " & #lect_client.cp# & ", Nouveau : " & #form.cp# & "</p>">
    </cfif>
	<cfif #form.ville# neq #lect_client.ville#>
		<cfset mod_txt = #mod_txt# & "Anciene ville : " & #lect_client.ville# & ", Nouvelle : " & #form.ville# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Anciene ville : " & #lect_client.ville# & ", Nouvelle : " & #form.ville# & "</p>">
    </cfif>
	<cfif #form.tel# neq #lect_client.tel#>
		<cfset mod_txt = #mod_txt# & "Ancien Tel : " & #lect_client.tel# & ", Nouveau : " & #form.tel# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien Tel : " & #lect_client.tel# & ", Nouveau : " & #form.tel# & "</p>">
    </cfif>
	<cfif #form.teltravail# neq #lect_client.teltravail#>
		<cfset mod_txt = #mod_txt# & "Ancien Tel travail : " & #lect_client.teltravail# & ", Nouveau : " & #form.teltravail# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien Tel travail : " & #lect_client.teltravail# & ", Nouveau : " & #form.teltravail# & "</p>">
    </cfif>
	<cfif #form.telportable# neq #lect_client.telportable#>
		<cfset mod_txt = #mod_txt# & "Ancien Tel portable : " & #lect_client.telportable# & ", Nouveau : " & #form.telportable# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien Tel portable : " & #lect_client.telportable# & ", Nouveau : " & #form.telportable# & "</p>">
    </cfif>
	<cfif #form.QuotientFamilial# neq #lect_client.QuotientFamilial#>
		<cfif findnocase(",",#form.QuotientFamilial#)>
        	<cfset form.QuotientFamilial = replacenocase(#form.QuotientFamilial#,",",".")>
        </cfif>
		<cfset mod_txt = #mod_txt# & "Ancien Quotient Familial : " & #lect_client.QuotientFamilial# & ", Nouveau : " & #form.QuotientFamilial# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien QuotientFamilial : " & #lect_client.QuotientFamilial# & ", Nouveau : " & #form.QuotientFamilial# & "</p>">
    </cfif>
	<cfif #form.numallocataire# neq #lect_client.numallocataire#>
    	<cfset mod_txt = #mod_txt# & "Ancien N° Allocataire : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien N° Allocataire : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & "</p>">
    </cfif>
    <cfif #form.numtelemployeur# neq #lect_client.numtelemployeur#>
    	<cfset mod_txt = #mod_txt# & "Ancien N° Tel employeur : " & #lect_client.numtelemployeur# & ", Nouveau : " & #form.numtelemployeur# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien N° Tel employeur : " & #lect_client.numtelemployeur# & ", Nouveau : " & #form.numtelemployeur# & "</p>">
    </cfif>        
	<cfif #form.nom2# neq #lect_client.nom2#> 
		<cfset mod_txt1 = #mod_txt1# & "Ancien nom : " & #lect_client.nom2# & ", Nouveau : " & #form.nom2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien nom : " & #lect_client.nom2# & ", Nouveau : " & #form.nom2# & "</p>">
	</cfif>
	<cfif #form.prénom2# neq #lect_client.prénom2#> 
		<cfset mod_txt1 = #mod_txt1# & "Ancien prénom : " & #lect_client.prénom2# & ", Nouveau : " & #form.prénom2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien prénom : " & #lect_client.prénom2# & ", Nouveau : " & #form.prénom2# & "</p>">
	</cfif>
	<cfif #form.NoRue2# neq #lect_client.NoRue2#>
		<cfset mod_txt1 = #mod_txt1# & "Ancien N° de rue : " & #lect_client.norue2# & ", Nouveau N° : " & #form.norue2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien N° de rue : " & #lect_client.norue2# & ", Nouveau N° : " & #form.norue2# & "</p>">
    </cfif>
	<cfif #form.adr21# neq #lect_client.adr21#>
		<cfset mod_txt1 = #mod_txt1# & "Anciene adresse : " & #lect_client.adr21# & ", Nouvelle : " & #form.adr21# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Anciene adresse : " & #lect_client.adr21# & ", Nouvelle : " & #form.adr21# & "</p>">
    </cfif>
	<cfif #form.adr22# neq #lect_client.adr22#>
		<cfset mod_txt1 = #mod_txt1# & "Ancien complément d'adresse : " & #lect_client.adr22# & ", Nouveau : " & #form.adr22# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien complément d'adresse : " & #lect_client.adr22# & ", Nouveau : " & #form.adr22# & "</p>">
    </cfif>
	<cfif #form.cp2# neq #lect_client.cp2#>
		<cfset mod_txt1 = #mod_txt1# & "Ancien code postal : " & #lect_client.cp2# & ", Nouveau : " & #form.cp2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien code postal : " & #lect_client.cp2# & ", Nouveau : " & #form.cp2# & "</p>">
    </cfif>
	<cfif #form.ville2# neq #lect_client.ville2#>
		<cfset mod_txt1 = #mod_txt1# & "Anciene ville : " & #lect_client.ville2# & ", Nouvelle : " & #form.ville2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Anciene ville : " & #lect_client.ville2# & ", Nouvelle : " & #form.ville2# & "</p>">
    </cfif>
	<cfif #form.tel2# neq #lect_client.tel2#>
		<cfset mod_txt1 = #mod_txt1# & "Ancien Tel : " & #lect_client.tel2# & ", Nouveau : " & #form.tel2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien Tel : " & #lect_client.tel2# & ", Nouveau : " & #form.tel2# & "</p>">
    </cfif>
	<cfif #form.teltravail2# neq #lect_client.teltravail2#>
		<cfset mod_txt1 = #mod_txt1# & "Ancien Tel travail : " & #lect_client.teltravail2# & ", Nouveau : " & #form.teltravail2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien Tel travail : " & #lect_client.teltravail2# & ", Nouveau : " & #form.teltravail2# & "</p>">
    </cfif>
	<cfif #form.telportable2# neq #lect_client.telportable2#>
		<cfset mod_txt1 = #mod_txt1# & "Ancien Tel portable : " & #lect_client.telportable2# & ", Nouveau : " & #form.telportable2# & #chr(13)#> 
    	<cfset mod_html1 = #mod_html1# & "<p>Ancien Tel portable : " & #lect_client.telportable2# & ", Nouveau : " & #form.telportable2# & "</p>">
    </cfif>
    <cfif #form.numtelemployeur2# neq #lect_client.numtelemployeur2#>
    	<cfset mod_txt = #mod_txt# & "Ancien N° Tel employeur : " & #lect_client.numtelemployeur2# & ", Nouveau : " & #form.numtelemployeur2# & #chr(13)#> 
    	<cfset mod_html = #mod_html# & "<p>Ancien N° Tel employeur : " & #lect_client.numtelemployeur2# & ", Nouveau : " & #form.numtelemployeur2# & "</p>">
    </cfif> 
    <cfif #form.bic# neq #lect_client.bic#><!--- modif iban --->
    	<cfif #form.bic# eq "">
        	<cfquery name="maj" datasource="f8w">
            	Update client set bic='', iban='', ficapnom='', bcnom='', LibPrel='', ModePaiement=0, NumRolePremSepa=0 
                Where id='#form.id#'
            </cfquery>
        <cfelse>
			<cfset ModePaiement = 1>
			<cfset bcnom = module.ConformiteSepa(#ucase(form.bcnom)#)>
            <cfset ficapnom = module.ConformiteSepa(#ucase(form.ficapnom)#)>
            <cfset titulaire = #form.bcnom#>
            <cfset iban = #ucase(form.iban1)# & #form.iban2# & #form.iban3# & #form.iban4# & #form.iban5# & #form.iban6# & #form.iban7#>
            <cfset rum = CreateUUID()>
            <cfset rum = mid(#rum#,1, 35)>
            <cfset DateSignature = lsdateformat(now(),"dd/mm/YYYY")>    
			<cfset NumRolePremSepa = 0>
            <cfquery name="maj" datasource="f8w">
            	Update client set bic='#form.bic#', iban='#iban#', ficapnom='#ficapnom#', 
                bcnom='#bcnom#', LibPrel='#form.LibPrel#', ModePaiement=1, NumRolePremSepa=0, rum='#rum#'
                Where id='#form.id#'
            </cfquery>
    	</cfif>
    </cfif>
	<cfif len(#mod_txt#) or len(#mod_txt1#)>
		<cfupdate datasource="f8w" tablename="client" formfields="id,Norue,adr1,adr2,cp,ville,tel,teltravail,telportable,Quotientfamilial,nom2,prénom2,Norue2,adr21,adr22,cp2,ville2,tel2,teltravail2,telportable2,numtelemployeur,numtelemployeur2,numallocataire">
		<cfset sujet = "Modification de données par " & #lect_client.nom# & " " & #lect_client.prénom#>
		<cfset txt_body = "Des données ont été modifiées dans l'espace parent :" & #chr(13)#>
        <cfset html_body = "<p>Des données ont été modifiées dans l'espace parent :</p>">
		<cfif len(#mod_txt#)>
			<cfset txt_body = #txt_body# & "Pour le parent 1 (" & #lect_client.nom# & " " & #lect_client.prénom# & ") :" & #chr(13)#>
			<cfset txt_body = #txt_body# & #mod_txt#>
			<cfset html_body = #html_body# & "<p>Pour le parent 1 (" & #lect_client.nom# & " " & #lect_client.prénom# & ") :</p>">
			<cfset html_body = #html_body# & #mod_html#>
        </cfif>
        <cfif len(#mod_txt1#)>
			<cfset txt_body = #txt_body# & "Pour le parent 2 (" & #lect_client.nom2# & " " & #lect_client.prénom2# & ") :" & #chr(13)#>
			<cfset txt_body = #txt_body# & #mod_txt1#>
			<cfset html_body = #html_body# & "<p>Pour le parent 2 (" & #lect_client.nom2# & " " & #lect_client.prénom2# & ") :</p>">
			<cfset html_body = #html_body# & #mod_html1#>            
		</cfif>
		<cfquery name="cptmailserv" datasource="mission">
            Select * from cpt_appel_msg where pour='MSG'
        </cfquery>        
        <cfquery name="addspool" datasource="services">
            Insert into spool_mail (de,pour,sujet,html_body,txt_body,joint,date_crea,
            fail_to,EmailAccuseRecep,EmailNotifRecep,siret,leserveur,
            utilisateur,mailutilisateur,motdepasse) 
            values('message@cantine-de-france.fr','#lect_etablissement.EmailE#',
            <cfqueryparam value="#sujet#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,
            '',#now()#,'#lect_etablissement.EmailE#',
            '#lect_etablissement.EmailAccuseRecep#','#lect_etablissement.EmailNotifRecep#',
            '#lect_etablissement.siret#',
            '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
        </cfquery>
		<cfset article = "">
        <cfswitch expression="#mid(lect_etablissement.nomE,1,2)#">
        	<cfcase delimiters="," value="CO,MA">
            	<cfset article = "La">
            </cfcase>
        	<cfcase delimiters="," value="SI,CE">
            	<cfset article = "Le">
            </cfcase>
            <cfcase value="AS">
            	<cfset article = "L'">
            </cfcase>
            <cfdefaultcase><cfset article = ""></cfdefaultcase>
        </cfswitch>
		<cfset url.msg = "Modification effectuées avec succès, " & #article# & " " & #lect_etablissement.nomE# & " a été averti par mail.">




        <cfquery name="lect_client" datasource="f8w">
            Select * from client use index(siret_NumClient) Where siret = '#lect_user.siret#' and NumClient = '#lect_user.NumClient#'
        </cfquery>
    </cfif>
</cfif>