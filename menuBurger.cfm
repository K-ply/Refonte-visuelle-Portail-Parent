﻿<nav id="navMobile" class="col-lg-2"><!--- MENU --->
    <ul id="navMobileItems">
        <!--- pour document obligatoire --->
        <cfif IsDefined("Documents")>
            <cfquery name="docobliattentvalidationbloque" dbtype="query">
                Select * from Documents where Obligatoire=1 and statut<>'Validé'
            </cfquery>
        <cfelse>
            <cfquery name="docobliattentvalidationbloque" datasource="f8w_test">
                Select * from application_client where id=0
            </cfquery>
        </cfif>
        <!--- --->

        <cfloop query="lect_menu"><!--- affichage menu niveau 0 (pas de sous menu) --->
            <cfset onaffiche = "oui">
            <cfif (#lect_menu.id# eq 88 and #reinscription_ent.recordcount# eq 0)>
                <cfset onaffiche = "non">
            </cfif>

            <cfquery name="autreetabaveccb" datasource="f8w_test">
                Select * from application_client where siret like '#lect_etablissement.siret#%' 
                and (CBBanque>0 or CBTipi=1)
            </cfquery>

            <!--- menu acheter --->
            <cfif #lect_menu.id# eq 150 and (#lect_etablissement.CBBanque# gt 0 or #lect_etablissement.CBTipi# eq 1) or #lect_menu.id# eq 150 and #autreetabaveccb.recordcount#>
                <cfquery name="tick" datasource="f8w_test" maxrows="1">
                    Select id from prestation use index(siret) Where siret='#lect_etablissement.siret#' and Ticket=1 
                    and TopWeb=1 and ResaPeriode=0 and del=0 limit 0,1
                </cfquery>
                <cfif #tick.recordcount# eq 0>
                    <cfset onaffiche = "non">
                </cfif>
                <!--- pour st quentin la poterie des client en prelèvement et d'autre en prepaiement --->
                <cfif #lect_etablissement.siret# eq "11821135031090">
                    <!--- client dans groupe client prépaiement ? --->
                    <cfquery name="clientgroupe" datasource="f8w_test">
                        Select * from clientgroupe use index(siret_NumClient_NumGroupe) Where siret='#lect_etablissement.siret#' 
                        and NumClient='#lect_client.numclient#' and NumGroupe='1000000000'
                    </cfquery>
                    <cfif #clientgroupe.recordcount# eq 0>
                        <cfset onaffiche = "non">
                    </cfif>
                </cfif>
                    
            <cfelseif #lect_menu.id# eq 150>   
                <cfset onaffiche = "non">
            </cfif>
            
            <cfif #lect_menu.id# eq 152 and #menucantine.recordcount# eq 0><!--- menu cantine enfant --->
                <cfif len(#lect_etablissement.UrlMenu#) eq 0>
                    <cfset onaffiche = "non">
                </cfif>
            </cfif>

        <!--- Gestion affichage onglet ajout, modif enfant et modif parent --->
            <cfset afficheajoutenfant = "oui">
            <cfset affichemodifparent = "oui">
            <cfset affichemodifenfant = "oui">

            <cfif #lect_etablissement.InscriptionOnline# neq 1><!--- ajout enfant --->
                <cfset afficheajoutenfant = "non">
            </cfif>
        
            <cfif #lect_etablissement.AjoutEnfantOnline# neq 1><!--- ajout enfant --->
                <cfset afficheajoutenfant = "non">
            </cfif>
            <cfif #lect_etablissement.NombreEnfantMax# gt 0>
                <cfif #session.NombreEnfantMax# is true>
                    <cfset afficheajoutenfant = "non">
                </cfif>
            </cfif>

            <cfif#lect_etablissement.ModifParentParParent# neq 1><!--- Modif Données Parent --->
                <cfset affichemodifparent = "non">
            </cfif>
            <cfif #lect_etablissement.ModifEnfantParParent# neq 1 or #aumoinsunenfant.recordcount# eq 0><!--- Modif Données Enfant --->
                <cfset affichemodifenfant = "non">
            </cfif>

            <cfif #lect_menu.id# eq 151 && afficheajoutenfant eq "non" && affichemodifparent eq "non" && affichemodifenfant eq "non">
                <cfset onaffiche = "non">
            </cfif>
        <!--- --->

            <!--- Affiche ---><!---
            <cfset docobliattentvalidationbloque.recordcount = 2>--->
            <cfif #docobliattentvalidationbloque.recordcount# neq 0 and (#lect_menu.id# neq 153 and #lect_menu.id# neq 148 and #lect_menu.id# neq 146 and #lect_menu.id# neq 151)><!--- document a transmetre  et pas menu document ou message ou deconnexion ou ajouter un enfant ou donnée parent ou données enfants --->
                <cfset onaffiche = "non">
            </cfif>

            <cfif #onaffiche# eq "oui">
                <cfset post = "">
                <cfif #lect_menu.id# eq 24><!--- message --->
                    <cfquery name="message" datasource="f8w_test">
                        Select id from message use index(id_client_Lu) Where id_client='#lect_client.id#' and Lu=0
                    </cfquery>
                    <cfif #message.recordcount#>
                        <cfset post = "(" & #message.recordcount# & ")">
                    </cfif>
                </cfif>

                <!--- Permet de définir le padding bottom de l'élément du menu (plus grand pour l'accueil et la déconnexion)--->
                <cfif #lect_menu.id# eq 149 or #lect_menu.id# eq 145>
                    <li class="pb-5">
                <cfelse>
                    <li class="pb-3">
                </cfif>

                <cfif #url.id_mnu# eq #lect_menu.id# and #url.id_Smnu# eq 0><!--- selectionné et pas de sous menu cliqué--->
                    <a href="#" class="d-flex align-items-center select <cfoutput>#lect_menu.class_icon_active#</cfoutput>"><span class="ps-2"><cfoutput>#lect_menu.Designation# #post#</cfoutput></span></a></li>
                <cfelse><!--- pas selectionné --->
                    <a href="<cfoutput>#lect_menu.url#</cfoutput>" class="d-flex align-items-center <cfoutput>#lect_menu.class_icon_inactive#</cfoutput>" target="<cfoutput>#lect_menu.target#</cfoutput>"><span class="ps-2"><cfoutput>#lect_menu.Designation# #post#</span></cfoutput></a></li>
                </cfif>

                <!--- sous menu (pour le moment pas de sous menu utilisés) --->
                <cfif #lect_menu.enfant# eq 1 and #url.id_mnu# eq #lect_menu.id#>
                    <cfquery name="lect_sous_menu" datasource="f8w_test">
                        Select * from application_menu Where parent='#lect_menu.id#' and find_in_set(id,'#lect_niveau.lst_menu#') and actif = 1 and validé=1 order by ordre
                    </cfquery>
                    <cfloop query="lect_sous_menu">
                        <cfif #url.id_Smnu# eq #lect_sous_menu.id#><!--- selectionné --->
                            <li><a href="#"><cfoutput>#lect_sous_menu.Designation#</cfoutput></a></li>
                        <cfelse><!--- pas selectionné --->
                            <li><a href="<cfoutput>#lect_sous_menu.url#</cfoutput>" target="<cfoutput>#lect_sous_menu.target#</cfoutput>"><cfoutput>#lect_sous_menu.Designation#</cfoutput></a></li>
                        </cfif>
                    </cfloop>
                </cfif>
            </cfif>
        </cfloop>
        <li class="pb-3"><a href="/img/FasciculeCantinedeFrance.pdf" class="d-flex align-items-center icon-aide" target="_blank"><span class="ps-2">Aide</span></a></li>
    </ul>
</nav>