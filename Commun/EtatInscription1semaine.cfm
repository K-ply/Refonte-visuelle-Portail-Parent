﻿<cfdocument format="PDF" filename="#leficpdf#" orientation="LANDSCAPE" pagetype="A4" unit="cm" margintop="1" marginbottom="1" marginleft="1" marginright="1">
    <cfoutput>
    <cfset laimg = replacenocase(#quer1.logoedition#,"C:/inetpub/wwwroot/gestion1.cantine-de-france","")>        
    <table width="100%" border="0">
        <tbody>
            <tr>
                <td rowspan="4"><img width="80" height="80" src="#laimg#"></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><font face="arial">#quer1.etablissement#</font></td>
            </tr>
            <tr>
                <td><font face="arial">Etat des inscriptions (commande de repas) de la prestation #quer1.prestation#</font></td>
            </tr>
            <tr>
                <td><font face="arial">Pour la semaine du #quer1.jour#</font></td>
            </tr>
            <cfif len(#quer1.delai#)>
                <tr>
                    <td colspan="2" align="center"><font color="##F80409" face="arial"><b>#quer1.delai#</b></font></td>
                </tr>
            </cfif>
        </tbody>
    </table>
    <table width="100%" border="0">
        <tbody>
            <tr>
                <td><font face="arial"><b>Ecole et classe</b></font></td>
                <td><font face="arial"><b>Mention</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Lu&nbsp;</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Ma&nbsp;</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Me&nbsp;</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Je&nbsp;</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Ve&nbsp;</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Sa&nbsp;</b></font></td>
                <td><font align="center" face="arial"><b>&nbsp;Di&nbsp;</b></font></td>
            </tr>
            <cfloop query="quer1">
                <cfif #quer1.typeligne# eq 0 and isdefined("form.editionblanche") is false>
                    <cfset fond = "##BBB4B4">
                <cfelse>
                    <cfset fond = "##FBF8F8">
                </cfif>
                <tr bgcolor="#fond#">
                    <td nowrap><font face="arial">#quer1.classe# #quer1.instit#</font></td>
                    <td nowrap><font face="arial">#quer1.mention#</font></td>
                    <td align="center"><font face="arial">#quer1.nj1#</font></td>
                    <td align="center"><font face="arial">#quer1.nj2#</font></td>
                    <td align="center"><font face="arial">#quer1.nj3#</font></td>
                    <td align="center"><font face="arial">#quer1.nj4#</font></td>
                    <td align="center"><font face="arial">#quer1.nj5#</font></td>
                    <td align="center"><font face="arial">#quer1.nj6#</font></td>
                    <td align="center"><font face="arial">#quer1.nj7#</font></td>
                </tr>
            </cfloop>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="9"><font face="arial"><cfoutput>Imprimé par www.cantine-de-france.fr le #lsDateFormat(Now(), "dd mmmm yyyy")# à  #TimeFormat(Now(), "HH:mm:ss")#</cfoutput></font></td>
            </tr>
        </tbody>
    </table>
    </cfoutput>
</cfdocument>