﻿<div class="col p-3">
	<table class="table calendar">
		<tr>
			<td colspan="7" align="center">
				<h4 class="date-title">  
					<b><cfoutput>
						<script>
							var month='#lsdateformat(Date_calend_dep,"mmmm")#';
							document.write(month.charAt().toUpperCase() + month.substring(1).toLowerCase());                
						</script>
					</cfoutput></b>&nbsp;
					<cfoutput>#lsdateformat(Date_calend_dep,"YYYY")#</cfoutput>
				</h4>
			</td>
		</tr>
		<tr align="center">
			<td class="bold">lun</td>
			<td class="bold">mar</td>
			<td class="bold">mer</td>
			<td class="bold">jeu</td>
			<td class="bold">ven</td>
			<td class="bold">sam</td>
			<td class="bold">dim</td>
		</tr>

		<cfoutput query="calendrier">
			<cfset bg11 = removeChars(#bg1#, 1, 1)>
			<cfset bg22 = removeChars(#bg2#, 1, 1)>
			<cfset bg33 = removeChars(#bg3#, 1, 1)>
			<cfset bg44 = removeChars(#bg4#, 1, 1)>
			<cfset bg55 = removeChars(#bg5#, 1, 1)>
			<cfset bg66 = removeChars(#bg6#, 1, 1)>
			<cfset bg77 = removeChars(#bg7#, 1, 1)>

			<tr align="center">
				<td class="bg-#bg11#"><font size="-1">#cont1#</font></td>
				<td class="bg-#bg22#"><font size="-1">#cont2#</font></td>
				<td class="bg-#bg33#"><font size="-1">#cont3#</font></td>
				<td class="bg-#bg44#"><font size="-1">#cont4#</font></td>
				<td class="bg-#bg55#"><font size="-1">#cont5#</font></td>
				<td class="bg-#bg66#"><font size="-1">#cont6#</font></td>
				<td class="bg-#bg77#"><font size="-1">#cont7#</font></td>
			</tr>
		</cfoutput>
	</table>
</div>