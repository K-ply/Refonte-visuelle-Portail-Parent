﻿<cfinclude template="/Query_Header.cfm">

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Mentions légales</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

    <body>
        <div id="bgBlur">
            <script type="text/javascript"> //Script gestion menu burger
                function openNav() {
                    document.getElementById("navMobileItems").style.display= "block";
                    document.getElementById("navMobileLabel").style.display= "none";
                    const background = document.getElementById("bgBlur");
                    background.className = "bg-blur";
                    background.addEventListener("click", closeNav);
                }
                function closeNav() {
                    document.getElementById("navMobileItems").style.display= "none";
                    document.getElementById("bgBlur").classList.remove("bg-blur");
                    document.getElementById("navMobileLabel").style.display= "block";
                }
            </script>

            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>

            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-9 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p>
                            <h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>#lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3>
                        </p>
                    </div>

                    <!-- CONTENT -->
                    <div class="mt-5 m-3 p-2 p-lg-5 bg-white section scroll mentionslegales">
                        <cfinclude  template="/mentionslegales/mentionslegales.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions légales et confidentialité</a>
                <p class="m-0 pe-3 ps-3">N° de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" alt="bouton menu déroulant" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">
    </body>
</html>