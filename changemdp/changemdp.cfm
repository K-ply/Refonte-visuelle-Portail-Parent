﻿<div class="row d-flex justify-content-center mt-5 content">
    <div class="p-2 p-lg-5 bg-white section scroll mobile-no-scroll">
        <div class="mb-5 text-md-center">
            <p><span class="bold">Modifier mon mot de passe</span></br>
            <span class="fst-italic">Veuillez saisir votre ancien mot de passe et votre nouveau</span></br>
            <span class="text-red">Le mot de passe doit contenir au moins 8 caract&egrave;res avec au moins un chiffre et une lettre.</span></p>
        </div>

        <cfform class="separation-line pb-3 change-infos" name="modpsw" method="post" preservedata="yes">
            <table class="table w-100 text-grey">
                <tr class="row">
                    <td class="border-none d-md-flex justify-content-md-end col-12 col-md">
                        <label for="oldmdp">Mon ancien mot de passe* :</label>
                    </td>

                    <td class="border-none col-12 col-md">
                        <cfinput name="oldmdp" id="oldmdp" type="password" required="yes" message="Veuillez renseigner votre ancien mot de passe">
                    </td>
                </tr>

                <tr class="row">
                    <td class="border-none d-md-flex justify-content-md-end col-12 col-md">
                        <label for="mdp">Mon nouveau mot de passe* :</label>
                    </td>

                    <td class="border-none col-12 col-md">
                        <cfinput name="mdp" id="mdp" type="text" required="yes" message="Veuillez renseigner votre nouveau mot de passe">
                    </td>
                </tr>
                <tr class="row">
                    <td class="border-none d-md-flex justify-content-md-end col-12 col-md">
                        <label for="mdpconf">Confirmation nouveau mot de passe* :</label>
                    </td>

                    <td class="border-none col-12 col-md">
                        <cfinput name="mdpconf" id="mdpconf" type="text" required="yes" message="Veuillez confirmer votre nouveau mot de passe">
                    </td>
                </tr>
                <tr class="d-flex justify-content-center">
                    <td class="border-none">
                        <cfif len(#msg#)>
                            <p class="text-red"><cfoutput>#msg#</cfoutput></p>
                        <cfelseif len(#msg1#)>
                            <p class="text-red"><cfoutput>#msg1#</cfoutput></p>
                        </cfif>

                        <cfif #session.log_demo#>
                            <cfinput disabled name="modifpsw" id="modifpsw" type="submit" value="Valider" class="btn btn-primary">
                        <cfelse>
                            <cfinput name="modifpsw" id="modifpsw" type="submit" value="Valider" class="btn btn-primary">
                        </cfif>
                    </td>
                </tr>
            </table>
        </cfform>

        <div class="mt-5 mb-5 text-md-center">
            <p><span class="bold">Modifier mes options de notification et de s&eacute;curit&eacute;</span></br>
            <span class="text-red">Saisir votre mot de passe pour modifier vos options.</span></p>
        </div>
        <cfform class="d-flex justify-content-center change-infos" name="modnotif" method="post">
            <table class="table w-100 text-grey text-grey">
                <tr class="row">
                    <td class="border-none d-md-flex justify-content-md-end col-12 col-md">
                        <label for="mdp">Saisir votre mot de passe* :</label>
                    </td>
                    <td class="border-none col-12 col-md">
                        <cfinput name="mdpver" id="mdpver" type="password" required="yes" message="Veuillez renseigner votre mot de passe">
                    </td>
                    <cfif len(#msg2#)>
                        <td class="border-none col-12 col-md">
                            <p class="text-red"><cfoutput>#msg2#</cfoutput></p>
                        </td>
                    </cfif>
                </tr>
                <tr class="row">
                    <td class="border-none d-md-flex justify-content-md-end col-12 col-md">
                        Me notifier par mail à chaque connexion à mon espace parent* :
                    </td>
                    <td class="border-none col-12 col-md">
                        <cfif #lect_user.NotifConnect# eq 1>
                            <cfinput type="radio" name="NotifConnect" value="1" checked="yes" message="Veuillez cocher une case"> Oui <cfinput type="radio" name="NotifConnect" value="0" checked="no"> Non
                        <cfelse>
                            <cfinput type="radio" name="NotifConnect" value="1" checked="no" message="Veuillez cocher une case"> Oui <cfinput type="radio" name="NotifConnect" value="0" checked="yes"> Non                            
                        </cfif>
                        <cfinput type="Hidden" name="DblAuth" value="0">
                    </td>
                </tr>
                <tr class="d-flex justify-content-center">
                    <td class="border-none d-lg-flex justify-content-lg-center">
                        <cfinput name="modnotif" type="submit" value="Valider" class="btn btn-primary">
                    </td>
                </tr>
            </table>
        </cfform>
        <p class="text-grey">*Champ obligatoire</p>
    </div>
</div>