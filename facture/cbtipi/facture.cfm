﻿<cfif #lect_etablissement.régierecette# neq 0>
    <!---<cfif #lect_etablissement_miseadispo.recordcount#>
        <cfquery name="enc" datasource="f8w_test">
            Select sum(MontantPayé) as tot from encaissement use index(siret_NumClient) 
            Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#' 
            and NumE='#lect_etablissement.NumEtablissement#' 
            or siret='#lect_etablissement_miseadispo.siret_miseadispo#' and IdClient='#lect_client.id#'
        </cfquery>
        <cfquery name="facture" datasource="f8w_test">
            Select * from facture use index(siret_numclient) 
            Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#'
            and NumE='#lect_etablissement.NumEtablissement#'
            or siret='#lect_etablissement_miseadispo.siret_miseadispo#' and IdClient='#lect_client.id#'
        </cfquery>                        
    <cfelse>--->
    <cfquery name="enc" datasource="f8w_test">
        Select sum(MontantPayé) as tot from encaissement use index(siret_NumClient) 
        Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#' 
        and NumE='#lect_etablissement.NumEtablissement#'
    </cfquery>
    <cfquery name="facture" datasource="f8w_test">
        Select * from facture use index(siret_numclient) 
        Where siret='#lect_user.siret#' and NumClient='#lect_user.NumClient#'
        and NumE='#lect_etablissement.NumEtablissement#'
    </cfquery>
    <!---</cfif>--->
    <cfif #facture.recordcount#>
        <cfset tot = 0>
        <cfloop query="facture">
            <cfquery name="role" datasource="f8w_test">
                Select id from role use index(siret_NumRole) 
                Where siret='#facture.siret#' and NumRole='#facture.NumRole#' and typerole<>2 and del=0
            </cfquery>    
            <cfif #role.recordcount#>
                <cfset tot = #tot# + #facture.TotalTTC#>
            </cfif>
        </cfloop>
        <cfset solde = val(#tot#) - val(#enc.tot#)>
        <cfset msgsolde = "Solde de votre compte : " & #numberformat(solde,"9999.99")# & " € ">
        <cfif #solde# lt 0>
            <cfset msgsolde = #msgsolde# & "à vous devoir.">
            <cfset msgsolde = replace(#msgsolde#,"-","")>
        <cfelseif #solde# gt 0>
            <cfset msgsolde = #msgsolde# & "à nous devoir.">
        </cfif>
    <cfelse>
        <cfset msgsolde = "">
    </cfif>
<cfelse>
    <cfset msgsolde = "">
</cfif>

<div class="row d-flex justify-content-between mt-5 content">
    <div class="p-2 p-lg-5 bg-white section scroll">

        <cfif isdefined("url.CFGRIDKEY") is false>
            <cfform name="pourfacture" class="blue-select"> 
                <cfif #lect_etablissement_miseadispo.recordcount#>     
                    <cfquery name="role" datasource="f8w_test">
                        Select * from role use index(siret_NumRole) 
                        Where siret = '#lect_etablissement.siret#' and afficher_facture=1 and del=0 
                        or siret='#lect_etablissement_miseadispo.siret_miseadispo#'
                        <cfif #session.neocim# is false> and afficher_facture=1</cfif> and del=0
                        <!---order by NumRole Desc--->
                        order by DateCreaMaj Desc
                    </cfquery>
                <cfelse>
                    <cfquery name="role" datasource="f8w_test">
                        Select * from role use index(siret_NumRole) 
                        Where siret = '#lect_etablissement.siret#' and TitreRole<>'ROLE DE CONTENTIEUX'
                        <cfif #session.neocim# is false>and afficher_facture=1</cfif> and del=0 order by NumRole Desc
                    </cfquery>                    
                </cfif>

                <table class="table" name="listeFacture">
                    <tr>
                        <th class="pb-4">Date</th>
                        <th class="pb-4 hidden">Montant</th>
                        <th class="pb-4 hidden">Période</th>
                        <th class="pb-4">Prestation</th>
                        <th class="pb-4">Régler par CB</th>
                        <th class="pb-4">Voir</th>
                    </tr>

                    <cfloop query="role">
                        <cfset datepaierole = lsdateformat(#role.DatePaiement#,"YYYYMMdd")>                                    	
                        <cfif #role.siret# eq #lect_user.siret#>
                            <cfquery name="facture" datasource="f8w_test">
                                Select * from facture use index(siret_NumClient) 
                                Where siret = '#role.siret#' and NumClient = '#lect_user.NumClient#' 
                                and NumRole=#role.NumRole# and del=0
                            </cfquery>
                        <cfelse>
                            <cfquery name="facture" datasource="f8w_test">
                                Select * from facture use index(siret_NumClient) 
                                Where siret = '#role.siret#' and IdClient = '#lect_client.id#' 
                                and NumRole=#role.NumRole# and del=0
                            </cfquery>
                        </cfif>
                        <cfquery name="lect_etablissement_facture" datasource="f8w_test">
                            Select * from application_client use index(siret)
                            where siret like '%#facture.siret#%' and NumEtablissement='#facture.numE#'
                        </cfquery>
                        <!---<cfif #lect_etablissement_miseadispo.recordcount#>
                            <cfquery name="facture" datasource="f8w_test">
                                Select * from facture use index(siret_NumClient) 
                                Where siret = '#role.siret#' and NumClient = '#lect_user.NumClient#' 
                                and NumRole=#role.NumRole# and del=0 
                                or siret = '#role.siret#' and IdClient = '#lect_client.id#' 
                                and NumRole=#role.NumRole# and del=0
                            </cfquery>
                        <cfelse>
                            <cfquery name="facture" datasource="f8w_test">
                                Select * from facture use index(siret_NumClient) 
                                Where siret = '#role.siret#' and NumClient = '#lect_user.NumClient#' 
                                and NumRole=#role.NumRole# and del=0
                            </cfquery>
                        </cfif>--->
                        <cfif #facture.recordcount#>
                            <cfif #facture.siret# eq #lect_user.siret#>
                                <cfquery name="regle" datasource="f8w_test">
                                    Select * from encaissement use index(siret_NumClient_NumRole2) 
                                    Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                                    and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' 
                                    or NumRole22='#role.NumRole#' or NumRole23='#role.NumRole#')
                                </cfquery>
                                <cfquery name="sumregle" datasource="f8w_test">
                                    Select sum(MontantPayé) as tot from encaissement use index(siret_NumClient_NumRole2) 
                                    Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                                    and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' 
                                    or NumRole22='#role.NumRole#' or NumRole23='#role.NumRole#')
                                </cfquery>
                            <cfelse>
                                <cfquery name="regle" datasource="f8w_test">
                                    Select * from encaissement use index(siret_NumClient_NumRole2) 
                                    Where siret = '#role.siret#' and IdClient = '#lect_client.id#' 
                                    and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' 
                                    or NumRole22='#role.NumRole#' or NumRole23='#role.NumRole#')
                                </cfquery>
                                <cfquery name="sumregle" datasource="f8w_test">
                                    Select sum(MontantPayé) as tot from encaissement use index(siret_NumClient_NumRole2) 
                                    Where siret = '#role.siret#' and IdClient = '#lect_client.id#' 
                                    and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' 
                                    or NumRole22='#role.NumRole#' or NumRole23='#role.NumRole#')
                                </cfquery>
                            </cfif>
                            
                            <!---<cfif #lect_etablissement_miseadispo.recordcount#> 
                                <cfquery name="regle" datasource="f8w_test">
                                    Select * from encaissement use index(siret_NumClient_NumRole2) 
                                    Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                                    and NumRole2='#role.NumRole#' 
                                    or siret = '#role.siret#' and IdClient = '#lect_client.id#' 
                                    and NumRole2='#role.NumRole#'
                                </cfquery>
                            <cfelse>
                                <cfquery name="regle" datasource="f8w_test">
                                    Select * from encaissement use index(siret_NumClient_NumRole2) 
                                    Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                                    and NumRole2='#role.NumRole#'
                                </cfquery>
                            </cfif>--->
                            <cfset txtregle = "">
                            <!--- pour prepaye --->
                            <cfquery name="numprestation" datasource="f8w_test">
                                Select NumPrestation from itemfacture use index(siret_NumFacture)
                                Where siret='#role.siret#' and NumFacture='#facture.numfacture#' 
                                and numprestation>0 group by numPrestation
                            </cfquery>
                            <cfset QueDuTicket = True>
                            <cfloop query="numprestation">
                                <cfquery name="prestation" datasource="f8w_test">
                                    Select id from prestation use index(siret_numprestation) 
                                    Where siret='#role.siret#' 
                                    and numprestation='#numprestation.numprestation#' 
                                    and Ticket=0
                                </cfquery>
                                <cfif #prestation.recordcount#>
                                    <cfset QueDuTicket = False>
                                    <cfbreak>
                                </cfif>
                            </cfloop>
                            <cfif #QueDuTicket# is true>
                                <cfset txtregle = "Déjà réglée">
                            </cfif>
                            <cfif #facture.ModePaiement# eq 0>
                                <cfif #regle.recordcount#>
                                    <cfif #regle.MoyenPaiement# eq 4>
                                        <cfset txtregle = "CONTENTIEUX le " & lsdateformat(#regle.DatePaiement#,"dd/MM/YYYY")>
                                    <cfelse>
                                        <cfif #sumregle.tot# gte #facture.totalttc#>
                                            <cfset txtregle = "Payée le " & lsdateformat(#regle.DatePaiement#,"dd/MM/YYYY")>
                                        <cfelse>
                                            <cfset txtregle = "Payée partiellement">
                                        </cfif>
                                    </cfif>
                                <cfelseif #lect_etablissement_facture.bloquecbtipi# eq 1 and #dtstampnow# gt #datepaierole#>
                                    <cfset txtregle = "CONTENTIEUX">
                                <cfelseif #txtregle# neq "Déjà réglée">
                                    <cfquery name="cb" datasource="f8w_test" maxrows="1">
                                        Select * from cb use index(id_facture) 
                                        Where id_facture='#facture.id#' order by id desc limit 0,1
                                    </cfquery>
                                    <cfif #cb.recordcount#>
                                        <cfif #cb.resultrans# eq "V">
                                            <cfset delaimin = datediff("n",#cb.creamaj#,now())>
                                            <cfset delaiaattendre = 30 - #delaimin#>
                                            <cfif #delaiaattendre# gt 0>
                                                <cfset txtregle = "En cours de validation..">
                                            <cfelse>
                                                <cfquery name="maj" datasource="f8w_test">
                                                    Update cb set resultrans='V Périmé' where id='#cb.id#'
                                                </cfquery>
                                                <cfset txtregle = "Cliquez pour payer par CB">
                                            </cfif>
                                        <cfelseif #cb.resultrans# eq "P">
                                            <cfset txtregle = "Payée le " & mid(#cb.dattrans#,1,2) & "/" & mid(#cb.dattrans#,3,2) & "/" & mid(#cb.dattrans#,5,2)>
                                        <cfelse>
                                            <cfquery name="verifsifactureregie" datasource="f8w_test">
                                                Select * from application_client where siret Like '#facture.siret#%' 
                                                and NumEtablissement='#facture.NumE#' and (CBTipi=1 or CBBanque>0)
                                            </cfquery>
                                            <cfif #verifsifactureregie.recordcount# and #lect_client.ModePaiement# neq 1>
                                                <cfset txtregle = "Cliquez pour payer par CB">
                                            </cfif>
                                        </cfif>
                                    <cfelse>
                                        <!--- pour 1 etab direct et un regie tipi ou cb --->
                                        <cfquery name="verifsifactureregie" datasource="f8w_test">
                                            Select * from application_client where siret Like '#facture.siret#%' 
                                            and NumEtablissement='#facture.NumE#' and (CBTipi=1 or CBBanque>0)
                                        </cfquery>
                                        <cfif (#tipisite.CBTipi# eq 1 and #facture.siret# eq #tipisite.siret#) or (#lect_etablissement.CBTipi# eq 1 and #facture.siret# eq #lect_etablissement.siret#) or (#lect_etablissement.CBBanque# gt 0 and #facture.ModePaiement# neq 1) or (#verifsifactureregie.recordcount# and #facture.ModePaiement# neq 1)>
                                            <cfset txtregle = "Cliquez pour payer par CB">
                                        </cfif>
                                    </cfif>
                                </cfif>
                                <cfif #QueDuTicket# is true>
                                    <cfset txtregle = "N'est pas à régler">
                                    <cfset role.STitreFacture = #role.STitreFacture# & " (relevé de consomation)">
                                </cfif>
                                <cfoutput>
                                    <tr>
                                        <td class="pt-3 pb-3">#role.datefacture#</td>
                                        <td class="pt-3 pb-3 hidden">#facture.TotalTTC# €</td>
                                        <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                        <td class="pt-3 pb-3">#role.STitreFacture#</td>
                                        <td class="pt-3 pb-3"><a class="link-red" href="/facture/cbtipi/?<cfoutput>CFGRIDKEY=#facture.id#&noclassactive=1&sess=#session.use_id#</cfoutput>">#txtregle#</a></td>
                                        <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#&noclassactive=1&sess=#session.use_id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                    </tr>
                                </cfoutput>
                            <cfelse>
                                <cfoutput>
                                    <tr>
                                        <td class="pt-3 pb-3">#role.datefacture#</td>
                                        <td class="pt-3 pb-3 hidden">#facture.TotalTTC# €</td>
                                        <td class="pt-3 pb-3 hidden">Du #role.DateDebPériode# au #role.DateFinPériode#</td>
                                        <td class="pt-3 pb-3">#role.STitreFacture#</td>
                                        <!---<td class="pt-3 pb-3 hidden"></td>!--->
                                        <td class="pt-3 pb-3"><a href="/commun/VisuPdf.cfm/?<cfoutput>CFGRIDKEY=#facture.id#&noclassactive=1&sess=#session.use_id#</cfoutput>"><img src="/img/icon/icon_view_blue.png" alt="icon d'un oeil"></a></td>
                                    </tr>
                                </cfoutput>
                            </cfif>
                        </cfif>
                    </cfloop>
                </table>
                
                <cfif #lect_etablissement_miseadispo.recordcount#> 
                    <cfquery name="anfacture" datasource="f8w_test">
                        Select mid(NoRole,1,4) as an from facture use index(siret_numclient) 
                        where siret='#lect_etablissement.siret#' and numclient='#lect_client.numclient#' 
                        or siret='#lect_etablissement_miseadispo.siret_miseadispo#' and IdClient='#lect_client.id#'
                        group by mid(NoRole,1,4)
                        order by mid(NoRole,1,4) desc
                    </cfquery>                             
                <cfelse>
                    <cfquery name="anfacture" datasource="f8w_test">
                        Select mid(NoRole,1,4) as an from facture use index(siret_numclient) 
                        where siret='#lect_etablissement.siret#' and numclient='#lect_client.numclient#' 
                        group by mid(NoRole,1,4)
                        order by mid(NoRole,1,4) desc
                    </cfquery> 
                </cfif>

                <label class="pt-4 text-red bold" for="ddsfdsfds">Pour votre déclaration d'impots :</label>
                <cfselect name="ddsfdsfds" id="ddsfdsfds" onChange="MM_jumpMenu('self',this,0)">
                    <option class="dropdown-item" value="">Attestation de facture pour l'année :</option>
                    <cfoutput query="anfacture">
                        <cfif isdefined("url.an")>
                            <cfif #url.an# eq #anfacture.an#>
                                <option class="dropdown-item" selected value="?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#anfacture.an#">Attestation de facture pour l'année : #anfacture.an#</option>
                            <cfelse>
                                <option class="dropdown-item" value="?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#anfacture.an#">Attestation de facture pour l'année : #anfacture.an#</option>
                            </cfif>
                        <cfelse>
                            <option class="dropdown-item" value="?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#anfacture.an#">Attestation de facture pour l'année : #anfacture.an#</option>
                        </cfif>
                    </cfoutput>
                </cfselect>
                <cfif isdefined("url.espaceparentfacture")>
                    <cfoutput>
                        <a class="link-grey" href="https://gestion.cantine-de-france.fr/app/content/edition/rolefacture/attestationfacture.cfm?espaceparentfacture=#lect_client.siret#&numclient=#lect_client.numclient#&an=#url.an#" target="_blank">Imprimer #lect_etablissement.nome#</a>
                        <cfif #lect_etablissement_miseadispo.recordcount#>
                            <cfquery name="nomdispo" datasource="f8w_test">
                                    Select nome from application_client use index(siret) 
                                    where siret='#lect_etablissement_miseadispo.siret_miseadispo#'
                            </cfquery>
                            <a class="link-grey" href="https://gestion.cantine-de-france.fr/app/content/edition/rolefacture/attestationfacture.cfm?espaceparentfacture=#lect_etablissement_miseadispo.siret_miseadispo#&numclient=#lect_client.numclient#&an=#url.an#&idcli=#lect_client.id#" target="_blank">Imprimer #nomdispo.nome#</a>
                        </cfif>
                    </cfoutput>
                </cfif>
            </cfform>
        <cfelse>
            <cfquery name="fact" datasource="f8w_test">
                Select * from facture Where id='#url.CFGRIDKEY#'
            </cfquery>
            <cfquery name="lect_etablissement_facture" datasource="f8w_test">
                Select * from application_client use index(siret)
                where siret like '#fact.siret#%' and NumEtablissement='#fact.numE#'
            </cfquery>
            <cfquery name="role" datasource="f8w_test">
                Select * from role use index(siret_numrole) 
                where siret='#fact.siret#' and numrole='#fact.numrole#'
            </cfquery>
            <cfset dtstampnow = lsdateformat(now(),"yyyymmdd")>
            <!---<cfset datepaierole = lsdateformat(#role.DatePaiement#,"YYYYMMdd")>--->
            <cfset datepaierole = mid(#role.DatePaiement#,7,4) & mid(#role.DatePaiement#,4,2) & mid(#role.DatePaiement#,1,2)>
            <cfif (#lect_etablissement_facture.bloquecbtipi# eq 1 and #dtstampnow# gt #datepaierole#) or #fact.ModePaiement# eq 1>
                <!--- paiement cb bloquée si facture échu ou facture prélevée --->
                <cflocation url="/facture/cbtipi/?id_mnu=12" addtoken="no">
            </cfif>
            <cfquery name="itemfacture" datasource="f8w_test">
                Select * from itemfacture use index(siret_numfacture) 
                where siret='#fact.siret#' and NumFacture='#fact.numfacture#'
            </cfquery>
                <!--- modif 19/03/2019 multi regie avec tipi (SIVSC) --->
            <cfquery name="tipisite" datasource="f8w_test">
                Select * from application_client 
                where siret Like '%#fact.siret#%' and numEtablissement='#Fact.NumE#'
            </cfquery>
            <cfset ontpaye = "oui">
            <cfif (findnocase(#fact.siret#,#tipisite.siret#) and #tipisite.CBTipi# eq 1) or (#fact.siret# eq #lect_etablissement.siret# and #lect_etablissement_facture.CBTipi# eq 1) or (#lect_etablissement_facture.CBBanque# gt 0 and #fact.ModePaiement# neq 1) >
            
            <cfelse>
                <cfset ontpaye = "non">
            </cfif>
            <cfquery name="cb" datasource="f8w_test" maxrows="1">
                Select * from cb use index(id_facture) Where id_facture='#url.CFGRIDKEY#' order by id desc limit 0,1
            </cfquery>
            <cfif #cb.resultrans# eq "P" or #cb.resultrans# eq "V">
                <!--- ont ne peut payer avec une transaction déjà effectuer que si le résultat de cette transaction
                est paiement refusé  ou vide (pas de retour)--->
                <cfset ontpaye = "non">
            <cfelseif #ontpaye# eq "oui">
                <!--- modif 08/02/2017 si contentieux ou autre ont paye plus par cb --->
                <cfif #fact.siret# eq #lect_etablissement.siret#>
                    <cfquery name="regle" datasource="f8w_test">
                        Select * from encaissement use index(siret_NumClient_NumRole2) 
                        Where siret = '#fact.siret#' and NumClient = '#lect_user.NumClient#' 
                        and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' or NumRole22='#role.NumRole#' 
                        or NumRole23='#role.NumRole#')
                    </cfquery>
                <cfelse>
                    <cfquery name="regle" datasource="f8w_test">
                        Select * from encaissement use index(siret_IdClient) 
                        Where siret = '#fact.siret#' and IdClient = '#fact.IdClient#' 
                        and (NumRole2='#role.NumRole#' or NumRole21='#role.NumRole#' or NumRole22='#role.NumRole#' 
                        or NumRole23='#role.NumRole#')
                    </cfquery>
                </cfif>
                <cfif #regle.recordcount# or (#lect_etablissement_facture.bloquecbtipi# eq 1 and #dtstampnow# gt #datepaierole#)>
                    <cfset ontpaye = "non">
                <cfelse>
                    <cfset MontantDuDeLaFacture = 0><!--- pour collias et autre prepaye (facture aquitée) --->
                    <cfset MontantDejaAcquiteDeLaFacture=0>
                    <cfloop query="itemfacture">
                        <cfquery name="ticket" datasource="f8w_test">
                            Select ticket from prestation use index(siret_numprestation) 
                            where siret='#fact.siret#' 
                            and numprestation='#itemfacture.numprestation#' and ticket=1 and del=0
                        </cfquery>
                        <cfif #ticket.recordcount# eq 1>
                            <cfset MontantDejaAcquiteDeLaFacture = #MontantDejaAcquiteDeLaFacture# + #itemfacture.ttc#>
                        <cfelse>
                            <cfset MontantDuDeLaFacture = #MontantDuDeLaFacture# + #itemfacture.ttc#>
                        </cfif>
                    </cfloop>
                    
                    <cfif #MontantDejaAcquiteDeLaFacture# eq 0>
                        <cfset montant = replacenocase(#fact.TotalTTC#,".","")>
                    <cfelse>
                        <cfset montant = replacenocase(#MontantDuDeLaFacture#,".","")>
                        <cfset montant = #montant# * 100>
                    </cfif>
                    
                    <cfset leobjet = #role.STitrefacture# & " " & #role.Z0#>
                    <cfset leobjet = #role.STitrefacture# & #role.Z0#>
                    <cfset leobjet1 = mid(#leobjet#,1,99)>
                    <cfset exer = mid(#role.NoRole#,1,4)>
                    <cfquery name="tipisite" datasource="f8w_test">
                        Select * from application_client 
                        where siret Like '#fact.siret#%' and numEtablissement='#Fact.NumE#'
                    </cfquery>
                    <cfset lemailcorrige = replace(#lect_user.login#," ","","all")>
                    <cfquery name="add" datasource="f8w_test">
                        Insert into cb (id_facture,numcli,exer,objet,montant,mel) Values ('#url.CFGRIDKEY#',
                        '#tipisite.SiteTIPI#','#exer#',
                        <cfqueryparam value="#leobjet1#" cfsqltype="cf_sql_varchar">,'#montant#','#lemailcorrige#')
                    </cfquery>
                    <cfquery name="cb" datasource="f8w_test" maxrows="1">
                        Select * from cb use index(id_facture) 
                        Where id_facture='#url.CFGRIDKEY#' order by id desc limit 0,1
                    </cfquery>
                </cfif>
            </cfif>
            <cfif #ontpaye# eq "oui" and #lect_etablissement_facture.CBBanque# gt 0>
                <cfquery name="cbbanque" datasource="f8w_test">
                    Select * from cbbanque use index(siret) where siret='#lect_etablissement.siret#'
                </cfquery>
                <cfset totalttc = #cb.montant# / 100>
                <cfif #totalttc# gt 0>
                    <cfset leuuid = createuuid()>
                    <cfset leuuid1 = replacenocase(#leuuid#,"-","","all")>
                    <cfset session.uuidpanier = right(#leuuid1#,30)>
                    <cfquery name="maj" datasource="f8w_test">
                        Update cb set siret='#lect_etablissement.siret#',uuid='#session.uuidpanier#',
                        numcli=<cfif len(#cbbanque.Identifiant#)>'#cbbanque.Identifiant#'<cfelse>'#cbbanque.merchant_id#'</cfif> 
                        where id='#cb.id#'
                    </cfquery>
                    <cfset cbbanquepourfacture = 1>
                    <cfif findnocase("/",#cbbanque.script#) eq 0>
                        <cfinclude template="/achats/#cbbanque.script#">
                    <cfelse>
                        <cfinclude template="#cbbanque.script#">
                    </cfif>
                </cfif>
            <cfelseif #ontpaye# eq "oui">
                <cfset montant= #cb.montant#>
                <cfset objet = replacenocase(#cb.objet#,"é","e","all")>
                <cfset objet = replacenocase(#objet#,"è","e","all")>
                <cfset objet = replacenocase(#objet#,"ê","e","all")>
                <cfset objet = replacenocase(#objet#,"-","","all")>
                <cfset objet = replacenocase(#objet#,"ô","o","all")>
                <cfset objet = replacenocase(#objet#,"ê","e","all")>
                <cfset objet = replacenocase(#objet#,"û","u","all")>
                <cfset objet = replacenocase(#objet#,"'","","all")>
                <cfset objet = replacenocase(#objet#,"ô","o","all")>
                <cfset objet = replacenocase(#objet#,"/"," ","all")>
                <cfset objet = replacenocase(#objet#,"\"," ","all")>
                <cfset objet = urlencodedformat(#objet#,"utf-8")>
                
                <cfset exer = #cb.exer#>
                <!---<cfset lemailcorrige = replace(#lect_client.email#," ","","all")>--->
                <cfset lemailcorrige = replace(#lect_user.login#," ","","all")>
                <div class="text-center">
                    <cfoutput>
                        <cfset urltipi = "https://www.payfip.gouv.fr/tpa/paiement.web?numcli=#cb.numcli#&exer=#exer#&refdet=#url.CFGRIDKEY#&objet=#objet#&montant=#montant#&mel=#lemailcorrige#&urlcl=http://gestion.cantine-de-france.fr/cb/&saisie=#tipisite.SaisieTIPI#">
                        <p>Facture #role.STitreFacture# du #role.datefacture#, période du  #role.DateDebPériode# au #role.DateFinPériode#, montant #fact.TotalTTC#€</p>
                        <form method="post" name="ssds" id="ssds" action="/facture/cbtipi/pstcb.cfm" target="frm">
                            <cfif #montant# gt 0>
                                <!---<font color="##F80409"><b>Suite à un problème national de fonctionnement du service de la DGFIP (payfip.gouv.fr), le paiement cb est TEMPORAIREMENT désactivé. Merci de réessayer plus tard.</b></font>--->
                                <input type="button" class="btn btn-primary bold" value="Payer ma facture par CB" name="dssq" id="dssq" onClick="payecb('ssds');">
                            <cfelse>
                                <input type="button" class="btn btn-primary bold" value="Payer ma facture par CB" name="dssq" id="dssq" disabled>
                                <p>Facture déjà acquittée</p>
                            </cfif>
                            <input type="hidden" name="idcb" value="#cb.id#">
                            <input type="hidden" name="url" id="url" value="#urltipi#">
                        </form>
                        <iframe name="frm" id="frm" width="0" height="0"></iframe>
                        
                        <!---<script type="text/javascript">
                            window.open('#urltipi#','_blank','height=700, width=900, toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, directories=no, status=no');
                        </script>--->
                    </cfoutput>     
                </div>                      
            </cfif>

            <div class="text-center">
                <cfif #ontpaye# eq "non" and #cb.resultrans# neq "V">
                    <cfquery name="facture" datasource="f8w_test">
                        Select * from facture 
                        Where id='#url.CFGRIDKEY#'
                    </cfquery>
                    <cfif #facture.siret# eq #lect_etablissement.siret#>
                        <cfquery name="regle" datasource="f8w_test">
                            Select * from encaissement use index(siret_NumClient_NumRole2) 
                            Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#' 
                            and NumRole2='#facture.NumRole#'
                        </cfquery>
                    <cfelse>
                        <cfquery name="idcli" datasource="f8w_test">
                            Select id from client use index(siret_numclient) 
                            Where siret = '#lect_etablissement.siret#' and NumClient = '#lect_user.NumClient#'
                        </cfquery>
                        <cfquery name="regle" datasource="f8w_test">
                            Select * from encaissement use index(siret_IdClient) 
                            Where siret = '#lect_etablissement.siret#' and IdClient = '#idcli.id#' 
                            and NumRole2='#facture.NumRole#'
                        </cfquery>                                    
                    </cfif>
                    <cfquery name="paie" datasource="f8w_test">
                        Select * from moyenpaiement use index(siret_NoMoyen) Where siret = '#lect_etablissement.siret#' 
                        and NoMoyen='#regle.MoyenPaiement#'
                    </cfquery>

                    <cfoutput>
                        <cfif #regle.MoyenPaiement# eq 4>
                            <p>CONTENTIEUX le #lsdateformat(regle.DatePaiement,"dd/MM/YYYY")#</p>
                        <cfelse>
                            <p>Payée le #lsdateformat(regle.DatePaiement,"dd/MM/YYYY")# par #paie.Désignation#.</p>
                        </cfif>        
                    </cfoutput>
                <cfelseif #ontpaye# eq "non">
                    <!---<cflocation url="/facture/cbtipi/?id_mnu=12" addtoken="no">--->
                    <cfset delaimin = datediff("n",#cb.creamaj#,now())>
                    <cfset delaiaattendre = 30 - #delaimin#>
                    <p>Une tentative de paiement a eu lieu il y a <cfoutput>#delaimin# minutes</cfoutput>, merci de patienter <cfoutput>#delaiaattendre#</cfoutput> minutes avant de réitérer l'opération.</p>                         
                </cfif>
                <a class="link-grey" href="/facture/?id_mnu=144">Retour à la liste des factures</a>
            </div>
        </cfif>  
    </div>
</div>