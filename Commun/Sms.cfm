﻿<cfparam name="Attributes.siret">
<cfparam name="Attributes.to">
<cfparam name="Attributes.msg">
<cfparam name="Attributes.type" default="Texte">
<cfparam name="Attributes.id_client" default="0">
<cfparam name="Attributes.compte">
<cfparam name="Attributes.debug" default="0">
<cfparam name="Attributes.nume" default="1">
<cfparam name="Attributes.EmailSiKo" default="0">    
    
<cfset Retour = "">
<cfset gsm = replacenocase(#Attributes.to#," ","","all")>
<cfset gsm = replacenocase(#gsm#,".","","all")>
<cfset gsm = replacenocase(#gsm#,"0033","0","all")>

<cfquery name="credit" datasource="mission" maxrows="1">
	Select * from sms_compte_credit use index(siret) where siret='#Attributes.siret#' and Numetablissement='#Attributes.nume#' 
    and id_compte='#Attributes.compte#' 
    and credit>0 order by credit asc limit 0,1
</cfquery>
<cfif #credit.recordcount# eq 0><!--- plus de credit --->
	<cfquery name="add" datasource="mission">
    	insert into sms_compte_credit_conso(siret,application,leto,msg,resultat,id_client,nume) 
        values ('#Attributes.siret#','#cgi.HTTP_HOST#',
        '#Attributes.to#','#Attributes.msg#','KO plus de crédit','#Attributes.id_client#','#Attributes.nume#')
    </cfquery>
	<cfset Retour = 'KO plus de crédit'>
<cfelse>
	<cfif len(#gsm#) neq 10 or (mid(#gsm#,1,2) neq "06" and mid(#gsm#,1,2) neq "07")>
        <!--- erreur numero ---> 
        <cfquery name="add" datasource="mission">
    		insert into sms_compte_credit_conso(siret,application,leto,msg,resultat,id_client,nume) 
            values ('#Attributes.siret#','#cgi.HTTP_HOST#',
        	'#Attributes.to#','#Attributes.msg#','KO erreur numero','#Attributes.id_client#','#Attributes.nume#')
    	</cfquery>
        <cfset Retour = 'KO erreur numero'>
    <cfelse>
    	<cfset gsm = "0033" & mid(#gsm#,2,9)>
    	<cfquery name="compte" datasource="mission">
        	Select * from sms_compte Where id='#credit.id_compte#'
        </cfquery>
    	<cfif #compte.id# eq 1><!--- acces http (ovh pour l'instant) --->
        	<cf_validetextesms texte="#Attributes.msg#"> 
            <cfset laurl = replacenocase(#compte.HttpUrl#,"@to@",#gsm#)>
            <cfset laurl = replacenocase(#laurl#,"@msg@",#cf_validetextesms.Retour#)>
            
            <cfquery name="add" datasource="mission" result="Res">
    			insert into sms_compte_credit_conso(siret,application,id_compte_credit,leto,msg,leurl,id_client,nume) 
                values ('#Attributes.siret#','#cgi.HTTP_HOST#','#credit.id#','#gsm#','#cf_validetextesms.Retour#',
                '#laurl#','#Attributes.id_client#','#Attributes.nume#')
    		</cfquery>
            
            <cfset CheminExe = "c:\SmsOvh\" & #Attributes.siret# & "\SmsOvhWeb.exe">
            <cfset CheminFichier = "c:\SmsOvh\" & #Attributes.siret# & "\Afaire\">    
            <cfset NomFichier = lstimeformat(now(),"HHmmssl") & ".sms">
            <cffile action="WRITE" file="#CheminFichier##NomFichier#" output="#laurl#"> 
                
            <cfexecute name="#CheminExe#" timeout="1000000">
                
            <cffile action="READ" file="#CheminFichier##NomFichier#" variable="FichierRetour">
            <cfset FichierRetour = replace(#FichierRetour#,'"',"","all")>
              
                
            <cfset CodeRetour = -1>
            <cfset Creditrestant = "">
            <cfset Idmessage = "">
            <cfset Recu = -1>
            <cfloop list="#FichierRetour#" delimiters="#chr(10)##chr(13)#" index="idx">
            	<cfset Retour = #Retour# & #idx# & " ">
				<cfif #CodeRetour# eq -1>
            		<cfif #idx# eq "KO">
                    	<cfset CodeRetour = 0>
                    	<cfbreak>
                    <cfelseif #idx# eq "OK">
                    	<cfset CodeRetour = 1>
                    </cfif>
               	<cfelseif #Creditrestant# eq "">
               		<cfset Creditrestant = #idx#>
               	<cfelse>     
                    <cfset Idmessage = #idx#>
            	</cfif>
            </cfloop>
            <cfif #CodeRetour# neq -1>
                <cffile action="DELETE" file="#CheminFichier##NomFichier#">    
            </cfif>
            <!---<cffile action="write" file="c:\tempweb\cfhttp.txt" output="#cfhttp.FileContent#" nameconflict="overwrite">--->
            <cfif len(#Attributes.msg#) lte 149>
            	<cfset cout = 1 * #credit.PuHt#>
                <cfset lecreditrestant = #credit.credit# - 1>    
            <cfelse>
            	<cfset cout = 2 * #credit.PuHt#>
                <cfset lecreditrestant = #credit.credit# - 2>    
            </cfif>
         	<cfset result = "id " & #Idmessage# & " credit " & #Creditrestant#>
            <cfif #CodeRetour# eq 1>
            	<!--- appel api pour etat délivré --->
            
            </cfif>
            <cfquery name="maj" datasource="mission">
            	Update sms_compte_credit_conso set id_msg='#Idmessage#', Emis='#CodeRetour#', resultat='#result#',
                PuHt='#credit.PuHt#', conso='#cout#', recu='#Recu#', cfhttp=<cfqueryparam value="#FichierRetour#" cfsqltype="CF_SQL_LONGVARCHAR"> 
                where id='#Res.GENERATED_KEY#'
            </cfquery>
              
            <cfif #CodeRetour# eq 1>
                <!---<cfquery name="maj" datasource="mission">
                    Update sms_compte_credit set credit='#Creditrestant#' where id='#credit.id#'
                </cfquery>--->
                <cfquery name="maj" datasource="mission">
                    Update sms_compte_credit set credit='#lecreditrestant#' where id='#credit.id#'
                </cfquery>
                
                    
			<cfelse>
            	<cfset CodeRetour = "KO">
                <cfset Retour = "KO">    
            </cfif>
        <cfelseif #compte.id# eq 2><!--- octopush --->
        	<cfset dateaant = dateadd("h",-1,now())>
			<cfif #Attributes.type# eq "Texte">
            	<cfset laurl = #compte.HttpUrl#>
                <cfset json1 = replace(#compte.JsonSmsTexte#,"@to@",#replace(gsm,"0033","+33")#)>
                <cfset json2 = replace(#json1#,"@msg@",#Attributes.msg#)>
                <cfset ladate = lsdateformat(#dateaant#,"YYYY-mm-dd") & "T" & lstimeformat(#dateaant#,"HH:MM:NN") & "+01:00">
                <cfset json3 = replace(#json2#,"@date@",#ladate#)>
                <cfset jsonfin = replace(#json3#,"@sender@",#credit.sender#)>
            <cfelse>
            	<cfset laurl = #compte.HttpUrlVoice#>
                <cfset json1 = replace(#compte.JsonSmsTexte#,"@to@",#replace(gsm,"0033","+33")#)>
                <cfset json2 = replace(#json1#,"@msg@",#Attributes.msg#)>
                <cfset ladate = lsdateformat(#dateaant#,"YYYY-mm-dd") & "T" & lstimeformat(#dateaant#,"HH:MM:NN") & "+01:00">
                <cfset json3 = replace(#json2#,"@date@",#ladate#)>
                <cfset json4 = replace(#json3#,"@genre@",#credit.genrevoice#)>
				<cfset jsonfin = replace(#json4#,"@sender@",#credit.sender#)>
            </cfif>
        	<cfquery name="add" datasource="mission" result="Res">
    			insert into sms_compte_credit_conso(siret,application,id_compte_credit,leto,msg,leurl,id_client,nume) 
                values ('#Attributes.siret#','#cgi.HTTP_HOST#','#credit.id#','#gsm#','#Attributes.msg#',
                '#laurl#','#Attributes.id_client#','#Attributes.nume#')
    		</cfquery>
            <cfhttp url="#laurl#" method="post" charset="utf-8">
            	<cfhttpparam type="header" name="Content-Type" value="application/json">
                <cfhttpparam type="header" name="api-key" value="#compte.ApiKey#">
                <cfhttpparam type="header" name="api-login" value="#compte.ApiLogin#">
                <cfhttpparam type="body" value="#jsonfin#">
            </cfhttp>
            <cfif #Attributes.debug# eq 1>        
                <cffile action="write" file="c:\tempweb\cfhttpbody.txt" output="#cfhttp.FileContent#" nameconflict="overwrite">
                <cffile action="write" file="c:\tempweb\cfhttpheader.txt" output="#cfhttp.header#" nameconflict="overwrite">
                <cffile action="write" file="c:\tempweb\cfhttpjson.txt" output="#jsonfin#" nameconflict="overwrite">
			    <cfdump var="#cfhttp#" output="c:\tempweb\cfhttpdump.txt">
			</cfif>
            <cfset RetourJson = DeserializeJSON(#cfhttp.FileContent#)>
            <cfif isdefined("RetourJson.sms_ticket") or isdefined("RetourJson.vocal_ticket") or isdefined("RetourJson.ticket_number")><!--- OK --->
            	<cfset CodeRetour = 1>
                <!---<cfdump var="#RetourJson#" output="c:\tempweb\jsondecode.txt">--->
				<cfif isdefined("RetourJson.sms_ticket")>
                	<cfset Idmessage = #RetourJson.sms_ticket#>
					<cfset cout = #RetourJson.number_of_sms_needed#>
                    <cfset Creditrestant = #credit.credit# - #cout#>
				<cfelse>
                    
                	<cfset Idmessage = #RetourJson.ticket_number#>
					<cfset cout = 1>
                    <cfset Creditrestant = #credit.credit# - #RetourJson.total_cost#>                
                </cfif>                
				<cfset recu= -1>
            	<cfset Retour = "OK">
                <cfset result = "OK">
            <cfelse><!--- KO --->
            	<cfset CodeRetour = 0>
                <cfset cout = 0>
                <cfset recu= -1>
                <cfset Idmessage = "Error">
                <cfset result = "KO code: ">
                <cfif isdefined("RetourJson.code")>
                    <cfset result = #result# & #RetourJson.code# & ", message: ">
                <cfelse>
                    <cfset result = #result# & "Pas définit, message: ">
                </cfif>
                <cfif isdefined("RetourJson.message")>
                    <cfset result = #result# & #RetourJson.message#>
                <cfelse>
                    <cfset result = #result# & "Pas définit">
                </cfif>    
                <cfset Retour = #result#>
            </cfif>
            <cfquery name="maj" datasource="mission">
            	Update sms_compte_credit_conso set id_msg='#Idmessage#', Emis='#CodeRetour#', resultat='#result#',
                PuHt='#credit.PuHt#', conso='#cout#', recu='#Recu#' where id='#Res.GENERATED_KEY#'
            </cfquery>
            <cfif #CodeRetour# eq 1>
                <cfquery name="maj" datasource="mission">
                    Update sms_compte_credit set credit='#Creditrestant#' where id='#credit.id#'
                </cfquery>
			</cfif>
			
        </cfif>
    </cfif>
</cfif>
<cfif find("KO",#Retour#) and #Attributes.EmailSiKo# eq 1><!--- conversion en mail --->
    <cfset LeObjet = "Sms en Mail : " & mid(#Attributes.msg#,1,20) & "....">
    <cfquery name="add" datasource="f8w" result="resultat">
        Insert into message_ent (siret,Objet,Text,PourLeParent,NumE) 
        values ('#Attributes.siret#',<cfqueryparam value="#LeObjet#" cfsqltype="cf_sql_varchar">,
        <cfqueryparam value="#Attributes.msg#" cfsqltype="cf_sql_longvarchar">,'#Attributes.id_client#',#Attributes.nume#')
    </cfquery>
    <cfquery name="add" datasource="f8w">
        Insert into message (id_ent,siret,id_client,Objet,Text,NumE) 
        values('#resultat.GENERATED_KEY#','#Attributes.siret#','#Attributes.id_client#',
        <cfqueryparam value="#LeObjet#" cfsqltype="cf_sql_varchar">,
        <cfqueryparam value="#Attributes.msg#" cfsqltype="cf_sql_longvarchar">,#Attributes.nume#')
    </cfquery>
    <cfquery name="lect_etablissement_sms" datasource="f8w">
        Select * from application_client use index(siret) where siret='#Attributes.siret#' and NumEtablissement='#Attributes.nume#'
    </cfquery>
    <cfquery name="envoie_sms" datasource="f8w">
        Select * from client  where id='#Attributes.id_client#'
    </cfquery>
    <cfquery name="cptmailserv" datasource="mission">
        Select * from cpt_appel_msg where pour='MSG'
    </cfquery>        
    <cfset sujet = #lect_etablissement_sms.Nome# & ", cantine et/ou périscolaire">
    <cfset html_body_post_insert = "">
    <cfset html_body = "<html><p><font face='Arial, Helvetica, sans-serif'><font color='##FF0000'>CANTINE</font></p><p>A l'attention de ">
    <cfset html_body = #html_body# & #civil# & " " & #envoie_sms.Nom# & " " & #envoie_sms.Prénom# & "</p>">
    <cfset html_body = #html_body# & "<p>Nous vous informons qu’un nouveau message est disponible dans votre espace parent accessible par</p>">
    <cfset html_body = #html_body# & "<p><a href='" & #lect_etablissement_sms.pourLienEspaceParent# & "'>" & #lect_etablissement_sms.pourLienEspaceParent# & "</a> .</p>">
    <cfset html_body = #html_body# & "<p><b>Objet : " & #LeObjet# & ".</b>" & "</p><p>Cordialement</p>">
    <cfswitch expression="#lect_etablissement_sms.titrelégal#">
        <cfcase value="1">
            <cfset html_body = #html_body# & "<p>Le Maire</p>">
        </cfcase>
        <cfcase value="2">
            <cfset html_body = #html_body# & "<p>La Maire</p>">
        </cfcase>
        <cfcase value="3">
            <cfset html_body = #html_body# & "<p>Le Président</p>">
        </cfcase>
        <cfcase value="4">
            <cfset html_body = #html_body# & "<p>La Présidente</p>">
        </cfcase>
        <cfcase value="5">
            <cfset html_body = #html_body# & "<p>La Secrétaire</p>">
        </cfcase>
    </cfswitch>
    <cfif len(#lect_etablissement_sms.NomPrénomMail#)>
        <cfset html_body = #html_body# & "<p>" & #lect_etablissement_sms.NomPrénomMail# & "</p>">
    <cfelse>
        <cfset html_body = #html_body# & "<p>" & #lect_etablissement_sms.NomPrénomLégal# & "</p>">
    </cfif>
    <cfset html_body = #html_body# & "<p><b>Ne répondez pas sur cette adresse mais dans votre espace parent</b></p></font></html>">
    <cfset txt_body = "CANTINE" & #chr(13)#>
    <cfif #envoie_sms.TitreCivil# eq 1><cfset civil="Mme"><cfelse><cfset civil="M"></cfif>
    <cfset txt_body = #txt_body# & "A l'attention de " & #civil# & " " & #envoie_sms.Nom# & " " & #envoie_sms.Prénom# & #chr(13)#>
    <cfset txt_body = #txt_body# & "Nous vous informons qu’un nouveau message est disponible dans votre espace parent accessible par" & #chr(13)#>
    <cfset txt_body = #txt_body# & #lect_etablissement_sms.pourLienEspaceParent# & #chr(13)#>
    <cfset txt_body = #txt_body# & "Objet : " & #LeObjet# & #chr(13)#>
    <cfset txt_body = #txt_body# & "Cordialement" & #chr(13)#>
    <cfswitch expression="#lect_etablissement_sms.titrelégal#">
        <cfcase value="1">
            <cfset txt_body = #txt_body# & "Le Maire" & #chr(13)#>
        </cfcase>
        <cfcase value="2">
            <cfset txt_body = #txt_body# & "La Maire" & #chr(13)#>
        </cfcase>
        <cfcase value="3">
            <cfset txt_body = #txt_body# & "Le Président" & #chr(13)#>
        </cfcase>
        <cfcase value="4">
            <cfset txt_body = #txt_body# & "La Présidente" & #chr(13)#>
        </cfcase>
        <cfcase value="5">
            <cfset txt_body = #txt_body# & "La Secrétaire" & #chr(13)#>
        </cfcase>
    </cfswitch>
    <cfif len(#lect_etablissement_sms.NomPrénomMail#)>
        <cfset txt_body = #txt_body# & #lect_etablissement_sms.NomPrénomMail# & #chr(13)#>
    <cfelse>
        <cfset txt_body = #txt_body# & #lect_etablissement_sms.NomPrénomLégal# & #chr(13)#>
    </cfif>
    <cfset txt_body = #txt_body# & "Ne répondez pas sur cette adresse mais dans votre espace parent">
    <cfif isvalid("email",#envoie_sms.email#)>
        <cfquery name="addspool" datasource="services">
            Insert into spool_mail (de,pour,sujet,html_body,txt_body,joint,date_crea,
            fail_to,
            EmailAccuseRecep,EmailNotifRecep,jointinsert,html_body_post_insert,jointinsertpied,siret,leserveur,
            utilisateur,mailutilisateur,motdepasse) 
            values('message@cantine-de-france.fr','#envoie_sms.email#',
            <cfqueryparam value="#sujet#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,
            '',#now()#,'#lect_etablissement_sms.EmailE#',
            '#lect_etablissement_sms.EmailAccuseRecep#','#lect_etablissement_sms.EmailNotifRecep#',
            <cfqueryparam value="#lect_etablissement_sms.logoMail#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body_post_insert#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#lect_etablissement_sms.logoMailPied#" cfsqltype="cf_sql_varchar">,'#Attributes.siret#',
            '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
        </cfquery>
    </cfif>
    <cfif isvalid("email",#envoie_sms.email2#) and #envoie_sms.email2# neq #envoie_sms.email#>
        <cfquery name="addspool" datasource="services">
            Insert into spool_mail (de,pour,sujet,html_body,txt_body,joint,date_crea,
            fail_to,EmailAccuseRecep,EmailNotifRecep,jointinsert,html_body_post_insert,jointinsertpied,siret,leserveur,
            utilisateur,mailutilisateur,motdepasse) 
            values('message@cantine-de-france.fr','#envoie_sms.email2#',
            <cfqueryparam value="#sujet#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,
            '',#now()#,'#lect_etablissement_sms.EmailE#',
            '#lect_etablissement_sms.EmailAccuseRecep#','#lect_etablissement_sms.EmailNotifRecep#',
            <cfqueryparam value="#lect_etablissement_sms.logoMail#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body_post_insert#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#lect_etablissement_sms.logoMailPied#" cfsqltype="cf_sql_varchar">,'#lect_etablissement_sms.siret#',
            '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
        </cfquery>
    </cfif>
</cfif>
<cfset Caller.cf_Sms.Retour = #Retour#>