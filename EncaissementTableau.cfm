<cfif findnocase("gestion.",#cgi.SERVER_NAME#)><!--- espace gestion --->
	<cfset espace = "gestion">
<cfelse><!--- espace parent --->
    <cfset espace = "parent">
</cfif>

<tr>
    <th class="pb-4">Date</th>
    <th class="pb-4">N°</th>
    <th class="pb-4">Facturé</th>
    <th class="pb-4">Payé</th>
    <th class="pb-4">Information</th>
</tr>

<cfoutput>
    <cfset numclient = parent.numclient>
    <cfset numetablissement = url.numetablissement>
    <cfset siret = lect_user.siret>
    <cfset siretparent = parent.siret>
    <cfset idparent = url.idparent>
    <cfset numparent = parent.numclient>
    <cfset sess = session.use_id>
    <cfset id = url.id>
    <cfset n = url.n>

    <cfinclude template="/EncaissementTableauLst.cfm">
    <!---
    <iframe name="listeregle" id="listeregle" scrolling="auto" width="100%" height="<cfif #espace# eq "gestion">250<cfelse>400</cfif>" src="/commun/EncaissementTableauLst.cfm?numclient=#parent.numclient#&numetablissement=#url.numetablissement#&siret=#lect_user.siret#&siretparent=#parent.siret#&idparent=#url.idparent#&numparent=#parent.numclient#&sess=#session.use_id#&id=#url.id###n#url.n#"></iframe>
    --->
</cfoutput>
<cfform name="hgghjgj">
    <tr>
        <td colspan="2" align="center">
            <cfif #espace# neq "gestion">
                <cfif #parent.recordcount# eq 0>
                    <img src="/img/icon/icon_print.png"/>
                <cfelse>
                    <a title="Imprimer le détail du compte client avec solde" href="/app/content/edition/encaissement/detfactpaie/?id=<cfoutput>#parent.id#</cfoutput>" target="_blank"><img src="/img/icon/icon_print.png"/></a>
                </cfif>                                
            <cfelse>
                &nbsp;&nbsp;&nbsp;
            </cfif>
        </td>
        <td class="zoneinput" align="right"><cfinput title="Facturé" name="totfac" readonly="yes" value="#numberformat(NewSolde.TotFact,'999999.99')#" size="8"></td>
        <td class="zoneinput" align="center"><cfinput title="Payé" name="totenc" readonly="yes" value="#numberformat(NewSolde.TotEnc,'999999.99')#" size="8"></td>
        <td class="libelle" align="right">Solde : 
            <cfif #NewSolde.Solde# eq 0>
                <cfinput name="solde" readonly="yes" value="#numberformat(NewSolde.Solde,'999999.99')#" size="5">
            <cfelseif #NewSolde.Solde# lt 0>
                <cfinput style="color:##00F" name="solde" readonly="yes" value="#numberformat(NewSolde.Solde,'999999.99')#" size="6">
            <cfelse>
                <cfinput style="color:##F00" name="solde" readonly="yes" value="#numberformat(NewSolde.Solde,'999999.99')#" size="6">
            </cfif>
        </td>
    </tr>
</cfform>