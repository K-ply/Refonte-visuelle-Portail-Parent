﻿<cfquery name="lect_regies" datasource="f8w_test"><!--- lecture des établissement (plusieur régie) --->
	Select * from application_client use index(siret) 
    Where siret Like '#lect_etablissement.siret#%' order by NumEtablissement
</cfquery>

<cfif isdefined("form.pourmessage")>
	<!--- creation du message --->
    <cftry>
        <cfquery name="add" datasource="f8w_test" result="Resultat">
            Insert into message (siret,id_client,EmisPar,Objet,Text,NumE) values('#lect_user.siret#','#lect_client.id#',
            '#lect_client.id#',<cfqueryparam value="#form.Objet#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#form.message#" cfsqltype="cf_sql_longvarchar">,'#form.numetablissement#')
        </cfquery>
		<cfcatch>
        	<cfset msg = "Une erreur c'est produite, le message n'a pas été envoyé !!">
			<cfset msg1 = urlencodedformat(#msg#,"utf-8")>
    		<cflocation url="/message1/?id_mnu=16&typemess=out&msg=#msg1#">
        </cfcatch>
	</cftry>
	<!--- envoie mail a la mairie --->
    <cfquery name="cptmailserv" datasource="mission">
        Select * from cpt_appel_msg where pour='MSG'
    </cfquery>
	<cfset sujet = "Cantine de France, nouveau message de " & #lect_client.nom# & " " & #lect_client.prénom#>
	<cfset html_body = "<p>Un nouveau message de " & #lect_client.nom# & " " & #lect_client.prénom# & " est disponible dans votre gestion.</p>">	
	<cfset html_body = #html_body# & "<p>Vous pouvez le consulter dans votre boite de réception du menu Documents / Messages.</p>">
	<cfset html_body = #html_body# & "<p>Cordialement</p><p>Cantine de France</p>">
 	<cfset txt_body = "">
	<!---<cfquery name="plusieuretab" datasource="f8w_test">
    	Select EmailE from application_client where siret like '#lect_etablissement.siret#%' 
        and EmailE<>'' and EmailE<>'#lect_etablissement.EmailE#' 
    </cfquery>
    <cfif #plusieuretab.recordcount#>
    	<cfset mailetab = #lect_etablissement.EmailE#>
        <cfloop query="plusieuretab">
        	<cfset mailetab = #mailetab# & ";" & #plusieuretab.EmailE#>
        </cfloop>
    <cfelse>
    	<cfset mailetab = #lect_etablissement.EmailE#>
    </cfif>--->
    <cfquery name="lect_etablissement" datasource="f8w_test">
    	Select * from application_client where siret like '#lect_etablissement.siret#%' and NumEtablissement='#form.numetablissement#'
    </cfquery>
    <cfset mailetab = #lect_etablissement.EmailE#>
    <cfquery name="addspool" datasource="services">
        Insert into spool_mail (de,pour,sujet,html_body,txt_body,joint,date_crea,
        fail_to,EmailAccuseRecep,EmailNotifRecep,jointinsert,html_body_post_insert,jointinsertpied,siret,leserveur,
        utilisateur,mailutilisateur,motdepasse) 
        values('#cptmailserv.mailutilisateur#','#mailetab#',
        <cfqueryparam value="#sujet#" cfsqltype="cf_sql_varchar">,
        <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
        <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,
        '',#now()#,'','','','','','','#lect_etablissement.siret#',
        '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
    </cfquery>
	<cfif #form.type# eq "reponse">
		<cfset msg = "La réponse au message a été envoyée avec succès !">
	<cfelse>
    	<cfset msg = "Le message a été envoyée avec succès !">
    </cfif>
	<cfset msg1 = urlencodedformat(#msg#,"utf-8")>
    <cflocation url="/message1/?id_mnu=16&typemess=out&msg=#msg1#">
</cfif>
      <article class="content"><!--- CONTENU --->
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        	<tr><td>&nbsp;</td></tr>
            <tr align="center" valign="middle">
            	<td align="center" valign="middle">
                    <cfform name="newmess">
					  <cfif isdefined("form.sav_idmessage")>
                            <cfquery name="messori" datasource="f8w_test">
                                Select * from message where id='#form.sav_idmessage#'
                            </cfquery>
                            <cfset Obj = "Re: " & #messori.objet#>
                            <cfset Body = "<p>&nbsp;</p><p>&nbsp;</p>">
                            <cfset Body = #Body# & '<table width="100%"  border="0" cellspacing="0" cellpadding="0">'>
                            <cfset Body = #Body# & '<tr><td width="1%">&nbsp;<td><td>Le '>
                            <cfset Body = #Body# & lsdateformat(#messori.datemessage#,"dddd") & " " & lsdateformat(#messori.datemessage#,"dd") & " " & lsdateformat(#messori.datemessage#,"MMMM") & " " & lsdateformat(#messori.datemessage#,"YYYY" & " à " & lstimeformat(#messori.datemessage#,"hh")) & ":" & lstimeformat(#messori.datemessage#,"mm") & ", " & #lect_etablissement.nome# & " a écrit:</td></tr>">
                            <cfset Body = #Body# & '<tr><td>&nbsp;<td><td><p>&nbsp;</p>' & #messori.text# & "</td></tr></table>">
                        	<cfinput name="type" type="hidden" value="reponse">
                        <cfelse>
                            <cfset Obj = "">
                            <cfset Body = "">
                            <cfinput name="type" type="hidden" value="nouveau">
                        </cfif>                    
                    <table width="98%" border="0" cellspacing="0" cellpadding="0">
                      <tr bgcolor="#ADB96E">
                        <td colspan="3" align="center"><cfif isdefined("form.sav_idmessage")>Répondre au message<cfelse>Nouveau message</cfif></td>
                      </tr>
                      <tr bgcolor="#C6D580">
                        <td width="1%">&nbsp;</td>
                        <td colspan="2">
                        	
                        </td>
                      </tr>
                      <cfif #lect_regies.recordcount# gt 1>
                      	<tr bgcolor="#C6D580">
                            <td width="1%">&nbsp;</td>
                            <td colspan="2">Etablissement : 
                            <cfselect class="zoneinput" name="numetablissement">
                                <cfoutput query="lect_regies">
                                    <cfif len(#lect_regies.NomR#)>
                                        <cfset nometab = #lect_regies.NomR#>
                                    <cfelse>
                                        <cfset nometab = #lect_regies.NomE#>
                                    </cfif>
                                    <cfif isdefined("form.sav_idmessage")>
                                    	<cfif #messori.NumE# eq #lect_regies.numetablissement#>
                                        	<option value="#lect_regies.numetablissement#">#nometab#</option>
                                        </cfif>
                                    <cfelse>
                                    	<option value="#lect_regies.numetablissement#">#nometab#</option>
                                	</cfif>
								</cfoutput>
                            </cfselect>
                            </td>
                          </tr>
                          <tr bgcolor="#C6D580">
                            <td width="1%">&nbsp;</td>
                            <td>
                            </td>
                            <td width="89%" colspan="-1"></td>
                          </tr>
                      <cfelse>
                      	<cfinput type="hidden" name="numetablissement" value="#lect_etablissement.numetablissement#">
                      </cfif>
                      <tr bgcolor="#C6D580">
                        <td width="1%">&nbsp;</td>
                        <td>Objet <font color="#FF0000">*</font> :
                        </td>
                        <td width="89%" colspan="-1"><cfinput tabindex="1" title="Objet du message" required="yes" message="Saisir un objet svp !!" name="Objet" size="80" maxlength="254" value="#Obj#"></td>
                      </tr>
                      <tr bgcolor="#C6D580">
                        <td width="1%">&nbsp;</td>
                        <td>
                        </td>
                        <td width="89%" colspan="-1"></td>
                      </tr>
                      <tr bgcolor="#C6D580">
                        <td width="1%">&nbsp;</td>
                        <td valign="top">Message <font color="#FF0000">*</font> :
                        </td>
                        <td width="89%" colspan="-1">
                        	<cftextarea tabindex="2" required="yes" message="Saisir un message svp !!" title="Texte du message" tooltip="Texte du message" toolbar="Basic" name="message" id="message" height="350" html="yes" richtext="yes" width="630" value="#Body#"></cftextarea>
                        </td>
                      </tr>
                      <tr bgcolor="#C6D580">
                        <td width="1%">&nbsp;</td>
                        <td>
                        </td>
                        <td width="89%" colspan="-1"></td>
                      </tr>
                      <tr bgcolor="#C6D580">
                        <td width="1%">&nbsp;</td>
                        <td colspan="2" align="center">
                        	<cfif isdefined("form.sav_idmessage")>
                        		<cfinput type="submit" name="pourmessage" value="Répondre au message">
                        	<cfelse>
                        		<cfinput type="submit" name="pourmessage" value="Envoyer le message">
                        	</cfif>
                        </td>
                      </tr>
                      <tr bgcolor="#C6D580">
                        <td>&nbsp;</td>
                        <td colspan="2" align="center">&nbsp;</td>
                      </tr>
                      <tr bgcolor="#ADB96E">
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                      </tr>
                    </table>
                    </cfform>
                </td>
            </tr>
        </table>
 
