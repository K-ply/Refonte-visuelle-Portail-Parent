﻿<cfparam name="Attributes.CharBarreCode" default="" type="string">
<cfparam name="Attributes.siret" default="">
<cfparam name="Attributes.type" default="EAN-8">
<cfif len(#Attributes.CharBarreCode#)>
    <cfset Retour = "">
    <cfloop from="1" to="7" index="idx">
        <cfset car = mid(#Attributes.CharBarreCode#,#idx#,1)>
        <cfquery name="barrecodedecode" datasource="f8w">
            Select NumBarreCode from barrecodedecode use index(siret_CharBarreCode) 
            Where (siret='' or siret='#Attributes.siret#') and type='#attributes.type#' 
            and CharBarreCode=<cfqueryparam value="#car#" cfsqltype="CF_SQL_VARCHAR">
        </cfquery>            
        <!--- une requette sur é ou è renvoie toujour le 2 --->
        <cfif #car# eq "é">
            <cfset barrecodedecode.NumBarreCode = 2>
        <cfelseif #car# eq "è">    
            <cfset barrecodedecode.NumBarreCode = 7>
        </cfif>
        <cfif #barrecodedecode.recordcount#>
            <cfset Retour = #Retour# & #barrecodedecode.NumBarreCode#>   
        <cfelse>
            <cfset Retour = "Erreur Codage">
            <cfbreak>
        </cfif>
        <!---<cfswitch expression="#car#">    
            <cfcase value="à">
                <cfset Retour = #Retour# & 0>
            </cfcase>
            <cfcase value="é">
                <cfset Retour = #Retour# & 2>
            </cfcase>
            <cfcase value="ç">
                <cfset Retour = #Retour# & 9>
            </cfcase>
            <cfcase value="_">
                <cfset Retour = #Retour# & 8>
            </cfcase>
        </cfswitch>  --->  
    </cfloop>
    <cfset Caller.cf_BarCodeCharToNum.Retour = #Retour#>
</cfif>