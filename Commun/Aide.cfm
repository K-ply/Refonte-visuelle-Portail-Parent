﻿<cfajaximport tags="cfwindow, cfform">
<cfif len(#session.aide#)>
	<cfquery name="aide" datasource="eurosyl">
    	Select * from aide use index(CodeAppli) Where CodeAppli='#session.CodeAppli#' order by Menu
    </cfquery>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    	<title>Aide</title>
		<script language="JavaScript" type="text/JavaScript">
            function MM_jumpMenu(targ,selObj,restore)
            	{ 
                    eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
                    if (restore) selObj.selectedIndex=1;
            	}
        </script>        
    </head>
        <body>
            <cfform name="aide">
            <table width="100%" border="1" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="1%">&nbsp;</td>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr valign="top">
                    <td width="1%">&nbsp;</td>
                    <td width="48%">
                    	<cfselect size="20" name="menu" onChange="MM_jumpMenu('detailaide',this,0)">
                    		<cfoutput query="aide">
                            	<cfif #aide.id# eq #session.aide#>
                                	<option selected="selected" value="detailaide.cfm?id=#aide.id#">#aide.menu#</option>
                                <cfelse>
                            		<option value="detailaide.cfm?id=#aide.id#">#aide.menu#</option>
                            	</cfif>
                            </cfoutput>
                    	</cfselect>
                    </td>
                    <td width="2%">&nbsp;</td>
                    <td width="48%">
                    	<iframe name="detailaide" id="detailaide" width="100" height="100" scrolling="auto"></iframe>
                    </td>
                    <td width="1%">&nbsp;</td>
                </tr>
            </table>
            </cfform>   
        </body>
    </html>
</cfif>