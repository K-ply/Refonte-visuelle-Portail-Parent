﻿<div class="form-popup card" id="popupForm">
    <button class="close-btn" onclick="closePopUp()"><img src="/img/icon/icon_close.png" alt="icon en forme de croix pour fermer la fenêtre"></button>

    <cfform class="card-body" action="/karine/popup/index.cfm" method="post">
        <p class="text-grey bold">Créer une inscription</p>
        <div class="mb-3">
            <p>Quel enfant souhaitez vous inscrire ?</p>
            <cfinput name="child" id="child1" type="radio" value="Mathilde" class="input-radio">
            <label for="child1">Mathilde</label>
            <cfinput name="child" id="child2" type="radio" value="Mathéo" class="input-radio">
            <label for="child2">Mathéo</label>
        </div>
        <div class="mb-3">
            <p>Choisissez une prestation</p>
            <cfinput name="prestation" id="prestation1" type="radio" value="prestation1" class="input-radio">
            <label for="prestation1">Cantine adulte</label>
            <cfinput name="prestation" id="prestation2" type="radio" value="prestation2" class="input-radio">
            <label for="prestation2">Garderie du soir</label>
            <cfinput name="prestation" id="prestation3" type="radio" value="prestation3" class="input-radio">
            <label for="prestation3">Garderie du midi</label>
            <cfinput name="prestation" id="prestation4" type="radio" value="prestation4" class="input-radio">
            <label for="prestation4">CLSH : Cirque</label>
        </div>
        <div class="mb-3">
            <p>Choisissez la durée</p>
            <cfinput name="periode" id="periode1" type="radio" value="periode1" class="input-radio">
            <label for="periode1">Un jour</label>
            <cfinput name="periode" id="periode2" type="radio" value="periode2" class="input-radio">
            <label for="periode2">UNNNNN</label>
            <cfinput name="periode" id="periode3" type="radio" value="periode3" class="input-radio">
            <label for="periode3">Une période</label>
        </div>
        <div>
            <cfinput type="submit" name="go" value="Sauvegarder" class="btn btn-primary bold mt-3">
        </div>

    </cfform>
</div>