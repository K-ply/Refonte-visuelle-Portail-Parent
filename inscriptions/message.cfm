﻿<div class="scroll p-3 section section-calendar">
    <p class="bold text-grey">
        <a href="/message1/?id_mnu=148" class="open-button">Nouveaux messages </a>
    </p>
    <ul>
        <cfquery name="vermsg" datasource="f8w_test" maxrows="1">
            Select id from message use index(id_client_Lu) Where id_client='#lect_client.id#' Limit 0,1
        </cfquery>
        <cfif #vermsg.recordcount#>
            <cfquery name="message" datasource="f8w_test">
                Select id,Objet,Lu,DateMessage from message use index(id_client_Lu) 
                Where id_client='#lect_client.id#' order by DateMessage DESC limit 10
            </cfquery>
            <cfoutput query="message">
                <li class="text-grey ps-2 pb-2">#lsdateformat(DateMessage,"dd/MM/YYYY")# <a class="link-blue" href="/message1/?id_mnu=148&id_message=#id#">#Objet#</a></li>
            </cfoutput>
        </cfif>
    </ul>
</div>