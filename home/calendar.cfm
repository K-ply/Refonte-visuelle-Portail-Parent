﻿<div class="p-3 section section-calendar">
    <p class="bold text-grey">
        <a href="/inscriptions/?id_mnu=149" class="open-button">Calendrier des r&eacute;servations </a>
    </p>

    <cfset today = now()>
    <div class="row g-0 pt-4">
        <cfif isdefined ("Form.go")>
            <cfset direction="#Form.direction#">
            <cfset date = "#Form.date#">

            <cfif direction is "left">
                <cfset date=dateAdd("M", -1, #date#)>
            <cfelseif direction is "right">
                <cfset date=dateAdd("M", 1, #date#)>
            </cfif>

            <cfform name="CF_calendar" class="col-1 card-body" action="/home/?id_mnu=21" method="post">
                <div class="d-flex justify-content-center">
                    <cfinput name="direction" type="hidden" id="left" value="left">
                    <cfinput name="date" type="hidden" id="date" value="#date#">
                    <cfinput type="submit" name="go" class="small-left" value=" ">
                </div>
            </cfform>
            <div class="col-10 ps-2 pe-2 d-flex justify-content-center">
                <cfinclude template="/home/calendrier.cfm">
            </div>
            
            <cfform name="CF_calendar" class="col-1 card-body" action="/home/?id_mnu=21" method="post">
                <div class="d-flex justify-content-center">
                    <cfinput name="direction" type="hidden" id="right" value="right">
                    <cfinput name="date" type="hidden" id="date" value="#date#">
                    <cfinput type="submit" name="go" class="small-right" value=" ">
                </div>
            </cfform>

        <cfelse>
            <cfset date=today>

            <cfform name="CF_calendar" class="col-1 card-body" action="/home/?id_mnu=21" method="post">
                <div class="d-flex justify-content-center">
                    <cfinput name="direction" type="hidden" id="left" value="left">
                    <cfinput name="date" type="hidden" id="date" value="#date#">
                    <cfinput type="submit" name="go" class="small-left" value=" ">
                </div>
            </cfform>

            <div class="col-10 ps-2 pe-2 d-flex justify-content-center">
                <cfinclude template="/home/calendrier.cfm">
            </div>

            <cfform name="CF_calendar" class="col-1 card-body" action="/home/?id_mnu=21" method="post">
                <div class="d-flex justify-content-center">
                    <cfinput name="direction" type="hidden" id="right" value="right">
                    <cfinput name="date" type="hidden" id="date" value="#date#">
                    <cfinput type="submit" name="go" class="small-right" value=" ">
                </div>
            </cfform>
        </cfif>
    </div>
</div>