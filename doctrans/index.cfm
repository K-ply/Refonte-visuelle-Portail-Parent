﻿<cfinclude template="/Query_Header.cfm">

<cfparam name="msg" default="">
<cfif isdefined("form.numenfant")>
	<cfset nompdf = createuuid() & ".pdf">
    <cfset chemin = "C:\TempWeb\">
    <cftry>
    	<cffile action="upload" filefield="fichier" destination="#chemin##nompdf#">
    	<cfcatch>
        	<cfset msg = "Impossible de transmettre le fichier !!">
        </cfcatch>
    </cftry>
    <cfif #msg# eq "">
        <cfset extention = #cffile.serverFileExt#>
        <cfquery name="typedoc" datasource="classactive">
        	Select * from type_doc use index(type_doc_CodeAppli) where type_doc='#form.type_doc#' and CodeAppli='F8'
        </cfquery>  
    	<cfif #form.numenfant# gt 0>
        	<cfquery name="enfant" datasource="f8w_test">
            	Select id,nom,prénom from enfant use index(siret_numenfant) where siret='#lect_client.siret#' 
                and numenfant='#form.numenfant#'
            </cfquery>
            <cfset idproprio = #enfant.id#>
            <cfset libdoc = #typedoc.désignation# & " (" & #enfant.prénom# & " " & #enfant.nom# & ")">
        <cfelse>
        	<cfset idproprio = #lect_client.id#>
            <cfset libdoc = #typedoc.désignation#>
        </cfif>
        <cfquery name="docencour" dbtype="query">
        	select * from Documents where type_doc='#form.type_doc#'
        </cfquery>
        <cfif #docencour.validation# eq 1>
        	<cfset valide = 0>
        <cfelse>
        	<cfset valide = 1>
        </cfif>
        <cftry>
        	<cffile variable="filefichier" action="readbinary" file="#chemin##nompdf#">
        	<cfcatch>
        		<cfset msg = "Impossible de transmettre le fichier !!">
        	</cfcatch>
		</cftry>
		<cfif #msg# eq "">
        	<cftry>
				<cfset b = tobase64(#filefichier#)>
            	<cfcatch>
        			<cfset msg = "Impossible de transmettre le fichier !!">
        		</cfcatch>
            </cftry>
            <cfif #msg# eq "">
            	<cftry>
                    <cfquery name="add" datasource="classactive">
                        Insert into documents(siret,Id_Proprio,CodeAppli,Type_Proprio,Type_Doc,Table_origine,
                        Id_Table_Origine,Désignation,Document,Recherche,extention,Validé) values('#lect_client.siret#',
                        '#idproprio#','F8','#typedoc.type_proprio#','#form.type_doc#','','0',
                        <cfqueryparam value="#libdoc#" cfsqltype="cf_sql_varchar">,
                        <cfqueryparam value="#b#" cfsqltype="cf_sql_longvarchar">,'','#extention#','#valide#')
                    </cfquery>
					<cfcatch>
        				<cfset msg = "Impossible de transmettre le fichier !!">
        			</cfcatch>
            	</cftry>
			</cfif>
            <cfif #msg# eq "">
				<!--- l'envoi  de mail d'alerte à gestionnaire se fera via robot--->
                <cfset url.type_doc = "">
                <cfset url.numenfant = "">
                <cfset Documents = ModuleCDF.DocumentDemande(#lect_etablissement.siret#,#lect_client.id#)>
                <cfset msg = "Votre document a bien été transmis">
			</cfif>
		</cfif>
    </cfif>
</cfif>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <title>C2F - Documents</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

    <body>
        <div id="bgBlur">
            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>
            
            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-7 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p><h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>
                            #lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3></p>
                        <p class="description ps-5 pe-5">Transmettez ci-dessous vos documents.</p>
                    </div>

                    <!-- CONTENT -->
                    <cfinclude template="/doctrans/doctrans.cfm">
                </div>

                <!-- SECTION RIGHT -->
                <div class="col-lg-3 section-right">
                    <div class="mt-5">
                        <cfinclude template="/home/calendar.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions légales et confidentialité</a>
                <p class="m-0 pe-3 ps-3">N&deg; de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" alt="bouton menu déroulant" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">
        
        <script type="text/javascript"> //Script gestion menu burger
            function openNav() {
                document.getElementById("navMobileItems").style.display= "block";
                document.getElementById("navMobileLabel").style.display= "none";
                const background = document.getElementById("bgBlur");
                background.className = "bg-blur";
                background.addEventListener("click", closeNav);
            }
            function closeNav() {
                document.getElementById("navMobileItems").style.display= "none";
                document.getElementById("bgBlur").classList.remove("bg-blur");
                document.getElementById("navMobileLabel").style.display= "block";
            }
            function MM_jumpMenu(targ,selObj,restore) { 
                eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
                if (restore) selObj.selectedIndex=1;
            }
            function val() {
				document.getElementById("pubdoc").disabled = true;
				document.docdemande.submit();
			}
        </script>
    </body>
</html>