﻿<cfcomponent>    
    <cffunction name="ConformiteSepa" access="public" returntype="any">
        <!---
        '***********************************************************
        '* La fonction retourne la chaine a$ après avoir converti
        '* tous les caractères en caractères acceptés par SEPA.
        '* Liste des caractères admis:
        '*
        '* a b c d e f g h i j k l m n o p q r s t u v w x y z
        '* A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
        '* 0 1 2 3 4 5 6 7 8 9
        '* / - ? : ( ) . , ‘ +
        '* Espace
        '***********************************************************
		--->
        <cfargument name="a$" type="string">
        <cfargument name="longueur" type="any" required="no" default="0">
        <cfargument name="majuscule" type="any" required="no" default="0">
        <cfset b$ = "">
        <cfloop from="1" to="#Len(a$)#" index="i">
            <cfset c$ = Mid(#a$#, i, 1)>
            <cfswitch expression="#c$#">
                <cfcase value="é,è,ë,ê" delimiters=",">
                    <cfset b$ = #b$# & "e">
                </cfcase>  
                <!---<cfcase value="É,È,Ê,Ë" delimiters=",">
                    <cfset b$ = #b$# & "E">
                </cfcase>--->
                <!---<cfcase value="î,ï">
                    <cfset b$ = #b$# & "i">
                </cfcase>--->
                <cfcase value="Ì,Í,Î,Ï" delimiters=",">
                    <cfset b$ = #b$# & "I">
                </cfcase>
                <!---<cfcase value="ô,ö">
                    <cfset b$ = #b$# & "o">
                </cfcase>--->
                <cfcase value="Ò,Ó,Ô,Õ,Ö,Ø" delimiters=",">
                    <cfset b$ = #b$# & "O">
                </cfcase>
                <!---<cfcase value="ü,û,ù">
                    <cfset b$ = #b$# & "u">
                </cfcase>--->
                <cfcase value="Ù,Ú,Û,Ü" delimiters=",">
                    <cfset b$ = #b$# & "U">
                </cfcase>
                <!---<cfcase value="à,ä,â">
                    <cfset b$ = #b$# & "a">
                </cfcase>--->
                <cfcase value="Ä,Å,À,Â,Ã" delimiters=",">
                    <cfset b$ = #b$# & "A">
                </cfcase>
                <cfcase value="ç">
                    <cfset b$ = #b$# & "c">
                </cfcase>
                <cfcase value="Ý">
              		<cfset b$ = #b$# & "Y">
                </cfcase>
                <cfcase value="',’" delimiters=",">
                	<cfset b$ = #b$# & " ">
                </cfcase>
                <cfcase value="&">
                	<cfset b$ = #b$# & " ">
                </cfcase>
                <cfcase value=">">
                	<cfset b$ = #b$# & "sup">
                </cfcase>
                <cfcase value="<">
                	<cfset b$ = #b$# & "inf">
                </cfcase>
                <!---<cfcase delimiters="," value="/,-,?,:,(,),.,',+">
                	<cfset b$ = #b$# & #c$#>
                <cfcase>--->
                <cfcase value='"'>
                	<cfset b$ = #b$# & " ">
                </cfcase>
                <cfcase value="´">
                	<cfset b$ = #b$# & " ">
                </cfcase>
                <cfcase value="%">
                	<cfset b$ = #b$# & " Pourc.">
                </cfcase>                
                <cfcase value="|">
                	<cfset b$ = #b$# & " ">
                </cfcase>
                <cfdefaultcase>
                    <cfset b$ = #b$# & #c$#>
                </cfdefaultcase>
            </cfswitch>
        </cfloop>
        <cfset b$ = trim(b$)>
        <cfif #arguments.longueur# gt 0>
        	<cfset b$ = mid(#b$#,1,#arguments.longueur#)>
        </cfif>
        <cfif #arguments.majuscule# eq 1>
        	<cfset b$ = Ucase(#b$#)>
        </cfif>
		<cfreturn b$>
	</cffunction>
	
	<!--- egalement present dans le module de gestion et agent --->
	<cffunction name="ResultatTicketPortefeuille" access="public" returntype="query">
    	<cfargument name="siret" type="any" required="yes">
        <cfargument name="NumClient" type="numeric" required="yes">
    	<cfargument name="numEnfant" type="numeric" required="yes">
        <cfargument name="NumPrestation" type="numeric" required="yes">
        <cfargument name="IdPortefeuille" type="numeric" required="yes">
        <cfargument name="PourLeJour" type="string" required="yes">
        <cfargument name="action" type="string">
        <cfargument name="CodePrix" type="numeric" required="no" default="1">
        <cfargument name="AvecPresenceAbsence" type="numeric" required="no" default="0">
    	<cfset Retour = QueryNew("MontantDisponible,MontantDu,IdPortefeuille","Decimal,Decimal,Integer")>
    	<cfquery name="lect_client" datasource="f8w_test">
        	Select QuotientFamilial from client use index(siret_numclient) Where siret='#arguments.siret#' 
            and numclient='#arguments.numclient#'
        </cfquery>
        <cfquery name="enfant" datasource="f8w_test">
        	Select NumEnfant from enfant use index(siret_NumClientI) Where siret='#arguments.siret#' 
            and numclientI='#arguments.numclient#' and del=0
        </cfquery>
        <cfquery name="prestation" datasource="f8w_test">
        	Select Modalité from prestation use index(siret_numprestation) Where siret='#arguments.siret#' 
            and numprestation='#arguments.numprestation#'
        </cfquery>

		<!---- collias alsh pour l'instant --->
        <cfquery name="prixprestation" datasource="f8w_test">
    		Select * from prixprestation use index(siret_numprestation) Where siret='#arguments.siret#' 
            and numprestation='#arguments.numprestation#' and CodePrix='#arguments.CodePrix#' and del=0
    	</cfquery>
        <cfif #prixprestation.recordcount# eq 0>
            <cfquery name="prixprestation" datasource="f8w_test">
                Select * from prixprestation use index(siret_numprestation) Where siret='#arguments.siret#' 
                and numprestation='#arguments.numprestation#' and CodePrix=1 and del=0
            </cfquery>        
        </cfif>
        
        <!--- numgroupreduction pour le client --->
        <cfquery name="clientgrpclient" datasource="f8w_test">
        	Select NumGroupe from clientgroupe use index(siret_numclient) Where siret='#arguments.siret#' 
            and NumClient='#arguments.numclient#'
        </cfquery>
        <cfset LSTnumgroupe = "0,">
        <cfloop query="clientgrpclient">
        	<cfquery name="groupeclient" datasource="f8w_test">
            	Select id from groupeclient use index(siret_numgroupe) Where siret='#arguments.siret#' 
                and Numgroupe='#clientgrpclient.numgroupe#' and numprofil=0 and del=0
            </cfquery>
            <cfif #groupeclient.recordcount#>
            	<cfset LSTnumgroupe = #LSTnumgroupe# & #clientgrpclient.Numgroupe# & ",">
            </cfif>
        </cfloop>
        
        <!--- numgroupreduction pour l'enfant  --->
        <cfquery name="clientgrpclient" datasource="f8w_test">
        	Select NumGroupe from clientgroupe use index(siret_numenfant) Where siret='#arguments.siret#' 
            and NumEnfant='#arguments.numenfant#'
        </cfquery>
        <cfif #clientgrpclient.recordcount#>
            <!--- Si il y a des réductions affectées à cet enfant elles prévalent sur les autres --->
            <!--- ont annule les réductions de type parents --->
            <!--- provoquai bug sur croezs --->
            <cfif #arguments.siret# neq "99041357221611">
                <cfset LSTnumgroupe = "">
            </cfif>
        </cfif>
        <cfloop query="clientgrpclient">
        	<cfquery name="groupeclient" datasource="f8w_test">
            	Select id from groupeclient use index(siret_numgroupe) Where siret='#arguments.siret#' 
                and Numgroupe='#clientgrpclient.numgroupe#' and numprofil=0 and del=0
            </cfquery>
            <cfif #groupeclient.recordcount#>
            	<cfset LSTnumgroupe = #LSTnumgroupe# & #clientgrpclient.Numgroupe# & ",">
            </cfif>
        </cfloop>
        
        
        <cfquery name="reductions" datasource="f8w_test">
        	Select * from reductions use index(siret_numprestation) 
            where siret='#arguments.siret#' 
            and NumPrestation='#arguments.NumPrestation#' 
            and CodePrix='#arguments.CodePrix#'
            and find_in_set(GroupeClient,'#LSTnumgroupe#')
            and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
            and del=0 order by ApartirDeNbrEnfant
        </cfquery>
        <cfif #reductions.recordcount# eq 0>
            <cfquery name="reductions" datasource="f8w_test">
                Select * from reductions use index(siret_numprestation) 
                where siret='#arguments.siret#' 
                and NumPrestation='#arguments.NumPrestation#' 
                and find_in_set(GroupeClient,'#LSTnumgroupe#')
                and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                and del=0 order by ApartirDeNbrEnfant
            </cfquery>
        </cfif>        
        <!---<cfset ficlog = #arguments.siret# & "_" & #arguments.numclient# & "_resulticport.log">    
        <cffile action="WRITE" file="c:\tempweb\#ficlog#" nameconflict="OVERWRITE" output="ligne 195 #reductions.recordcount# réduction(s)">    
        <cffile action="APPEND" file="c:\tempweb\#ficlog#" nameconflict="OVERWRITE" output="ligne 196 lstnumgroupe #LSTnumgroupe#">--->
        
        <cfquery name="prestation_prepaye_portefeuille" datasource="f8w_test">
        	Select id,Montant from prestation_prepaye_portefeuille Where id='#arguments.IdPortefeuille#'
        </cfquery>
		
		<!---- collias alsh pour l'instant --->
        <cfif #lect_client.QuotientFamilial# eq 0 and #arguments.siret# eq "11612111549230">
        	<cfset r = QueryAddRow(Retour)>
            <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
            <cfset r = QuerySetCell(Retour,"MontantDu",val(#prixprestation.PU#))>
            <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)>
        <cfelse>
        	<cfif #arguments.action# neq "desinscr"> 
				<cfset NbrEnfantCeJour = 1><!--- demande d'inscription --->
            <cfelse>
            	<cfset NbrEnfantCeJour = 0><!--- demande desinscription --->
            </cfif>
            <cfloop query="enfant">
            	<cfquery name="lect_inscriptionplanning" datasource="f8w_test">
        			Select id from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
            		Where siret = '#arguments.siret#' 
            		and NumPrestation = '#arguments.NumPrestation#' and NumEnfant = '#enfant.NumEnfant#' 
                    and DateInscription = '#arguments.PourLeJour#' and Nature=0
        		</cfquery>
            	<cfset NbrEnfantCeJour = #NbrEnfantCeJour# + #lect_inscriptionplanning.recordcount#>
                <cfif #arguments.AvecPresenceAbsence# eq 1>
                	<cfquery name="ver" datasource="f8w_test">
                        Select PrésentAbsent from présenceabsence use index(siret_NumEnfant_NumPrestation_Date) 
                        Where siret='#arguments.siret#' and NumEnfant='#enfant.NumEnfant#' 
                        and NumPrestation='#arguments.NumPrestation#' and Date='#arguments.PourLeJour#'
                    </cfquery>
                	<cfif #ver.PrésentAbsent# eq 3 or #ver.PrésentAbsent# eq 4>
                        <!--- Présent justifié ou Présent injustifié --->
                    	<cfset NbrEnfantCeJour = #NbrEnfantCeJour# + 1>
                    <cfelseif #ver.PrésentAbsent# eq 1 or #ver.PrésentAbsent# eq 2>
                        <!--- Absent justifié ou Absent injustifié--->  
                		<cfset NbrEnfantCeJour = #NbrEnfantCeJour# - 1>
                    </cfif>
                </cfif>
            </cfloop>
            <cfif #NbrEnfantCeJour# eq 1>
            	<cfquery name="reducfin" dbtype="query">
                	Select * from reductions Where ApartirDeNbrEnfant=1
                </cfquery>
				<cfif #reducfin.recordcount# eq 0>
                    <cfquery name="reducfin" dbtype="query">
                        Select * from reductions Where ApartirDeNbrEnfant=0 and EnfantSuivantSiConsoMemeJour=0
                    </cfquery>
				</cfif>
            <cfelse>
            	<cfquery name="reducfin" dbtype="query">
                	Select * from reductions Where ApartirDeNbrEnfant=#NbrEnfantCeJour# and EnfantSuivantSiConsoMemeJour=0
                </cfquery>
				<cfif #reducfin.recordcount# eq 0>
                    <cfquery name="reducfin" dbtype="query">
                        Select * from reductions Where ApartirDeNbrEnfant=0 and EnfantSuivantSiConsoMemeJour=0
                    </cfquery>
				</cfif>                           
            </cfif>
        	
            <!---<cfif #reducfin.recordcount# eq 0 and (#Arguments.siret# eq "38403123229130" or #Arguments.siret# eq "38374091723171")>--->
            <cfif #Arguments.siret# eq "38403123229130" or #Arguments.siret# eq "38374091723171">    
                <!--- cas une presta garderie matin et soir remplace garderie matin et garderie soir (sivom momers & sirs domessarges)--->    
                <cfquery name="prestaremplace" datasource="f8w_test"><!--- prestation qui en remplace d'autre --->
                    Select * from prestation use index(siret) 
                    Where siret='#Arguments.siret#' and ResaConstat=0 
                    and ResaConstatAnnulRemplace=1 and ResaConstatAnnulRemplaceSiConsomemejour=1 and ticket=1 and del=0
                </cfquery>    
                <cfif #prestaremplace.recordcount#>
                    <!---<cffile action="APPEND" file="c:\tempweb\logodomes.txt" output="Ligne 269">--->
                    <cfloop query="prestaremplace">
                        <!--- prix et réduction de la remplacante --->
                        <cfquery name="prixprestationremplace" datasource="f8w_test">
                            Select * from prixprestation use index(siret_numprestation) Where siret='#arguments.siret#' 
                            and numprestation='#prestaremplace.numprestation#' and CodePrix='#arguments.CodePrix#' and del=0
                        </cfquery>
                        <cfif #prixprestationremplace.recordcount# eq 0>
                            <cfquery name="prixprestationremplace" datasource="f8w_test">
                                Select * from prixprestation use index(siret_numprestation) Where siret='#arguments.siret#' 
                                and numprestation='#prestaremplace.numprestation#' and CodePrix=1 and del=0
                            </cfquery>        
                        </cfif>
                        <cfquery name="reductionsremplace" datasource="f8w_test">
                            Select * from reductions use index(siret_numprestation) 
                            where siret='#arguments.siret#' 
                            and NumPrestation='#prestaremplace.NumPrestation#' 
                            and find_in_set(GroupeClient,'#LSTnumgroupe#')
                            and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                            and del=0 order by ApartirDeNbrEnfant
                        </cfquery>
                        <cfif #NbrEnfantCeJour# eq 1>
                            <cfquery name="reducfinremplace" dbtype="query">
                                Select * from reductionsremplace Where ApartirDeNbrEnfant=1
                            </cfquery>
                            <cfif #reducfinremplace.recordcount# eq 0>
                                <cfquery name="reducfinremplace" dbtype="query">
                                    Select * from reductionsremplace Where ApartirDeNbrEnfant=0 and EnfantSuivantSiConsoMemeJour=0
                                </cfquery>
                                <cfif #reducfinremplace.recordcount# eq 0>
                                    <cfquery name="reducfinremplace" dbtype="query">
                                        Select * from reductionsremplace Where ApartirDeNbrEnfant=0 and EnfantSuivantSiConsoMemeJour=1
                                    </cfquery>    
                                </cfif>
                            </cfif>
                        <cfelse>
                            <cfquery name="reducfinremplace" dbtype="query">
                                Select * from reductionsremplace Where ApartirDeNbrEnfant=#NbrEnfantCeJour# and EnfantSuivantSiConsoMemeJour=0
                            </cfquery>
                            <cfif #reducfinremplace.recordcount# eq 0>
                                <cfquery name="reducfinremplace" dbtype="query">
                                    Select * from reductionsremplace Where ApartirDeNbrEnfant=#NbrEnfantCeJour# and EnfantSuivantSiConsoMemeJour=1
                                </cfquery>    
                                <cfif #reducfinremplace.recordcount# eq 0>
                                    <cfquery name="reducfinremplace" dbtype="query">
                                        Select * from reductionsremplace Where ApartirDeNbrEnfant=0 and EnfantSuivantSiConsoMemeJour=0
                                    </cfquery>                                
                                </cfif>
                            </cfif>                           
                        </cfif>                        
                        
                        <cfquery name="toutesprestationlie" datasource="f8w_test"><!--- la prestation en cour peut etre remplacé ? --->
                            Select * from prestationlie use index(id_prestation) where id_prestation='#prestaremplace.id#' 
                        </cfquery>                        
                        <cfquery name="prestationlie" datasource="f8w_test"><!--- la prestation en cour peut etre remplacé ? --->
                            Select * from prestationlie use index(id_prestation) where id_prestation='#prestaremplace.id#' 
                            and liea_numprestation='#Arguments.numprestation#'
                        </cfquery>
                        <cfif #prestationlie.recordcount#> 
                            <cfquery name="prestationliefin" datasource="f8w_test"><!--- si la prestation en cour peut etre remplacé, ont prend les autres concernées--->
                                Select * from prestationlie use index(id_prestation) where id_prestation='#prestaremplace.id#' 
                                and liea_numprestation<>'#Arguments.numprestation#'
                            </cfquery>
                            <cfset presentauprestation=0>
                            <cfloop query="prestationliefin">    
                                <cfquery name="lect_inscriptionplanning" datasource="f8w_test">
                                    Select id from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                                    Where siret = '#arguments.siret#' 
                                    and NumPrestation = '#prestationliefin.liea_numprestation#' and NumEnfant = '#arguments.NumEnfant#' 
                                    and DateInscription = '#arguments.PourLeJour#' and Nature=0
                                </cfquery>
                                <cfif #lect_inscriptionplanning.recordcount#>
                                     <cfset presentauprestation = #presentauprestation# + 1>
                                </cfif>
                                <cfif #arguments.AvecPresenceAbsence# eq 1>
                                    <cfquery name="ver" datasource="f8w_test">
                                        Select PrésentAbsent from présenceabsence use index(siret_NumEnfant_NumPrestation_Date) 
                                        Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#' 
                                        and NumPrestation='#prestationliefin.liea_numprestation#' and Date='#arguments.PourLeJour#'
                                    </cfquery>
                                    <cfif #ver.PrésentAbsent# eq 3 or #ver.PrésentAbsent# eq 4>
                                        <!--- Présent justifié ou Présent injustifié --->
                                        <cfset presentauprestation = #presentauprestation# + 1>
                                    <cfelseif #ver.PrésentAbsent# eq 1 or #ver.PrésentAbsent# eq 2>
                                        <!--- Absent justifié ou Absent injustifié--->  
                                        <cfset presentauprestation = #presentauprestation# - 1>
                                    </cfif>
                                </cfif>                                        
                            </cfloop>
                            <cfif #presentauprestation# eq #prestationliefin.recordcount# or #presentauprestation# eq #toutesprestationlie.recordcount#><!--- les conditions de remplacement sont remplies --->
                                <!---<cffile action="APPEND" file="c:\tempweb\logodomes.txt" output="Ligne 358">--->
                                <cfset r = QueryAddRow(Retour)>
                                <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
                                <cfif #prixprestation.PU# eq #prixprestationremplace.PU#>
                                    <!--- cas momers, la garderie matin et soir coute comme une garderie matin et soir
                                    ça a déja ete retirer du portefeuille --->
                                    <cfset r = QuerySetCell(Retour,"MontantDu",0)>
                                <cfelse>
                                    <!--- cas sirs dommessarge, la garderie matin et soir coute 2 et matin ou soir 1.30 --->
                                    <cfset prixreduit = #prixprestation.PU# - val(#reducfin.MontReduc#)><!---
                                    ---><cffile action="APPEND" file="c:\tempweb\logodomes.txt" output="prix reduit #prixreduit#">
                                    
                                    <cfset prixreduitremplace = #prixprestationremplace.PU# - val(#reducfinremplace.MontReduc#)>
                                    <!---<cffile action="APPEND" file="c:\tempweb\logodomes.txt" output="prixreduitremplace #prixreduitremplace#"> --->   
                                    
                                    <!---<cfset prixreduitfin = #prixreduitremplace# - #prixreduit#>
                                    <cffile action="APPEND" file="c:\tempweb\logodomes.txt" output="prixreduitfin #prixreduitfin#"> ---> 
                                        
                                    
                                    <cfset r = QuerySetCell(Retour,"MontantDu",#prixreduitremplace#)>
                                </cfif>
                                <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)> 
                            
                            </cfif>    
                            <cfbreak>
                        </cfif>    
                    </cfloop>
                </cfif>
                <!--- cas Xeme enfant --->                
                                
            </cfif>    
                
            <!--- modif collorgues & sivom momers & sirs dommessargues garderie prepaye gratuite à partir du Xeme enfant --->
            <cfif #reducfin.recordcount# eq 0 and #NbrEnfantCeJour# gt 0 and #Retour.recordcount# eq 0>
            	<cfquery name="reducfin" dbtype="query">
                    Select * from reductions Where 
                    EnfantSuivantSiConsoMemeJour>0 and EnfantSuivantSiConsoMemeJour<=#NbrEnfantCeJour#
                </cfquery>
                <cfset prixreduit = #prixprestation.PU# - val(#reducfin.MontReduc#)>
                <cfset r = QueryAddRow(Retour)>
                <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
                <cfset r = QuerySetCell(Retour,"MontantDu",#prixreduit#)>
                <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)> 
            
            <cfelseif #reducfin.recordcount# eq 1 and #Retour.recordcount# eq 0> 
                <cfset prixreduit = #prixprestation.PU# - val(#reducfin.MontReduc#)>
                <cfset r = QueryAddRow(Retour)>
                <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
                <cfset r = QuerySetCell(Retour,"MontantDu",#val(prixreduit)#)>
                <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)>
            
            <cfelseif #reducfin.recordcount# gt 1 and #Retour.recordcount# eq 0>        
                <cfquery name="reducfinfin" dbtype="query"><!--- d'abord sur groupe reduction --->
                	Select * from reducfin Where GroupeClient>0
                </cfquery>        
                <cfif #reducfinfin.recordcount# eq 1>
                    <cfset prixreduit = #prixprestation.PU# - val(#reducfinfin.MontReduc#)>
                    <cfset r = QueryAddRow(Retour)>
                    <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
                    <cfset r = QuerySetCell(Retour,"MontantDu",#prixreduit#)>
                    <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)>
                <cfelse>
                
                    
                    
                    
                </cfif>
            </cfif>
        
        
        
            <cfif #Retour.recordcount# eq 0 and #reducfin.recordcount# eq 0>
                <cfset r = QueryAddRow(Retour)>
                <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
                <cfset r = QuerySetCell(Retour,"MontantDu",val(#prixprestation.PU#))>
                <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)>
            <cfelseif #Retour.recordcount# eq 0 and #reducfin.recordcount# eq 1>
                <cfset prixreduit = #prixprestation.PU# - val(#reducfin.MontReduc#)>
                <cfset r = QueryAddRow(Retour)>
                <cfset r = QuerySetCell(Retour,"MontantDisponible",val(#prestation_prepaye_portefeuille.Montant#))>
                <cfset r = QuerySetCell(Retour,"MontantDu",#prixreduit#)>
                <cfset r = QuerySetCell(Retour,"IdPortefeuille",#arguments.IdPortefeuille#)>                    
            </cfif>
    	</cfif>
        <cfreturn Retour>
    </cffunction>
    <!---               --->
    
    
    
    
    <cffunction name="GetEnfant" access="public" returntype="query">
    	<cfargument name="siret" type="any" required="yes">
        <cfargument name="NumClient" type="numeric" required="yes">
        <cfquery name="enfant" datasource="f8w_test">
        	Select * from enfant use index(siret_NumClient) 
            Where siret='#arguments.siret#' and NumClientI='#arguments.NumCLient#'
        </cfquery>
        <cfreturn enfant>
    </cffunction>
    
    <cffunction name="GetPrestation" access="public" returntype="query">
    	<cfargument name="siret" type="any" required="yes">
        <cfargument name="NumClient" type="numeric" required="yes">
    	<cfargument name="NumEnfant" type="numeric" required="yes">
    	<cfquery name="prestation" datasource="f8w_test">
    		Select * from prestation use index(siret) Where siret='#arguments.siret#' and TopWeb=1 order by ordre
    	</cfquery>
        <cfset lst_non = ""><!--- pas droit a la prestation --->
        <cfloop query="prestation">
        	<cfset reserve_a = prestation_reserve_a1(#arguments.siret#,#prestation.NumPrestation#,#arguments.NumClient#,#arguments.NumEnfant#,0,"")>
            <cfif #reserve_a# neq "OK">
            	<cfset lst_non = #lst_non# & #prestation.id# & ",">
            </cfif>
        </cfloop>
        <cfif len(#lst_non#)>
            <cfquery name="prestation" datasource="f8w_test">
                Select * from prestation use index(siret) 
                Where siret='#arguments.siret#' and TopWeb=1 and find_in_set(id,'#lst_non#')=0 order by ordre
            </cfquery>
        </cfif>
        <cfreturn prestation>
    </cffunction>
    
    <cffunction name="GetInscription" access="public" returntype="query">
    	<cfargument name="siret" type="any" required="yes">
    	<cfargument name="NumClient" type="any" required="yes">
    	<cfargument name="NumEnfant" type="any" required="yes">
        <cfargument name="NumPrestation" type="any" required="yes">
    	<cfargument name="DateDeb" type="string" required="yes">
        <cfargument name="DateFin" type="string" required="yes">
        <cfquery name="lect_prestation" datasource="f8w_test">
    		Select * from prestation use index(siret) Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#'
    	</cfquery>
		<cfquery name="lect_enfantfanfan" datasource="f8w_test">
        	Select * from enfant use index(siret_NumClient) 
            Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#'
        </cfquery>        
        <cfset Arguments.Date_calend_dep = createdate(#mid(arguments.DateDeb,7,4)#,#mid(arguments.DateDeb,4,2)#,#mid(arguments.DateDeb,1,2)#)>
        <cfset Arguments.Date_calend_fin = createdate(#mid(arguments.DateFin,7,4)#,#mid(arguments.DateFin,4,2)#,#mid(arguments.DateFin,1,2)#)>
        <cfset NbrJour = datediff("d",#Arguments.Date_calend_dep#,#Arguments.Date_calend_fin#) + 1>
		<cfset inscription = querynew("Jour,NumJour,Enfant,Prestation,Etat,bg,cont","Varchar,Integer,Varchar,Varchar,Integer,Varchar,Varchar")>
<!---		<cfif #lect_prestation.ResaPeriode# eq 0><!--- prestation normale --->
--->		<cfloop from="1" to="#NbrJour#" index="idx">
				<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfanfan.id#)>
                <cfset etat = -2>
                <cfswitch expression="#a.bg#">
					<cfcase value="##666666"><!--- gris fermé  --->
                		<cfset etat = -1>
                	</cfcase>
					<cfcase value="##FFFFFF"><!--- blanc ouvert pas inscrit --->
                		<cfset etat = 0>
                	</cfcase>
					<cfcase value="##B4F8A0"><!--- vert clair ouvert inscrit passé --->
                		<cfset etat = 1>
                	</cfcase>
					<cfcase value="##00FF00"><!--- vert ouvert inscrit --->
                		<cfset etat = 2>
                	</cfcase>
					<cfcase value="##FF0000"><!--- rouge ouvert desinscrit --->
                		<cfset etat = 3>
                	</cfcase>
                </cfswitch>
                <cfset r = queryaddrow(inscription)>
                <cfset r = querysetcell(inscription,"Jour",lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY"))>
                <cfset r = querysetcell(inscription,"NumJour",#idx#)>
                <cfset leenfant = #lect_enfantfanfan.prénom# & " " & #lect_enfantfanfan.nom#>
                <cfset r = querysetcell(inscription,"Enfant",#leenfant#)>
                <cfset r = querysetcell(inscription,"Prestation",#lect_prestation.désignation#)>
                <cfset r = querysetcell(inscription,"Etat",#etat#)>
                <cfset r = querysetcell(inscription,"bg",#a.bg#)>
                <cfif #etat# eq 0 or #etat# eq 2 or #etat# eq 3>
                	<cfif datecompare(now(),#Arguments.Date_calend_dep#) eq -1>	
						<cfset r = querysetcell(inscription,"cont",replacenocase(#a.cont#,"/inscriptions/?id_mnu=22","?UpldInscr=1&id_mnu=#url.id_mnu#"))>
        			<cfelse>
                    	<cfset r = querysetcell(inscription,"cont",replacenocase(#a.cont#,"/inscriptions/?id_mnu=22","?UpldInscr=0&id_mnu=#url.id_mnu#"))>
                    </cfif>
                <cfelse>
                	<cfset r = querysetcell(inscription,"cont",replacenocase(#a.cont#,"/inscriptions/?id_mnu=22","?UpldInscr=0&id_mnu=#url.id_mnu#"))>
                </cfif>
				<cfset Arguments.Date_calend_dep = dateadd("D",1,#Arguments.Date_calend_dep#)>
        	</cfloop>
        <!---<cfelse><!--- prestation type tap --->
        
        </cfif>--->
        <cfreturn inscription>
    </cffunction>
    <cffunction name="GetInscriptionV2" access="public" returntype="query">
    	<cfargument name="siret" type="any" required="yes">
    	<cfargument name="NumClient" type="numeric" required="yes">
    	<cfargument name="NumEnfant" type="numeric" required="yes">
        <cfargument name="NumPrestation" type="numeric" required="yes">
    	<cfargument name="DateDeb" type="string" required="yes">
        <cfargument name="DateFin" type="string" required="yes">
        <cfargument name="presabs" type="numeric" required="no" default="0">
       	<cfargument name="siretdispo" required="no" default="">
        <cfargument name="id_reserve_a" required="no" default="0">
        <cfif len(#arguments.siretdispo#) eq 0>
        	<cfset arguments.siretdispo = #arguments.siret#>
        </cfif>
        <cfset inscription = querynew("Jour,NumJour,Inscrit","Varchar,Integer,Integer")>
        <cfset DateDeb = createdate(#mid(arguments.DateDeb,7,4)#,#mid(arguments.DateDeb,4,2)#,#mid(arguments.DateDeb,1,2)#)>
        <cfset DateFin = createdate(#mid(arguments.DateFin,7,4)#,#mid(arguments.DateFin,4,2)#,#mid(arguments.DateFin,1,2)#)>
    	<cfset DateDebSTP = lsdateformat(#DateDeb#,"YYYYMMdd")>
    	<cfset DateFinSTP = lsdateformat(#DateFin#,"YYYYMMdd")>
        <cfset NbrJour = datediff("d",#DateDeb#,#DateFin#) + 1>
    	
		<!--- collect des inscriptionplanning --->
        <cfquery name="inscriptionplanning" datasource="f8w_test">
    		Select DateInscription,Nature from inscriptionplanning use index(siret_NumEnfant_NumPrestation_DateInscriptionSTP) 
            Where siret = '#Arguments.siret#' and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#' 
            and DateInscriptionSTP >= #DateDebSTP# and DateInscriptionSTP <= #DateFinSTP# 
            <cfif #arguments.id_reserve_a# gt 0>and id_reserve_a='#arguments.id_reserve_a#'</cfif>
    	</cfquery>
        <!--- collect des inscriptions --->
        <cfquery name="inscriptionavantdebutperiode" datasource="f8w_test" maxrows="1">
        	Select id,inscriptionimpaire,DateDeb from inscription use index(siret_NumEnfant_NumPrestation_DateDebSTP) 
            Where siret = '#Arguments.siret#' and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#'
            and DateDebSTP <= #DateDebSTP# order by DateDebSTP DESC limit 0,1
        </cfquery>
        <cfquery name="inscriptionpendantperiode" datasource="f8w_test">
        	Select id,inscriptionimpaire,DateDeb,DatedebSTP from inscription use index(siret_NumEnfant_NumPrestation_DateDebSTP) 
            Where siret = '#Arguments.siret#' and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#'
            and DateDebSTP >= #DateDebSTP# and DateDebSTP <= #DateFinSTP#
        </cfquery>
		<cfquery name="etabpourzone" datasource="f8w_test">
            Select Zone from application_client use index(siret) Where siret = '#Arguments.siret#'
        </cfquery>
 		<cfquery name="lect_prestation" datasource="f8w_test">
        	Select modalité,JoursExclus from prestation use index(siret_numprestation) 
            Where siret='#arguments.siretdispo#' and NumPrestation='#arguments.NumPrestation#'
        </cfquery>                    
        <cfquery name="ptv" datasource="f8w_test"><!--- pour calendier type vacance si besoint --->
            Select typevacance,PasDeVacance from prestation use index(siret_numprestation) 
            Where siret = '#Arguments.siret#' and NumPrestation = '#Arguments.numprestation#'
        </cfquery>
        <cfset Arguments.Date_calend_dep = #DateDeb#>
        <cfloop from="1" to="#NbrJour#" index="idx">
        	<cfset inscrit = 0>
        	<cfset NumJourI = DayOfWeek(#Arguments.Date_calend_dep#)>
			<cfset DateJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd/MM/YYYY")>
            <cfset DateJourStp = LsDateFormat(#Arguments.Date_calend_dep#,"YYYYMMdd")> 

            
            <cfquery name="jourexclus" datasource="f8w_test">
                Select * from jourseclus#ptv.typevacance# use index(siret_NumPrestation_DateJour) 
                Where siret = '#Arguments.siret#' and (NumPrestation = '#Arguments.numprestation#' or NumPrestation=0) and DateJour = '#DateJour#' 
                or siret='' and NumPrestation = 0 and DateJour = '#DateJour#' and zone = '#etabpourzone.Zone#'
            </cfquery>
            <cfquery name="jourNONexclus" datasource="f8w_test">
                Select * from joursNONeclus#ptv.typevacance# use index(siret_Numprestation) Where siret = '#Arguments.siret#' 
                and NumPrestation = '#Arguments.numprestation#' and DateJour = '#DateJour#'
            </cfquery>        
        	<cfset fermé = false>
        	<!---
			<cfif #jourexclus.recordcount# gt 0 and #jourNONexclus.recordcount# eq 0>
				<cfset fermé = true>
            </cfif>
            --->
			<cfif #jourexclus.recordcount# gt 0>
                <cfloop query="jourexclus">
                    <cfif #jourexclus.NumClasse# eq 0 and #ptv.PasDeVacance# eq 0>
                        <cfset fermé = true>
                        <cfbreak>        
                    <cfelse>
                        <cfquery name="enfantclasse" datasource="f8w_test">
                            Select id from enfantclasse use index(siret_NumEnfant_NumClasse) Where siret = '#Arguments.siret#' 
                            and NumEnfant='#Arguments.NumEnfant#' and NumClasse='#jourexclus.NumClasse#'
                        </cfquery>
                        <cfif #enfantclasse.recordcount#>
                            <cfquery name="classe" datasource="f8w_test">
                                Select id from classe use index(Siret_NumClasse) Where siret = '#Arguments.siret#' 
                                and NumClasse='#jourexclus.NumClasse#' and del=0
                            </cfquery>
                            <cfif #classe.recordcount#>
                                <cfset fermé = true>
                                <cfbreak>
                            </cfif>
                        </cfif>
                    </cfif>
                </cfloop>		
            </cfif>            
            
            
            
            
            
            
            
            
        	<cfif #fermé# is false>
				<cfif Resajour(#lect_prestation.JoursExclus#,#NumJourI#) eq 1>
					<cfset fermé = true>
            	</cfif>
            </cfif>
        	<cfif #fermé# is false>
            	<cfset presabse = "non">
            	<cfif #arguments.presabs# eq 1 and #lect_prestation.modalité# eq 0>
                	<cfquery name="presenceabsence" datasource="f8w_test">
                		Select PrésentAbsent from présenceabsence use index(siret_NumEnfant_NumPrestation_Date) Where siret = '#Arguments.siret#' 
                         and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#' and date='#DateJour#'
                	</cfquery>
                    <cfif #presenceabsence.recordcount#>
                    	<cfset presabse = "oui">
                        <cfif #presenceabsence.PrésentAbsent# eq 0 or #presenceabsence.PrésentAbsent# eq 3 or #presenceabsence.PrésentAbsent# eq 4>
                        	<cfset inscrit = 1>
                        <cfelseif #presenceabsence.PrésentAbsent# eq 1 or #presenceabsence.PrésentAbsent# eq 2>
                        	<cfset inscrit = 0>
                        </cfif>
                    </cfif>
                </cfif>
                <cfif #presabse# eq "non">
					<!--- verif si inscription hebdo avant la période --->
                    <cfif #inscriptionavantdebutperiode.recordcount# and #inscriptionavantdebutperiode.inscriptionimpaire# gt 0>
                        <cfif Resajour(#inscriptionavantdebutperiode.InscriptionImpaire#,#NumJourI#) eq 1>
                            <cfset inscrit = 1>
                        </cfif>                
                    </cfif>
                    <!--- verif si inscription hebdo pendant la periode --->
                    <cfif #inscriptionpendantperiode.recordcount#>
                        <cfquery name="verinscription" dbtype="query" maxrows="1">
                            Select * from inscriptionpendantperiode where DatedebSTP<=#DateJourStp# order by DatedebSTP DESC
                        </cfquery>
                        <cfif #verinscription.recordcount#>
                            <cfif #verinscription.inscriptionimpaire# eq 0>
                                <cfset inscrit = 0>
                            <cfelse>
                                <cfif Resajour(#verinscription.InscriptionImpaire#,#NumJourI#) eq 1>
                                    <cfset inscrit = 1>
                                </cfif>                    
                            </cfif>
                        </cfif>
                    </cfif>
                    <cfif #inscriptionplanning.recordcount#>
                        <cfquery name="verinscriptionplanning" dbtype="query">
                            Select * from inscriptionplanning where DateInscription='#DateJour#'
                        </cfquery>
                        <cfif #verinscriptionplanning.nature# eq 0>
                            <cfset inscrit = 1>
                        <cfelseif #verinscriptionplanning.nature# eq 1>
                            <cfset inscrit = 0>
                        </cfif>
                    </cfif>
				</cfif>
        	</cfif>
			<cfset r = queryaddrow(inscription)>
            <cfset r = querysetcell(inscription,"Jour",lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY"))>
            <cfset r = querysetcell(inscription,"NumJour",#idx#)>
            <cfset r = querysetcell(inscription,"Inscrit",#inscrit#)>            
        	<cfset Arguments.Date_calend_dep = dateadd("D",1,#Arguments.Date_calend_dep#)>
        </cfloop>
        <cfreturn inscription>
    </cffunction>
    <cffunction name="UpdateInscription" access="public" returntype="string">
    	<cfargument name="id_prestation" type="any" required="yes">
    	<cfargument name="id_enfant" type="any" required="yes">
        <cfargument name="numjour" type="any" required="yes">
        <cfargument name="datejour" type="any" required="yes">
        <cfargument name="numenfant" type="any" required="yes">
        <cfargument name="numprestation" type="any" required="yes">
        <cfargument name="action" type="any" required="yes">
        <cfargument name="nature" type="any" required="yes">
        <cfargument name="NumClient" type="any" required="yes">
        <cfargument name="session_log_mairie" type="any" required="yes">
        <cfargument name="session_use_id" type="any" required="yes">
        <cfset msg = "">
		<!--- verif si action possible --->
        <cfquery name="lect_prestation" datasource="f8w_test">
            Select * from prestation Where id = '#arguments.id_prestation#'
        </cfquery>
        <cfquery name="lect_client" datasource="f8w_test">
        	Select * from client use index(siret_numclient) Where siret='#lect_prestation.siret#' and numclient='#arguments.numclient#'
        </cfquery>
        <cfquery name="lect_etablissement" datasource="f8w_test">
        	Select * from application_client use index(siret) where siret='#lect_prestation.siret#'
        </cfquery>
		<cfset datedeb = createdate(mid(#arguments.datejour#,7,4),mid(#arguments.datejour#,4,2),mid(#arguments.datejour#,1,2))>
    	<cfset date_no_good = false>
		<!--- modif 07/11/2014 --->
        <cfset NumDaujourDhui = DayOfWeek(now())>
        <cfquery name="verifnewdelresaweb" datasource="f8w_test">
            Select * from resawebdelai use index(siret_NumPrestation) Where siret='#lect_prestation.siret#' 
            and NumPrestation='#lect_prestation.NumPrestation#' and LejourSemaine='#NumDaujourDhui#'
        </cfquery>
		<cfif #verifnewdelresaweb.recordcount# gt 0>
            <cfset HeureDaujourDhui = LsTimeFormat(now(),"HHMMSS")>
            <cfset HeureLimite = replacenocase(#verifnewdelresaweb.JusquaH#,":","","all")>
            <cfif #HeureDaujourDhui# lte #HeureLimite#>
                <cfset NbrJourEcartMax = #verifnewdelresaweb.DelaiJusqua#>
            <cfelse>    
                <cfset NbrJourEcartMax = #verifnewdelresaweb.DelaiApres#>
            </cfif>
            <cfif DateDiff("d",now(),#datedeb#) + 1 lt #NbrJourEcartMax#>
                <cfset date_no_good = true>
            </cfif>
	    <cfelse>
	        <cfset dateminiresa = DateMiniResaNew(#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebj#,#lect_prestation.DélaiRésaWebH#,now())>
        	<cfset date_no_good = false>
			<cfif isdate(#dateminiresa#)>
                <!--- dateminiresa = 1er jour sur lequel ont peut intervenir, l'heure étant l'heure limite pour aujourdh'ui --->
                <cfset ndate =  DateCompare(#datedeb#,#DateMiniResa#,"d")><!--- ont compare le jour demandé avec le jour limite --->
                <cfif #ndate# eq -1 ><!--- Si jour demandé est antérieure à DateMiniResa --->
                    <!--- pas le droit --->
                    <cfset date_no_good = true>
                <cfelseif #ndate# eq 0><!--- si jour demandé est égal à DateMiniResa --->
                    <!--- ont verifie l'heure --->
                    <cfset hdate =  DateCompare(now(),#DateMiniResa#)>
                    <cfif #hdate# eq 1><!--- si heure maintenant est postérieure à DateMiniResa --->
                        <cfset date_no_good = true>
                    <cfelse>
                        <cfset date_no_good = false>
                    </cfif>
                <cfelseif #ndate# eq 1><!--- si jour demandé est postérieure à DateMiniResa --->
                    <!--- ont peut --->
                    <cfset date_no_good = false>
                </cfif>
            <cfelseif #dateminiresa# neq "OK">
                <cfset date_no_good = true>
            </cfif>
		</cfif>
		<cfif #date_no_good# is false><!--- jour spécial sur jour --->
            <cfset h = lstimeformat(now(),"HH")>
            <cfset dateapartirstp = lsdateformat(now(),"YYYYMMdd") & #h#>
            <cfquery name="joursspecial" datasource="f8w_test">
                Select id from ajustresaweb_date use index(siret) Where siret='#lect_prestation.siret#' 
                and NumPrestation='#lect_prestation.NumPrestation#' and DateApartirSTP <= #dateapartirstp# 
                and DatePour = '#arguments.datejour#' and JourOuPeriode = 0
            </cfquery>
            <cfif #joursspecial.recordcount#>
                <cfset date_no_good = true>
            </cfif>
        </cfif>
		<cfif #date_no_good# is false><!--- reserve a (tap) --->
            <cfset reserve_a = prestation_reserve_a1(#lect_prestation.siret#,#lect_prestation.NumPrestation#,#lect_client.NumClient#,#arguments.NumEnfant#,DayOfYear(#datedeb#),"clic",#arguments.datejour#)>
            <cfif #reserve_a# neq "OK">
                <cfset msg = #reserve_a#>
                <cfset date_no_good = true>
            </cfif>
        </cfif>
		<cfif #date_no_good# and #arguments.session_log_mairie# is false>
            <cfquery name="lect_enfant" datasource="f8w_test">
                select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#arguments.id_enfant#'
            </cfquery>        
                <!--- log --->
            <cfif #url.action# eq "desinscr">
	            <cfset log_action_web = "Désinscription ponctuelle IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
            <cfelse>
                <cfset log_action_web = "Inscription ponctuelle IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
            </cfif>
            <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
            <cfset log_action_web = #log_action_web# & "<br>Application le " & #arguments.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
            <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
            <cfset dtapplication = #arguments.datejour#>
            <cfquery name="log" datasource="f8w_test">
                Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_prestation.siret#',
                '#arguments.session_use_id#',<cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#',
                '#dtapplication#','#arguments.id_prestation#')
            </cfquery>
            <cfset msg = "Vous ne pouvez plus intervenir sur la journée du " & #arguments.datejour# & " !">
        <cfelse><!--- ont a le droit de desinscrire ou d'inscrire --->
            <!--- verif si quota --->
            <cfquery name="enfantclasse" datasource="f8w_test">
                Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#lect_prestation.siret#' 
                and NumEnfant = '#arguments.NumEnfant#'
            </cfquery>
            <cfset NumLieuPrestation = 0>
            <cfset NumLieuPrestationQuota = 0>
            <cfif #enfantclasse.recordcount# gt 0>         
                <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                    Select NumLieu,Quota from lieuprestationclasse use index(siret_NumClasse_NumPrestation) Where siret='#lect_prestation.siret#' 
                    and NumClasse = '#enfantclasse.NumClasse#' and NumPrestation = '#lect_prestation.NumPrestation#' limit 0,1
                </cfquery>
                <cfif #lieuprestationclasse.recordcount#>
                    <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                    <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                </cfif>
            </cfif>        
			<cfif #arguments.action# eq "inscr" and #arguments.nature# eq "0" or #arguments.action# eq "reinscr" and #arguments.nature# eq "2"><!--- sauf si limitation d'inscription/prestation/semaine --->		
                <cfset msg = limitation(#arguments.id_enfant#,#arguments.id_prestation#,#arguments.datejour#)>
                <!--- ou quota de place maxi sur prestation --->
                <cfif #NumLieuPrestationQuota# gt 0 and len(#msg#) eq 0><!--- Quota --->
                    <cfset Msgquota = JourPrestationLieuQuota(#lect_prestation.siret#,#arguments.datejour#,#arguments.NumPrestation#,#NumLieuPrestation#)>
                    <cfif #Msgquota# gte #NumLieuPrestationQuota#>
                        <cfquery name="lect_enfant" datasource="f8w_test">
                            select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#arguments.id_enfant#'
                        </cfquery>                        	
                        <cfset msg = "Désolé, vous ne pouvez pas inscrire " & #lect_enfant.lenfant# & " le " & #arguments.datejour# & " à cette prestation.">
                        <cfset msg = #msg# & " Nombre de place maxi : " & #NumLieuPrestationQuota# & " , nombre d'inscrit : " & #Msgquota# & " !">
                        <cfif #session.log_mairie# is true>
                            <cfset log_action_web = "Inscription ponctuelle par la mairie IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                        <cfelse>
                            <cfset log_action_web = "Inscription ponctuelle IMPOSSIBLE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
                        </cfif>
                        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                        <cfset log_action_web = #log_action_web# & "<br>Application le " & #arguments.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
                        <cfset log_action_web = #log_action_web# & "<br>Quota : " & #NumLieuPrestationQuota# & ", nombre d'inscrit : " & #Msgquota# & ".">
                        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfset dtapplication = #arguments.datejour#>
                        <cfquery name="log" datasource="f8w_test">
                            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values 
                            ('#lect_prestation.siret#','#arguments.session_use_id#',
                            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#',
                            '#arguments.id_prestation#')
                        </cfquery>
                    </cfif>
                </cfif>
            </cfif>
            <cfif len(#msg#) eq 0>
				<!--- creation ou suppression inscription --->
                <cfif #lect_prestation.ticket# eq 1>
                    <cfquery name="prestation_prepaye" datasource="f8w_test">
                        Select * from prestation_prepaye use index(siret_NumPrestation_NumClient) 
                        Where siret='#lect_prestation.siret#' and NumPrestation='#lect_prestation.NumPrestation#' 
                        and NumClient='#lect_client.NumClient#' or siret='#lect_prestation.siret#' and NumPrestation='0' 
                        and NumClient='#lect_client.NumClient#'
                    </cfquery>
                    <cfif #prestation_prepaye.qte# lte 0 and #arguments.action# neq "desinscr" or #prestation_prepaye.recordcount# eq 0 and #arguments.action# neq "desinscr">
                        <cfset msg = "Désolé, vous avez consommé la totalité des réservations payées d'avance, vous ne pouvez plus ajouter d'inscription !!">
                    </cfif>	            
                </cfif>
				<cfif len(#msg#) eq 0>
                    <cfquery name="lect_inscriptionplanning" datasource="f8w_test">
                        Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                        Where siret = '#lect_prestation.siret#' and NumPrestation = '#lect_prestation.NumPrestation#' 
                        and NumEnfant = '#arguments.NumEnfant#' and DateInscription = '#arguments.datejour#'
                    </cfquery>
                    <cfquery name="lect_enfant" datasource="f8w_test">
                        select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#arguments.id_enfant#'
                    </cfquery>
					<cfif #arguments.nature# eq 2><!--- ont supprime --->
                        <cfif #lect_inscriptionplanning.recordcount#>
                            <cfquery name="del" datasource="f8w_test">
                                Delete from inscriptionplanning where id = '#lect_inscriptionplanning.id#'
                            </cfquery>
                            <cfif #lect_prestation.ticket# eq 1>
                                <cfset NewQte = #prestation_prepaye.Qte# + 1>
                                <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                    Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                </cfquery>            
                                <cfquery name="addhisto" datasource="f8w_test">
                                    Insert into prestation_prepaye_histo (siret,NumPrestation,NumClient,Qte_Precedante,
                                    Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                    ('#lect_prestation.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                    '#prestation_prepaye.Qte#','1','#NewQte#','1')
                                </cfquery>                
                            </cfif>				
                            <cfif #lect_etablissement.FullWeb# neq 1>
                                <cfset req_sql = "delete from inscriptionplanning where Numinscription=" & #lect_inscriptionplanning.NumInscription#>
                                <cfset req_sql = #req_sql# & " and numprestation=" & #lect_inscriptionplanning.numprestation# & " and numenfant=">
                                <cfset req_sql = #req_sql# & #lect_inscriptionplanning.numenfant# >
                                <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                                <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                                <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                                <cfquery name="SYNCHRO" datasource="services">
                                    Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) 
                                    Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                                    '#lect_prestation.siret#','#datesoumission#','F8')
                                </cfquery>
                            </cfif>
                            <cfset msg = "Modification enregistrée !">
                        </cfif>        
                    <cfelse>
						<cfif #lect_inscriptionplanning.recordcount#>
                            <cfif #lect_inscriptionplanning.Nature# neq #url.nature#>
                                <cfquery name="maj" datasource="f8w_test">
                                    Update inscriptionplanning set Nature='#arguments.nature#', NumLieu='#NumLieuPrestation#' 
                                    Where id = '#lect_inscriptionplanning.id#'
                                </cfquery>
                                <cfif #lect_prestation.ticket# eq 1>
                                    <cfif #arguments.nature# eq 1><!--- desinscr --->
                                        <cfset NewQte = #prestation_prepaye.Qte# + 1>
                                        <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                            Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                        </cfquery>            
                                        <cfquery name="addhisto" datasource="f8w_test">
                                            Insert into prestation_prepaye_histo
                                            (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                            ('#lect_prestation.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                            '#prestation_prepaye.Qte#','1','#NewQte#','1')
                                        </cfquery>                
                                    <cfelseif #arguments.nature# eq 2 or #arguments.nature# eq 0><!--- inscr ou reinscr --->
                                        <cfset NewQte = #prestation_prepaye.Qte# - 1>
                                        <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                            Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                        </cfquery>
                                        <cfquery name="addhisto" datasource="f8w_test">
                                            Insert into prestation_prepaye_histo 
                                            (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                            ('#lect_prestation.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                            '#prestation_prepaye.Qte#','-1','#NewQte#','1')
                                        </cfquery>                    
                                    </cfif>
                                </cfif>				
                                <cfif #lect_etablissement.FullWeb# neq 1>
                                    <cfset req_sql = "Update inscriptionplanning set Nature=" & #arguments.nature# & " where Numinscription=" & #lect_inscriptionplanning.NumInscription#>
                                    <cfset req_sql = #req_sql# & " and numprestation=" & #lect_inscriptionplanning.numprestation# & " and numenfant=">
                                    <cfset req_sql = #req_sql# & #lect_inscriptionplanning.numenfant# >
                                    <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                                    <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                                    <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                                    <cfquery name="SYNCHRO" datasource="services">
                                        Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                                        '#lect_prestation.siret#','#datesoumission#','F8')
                                    </cfquery>            
                                </cfif>
                            </cfif>
                            <cfset msg = "Modification enregistrée !">
                        <cfelse>
                            <cfset NumInscription = NumSuivant6(#lect_prestation.siret#,"inscriptionplanning","NumInscription")>
                            <cfset datedebstp = lsdateformat(#datedeb#,"YYYYMMdd")>
                            <cfquery name="add" datasource="f8w_test">
                                Insert into inscriptionplanning (siret,NumInscription,numprestation,numenfant,dateinscription,Nature,
                                DateInscriptionSTP,NumLieu) Values ('#lect_prestation.siret#','#NumInscription#','#lect_prestation.NumPrestation#','#arguments.NumEnfant#','#arguments.datejour#',
                                '#arguments.nature#','#datedebstp#','#NumLieuPrestation#')
                            </cfquery>
                            <cfif #lect_prestation.ticket# eq 1>
                                <cfif #arguments.nature# eq 1><!--- desinscr --->
                                    <cfset NewQte = #prestation_prepaye.Qte# + 1>
                                    <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                        Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                    </cfquery>            
                                    <cfquery name="addhisto" datasource="f8w_test">
                                        Insert into prestation_prepaye_histo
                                         (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                        ('#lect_etablissement.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                        '#prestation_prepaye.Qte#','1','#NewQte#','1')
                                    </cfquery>                
                                <cfelseif #arguments.nature# eq 2 or #arguments.nature# eq 0><!--- inscr ou reinscr --->
                                    <cfset NewQte = #prestation_prepaye.Qte# - 1>
                                    <cfquery name="maj_prestation_prepaye" datasource="f8w_test">
                                        Update prestation_prepaye set Qte = '#NewQte#' Where id = '#prestation_prepaye.id#'
                                    </cfquery>
                                    <cfquery name="addhisto" datasource="f8w_test">
                                        Insert into prestation_prepaye_histo 
                                        (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,Qte_Nouvelle,FaitPar) values 
                                        ('#lect_etablissement.siret#','#prestation_prepaye.NumPrestation#','#lect_client.NumClient#',
                                        '#prestation_prepaye.Qte#','-1','#NewQte#','1')
                                    </cfquery>                    
                                </cfif>
                            </cfif>				                
                            <cfif #lect_etablissement.FullWeb# neq 1>
                                <cfset req_sql = "Insert into inscriptionplanning (NumInscription,numprestation,numenfant,dateinscription,Nature">
                                <cfset req_sql = #req_sql# & ") Values (" & #NumInscription# & "," & #arguments.NumPrestation# & "," & #arguments.NumEnfant#>
                                <cfset req_sql = #req_sql# & ",'" & #arguments.datejour# & "'," & #arguments.nature# & ")">
                                <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                                <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                                <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                                <cfquery name="SYNCHRO" datasource="services">
                                    Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) 
                                    Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                                    '#lect_etablissement.siret#','#datesoumission#','F8')
                                </cfquery>
                            </cfif>
                            <cfset msg = "Modification enregistrée !">
						</cfif>
                        	<!--- log --->
                        <cfif #arguments.action# eq "desinscr">
                            <cfif #arguments.session_log_mairie# is true>
                                <cfset log_action_web = "Désinscription ponctuelle par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
                            <cfelse>
                                <cfset log_action_web = "Désinscription ponctuelle par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            </cfif>
                        <cfelse>
                            <cfif #arguments.session_log_mairie# is true>
                                <cfset log_action_web = "Inscription ponctuelle par  la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>            
                            <cfelse>
                                <cfset log_action_web = "Inscription ponctuelle par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            </cfif>
                        </cfif>
                        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                        <cfset log_action_web = #log_action_web# & "<br>Application le " & #arguments.datejour# & " (DélaiRésaWebJ = " & #lect_prestation.DélaiRésaWebJ# & " )">
                        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfset dtapplication = #arguments.datejour#>
                        <cfquery name="log" datasource="f8w_test">
                            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_etablissement.siret#','#arguments.session_use_id#',
                            <cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#','#dtapplication#','#arguments.id_prestation#')
                        </cfquery>
				
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn msg>
    </cffunction>
    
<!---	'***************************************************************
        '* iv est un entier codifiant les jours réservés ou non
        '* d'une semaine sous la forme
        '* Dimanche*1 + lundi*2 + mardi*4 + mercredi*8 +
        '* jeudi*16 + vendredi*32 + samedi*64,
        '* chaque jour prenant la valeur 1 (exclus) ou 0(permis)
        '* La function retourne 1 ou zéro pour le jour noj
        '* si une résa est faite.
        '***************************************************************    --->
    <cffunction name="Resajour" access="public" returntype="any">
		<cfargument name="iv" type="numeric" required="yes">
		<cfargument name="noj" type="numeric" required="yes">
        <cfset r = #arguments.iv#>
        <cfloop from="6" to="0" step="-1" index="k">
        	<cfset q = #r# \ (2 ^ k)>
            <cfIf k + 1 eq #arguments.noj#>
            	<cfbreak>
            </cfIf>
            <cfset r = #r# Mod (2 ^ #k#)>
        </cfloop>
		<cfset Retour = #q# >
		<cfreturn Retour>
	</cffunction>
<!--- '***************************************************************     --->
    <cffunction name="DateMiniResaNew" access="public" returntype="any">
    	<cfargument name="JoursExclus" type="numeric" required="yes">
        <cfargument name="DelaiResaWebj" type="numeric" required="yes"><!--- mini en jour pour agir --->
    	<cfargument name="DelaiResaWebH" type="any" required="yes"><!--- heure butoir pour j + X --->
        <cfargument name="DateAtester" type="date" required="yes"><!--- ont veut agire sur cette datetime --->
        <cfset Retour = "">
        <cfset Moment_de_laction = now()>
        <!--- Ont est avant ou après l'heure butoir ? --->
			<cfset Pour_heure_butoir = Createdatetime(year(now()),month(now()),day(now()),#mid(arguments.DelaiResaWebH,1,2)#,#mid(arguments.DelaiResaWebH,4,2)#,#mid(arguments.DelaiResaWebH,7,2)#)>
            <cfif DateCompare(#Moment_de_laction#,#Pour_heure_butoir#) eq 1 and #arguments.DelaiResaWebj# lte 1><!--- ont a dépassé l'heure --->
            	<cfset arguments.DelaiResaWebj = #arguments.DelaiResaWebj# + 1><!--- ont fait + 1 sur jour --->
            </cfif>
        <!--- --->
		<!---
		Ont produit DateMiniResa1 qui est la date la plus proche possible d'aujourdui en fonction des
		parametres DelaiResaWebH ou DelaiResaWebj de la prestation --->
		<cfif #Arguments.DelaiResaWebj# neq 0>
			<!--- creation de la date suivant resawebj et resawebh --->
			<cfset DateMiniResa = DateAdd("d",#Arguments.DelaiResaWebj#,now())>
            <cfset DateMiniResa1 = createdatetime(year(#DateMiniResa#),month(#DateMiniResa#),day(#DateMiniResa#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
        <cfelse>
        	<cfset DateMiniResa = now()>
            <cfset DateMiniResa1 = createdatetime(year(#DateMiniResa#),month(#DateMiniResa#),day(#DateMiniResa#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
            <cfif DateCompare(#DateMiniResa1#,now()) eq -1><!--- l'heure maxi pour resa est dépassé --->
            	<cfset DateMiniResa1 = DateAdd("d",1,#DateMiniResa1#)>
            </cfif>
        </cfif>
        <!--- --->
        <!---
		Ont verifie que DateMiniResa1 n'est pas sur un jour exclus
		Si oui ont fait j + 1 jusqu'a ne pas etre sur un jour exclus --->
        <cfset a = 1>
        <cfloop condition="#a# eq 1">
            <cfset a = Resajour(#Arguments.JoursExclus#,#DayOfWeek(DateMiniResa1)#)>
            <cfif #a# eq 1>
                <cfset DateMiniResa1 = DateAdd("d",1,#DateMiniResa1#)>
            <cfelse>
                <cfbreak>
            </cfif>
        </cfloop>
        <!--- --->
        <!---
		Ont compare la date demandé par l'utilisateur (Arguments.DateAtester) à DateMiniResa1
        Il faut dabord traiter la date demandé :
		1 - c'est la date du jour, l'heure de la demande doit etre reel
		2 - c'est la meme date que miniresa mais dans le futur
		3 - c'est une date dans le future ont touche pas (25/02/2100 00:00:00)
		--->
		<cfif lsdateformat(now(),"dd/MM/YYYY") eq lsdateformat(#Arguments.DateAtester#,"dd/MM/YYYY")><!--- ont demande date du jour --->
        	<cfset Arguments.DateAtester = createdateTime(year(#Arguments.DateAtester#),month(#Arguments.DateAtester#),day(#Arguments.DateAtester#),hour(now()),minute(now()),second(now()))>
        <cfelseif lsdateformat(#DateMiniResa1#,"dd/MM/YYYY") eq lsdateformat(#Arguments.DateAtester#,"dd/MM/YYYY")>
			<!--- ont demande meme date que mini resa mais dans le futur --->
        	<cfset Arguments.DateAtester = createdateTime(year(#Arguments.DateAtester#),month(#Arguments.DateAtester#),day(#Arguments.DateAtester#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
        </cfif>
        <!--- --->
        <cfif DateCompare(#Arguments.DateAtester#,#DateMiniResa1#) eq -1><!--- Arguments.DateAtester est antérieure à DateMiniResa1 --->
            <!--- j'ai demandé résa pour le 20 juillet et mini est 25 juillet --->
            
			<cfset Retour = #DateMiniResa1#><!--- ont retourne la date mini --->
			<!--- <cfset Retour = "OK"> --->
		<cfelseif DateCompare(#Arguments.DateAtester#,#DateMiniResa1#) eq 1><!--- Arguments.DateAtester est postérieure à DateMiniResa1 --->
            <!--- j'ai demandé résa pour le 30 juillet et mini est 25 juillet --->
			<!--- avant de retourner date demandé, ont verifie jour exclus --->
            <cfset a = 1>
            <cfset modif_sur_jour_exclus = "non">
            <cfloop condition="#a# eq 1">
                <cfset a = Resajour(#Arguments.JoursExclus#,#DayOfWeek(Arguments.DateAtester)#)>
                <cfif #a# eq 1>
                    <cfset Arguments.DateAtester = DateAdd("d",1,#Arguments.DateAtester#)>
                    <cfset modif_sur_jour_exclus = "oui">
                <cfelse>
                    <cfbreak>
                </cfif>
            </cfloop>
            <cfif #modif_sur_jour_exclus# eq "non">
            	<cfset Retour = "OK">
            <cfelse>
            	<cfset Retour = #Arguments.DateAtester#>
            </cfif>
        <cfelse><!--- Arguments.DateAtester est égale à DateMiniResa1 --->
            <!--- <cfset Retour = #Arguments.DateAtester#><!--- ont retourne date demandé ---> --->
            <cfset Retour = "OK"> 
        </cfif>
        
        
        <cfreturn Retour>
    </cffunction>        
<!--- '***************************************************************     --->
    <cffunction name="DateMiniResa" access="public" returntype="any">
    	<cfargument name="JoursExclus" type="numeric" required="yes">
        <cfargument name="DelaiResaWebj" type="numeric" required="yes">
    	<cfargument name="DelaiResaWebH" type="numeric" required="yes">
        <cfargument name="DateAtester" type="date" required="yes">
		
		<!---
		Ont produit DateMiniResa1 qui est la date la plus proche possible d'aujourdui en fonction des
		parametres DelaiResaWebH ou DelaiResaWebj de la prestation --->
		<cfif #Arguments.DelaiResaWebj# neq 0>
			<!--- creation de la date suivant resawebj et resawebh --->
			<cfset DateMiniResa = DateAdd("d",#Arguments.DelaiResaWebj#,now())>
            <cfset DateMiniResa1 = createdatetime(year(#DateMiniResa#),month(#DateMiniResa#),day(#DateMiniResa#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
        <cfelse>
        	<cfset DateMiniResa = now()>
            <cfset DateMiniResa1 = createdatetime(year(#DateMiniResa#),month(#DateMiniResa#),day(#DateMiniResa#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
            <cfif DateCompare(#DateMiniResa1#,now()) eq -1><!--- l'heure maxi pour resa est dépassé --->
            	<cfset DateMiniResa1 = DateAdd("d",1,#DateMiniResa1#)>
            </cfif>
        </cfif>
        <!--- --->
        <!---
		Ont verifie que DateMiniResa1 n'est pas sur un jour exclus
		Si oui ont fait j + 1 jusqu'a ne pas etre sur un jour exclus --->
        <cfset a = 1>
        <cfloop condition="#a# eq 1">
            <cfset a = Resajour(#Arguments.JoursExclus#,#DayOfWeek(DateMiniResa1)#)>
            <cfif #a# eq 1>
                <cfset DateMiniResa1 = DateAdd("d",1,#DateMiniResa1#)>
            <cfelse>
                <cfbreak>
            </cfif>
        </cfloop>
        <!--- --->
        <!---
		Ont compare la date demandé par l'utilisateur (Arguments.DateAtester) à DateMiniResa1
        Il faut dabord traiter la date demandé :
		1 - c'est la date du jour, l'heure de la demande doit etre reel
		2 - c'est la meme date que miniresa mais dans le futur
		3 - c'est une date dans le future ont touche pas (25/02/2100 00:00:00)
		--->
		<cfif lsdateformat(now(),"dd/MM/YYYY") eq lsdateformat(#Arguments.DateAtester#,"dd/MM/YYYY")><!--- ont demande date du jour --->
        	<cfset Arguments.DateAtester = createdateTime(year(#Arguments.DateAtester#),month(#Arguments.DateAtester#),day(#Arguments.DateAtester#),hour(now()),minute(now()),second(now()))>
        <cfelseif lsdateformat(#DateMiniResa1#,"dd/MM/YYYY") eq lsdateformat(#Arguments.DateAtester#,"dd/MM/YYYY")>
			<!--- ont demande meme date que mini resa mais dans le futur --->
        	<cfset Arguments.DateAtester = createdateTime(year(#Arguments.DateAtester#),month(#Arguments.DateAtester#),day(#Arguments.DateAtester#),mid(#Arguments.DelaiResaWebH#,1,2),mid(#Arguments.DelaiResaWebH#,4,2),mid(#Arguments.DelaiResaWebH#,7,2))>
        </cfif>
        <!--- --->
        <cfif DateCompare(#Arguments.DateAtester#,#DateMiniResa1#) eq -1><!--- Arguments.DateAtester est antérieure à DateMiniResa1 --->
            <!--- j'ai demandé résa pour le 20 juillet et mini est 25 juillet --->
            
			<cfset Retour = #DateMiniResa1#><!--- ont retourne la date mini --->
			<!--- <cfset Retour = "OK"> --->
		<cfelseif DateCompare(#Arguments.DateAtester#,#DateMiniResa1#) eq 1><!--- Arguments.DateAtester est postérieure à DateMiniResa1 --->
            <!--- j'ai demandé résa pour le 30 juillet et mini est 25 juillet --->
			<!--- avant de retourner date demandé, ont verifie jour exclus --->
            <cfset a = 1>
            <cfset modif_sur_jour_exclus = "non">
            <cfloop condition="#a# eq 1">
                <cfset a = Resajour(#Arguments.JoursExclus#,#DayOfWeek(Arguments.DateAtester)#)>
                <cfif #a# eq 1>
                    <cfset Arguments.DateAtester = DateAdd("d",1,#Arguments.DateAtester#)>
                    <cfset modif_sur_jour_exclus = "oui">
                <cfelse>
                    <cfbreak>
                </cfif>
            </cfloop>
            <cfif #modif_sur_jour_exclus# eq "non">
            	<cfset Retour = "OK">
            <cfelse>
            	<cfset Retour = #Arguments.DateAtester#>
            </cfif>
        <cfelse><!--- Arguments.DateAtester est égale à DateMiniResa1 --->
            <!--- <cfset Retour = #Arguments.DateAtester#><!--- ont retourne date demandé ---> --->
            <cfset Retour = "OK"> 
        </cfif>
        
        <!--- --->
        <cfreturn Retour>
    </cffunction>
    <cffunction name="resaponctuelle" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
    	<cfargument name="JoursExclus" type="string" required="no" default="0">
        <cfargument name="DélaiRésaWebJ" type="string" required="no" default="">
        <cfargument name="DélaiRésaWebH" type="string" required="no" default="">
        <cfargument name="idprestation" type="string" required="no" default="">
        <cfargument name="idenfant" type="string" required="no" default="">
    	<cfset aa = querynew("bg,cont")>
    	<cfset NumJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd")>
        <cfset NumJourI = DayOfWeek(#Arguments.Date_calend_dep#)>
        <cfset DateJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd/MM/YYYY")>
        <cfset DateJourStp = LsDateFormat(#Arguments.Date_calend_dep#,"YYYYMMdd")>
		<cfset CasTraiTe = false><!--- pour eviter case noir cas non traité --->
		<!--- Blanc : Prestation ouverte, pas d'inscription recurente ni ponctuelle, si clic : inscription ponctuelle --->
		<!--- FUTUR : --->
		<cfset FondBlanc = "##FFFFFF">
        <cfset ContenuUrlBlanc = '<a title=""  href="/inscriponctu/?id_mnu=1&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&action=inscr&nature=urlnature">#NumJour#</a>'>
        <!--- PASSE : --->
		<cfset msg = "Votre enfant n'était pas inscrit à la prestation le " & #DateJour# & " (sous réserve de constat de présence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlBlancPASS = '<a title="#msg#"  href="/inscriponctu/?id_mnu=1&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&msg1=#msg1#">#NumJour#</a>'>        
		<!--- --->
        <!--- Vert : Prestation ouverte, inscrit à la prestation, si clic : ont désinscrit --->
        <cfset FondVert = "##00FF00">
        <cfset ContenuUrlVert = '<a title="" href="/inscriponctu/?id_mnu=1&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&action=desinscr&nature=urlnature">#NumJour#</a>'>
        <!--- --->
        <!--- Rouge : Prestation ouverte, désinscrit à la prestation, si clic : ont réinscrit --->
        <cfset FondRouge = "##FF0000">
        <cfset ContenuUrlRouge = '<a title="" href="/inscriponctu/?id_mnu=1&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&action=reinscr&nature=urlnature"><font color="##FFFFFF">#NumJour#</font></a>'>
        <!--- --->
        <!--- Gris : Prestation fermé --->
        <cfset FondGris = "##666666">
        <cfset ContenuUrlGris = '<font color="##FFFFFF">#NumJour#</font>'>
		<!--- --->        
        <!--- Vert clair : Etait inscrit --->
        <cfset FondVertClair = "##B4F8A0">
        <cfset msg = "Votre enfant était inscrit à la prestation le " & #DateJour# & " (sous réserve de constat d'absence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlVertClair = '<a title="#msg#"  href="/inscriponctu/?id_mnu=1&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&msg1=#msg1#">#NumJour#</a>'>
		<!--- --->
        <!--- 1 : Jour exclus pour la prestation ? --->
        <cfquery name="jourexclus" datasource="f8w_test">
        	Select id from jourseclus use index(siret_Numprestation) 
            Where siret = '#Arguments.siret#' and (NumPrestation = '#Arguments.numprestation#' or numprestation=0) and DateJour = '#DateJour#' 
            or siret='' and NumPrestation = 0 and DateJour = '#DateJour#'
        </cfquery>
        <cfquery name="jourNONexclus" datasource="f8w_test">
        	Select id from joursNONeclus use index(siret_Numprestation) 
            Where siret = '#Arguments.siret#' and NumPrestation = '#Arguments.numprestation#' and DateJour = '#DateJour#' or 
            siret = '#Arguments.siret#' and NumPrestation = '0' and DateJour = '#DateJour#'
        </cfquery>
        <cfset fermé = false>
        <cfif #jourexclus.recordcount# gt 0><cfset fermé = true></cfif>
        <cfif Resajour(#Arguments.JoursExclus#,#NumJourI#) eq 1><cfset fermé = true></cfif>
        <cfif #fermé# is true>
        	<cfif #jourNONexclus.recordcount# gt 0><cfset fermé = false></cfif>
        </cfif>
        <cfif #fermé# is true><!--- jour exclus --->
			<cfset newRow = QueryAddRow(aa)>
        	<cfset temp = QuerySetCell(aa, "bg",#FondGris#)>
			<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlGris#)>
            <cfset CasTraiTe = true>
        <cfelse>
        	<!--- 2 : Inscription Hebdo ??? --->
            <cfquery name="lect_inscription" datasource="f8w_test" maxrows="1">
            	Select InscriptionImpaire,DateDebSTP from inscription use index(siret_Numenfant) Where siret = '#Arguments.siret#' 
                and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#' 
                and DateDebSTP <= '#DateJourStp#' order by DateDebSTP DESC,id DESC limit 0,1
            </cfquery>
            <!--- pour traiter init --->
            <cfquery name="lect_inscription_une_avant" datasource="f8w_test" maxrows="1">
            	Select id from inscription use index(siret_Numenfant) Where siret = '#Arguments.siret#' and Numenfant = '#Arguments.numenfant#' 
                and Numprestation = '#Arguments.numprestation#' and DateDebSTP < '#lect_inscription.DateDebSTP#' 
                and InscriptionImpaire > 0 order by DateDebSTP DESC,id DESC limit 0,1
            </cfquery>
            <!--- 2.1 : Inscrit ponctuel --->
            <cfquery name="lect_inscriptionplanning" datasource="f8w_test">
            	Select nature from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) Where siret = '#Arguments.siret#' 
                and Numprestation = '#Arguments.numprestation#' and Numenfant = '#Arguments.numenfant#' and DateInscription = '#DateJour#'
            </cfquery>
            <cfif #lect_inscription.recordcount#>
            	<cfif #lect_inscription.InscriptionImpaire# gt 0>
                	<cfif Resajour(#lect_inscription.InscriptionImpaire#,#NumJourI#) eq 1><!--- inscrit hebdo --->
						<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
							<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                                <cfset CasTraiTe = true>                                               						
                        	<cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                                               
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                            	<!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                <!--- donc inscrit --->
                            	<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>
                            </cfif>
                        <cfelse>
                        	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                                              
							<cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)> 
                                <cfset CasTraiTe = true>                                                      
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                            	<!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                <!--- donc inscrit --->
                            	<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>  
                                <cfset CasTraiTe = true>                          
                            </cfif>
						</cfif>
                    <cfelse><!--- pas inscrit hebdo --->
                    	<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
							<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                            	<cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            	<cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                	<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfelse>
									<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                                </cfif>
                                <cfset CasTraiTe = true>                       
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                                <cfset CasTraiTe = true>                                               						
                            </cfif>
                        <cfelse>
                        	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                                                                   
							<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
								<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                          
                            </cfif>
						</cfif>
                    </cfif>
                <cfelse><!--- desinscrit --->
					<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
						<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
							<cfif #lect_inscription_une_avant.recordcount# eq 1><!--- pour traiter init --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                      
	                       <cfelse><!--- pour traiter init --->
                                <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                                          
                            </cfif>
						<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
							<cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                            <cfset CasTraiTe = true>                                              						
                        <cfelse>
							<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                            <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                            <cfset msg1 = UrlEncodedFormat(#msg#)>
                            <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                            <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                            <cfset CasTraiTe = true>                                                  
                        </cfif>
					<cfelse>
                    	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
							<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                            <cfset CasTraiTe = true>                 
						<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
							<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                            <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                            <cfset CasTraiTe = true>                                                                      
                        <cfelseif #lect_inscriptionplanning.nature# eq 1><!--- ILLOGIQUE !!! --->
                        
                        
                        </cfif>
                    </cfif>
                </cfif>
            <cfelse><!--- pas inscription hebdo --->
				<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
                    <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
						<cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
						<cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                        </cfif>
                        <cfset CasTraiTe = true>                    
					<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
						<cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                        <cfset CasTraiTe = true>                                               						
                    </cfif>
                <cfelse>
                	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
						<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                        <cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
                        <cfset CasTraiTe = true>                                                                 
					<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
						<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                        <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                        <cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)> 
                        <cfset CasTraiTe = true>                                       
                    </cfif>
                </cfif>
            </cfif>
        </cfif>
        <cfif #CasTraiTe# is false>
			<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
            <cfset newRow = QueryAddRow(aa)> 
            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
            <cfset CasTraiTe = true>                                                                         
        </cfif>
        <cfreturn aa>
    </cffunction>
    <cffunction name="resaponctuelle1" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
    	<cfargument name="JoursExclus" type="string" required="no" default="0">
        <cfargument name="DélaiRésaWebJ" type="string" required="no" default="">
        <cfargument name="DélaiRésaWebH" type="string" required="no" default="">
        <cfargument name="idprestation" type="string" required="no" default="">
        <cfargument name="idenfant" type="string" required="no" default="">
    	<cfset aa = querynew("bg,cont")>
    	<cfset NumJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd")>
        <cfset NumJourI = DayOfWeek(#Arguments.Date_calend_dep#)>
        <cfset DateJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd/MM/YYYY")>
        <cfset DateJourStp = LsDateFormat(#Arguments.Date_calend_dep#,"YYYYMMdd")>
		<cfset CasTraiTe = false><!--- pour eviter case noir cas non traité --->
        
        <cfset lenow = createdatetime(year(now()),month(now()),day(now()),0,0,0)><!--- pour soucis jour courant dans le passé --->
        
		<!--- Blanc : Prestation ouverte, pas d'inscription recurente ni ponctuelle, si clic : inscription ponctuelle --->
		<!--- FUTUR : --->
		<cfset FondBlanc = "##FFFFFF">
        <cfset ContenuUrlBlanc = '<a title="" target="_self"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&action=inscr&nature=urlnature">#NumJour#</a>'>
        <!--- PASSE : --->
		<cfset msg = "Votre enfant n'était pas inscrit à la prestation le " & #DateJour# & " (sous réserve de constat de présence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlBlancPASS = '<a title="#msg#" target="_self"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&msg1=#msg1#">#NumJour#</a>'>        
		<!--- --->
        <!--- Vert : Prestation ouverte, inscrit à la prestation, si clic : ont désinscrit --->
        <cfset FondVert = "##00FF00">
        <cfset ContenuUrlVert = '<a title="" target="_self"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&action=desinscr&nature=urlnature">#NumJour#</a>'>
        <!--- --->
        <!--- Rouge : Prestation ouverte, désinscrit à la prestation, si clic : ont réinscrit --->
        <cfset FondRouge = "##FF0000">
        <cfset ContenuUrlRouge = '<a title="" target="_self"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&action=reinscr&nature=urlnature"><font color="##FFFFFF">#NumJour#</font></a>'>
        <!--- --->
        <!--- Gris : Prestation fermé --->
        <cfset FondGris = "##666666">
        <cfset ContenuUrlGris = '<font color="##FFFFFF">#NumJour#</font>'>
		<!--- --->        
        <!--- Vert clair : Etait inscrit --->
        <cfset FondVertClair = "##B4F8A0">
        <cfset msg = "Votre enfant était inscrit à la prestation le " & #DateJour# & " (sous réserve de constat d'absence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlVertClair = '<a title="#msg#" target="_self"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&msg1=#msg1#">#NumJour#</a>'>
		<!--- --->
        <!--- Jaune clair : inscrit & absent --->
        <cfset FondAbsent = "##E9E29B">
        <cfset msg = "Votre enfant était inscrit à la prestation le " & #DateJour# & " et a été noté comme absent">    
        <cfset ContenuUrlAbsent = '<a title="#msg#">#NumJour#</a>'>
        <!--- --->
        <!--- Bleu clair : pas inscrit & présent --->
        <cfset FondPrésent = "##2D8DC3">
        <cfset msg = "Votre enfant n'était pas inscrit à la prestation le " & #DateJour# & " et a été noté comme présent">    
        <cfset ContenuUrlPrésent = '<a title="#msg#">#NumJour#</a>'>
        <!--- --->            
        <!--- 1 : Jour exclus pour la prestation ? --->
        <cfquery name="etabpourzone" datasource="f8w_test">
        	Select Zone from application_client use index(siret) Where siret = '#Arguments.siret#'
        </cfquery>
        <cfquery name="ptv" datasource="f8w_test"><!--- pour calendier type vacance si besoint --->
        	Select * from prestation 
            <!---use index(siret_numprestation) 
            Where siret = '#Arguments.siret#' and NumPrestation = '#Arguments.numprestation#'--->
        	Where id='#Arguments.idprestation#'
        </cfquery>
        <cfquery name="jourexclus" datasource="f8w_test">
        	Select id,NumClasse from jourseclus#ptv.typevacance# use index(siret_NumPrestation_DateJour) 
            Where siret = '#ptv.siret#' and (NumPrestation = '#Arguments.numprestation#' or numprestation=0) and DateJour = '#DateJour#' 
            or siret='' and NumPrestation = 0 and DateJour = '#DateJour#' and zone = '#etabpourzone.Zone#'
        </cfquery>
        <cfquery name="jourNONexclus" datasource="f8w_test">
        	Select id from joursNONeclus#ptv.typevacance# use index(siret_Numprestation) Where siret = '#ptv.siret#' 
            and NumPrestation = '#Arguments.numprestation#' and DateJour = '#DateJour#'
        </cfquery>        
        <cfset fermé = false>
        <cfif #jourexclus.recordcount# gt 0>
			<cfloop query="jourexclus">
            	<cfif #jourexclus.NumClasse# eq 0 and #ptv.PasDeVacance# eq 0>
        			<cfset fermé = true>
                    <cfbreak>        
                <cfelse>
            		<cfquery name="enfantclasse" datasource="f8w_test">
            			Select id from enfantclasse use index(siret_NumEnfant_NumClasse) Where siret = '#Arguments.siret#' 
                        and NumEnfant='#Arguments.NumEnfant#' and NumClasse='#jourexclus.NumClasse#'
            		</cfquery>
                    <cfif #enfantclasse.recordcount#>
                    	<cfquery name="classe" datasource="f8w_test">
                        	Select id from classe use index(Siret_NumClasse) Where siret = '#Arguments.siret#' 
                            and NumClasse='#jourexclus.NumClasse#' and del=0
                        </cfquery>
                        <cfif #classe.recordcount#>
                        	<cfset fermé = true>
                    		<cfbreak>
                        </cfif>
                    </cfif>
            	</cfif>
            </cfloop>		
		</cfif>
        <cfif Resajour(#Arguments.JoursExclus#,#NumJourI#) eq 1><cfset fermé = true></cfif>
        <cfif #fermé# is false><!--- reserve_a ??? --->
        	<cfquery name="lect_enfant" datasource="f8w_test">
				Select NumClientI from enfant use index(siret_NumEnfant) 
                Where siret = '#Arguments.siret#' and NumEnfant='#Arguments.NumEnfant#'
            </cfquery>
            <!---<cfif datecompare(#Arguments.Date_calend_dep#,now()) gte 0><!--- si present ou futur --->--->
				<cfset reserve_a = prestation_reserve_a1(#Arguments.siret#,#Arguments.NumPrestation#,#lect_enfant.NumClientI#,#Arguments.NumEnfant#,#DayOfYear(Arguments.Date_calend_dep)#,"aff",#Arguments.Date_calend_dep#,#ptv.siret#)>
				<cfif #reserve_a# neq "OK">
                    <cfset fermé = true>
                </cfif>
			<!---</cfif>--->
        </cfif>
		<cfif #fermé# is true>
        	<cfif #jourNONexclus.recordcount# gt 0><cfset fermé = false></cfif>
        </cfif>
        <cfif #fermé# is true><!--- jour exclus --->
			<cfset newRow = QueryAddRow(aa)>
        	<cfset temp = QuerySetCell(aa, "bg",#FondGris#)>
			<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlGris#)>
            <cfset CasTraiTe = true>
        </cfif>
        <cfif #ptv.UnSeulEnfantPresentParJour# eq 1><!--- participation parent --->
            <cfquery name="ver_lect_inscriptionplanning" datasource="f8w_test">
                Select id from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) Where siret = '#Arguments.siret#' 
                and Numprestation = '#Arguments.numprestation#' and NumEnfant <> '#Arguments.numenfant#' and DateInscription = '#DateJour#'
            </cfquery>    
            <cfif #ver_lect_inscriptionplanning.recordcount#>
                <cfset newRow = QueryAddRow(aa)>
        	    <cfset temp = QuerySetCell(aa, "bg",#FondGris#)>
			    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlGris#)>
                <cfset CasTraiTe = true>    
            </cfif>
        </cfif>
        <cfif #CasTraiTe# is false>
        	<!--- présence / absence ??? --->
            <!--- modif 11/01/2023 --->
            <cfquery name="verpresabsnnn" datasource="f8w_test" maxrows="1">
                Select * from présenceabsence use index(siret_NumEnfant_NumPrestation_Date) Where siret = '#Arguments.siret#' 
                and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#' and Date='#DateJour#' 
                order by id desc limit 0,1
            </cfquery>
            <cfif #verpresabsnnn.recordcount#>
                <cfif #ptv.Modalité# neq 6>
                    <cfswitch expression="#verpresabsnnn.PrésentAbsent#">
                        <cfcase value="1">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondAbsent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlAbsent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfcase value="2">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondAbsent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlAbsent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfcase value="3">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondPrésent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlPrésent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfcase value="4">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondPrésent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlPrésent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfdefaultcase></cfdefaultcase>
                    </cfswitch>
                <cfelse>
                    <cfswitch expression="#verpresabsnnn.PrésentAbsent#">
                        <cfcase value="2">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondAbsent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlAbsent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfcase value="3">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondAbsent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlAbsent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfcase value="4">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondPrésent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlPrésent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfcase value="5">
                            <cfset newRow = QueryAddRow(aa)>
                            <cfset temp = QuerySetCell(aa, "bg",#FondPrésent#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlPrésent#)>
                            <cfset CasTraiTe = true>
                        </cfcase>
                        <cfdefaultcase></cfdefaultcase>
                    </cfswitch>    
                </cfif>
            </cfif>    
            <cfif #CasTraiTe# is false>
                <!--- 2 : Inscription Hebdo ??? --->
                <cfquery name="lect_inscription" datasource="f8w_test" maxrows="1">
                    Select id,InscriptionImpaire,DateDebSTP from inscription use index(siret_Numenfant) Where siret = '#Arguments.siret#' 
                    and Numenfant = '#Arguments.numenfant#' and Numprestation = '#Arguments.numprestation#' 
                    and DateDebSTP <= '#DateJourStp#' order by DateDebSTP DESC,id DESC limit 0,1
                </cfquery>
                <!--- maj numlieu --->
                <cfquery name="enfantclasse" datasource="f8w_test">
                    Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#Arguments.siret#' 
                    and NumEnfant = '#Arguments.numenfant#'
                </cfquery>
                <cfset NumLieuPrestation = 0>
                <cfif #enfantclasse.recordcount# gt 0>         
                    <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                        Select NumLieu from lieuprestationclasse use index(siret_NumClasse_NumPrestation) Where siret = '#Arguments.siret#' 
                        and NumClasse = '#enfantclasse.NumClasse#' and NumPrestation = '#Arguments.numprestation#' limit 0,1
                    </cfquery>
                    <cfif #lieuprestationclasse.recordcount#>
                        <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                    </cfif>
                </cfif>
                <cfif #lect_inscription.recordcount#>
                    <cfquery name="maj" datasource="f8w_test">
                        Update inscription set NumLieu='#NumLieuPrestation#' Where id='#lect_inscription.id#'
                    </cfquery>
                </cfif>
                <!--- --->
                <!--- pour traiter init --->
                <cfquery name="lect_inscription_une_avant" datasource="f8w_test" maxrows="1">
                    Select id from inscription use index(siret_Numenfant) Where siret = '#Arguments.siret#' and Numenfant = '#Arguments.numenfant#' 
                    and Numprestation = '#Arguments.numprestation#' and DateDebSTP < '#lect_inscription.DateDebSTP#' 
                    and InscriptionImpaire > 0 order by DateDebSTP DESC,id DESC limit 0,1
                </cfquery>
                <!--- 2.1 : Inscrit ponctuel --->
                <cfquery name="lect_inscriptionplanning" datasource="f8w_test">
                    Select id,nature,HeureArriv 
                    from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) Where siret = '#Arguments.siret#' 
                    and Numprestation = '#Arguments.numprestation#' and Numenfant = '#Arguments.numenfant#' and DateInscription = '#DateJour#'
                </cfquery>
                <!--- maj numlieu --->
                <cfif #lect_inscriptionplanning.recordcount#>
                    <cfquery name="maj" datasource="f8w_test">
                        Update inscriptionplanning set NumLieu='#NumLieuPrestation#' Where id='#lect_inscriptionplanning.id#'
                    </cfquery>
                </cfif>
                <!--- --->
                <cfif #lect_inscription.recordcount#>
                    <cfif #lect_inscription.InscriptionImpaire# gt 0>
                        <cfif Resajour(#lect_inscription.InscriptionImpaire#,#NumJourI#) eq 1><!--- inscrit hebdo --->
                            <cfif Datecompare(#Arguments.Date_calend_dep#,#lenow#) eq -1><!--- le passé --->
                                <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>

                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 

                                    <cfset CasTraiTe = true>                                               						
                                <cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
                                    <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                    <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                    <cfset msg1 = UrlEncodedFormat(#msg#)>
                                    <!---<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>--->
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                    <cfset CasTraiTe = true>                                               
                                <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                                    <!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                    <!--- donc inscrit --->
                                    <!---<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>--->



                                    <cfset newRow = QueryAddRow(aa)> 
                                    <!---<cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>--->
                                    <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>

                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 

                                    <cfset CasTraiTe = true>
                                </cfif>
                            <cfelse>
                                <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour# #lect_inscriptionplanning.HeureArriv#"')>
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                    <cfset CasTraiTe = true>                                              
                                <cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
                                    <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                    <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)> 
                                    <cfset CasTraiTe = true>                                                      
                                <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                                    <!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                    <!--- donc inscrit --->
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour# #lect_inscriptionplanning.HeureArriv#"')>
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>  
                                    <cfset CasTraiTe = true>                          
                                </cfif>
                            </cfif>
                        <cfelse><!--- pas inscrit hebdo --->
                            <cfif Datecompare(#Arguments.Date_calend_dep#,#lenow#) eq -1><!--- le passé --->
                                <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                    <cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                        <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                    <cfelse>
                                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                                    </cfif>
                                    <cfset CasTraiTe = true>                       
                                <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                    <cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                        <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                    <cfelse>                                
                                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                                    </cfif>
                                    <cfset CasTraiTe = true>                                               						
                                </cfif>
                            <cfelse>
                                <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                                    <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                    <cfset CasTraiTe = true>                                                                   
                                <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour# #lect_inscriptionplanning.HeureArriv#"')>
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                    <cfset CasTraiTe = true>                          
                                </cfif>
                            </cfif>
                        </cfif>
                    <cfelse><!--- desinscrit --->
                        <cfif Datecompare(#Arguments.Date_calend_dep#,#lenow#) eq -1><!--- le passé --->
                            <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                                <cfif #lect_inscription_une_avant.recordcount# eq 1><!--- pour traiter init --->
                                    <cfif #DateJourStp# lte #lect_inscription.datedebstp#>
                                        <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                        <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                        <cfset msg1 = UrlEncodedFormat(#msg#)>
                                        <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                        <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','0')>
                                        <cfset newRow = QueryAddRow(aa)> 
                                        <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                        <cfset CasTraiTe = true>                      
                                    <cfelse>
                                        <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                        <cfset newRow = QueryAddRow(aa)> 
                                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                        <cfset CasTraiTe = true>                                                                          
                                    </cfif>
                               <cfelse><!--- pour traiter init --->
                                    <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                    <cfset newRow = QueryAddRow(aa)> 
                                    <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                    <cfset CasTraiTe = true>                                          
                                </cfif>
                            <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                    <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfelse>
                                    <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                                </cfif>
                                <cfset CasTraiTe = true>                                              						
                            <cfelse>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>

                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                                                  
                            </cfif>
                        <cfelse>
                            <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                                <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                 
                            <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour# #lect_inscriptionplanning.HeureArriv#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                                                                      
                            <cfelseif #lect_inscriptionplanning.nature# eq 1><!--- ILLOGIQUE !!! --->


                            </cfif>
                        </cfif>
                    </cfif>
                <cfelse><!--- pas inscription hebdo --->
                    <cfif Datecompare(#Arguments.Date_calend_dep#,#lenow#) eq -1><!--- le passé --->
                        <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            <cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                            <cfelse>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                            </cfif>
                            <cfset CasTraiTe = true>                    
                        <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                            <cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                            <cfelse>                        
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                            </cfif>
                            <cfset CasTraiTe = true>                                               						
                        </cfif>
                    <cfelse>
                        <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
                            <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
                            <cfset CasTraiTe = true>                                                                 
                        <cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
                            <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour# #lect_inscriptionplanning.HeureArriv#"')>
                            <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)> 
                            <cfset CasTraiTe = true>                                       
                        </cfif>
                    </cfif>
                </cfif>
            </cfif><!--- modif 11/01/2023 --->        
        </cfif>
        <cfif #CasTraiTe# is false>
			<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
            <cfset newRow = QueryAddRow(aa)> 
            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
            <cfset CasTraiTe = true>                                                                         
        </cfif>
        <cfreturn aa>
    </cffunction>    
    <cffunction name="Calendrier" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
		<cfset calendrier = querynew("bg1,cont1,bg2,cont2,bg3,cont3,bg4,cont4,bg5,cont5,bg6,cont6,bg7,cont7")>
        <cfset sav_Arguments.Date_calend_dep = #Arguments.Date_calend_dep#>
        <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
        	<cfquery name="lect_prestation" datasource="f8w_test">
            	Select id,JoursExclus,DélaiRésaWebJ,DélaiRésaWebH from prestation use index(siret_numprestation)
                Where siret = '#arguments.siret#' and numprestation = '#Arguments.numprestation#'
            </cfquery>
            <cfquery name="lect_enfant" datasource="f8w_test">
            	select id from enfant use index(siret_numenfant) Where siret = '#arguments.siret#' and numenfant = '#Arguments.numenfant#'
            </cfquery>
        </cfif>
        <cfloop from="1" to="6" index="lignetableau">	
			<cfset newRow = QueryAddRow(calendrier)> 
            <cfloop from="1" to="7" index="cpt">
                <cfswitch expression="#cpt#">
                    <cfcase value="1"><!--- lundi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 2 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg1",#a.bg#)>
								<cfset temp = QuerySetCell(calendrier, "cont1",#a.cont#)>
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg1","##00FF00")>
								<cfset temp = QuerySetCell(calendrier, "cont1",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                        	</cfif>
                            <cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg1","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont1","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="2"><!--- mardi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 3 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg2",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg2","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg2","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont2","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="3"><!--- mercredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 4 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg3",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg3","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg3","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont3","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="4"><!--- jeudi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 5 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                            	<cfset temp = QuerySetCell(calendrier, "bg4",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",#a.cont#)>                            
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg4","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg4","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont4","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="5"><!--- vendredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 6 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg5",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg5","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg5","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont5","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="6"><!--- samedi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 7 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg6",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",#a.cont#)>                            
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg6","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg6","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont6","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="7"><!--- dimanche --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 1 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg7",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",#a.cont#)>                                
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg7","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg7","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont7","&nbsp;")>
                        </cfif>                
                    </cfcase>            
                </cfswitch>
            </cfloop>
		</cfloop>        
    	<cfreturn calendrier>
    </cffunction>
    <cffunction name="Calendrier1" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
        <cfargument name="siretdispose" type="string" required="no" default="">
        <cfif len(#arguments.siretdispose#) eq 0>
        	<cfset arguments.siretdispose = #arguments.siret#>
        </cfif>
		<cfset calendrier = querynew("bg1,cont1,bg2,cont2,bg3,cont3,bg4,cont4,bg5,cont5,bg6,cont6,bg7,cont7,S")>
        <cfset sav_Arguments.Date_calend_dep = #Arguments.Date_calend_dep#>
        <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
        	<cfquery name="lect_prestation" datasource="f8w_test">
            	Select id,JoursExclus,DélaiRésaWebJ,DélaiRésaWebH from prestation use index(siret_numprestation)
                Where siret = '#arguments.siretdispose#' and numprestation = '#Arguments.numprestation#'
            </cfquery>
            <cfquery name="lect_enfantfghfhfhfhgfhgfhgfh" datasource="f8w_test">
            	select id from enfant use index(siret_numenfant) Where siret = '#arguments.siret#' and numenfant = '#Arguments.numenfant#'
            </cfquery>
        </cfif>
        <!--- ont arrive avec Arguments.Date_calend_dep = 01/MM/YYYY --->
        <cfloop from="1" to="6" index="lignetableau">	
			<cfset newRow = QueryAddRow(calendrier)>
            
            	<!--- numero de semaine --->
				
				<cfset NumSemaine = Week(#Arguments.Date_calend_dep#)>
                
				<!---<cfif day(#Arguments.Date_calend_dep#) eq 1 and dayofweek(#Arguments.Date_calend_dep#) eq 1>
                    
                    <cfset NumSemaine = #NumSemaine# - 1>
                </cfif>--->
                
                    
				<cfif year(#Arguments.Date_calend_dep#) eq 2021>
                    <cfset NumSemaine = #NumSemaine# - 1>
                </cfif>
                <!--- (10/12/2020) probleme 2022 il donne semaine 1 pour 1er janvier alors que c'est semaine 52 
				les semaine des mois suivant sur 2021 sont également décalée à + 1 --->
                <cfif year(#Arguments.Date_calend_dep#) eq 2022>
                	<cfset NumSemaine = #NumSemaine# - 1>
                	<cfif #NumSemaine# eq 0>
                    	<cfset NumSemaine = 52>
                    </cfif>
                </cfif>
                
				<cfif len(#NumSemaine#) eq 1>
                    <cfset NumSemaine = "0" & #NumSemaine#>
                </cfif>
                <cfset temp = QuerySetCell(calendrier, "S",#NumSemaine#)>     
            
            <cfloop from="1" to="7" index="cpt">
                <cfswitch expression="#cpt#">
                    <cfcase value="1"><!--- lundi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 2 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg1",#a.bg#)>
								<cfset temp = QuerySetCell(calendrier, "cont1",#a.cont#)>
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg1","##00FF00")>
								<cfset temp = QuerySetCell(calendrier, "cont1",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                        	</cfif>
                            <cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg1","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont1","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="2"><!--- mardi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 3 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg2",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg2","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg2","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont2","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="3"><!--- mercredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 4 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg3",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg3","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg3","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont3","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="4"><!--- jeudi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 5 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
                            	<cfset temp = QuerySetCell(calendrier, "bg4",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",#a.cont#)>                            
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg4","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg4","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont4","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="5"><!--- vendredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 6 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg5",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg5","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg5","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont5","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="6"><!--- samedi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 7 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg6",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",#a.cont#)>                            
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg6","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg6","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont6","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="7"><!--- dimanche --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 1 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelle1(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfantfghfhfhfhgfhgfhgfh.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg7",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",#a.cont#)>                                
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg7","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg7","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont7","&nbsp;")>
                        </cfif>                
                    </cfcase>            
                </cfswitch>
            </cfloop>
        </cfloop>        
        <cfreturn calendrier>
    </cffunction>
    <cffunction name="resaponctuelleLocal" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
    	<cfargument name="JoursExclus" type="string" required="no" default="0">
        <cfargument name="DélaiRésaWebJ" type="string" required="no" default="">
        <cfargument name="DélaiRésaWebH" type="string" required="no" default="">
        <cfargument name="idprestation" type="string" required="no" default="">
        <cfargument name="idenfant" type="string" required="no" default="">
    	<cfset aa = querynew("bg,cont")>
    	<cfset NumJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd")>
        <cfset NumJourI = DayOfWeek(#Arguments.Date_calend_dep#)>
        <cfset DateJour = LsDateFormat(#Arguments.Date_calend_dep#,"dd/MM/YYYY")>
        <!--- <cfset DateJourStp = LsDateFormat(#Arguments.Date_calend_dep#,"YYYYMMdd")> --->
		<!--- <cfset DateJourStp = #DateJour#> --->
        <cfset dateacces = createdate(1899,12,30)>
		<cfset DateJourStp = datediff("d",#dateacces#,#DateJour#)>
		<cfset CasTraiTe = false><!--- pour eviter case noir cas non traité --->
		<!--- Blanc : Prestation ouverte, pas d'inscription recurente ni ponctuelle, si clic : inscription ponctuelle --->
		<!--- FUTUR : --->
		<cfset FondBlanc = "##FFFFFF">
        <cfset ContenuUrlBlanc = '<a title=""  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlanc = #ContenuUrlBlanc# & '&action=inscr&nature=urlnature">#NumJour#</a>'>
        <!--- PASSE : --->
		<cfset msg = "Votre enfant n'était pas inscrit à la prestation le " & #DateJour# & " (sous réserve de constat de présence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlBlancPASS = '<a title="#msg#"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlBlancPASS = #ContenuUrlBlancPASS# & '&msg1=#msg1#">#NumJour#</a>'>        
		<!--- --->
        <!--- Vert : Prestation ouverte, inscrit à la prestation, si clic : ont désinscrit --->
        <cfset FondVert = "##00FF00">
        <cfset ContenuUrlVert = '<a title="" href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVert = #ContenuUrlVert# & '&action=desinscr&nature=urlnature">#NumJour#</a>'>
        <!--- --->
        <!--- Rouge : Prestation ouverte, désinscrit à la prestation, si clic : ont réinscrit --->
        <cfset FondRouge = "##FF0000">
        <cfset ContenuUrlRouge = '<a title="" href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlRouge = #ContenuUrlRouge# & '&action=reinscr&nature=urlnature"><font color="##FFFFFF">#NumJour#</font></a>'>
        <!--- --->
        <!--- Gris : Prestation fermé --->
        <cfset FondGris = "##666666">
        <cfset ContenuUrlGris = '<font color="##FFFFFF">#NumJour#</font>'>
		<!--- --->        
        <!--- Vert clair : Etait inscrit --->
        <cfset FondVertClair = "##B4F8A0">
        <cfset msg = "Votre enfant était inscrit à la prestation le " & #DateJour# & " (sous réserve de constat d'absence)">
        <cfset msg1 = UrlEncodedFormat(#msg#)>
        <cfset ContenuUrlVertClair = '<a title="#msg#"  href="/inscriptions/?id_mnu=22&id_prestation=#Arguments.idprestation#&id_enfant=#Arguments.idenfant#&numjour=#NumJour#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&datejour=#DateJour#&numenfant=#Arguments.numenfant#&numprestation=#Arguments.numprestation#'>
        <cfset ContenuUrlVertClair = #ContenuUrlVertClair# & '&msg1=#msg1#">#NumJour#</a>'>
		<!--- --->
        <!--- 1 : Jour exclus pour la prestation ? --->
        <cfquery name="jourexclus" datasource="f8w_test">
        	Select id from jourseclus use index(siret_Numprestation) 
            Where siret = '#Arguments.siret#' and (NumPrestation = '#Arguments.numprestation#' or numprestation=0) and DateJour = '#DateJour#' 
            or siret='' and NumPrestation = 0 and DateJour = '#DateJour#'
        </cfquery>
        <cfquery name="jourNONexclus" datasource="f8w_test">
        	Select id from joursNONeclus use index(siret_Numprestation) Where siret = '#Arguments.siret#' 
            and NumPrestation = '#Arguments.numprestation#' and DateJour = '#DateJour#'
        </cfquery>        
        <cfset fermé = false>
        <cfif #jourexclus.recordcount# gt 0><cfset fermé = true></cfif>
        <cfif Resajour(#Arguments.JoursExclus#,#NumJourI#) eq 1><cfset fermé = true></cfif>
        <cfif #fermé# is true>
        	<cfif #jourNONexclus.recordcount# gt 0><cfset fermé = false></cfif>
        </cfif>
        <cfif #fermé# is true><!--- jour exclus --->
			<cfset newRow = QueryAddRow(aa)>
        	<cfset temp = QuerySetCell(aa, "bg",#FondGris#)>
			<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlGris#)>
            <cfset CasTraiTe = true>
        <cfelse>
        	<!--- 2 : Inscription Hebdo ??? --->
            <cfquery name="lect_inscription" datasource="f8local" maxrows="1">
            	Select InscriptionImpaire,DateDeb from inscription Where 
                Numenfant = #Arguments.numenfant# and Numprestation = #Arguments.numprestation#
                and DateDeb <= #Arguments.Date_calend_dep# order by DateDeb DESC
            </cfquery>
 			<!--- maj numlieu --->
            <cfquery name="enfantclasse" datasource="f8w_test">
                Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#Arguments.siret#' 
                and NumEnfant = '#Arguments.numenfant#'
            </cfquery>
            <cfset NumLieuPrestation = 0>
			<cfif #enfantclasse.recordcount# gt 0>         
                <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                    Select NumLieu from lieuprestationclasse use index(siret_NumClasse_NumPrestation) Where siret = '#Arguments.siret#' 
                    and NumClasse = '#enfantclasse.NumClasse#' and NumPrestation = '#Arguments.numprestation#' limit 0,1
                </cfquery>
                <cfif #lieuprestationclasse.recordcount#>
                    <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                </cfif>
            </cfif>
            <!---<cfif #lect_inscription.recordcount#>
            	<cfquery name="maj" datasource="f8w_test">
                	Update inscription set NumLieu='#NumLieuPrestation#' Where id='#lect_inscription.id#'
                </cfquery>
            </cfif>--->
            <!--- --->
            <!--- pour traiter init --->
            <cfif #lect_inscription.recordcount#>
				<cfset pourdate = datediff("d",#dateacces#,#lect_inscription.DateDeb#)>
                <cfquery name="lect_inscription_une_avant" datasource="f8local" maxrows="1">
                    Select * from inscription Where Numenfant = #Arguments.numenfant#
                    and Numprestation = #Arguments.numprestation# and DateDeb < #Arguments.Date_calend_dep#
                    and InscriptionImpaire > 0 order by DateDeb DESC
                </cfquery>
			<cfelse>
            	<cfquery name="lect_inscription_une_avant" datasource="f8local" maxrows="1">
					Select * from inscription Where Numenfant = #Arguments.numenfant#
                    and Numprestation = #Arguments.numprestation#
                    and InscriptionImpaire > 0 order by DateDeb DESC                
                </cfquery>
            </cfif>
            <!--- 2.1 : Inscrit ponctuel --->
            <!--- <cfquery name="lect_inscriptionplanning" datasource="f8local">
            	Select nature from inscriptionplanning Where 
                Numprestation = #Arguments.numprestation# and Numenfant = #Arguments.numenfant# and DateInscription = #DateJour#
            </cfquery> --->
            <cfset pourdatejour = datediff("d",#dateacces#,#DateJour#)>
            <!---<cfquery name="lect_inscriptionplanning" datasource="f8local">
            	Select nature from inscriptionplanning Where 
                Numprestation = #Arguments.numprestation# and Numenfant = #Arguments.numenfant# and DateInscription = #pourdatejour#
            </cfquery> --->
            
            <cfquery name="lect_inscriptionplanning" datasource="f8local">
            	Select nature from inscriptionplanning Where 
                Numprestation = #Arguments.numprestation# and Numenfant = #Arguments.numenfant# and DateInscription = #Arguments.Date_calend_dep#
            </cfquery>
            
			<!--- maj numlieu --->
            <!---<cfif #lect_inscriptionplanning.recordcount#>
            	<cfquery name="maj" datasource="f8w_test">
                	Update inscriptionplanning set NumLieu='#NumLieuPrestation#' Where id='#lect_inscriptionplanning.id#'
                </cfquery>
            </cfif>--->
            <!--- --->
            <cfif #lect_inscription.recordcount#>
            	<cfif #lect_inscription.InscriptionImpaire# gt 0>
                	<cfif Resajour(#lect_inscription.InscriptionImpaire#,#NumJourI#) eq 1><!--- inscrit hebdo --->
						<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
							<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                                <cfset CasTraiTe = true>                                               						
                        	<cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                                               
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                            	<!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                <!--- donc inscrit --->
                            	<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>
                            </cfif>
                        <cfelse>
                        	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                                              
							<cfelseif #lect_inscriptionplanning.nature# eq 1><!--- inscriptionplanning --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)> 
                                <cfset CasTraiTe = true>                                                      
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- ATTENTION ILLOGIQUE --->
                            	<!--- cas ou l'enfant a dabord ete inscrit ponctuellement PUIS ebdo pour la meme date --->
                                <!--- donc inscrit --->
                            	<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','1')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>  
                                <cfset CasTraiTe = true>                          
                            </cfif>
						</cfif>
                    <cfelse><!--- pas inscrit hebdo --->
                    	<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
							<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                            	<cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            	<cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                                	<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfelse>
									<cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                                </cfif>
								<cfset CasTraiTe = true>                       
                        	<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
								<cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                                <cfset CasTraiTe = true>                                               						
                            </cfif>
                        <cfelse>
                        	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
								<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                                                                   
							<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
								<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                                <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                                <cfset CasTraiTe = true>                          
                            </cfif>
						</cfif>
                    </cfif>
                <cfelse><!--- desinscrit --->
					<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
						<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
							<cfif #lect_inscription_une_avant.recordcount# eq 1><!--- pour traiter init --->
								<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>
                                <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                                <cfset msg1 = UrlEncodedFormat(#msg#)>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                                <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                                <cfset CasTraiTe = true>                      
	                       <cfelse><!--- pour traiter init --->
                                <cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                                <cfset newRow = QueryAddRow(aa)> 
                                <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                                <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                                <cfset CasTraiTe = true>                                          
                            </cfif>
						<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
							<cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)>
                            <cfset CasTraiTe = true>                                              						
                        <cfelse>
							<cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'title="','title="Votre enfant a été désinscrit de la prestation le #DateJour#"')>

                            <cfset msg = "Votre enfant a été désinscrit de la prestation le #DateJour#">
                            <cfset msg1 = UrlEncodedFormat(#msg#)>
                            <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,"&action=reinscr","&msg1=#msg1#")>
                            <cfset ContenuUrlRouge = Replace(#ContenuUrlRouge#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondRouge#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlRouge#)>
                            <cfset CasTraiTe = true>                                                  
                        </cfif>
					<cfelse>
                    	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
							<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                            <cfset CasTraiTe = true>                 
						<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
							<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                            <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                            <cfset newRow = QueryAddRow(aa)> 
                            <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)>
                            <cfset CasTraiTe = true>                                                                      
                        <cfelseif #lect_inscriptionplanning.nature# eq 1><!--- ILLOGIQUE !!! --->
                        
                        
                        </cfif>
                    </cfif>
                </cfif>
            <cfelse><!--- pas inscription hebdo --->
				<cfif Datecompare(#Arguments.Date_calend_dep#,now()) eq -1><!--- le passé --->
                    <cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
						<cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
						<cfif lsdateformat(#Arguments.Date_calend_dep#,"dd/MM/YYYY") eq lsdateformat(now(),"dd/MM/YYYY")>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlancPASS#)>
                        </cfif>
                        <cfset CasTraiTe = true>                    
					<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
						<cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondVertClair#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVertClair#)> 
                        <cfset CasTraiTe = true>                                               						
                    </cfif>
                <cfelse>
                	<cfif #lect_inscriptionplanning.recordcount# eq 0><!--- pas inscriptionplanning --->
						<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
                        <cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
                        <cfset CasTraiTe = true>                                                                 
					<cfelseif #lect_inscriptionplanning.nature# eq 0><!--- inscriptionplanning --->
						<cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'title="','title="Vous pouvez désinscrire votre enfant de la prestation du #DateJour#"')>
                        <cfset ContenuUrlVert = Replace(#ContenuUrlVert#,'urlnature','2')>
                        <cfset newRow = QueryAddRow(aa)> 
                        <cfset temp = QuerySetCell(aa, "bg",#FondVert#)>
                        <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlVert#)> 
                        <cfset CasTraiTe = true>                                       
                    </cfif>
                </cfif>
            </cfif>
        </cfif>
        <cfif #CasTraiTe# is false>
			<cfset ContenuUrlBlanc = Replace(#ContenuUrlBlanc#,'urlnature','0')>
            <cfset newRow = QueryAddRow(aa)> 
            <cfset temp = QuerySetCell(aa, "bg",#FondBlanc#)>
            <cfset temp = QuerySetCell(aa, "cont",#ContenuUrlBlanc#)>   
            <cfset CasTraiTe = true>                                                                         
        </cfif>
        <cfreturn aa>
    </cffunction>    
    <cffunction name="CalendrierLocal" access="public" returntype="any">
    	<cfargument name="Date_calend_dep" type="numeric" required="yes">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="numprestation" type="string" required="no" default="">
        <cfargument name="numenfant" type="string" required="no" default="">
		<cfset calendrier = querynew("bg1,cont1,bg2,cont2,bg3,cont3,bg4,cont4,bg5,cont5,bg6,cont6,bg7,cont7")>
        <cfset sav_Arguments.Date_calend_dep = #Arguments.Date_calend_dep#>
        <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
        	<cfquery name="lect_prestation" datasource="f8w_test">
            	Select id,JoursExclus,DélaiRésaWebJ,DélaiRésaWebH from prestation use index(siret_numprestation)
                Where siret = '#arguments.siret#' and numprestation = '#Arguments.numprestation#'
            </cfquery>
            <cfquery name="lect_enfant" datasource="f8w_test">
            	select id from enfant use index(siret_numenfant) Where siret = '#arguments.siret#' and numenfant = '#Arguments.numenfant#'
            </cfquery>
        </cfif>
        <cfloop from="1" to="6" index="lignetableau">	
			<cfset newRow = QueryAddRow(calendrier)> 
            <cfloop from="1" to="7" index="cpt">
                <cfswitch expression="#cpt#">
                    <cfcase value="1"><!--- lundi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 2 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg1",#a.bg#)>
								<cfset temp = QuerySetCell(calendrier, "cont1",#a.cont#)>
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg1","##00FF00")>
								<cfset temp = QuerySetCell(calendrier, "cont1",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                        	</cfif>
                            <cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg1","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont1","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="2"><!--- mardi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 3 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg2",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg2","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont2",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg2","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont2","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="3"><!--- mercredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 4 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                                <cfset temp = QuerySetCell(calendrier, "bg3",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg3","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont3",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg3","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont3","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="4"><!--- jeudi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 5 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
                            	<cfset temp = QuerySetCell(calendrier, "bg4",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",#a.cont#)>                            
                            <cfelse>
                            	<cfset temp = QuerySetCell(calendrier, "bg4","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont4",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg4","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont4","&nbsp;")>
                        </cfif>
                    </cfcase>
                    <cfcase value="5"><!--- vendredi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 6 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg5",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",#a.cont#)>
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg5","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont5",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg5","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont5","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="6"><!--- samedi --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 7 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                           		<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg6",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",#a.cont#)>                            
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg6","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont6",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg6","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont6","&nbsp;")>
                        </cfif>                
                    </cfcase>
                    <cfcase value="7"><!--- dimanche --->
                        <cfif dayofweek(#Arguments.Date_calend_dep#) eq 1 and month(#Arguments.Date_calend_dep#) eq month(#sav_Arguments.Date_calend_dep#)>
                            <cfif len(#Arguments.numprestation#)><!--- calendrier pour f8 --->
                            	<cfset a = resaponctuelleLocal(#Arguments.Date_calend_dep#,#arguments.siret#,#Arguments.numprestation#,#Arguments.numenfant#,#lect_prestation.JoursExclus#,#lect_prestation.DélaiRésaWebJ#,#lect_prestation.DélaiRésaWebH#,#lect_prestation.id#,#lect_enfant.id#)>
								<cfset temp = QuerySetCell(calendrier, "bg7",#a.bg#)>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",#a.cont#)>                                
                            <cfelse>
								<cfset temp = QuerySetCell(calendrier, "bg7","##00FF00")>
                            	<cfset temp = QuerySetCell(calendrier, "cont7",lsdateformat(#Arguments.Date_calend_dep#,"dd"))>
                            </cfif>
							<cfset Arguments.Date_calend_dep = dateadd("d",1,#Arguments.Date_calend_dep#)>
                        <cfelse>
                            <cfset temp = QuerySetCell(calendrier, "bg7","##CCCCCC")>
                            <cfset temp = QuerySetCell(calendrier, "cont7","&nbsp;")>
                        </cfif>                
                    </cfcase>            
                </cfswitch>
            </cfloop>
		</cfloop>        
    	<cfreturn calendrier>
    </cffunction>
    <cffunction name="NumSuivant6" access="public" returntype="any">
    	<cfargument name="siret" type="string" required="yes">
        <cfargument name="table" type="string" required="yes">
        <cfargument name="champs" type="string" required="yes">
        <!---<!--- suite plantage plateforme --->
        <!--- du a la resynchro de base --->
        <cfquery name="verif" datasource="f8w_test">
        	Select max(#champs#) as nummaxi from #table# use index(siret) Where `siret` = '#Arguments.siret#'
        </cfquery>
        <!--- --->
        <cfquery name="lect" datasource="f8w_test">
        	Select id,nval from parametres use index(siret_tabl_champs) Where `siret` = '#Arguments.siret#' and `tabl` = '#Arguments.table#' 
            and `champs` = '#Arguments.champs#'
        </cfquery>
        <cfif #lect.recordcount#>
        	<cfif #verif.nummaxi# gt #lect.nval#>
            	<cfset NumSuivant6 = #verif.nummaxi# + 1>
            <cfelse>
				<cfset NumSuivant6 = #lect.nval# + 1>
            </cfif>
            <cfquery name="maj" datasource="f8w_test">
            	Update parametres set nval = '#NumSuivant6#' Where id = '#lect.id#'
            </cfquery>
        <cfelse>
        	<cfif #verif.nummaxi# gt 1000000000>
        		<cfset NumSuivant6 = #verif.nummaxi# + 1>
			<cfelse>
				<cfset NumSuivant6 = 1000000000>
            </cfif>
            <cfquery name="add" datasource="f8w_test">
            	Insert into parametres (`siret`,`tabl`,`champs`,`nval`) Values ('#Arguments.siret#','#Arguments.table#','#Arguments.champs#','#NumSuivant6#')
            </cfquery>
        </cfif>--->
        <cfquery name="lect" datasource="f8w_test">
        	Select id,nval from parametres use index(siret_tabl_champs) Where `siret` = '#Arguments.siret#' and `tabl` = '#Arguments.table#' 
            and `champs` = '#Arguments.champs#'
        </cfquery>
        <cfif #lect.recordcount#>
			<cfset NumSuivant6 = #lect.nval# + 1>
            <cfquery name="maj" datasource="f8w_test">
            	Update parametres set nval = '#NumSuivant6#' Where id = '#lect.id#'
            </cfquery>
        <cfelse>
			<cfset NumSuivant6 = 1000000000>
            <cfquery name="add" datasource="f8w_test">
            	Insert into parametres (`siret`,`tabl`,`champs`,`nval`) Values ('#Arguments.siret#','#Arguments.table#',
                '#Arguments.champs#','#NumSuivant6#')
            </cfquery>
        </cfif>
        <cfreturn NumSuivant6>
    </cffunction>
<!--- '***************************************************************     --->
    <cffunction name="limitation" access="public" returntype="any">
    	<cfargument name="id_enfant" type="numeric" required="yes">
        <cfargument name="id_prestation" type="numeric" required="yes">
        <cfargument name="datejour" type="string" required="yes">
    	<cfset Msglimitation = "">
		<cfquery name="verif_verification" datasource="f8w_test"><!--- des limitation pour cet enfant sur cette prestation ??? --->
			Select id,id_ent from limitation_resa use index(id_enfant_id_prestation) Where id_enfant='#Arguments.id_enfant#' 
            and id_prestation='#Arguments.id_prestation#'
		</cfquery>
		<cfif #verif_verification.recordcount# gt 0>
        	<cfquery name="enfantlimitation" datasource="f8w_test">
            	Select id,siret,NumEnfant,prénom from enfant where id='#Arguments.id_enfant#'
            </cfquery>
            <cfquery name="prestationlimitation" datasource="f8w_test">
            	Select id,numprestation,désignation,JoursExclus,DélaiRésaWebJ,DélaiRésaWebH from prestation where id='#Arguments.id_prestation#'
            </cfquery>
            <cfquery name="limitation_resa_ent" datasource="f8w_test"><!--- recup du Nbr de jour maxi par semaine --->
        		Select designation,NbrJour from limitation_resa_ent Where id='#verif_verification.id_ent#'
        	</cfquery>
        	<cfset DateDemandelimitation = createdate(#mid(Arguments.datejour,7,4)#,#mid(Arguments.datejour,4,2)#,#mid(Arguments.datejour,1,2)#)>
        	<cfswitch expression="#dayofweek(DateDemandelimitation)#">
        		<cfcase value="1"><!--- dimanche --->
                	<cfset DateDeplimitation = Dateadd("d",-6,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="2"><!--- lundi --->
                	<cfset DateDeplimitation = #DateDemandelimitation#>
                </cfcase>
        		<cfcase value="3"><!--- mardi --->
                	<cfset DateDeplimitation = dateadd("d",-1,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="4"><!--- mercredi --->
                	<cfset DateDeplimitation = dateadd("d",-2,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="5"><!--- jeudi --->
                	<cfset DateDeplimitation = dateadd("d",-3,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="6"><!--- vendredi --->
                	<cfset DateDeplimitation = dateadd("d",-4,#DateDemandelimitation#)>
                </cfcase>
        		<cfcase value="7"><!--- samedi --->
                	<cfset DateDeplimitation = dateadd("d",-5,#DateDemandelimitation#)>
                </cfcase>
        	</cfswitch>
        	<cfset NombreDeResaCetteSemaine = 1><!--- 0 + celle demandé --->    
            <cfloop from="1" to="7" index="nbrJourlimitation">
        		<cfset DateCherchelimitation = dateadd("d",#nbrJourlimitation# - 1,#DateDeplimitation#)>
                <cfset aaa = resaponctuelle(#DateCherchelimitation#,#enfantlimitation.siret#,#prestationlimitation.numprestation#,#enfantlimitation.numenfant#,#prestationlimitation.JoursExclus#,#prestationlimitation.DélaiRésaWebJ#,#prestationlimitation.DélaiRésaWebH#,#prestationlimitation.id#,#enfantlimitation.id#)>
                <cfif #aaa.bg# neq "##FF0000"<!--- rouge ---> and #aaa.bg# neq "##666666"<!--- gris ---> and #aaa.bg# neq "##FFFFFF"<!--- blanc ---> >
					<!--- inscrit a la presta --->
                    <cfset NombreDeResaCetteSemaine = #NombreDeResaCetteSemaine# + 1>
				</cfif>
            </cfloop>
            <cfif #NombreDeResaCetteSemaine# gt #limitation_resa_ent.NbrJour#>
            	<cfset Msglimitation = "Désolé, inscription impossible, vous ne pouvez inscrire " & #enfantlimitation.prénom# & " que " & #limitation_resa_ent.NbrJour# & " jour(s) ">
                <cfset Msglimitation = #Msglimitation# & "par semaine à la " & #prestationlimitation.désignation# & " ! Limitation : " & #limitation_resa_ent.designation#>
            </cfif>
        </cfif>
    	<cfreturn Msglimitation>    
    </cffunction>
<!--- '***************************************************************     --->
    <cffunction name="QuotaPonctuel" access="public" returntype="any">
    	<cfargument name="id_enfant" type="numeric" required="yes">
        <cfargument name="id_prestation" type="numeric" required="yes">
        <cfargument name="datejour" type="string" required="yes">
    	<cfargument name="Q" type="numeric" required="yes">
		<cfset Msgquota = "">
        <cfquery name="enfantquota" datasource="f8w_test">
        	Select * from enfant where id='#arguments.id_enfant#'
        </cfquery>
        <cfquery name="prestationquota" datasource="f8w_test">
        	Select * from prestation where id='#arguments.id_prestation#'
        </cfquery>
        <cfquery name="prestationquota1" datasource="f8w_test">
        	Select * from prestation where id='#arguments.id_prestation#'
        </cfquery>
        <!--- quota sur lieuactivité ?? --->
        <cfset quotasurlieuactivité = "oui">
        <cfquery name="enfantclassequota" datasource="f8w_test">
        	Select numclasse from enfantclasse use index(siret_NumEnfant) Where siret='#enfantquota.siret#' and NumEnfant='#enfantquota.NumEnfant#'
        </cfquery>
        <cfquery name="PourQuota" datasource="f8w_test">
        	Select id,Quota,ListeDattente,DelaiPremptionListeDattente from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
            Where siret='#enfantquota.siret#' and NumClasse='#enfantclassequota.numclasse#' and NumPrestation='#prestationquota.NumPrestation#'
        </cfquery>
        <cfif #PourQuota.Quota# eq 0 or #PourQuota.recordcount# eq 0><!--- quota sur prestation ?? --->
        	<cfquery name="PourQuota" dbtype="query">
        		Select id,Quota,ListeDattente,DelaiPremptionListeDattente from prestationquota
        	</cfquery>
            <cfset quotasurlieuactivité = "non">
        </cfif>
		<cfif #PourQuota.Quota# gt 0>
			<cfif #prestationquota.Modalité# neq 5><!--- pas de quantités --->
				<cfif #Arguments.Q# eq 1><!--- ont inscrit --->
					<cfif #quotasurlieuactivité# eq "non">
                        <cfquery name="quotasurponctuel" datasource="f8w_test">             
                            Select sum(Q) as totresponctu from quotasurponctuel use index(id_prestation_Date) Where id_prestation='#Arguments.id_prestation#' 
                            and Date='#Arguments.datejour#'
                        </cfquery>
                    <cfelse>
                        <cfquery name="quotasurponctuel" datasource="f8w_test">             
                            Select sum(Q) as totresponctu from quotasurponctuel use index(id_lieuprestationclasse_Date) 
                            Where id_lieuprestationclasse='#PourQuota.id#' and Date='#Arguments.datejour#'
                        </cfquery>
                    </cfif>
            		<cfif #quotasurponctuel.totresponctu# lte #PourQuota.Quota#><!--- ont est sous le quota donc ont peut inscrire --->
                		<cfif #quotasurlieuactivité# eq "non">
                            <cfquery name="addquota" datasource="f8w_test">
                                Insert into quotasurponctuel (id_prestation,Date,Q) Values ('#Arguments.id_prestation#','#Arguments.datejour#',1)
                            </cfquery>
						<cfelse>
                            <cfquery name="addquota" datasource="f8w_test">
                                Insert into quotasurponctuel (id_prestation,id_lieuprestationclasse,Date,Q) Values ('#Arguments.id_prestation#',
                                '#PourQuota.id#','#Arguments.datejour#',1)
                            </cfquery>                    
                        </cfif>
                	<cfelse><!--- message désolé + eventuellement ajout en liste d'attente --->
                    	<cfset Msgquota = "Désolé, la " & #prestationquota1.désignation# & " est complète (" & #PourQuota.Quota# & " places) pour le ">
                        <cfset Msgquota = #Arguments.datejour# & ". Pas d'inscription possible !">
                    </cfif>
                <cfelse><!--- ont desinscrit + eventuellement inscription d'un enfant en liste d'attente --->
					<cfif #quotasurlieuactivité# eq "non">
                        <cfquery name="addquota" datasource="f8w_test">
                            Insert into quotasurponctuel (id_prestation,Date,Q) Values ('#Arguments.id_prestation#','#Arguments.datejour#',-1)
                        </cfquery>
                    <cfelse>
                        <cfquery name="addquota" datasource="f8w_test">
                            Insert into quotasurponctuel (id_prestation,id_lieuprestationclasse,Date,Q) Values ('#Arguments.id_prestation#',
                            '#PourQuota.id#','#Arguments.datejour#',-1)
                        </cfquery>                    
                    </cfif>
            		
                
                </cfif>
            <cfelse><!--- ont gere les quantités --->

            </cfif>                
		</cfif>
		<cfreturn Msgquota>
	</cffunction>
	<cffunction name="QRCodeImage" returntype="string" access="public" output="true">
		<cfargument name="linkTo" required="true" hint="URL to link to. ex.: https://bizonbytes.com">
		<cfargument name="width" required="false" default="100" hint="Width of QR Code image">		
		<cfargument name="destination" required="false" default="C:\TempWeb\" hint="Where to save your image?">		
		<cfargument name="filename" required="false" default="#CreateUUID()#" hint="Filename without the extension">
		<cfargument name="showImage" required="false" default="true" type="boolean" hint="Do you want to show the image? No will only save the QR image file">		
		<cfargument name="abortIfExist" required="false" default="false" hint="Won't create the QR Code from google if the file already exist in your destination folder">
			<cfif structKeyExists(arguments, "linkTo")>

				<cfset file = "#arguments.destination#\#arguments.filename#.png">

				<cfif (arguments.abortIfExist EQ "false") OR
						(arguments.abortIfExist EQ "true" AND NOT FileExists(file))>					
				
					<!--- We will be calling google chart API to return the QR Code
							We are using urlencodedformat() since the data must be URL-encoded.
					 --->
    
					<cfhttp url="http://chart.apis.google.com/chart?chs=#arguments.width#x#arguments.width#&cht=qr&chl=#urlencodedformat(arguments.linkTo)#" 
							result="qrCode" 
							method="get" 
							getAsBinary="yes">

					<!--- If google return a qrCode then lets write the image to the destination folder --->
					<cfif structKeyExists(qrCode, "filecontent")>

						<!---<cfimage action="write" 
								destination="#fullPathDirectory()#/#arguments.destination#/#arguments.filename#.png"
								source="#qrCode.filecontent#"
								overwrite="true"/>--->
                        <cfimage action="write" 
								destination="#arguments.destination#\#arguments.filename#.png"
								source="#qrCode.filecontent#"
								overwrite="true"/>

					</cfif>							
				</cfif>
                
				<cfif arguments.showImage EQ "true">		
					<!---<img src="#arguments.destination#/#arguments.filename#.png" width="#arguments.width#" height="#arguments.width#">--->
                    <img src="/print/#arguments.filename#.png" width="#arguments.width#" height="#arguments.width#">
				</cfif>
	
			</cfif>

	</cffunction>
    <cffunction name="JourPrestationLieuQuota" access="public" returntype="any">
		<cfargument name="siret" required="true">
		<cfargument name="jour" required="true">		
		<cfargument name="prestation" required="true">
    	<cfargument name="lieu" required="true">
        <cfargument name="classe" required="no" default="0">
        <cfset TotResa = 0>
        <cfset datedeb1 = createdate(#mid(arguments.jour,7,4)#,#mid(arguments.jour,4,2)#,#mid(arguments.jour,1,2)#)>
		<!---<cfset datedeb1 = now()>--->
        <cfquery name="prestationquota" datasource="f8w_test">
        	Select * from prestation use index(siret_numprestation) 
            Where siret='#arguments.siret#' and NumPrestation='#arguments.prestation#'
        </cfquery>
        <cfif #arguments.lieu# gt 0 or #arguments.classe# gt 0><!--- sur un lieu precis donc classe --->
            <cfif #arguments.lieu# gt 0>
                <cfquery name="lieuprestationclassequota" datasource="f8w_test" maxrows="1">
                    Select NumClasse from lieuprestationclasse use index(siret_NumLieu_NumPrestation) Where siret='#arguments.siret#' 
                    and NumLieu='#arguments.lieu#' and NumPrestation='#arguments.prestation#' group by NumClasse
                </cfquery>
			<cfelse>
                <cfquery name="lieuprestationclassequota" datasource="f8w_test" maxrows="1">
                    Select NumClasse from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
                    Where siret='#arguments.siret#' 
                    and NumClasse='#arguments.classe#' and NumPrestation='#arguments.prestation#' group by NumClasse
                </cfquery>           
                <cfset lieuprestationclassequota.NumClasse = #arguments.classe#>
            </cfif>            
            <cfloop query="lieuprestationclassequota">
                <cfquery name="enfantclassequota" datasource="f8w_test">
                    Select NumEnfant from enfantclasse use index(siret_NumClasse) 
                    Where siret='#arguments.siret#' and NumClasse='#lieuprestationclassequota.NumClasse#' group by numenfant
                </cfquery>
                <cfloop query="enfantclassequota">
                	<cfquery name="verenfant" datasource="f8w_test">
                		Select parti,del from enfant use index(siret_numenfant) 
                        where siret='#arguments.siret#' and numenfant='#enfantclassequota.numenfant#'
                	</cfquery>
                    <cfif #verenfant.parti# eq 0 and #verenfant.del# eq 0>
						<cfset retourresa = resaponctuelle1(#datedeb1#,#Arguments.siret#,#Arguments.prestation#,#enfantclassequota.numenfant#,#prestationquota.JoursExclus#,#prestationquota.DélaiRésaWebJ#,#prestationquota.DélaiRésaWebH#,0,0)>
                        <cfif #retourresa.bg# neq "##FF0000"<!--- rouge ---> and #retourresa.bg# neq "##666666"<!--- gris ---> and #retourresa.bg# neq "##FFFFFF"<!--- blanc ---> >
                            <!--- inscrit a la presta --->
                            <cfset TotResa = #TotResa# + 1>
                        </cfif> 
					</cfif>
                </cfloop>
            </cfloop>
        <cfelse><!--- pas sur lieu donc pas classe donc tout enfant --->
            <cfquery name="enfantclassequota" datasource="f8w_test">
                Select numenfant from enfant use index(siret) where siret='#arguments.siret#' and parti=0 and del=0
            </cfquery>
        	<cfloop query="enfantclassequota">
            	<cfset retourresa = resaponctuelle1(#datedeb1#,#Arguments.siret#,#Arguments.prestation#,#enfantclassequota.numenfant#,#prestationquota.JoursExclus#,#prestationquota.DélaiRésaWebJ#,#prestationquota.DélaiRésaWebH#,0,0)>
				<cfif #retourresa.bg# neq "##FF0000"<!--- rouge ---> and #retourresa.bg# neq "##666666"<!--- gris ---> and #retourresa.bg# neq "##FFFFFF"<!--- blanc ---> >
                    <!--- inscrit a la presta --->
                    <cfset TotResa = #TotResa# + 1>
                </cfif> 
        	</cfloop>
        </cfif>
        <cfreturn TotResa>
    </cffunction>
    <cffunction name="JourPrestationLieuQuota1" access="public" returntype="any">
		<cfargument name="siret" required="true">
		<cfargument name="jour" required="true">		
		<cfargument name="prestation" required="true">
    	<cfargument name="lieu" required="true">
        <cfargument name="laclasse" required="true">
        <cfargument name="siretdispo" required="no" default="">
        <cfargument name="id_reserve_a" required="no" default="0">
        <cfif len(#arguments.siretdispo#) eq 0>
        	<cfset arguments.siretdispo = #arguments.siret#>
        </cfif>
        <cfset TotResa = 0>
        <cfset datedeb1 = createdate(#mid(arguments.jour,7,4)#,#mid(arguments.jour,4,2)#,#mid(arguments.jour,1,2)#)>
		<!---<cfset datedeb1 = now()>--->
        <cfquery name="prestationquota" datasource="f8w_test">
        	Select * from prestation use index(siret_numprestation) 
            Where siret='#arguments.siretdispo#' and NumPrestation='#arguments.prestation#'
        </cfquery>
        <cfquery name="prestationmiseadispo" datasource="f8w_test">
            Select * from prestation_miseadispo where id=0
        </cfquery>
        <cfif #prestationquota.recordcount# eq 0>
        	<!--- 06/07/2018 presta mise a dispo ??? --->
            <cfquery name="prestationmiseadispo" datasource="f8w_test">
                Select * from prestation_miseadispo 
                where find_in_set(lst_siret_dispose,'#arguments.siret#') 
                and NumPrestation_miseadispo='#arguments.prestation#' and actif=1 
            </cfquery>
			<cfif #prestationmiseadispo.recordcount#>
                <cfquery name="prestationquota" datasource="f8w_test">
                    Select * from prestation use index(siret_numprestation) 
                    Where siret='#prestationmiseadispo.siret_miseadispo#' and NumPrestation='#arguments.prestation#'
                </cfquery>        
        	</cfif>
        </cfif>
        <cfif val(#arguments.lieu#) gt 0><!--- sur un lieu precis donc classe --->
            <cfquery name="lieuprestationclassequota" datasource="f8w_test">
                Select NumClasse from lieuprestationclasse use index(siret_NumLieu_NumPrestation) Where siret='#arguments.siret#' 
                and NumLieu='#arguments.lieu#' and NumPrestation='#arguments.prestation#' group by NumClasse
            </cfquery>			          
            <cfloop query="lieuprestationclassequota">
                <cfquery name="enfantclassequota" datasource="f8w_test">
                    Select NumEnfant from enfantclasse use index(siret_NumClasse) 
                    Where siret='#arguments.siret#' and NumClasse='#lieuprestationclassequota.NumClasse#' group by numenfant
                </cfquery>
                <cfloop query="enfantclassequota">
                	<cfquery name="verenfant" datasource="f8w_test">
                		Select parti,del from enfant use index(siret_numenfant) 
                        where siret='#arguments.siret#' and numenfant='#enfantclassequota.numenfant#'
                	</cfquery>
                    <cfif #verenfant.parti# eq 0 and #verenfant.del# eq 0>
						<cfset retourresa = resaponctuelle1(#datedeb1#,#Arguments.siret#,#Arguments.prestation#,#enfantclassequota.numenfant#,#prestationquota.JoursExclus#,#prestationquota.DélaiRésaWebJ#,#prestationquota.DélaiRésaWebH#,0,0)>
                        <cfif #retourresa.bg# neq "##FF0000"<!--- rouge ---> and #retourresa.bg# neq "##666666"<!--- gris ---> and #retourresa.bg# neq "##FFFFFF"<!--- blanc ---> >
                            <!--- inscrit a la presta --->
                            <cfset TotResa = #TotResa# + 1>
                        </cfif> 
					</cfif>
                </cfloop>
            </cfloop>
        <cfelseif val(#arguments.laclasse#) gt 0><!--- sur une classe --->
        	<cfquery name="enfantclassequota" datasource="f8w_test">
                Select NumEnfant from enfantclasse use index(siret_NumClasse) 
                Where siret='#arguments.siret#' and NumClasse='#arguments.laclasse#' group by numenfant
            </cfquery>
        	<cfloop query="enfantclassequota">
                <cfquery name="verenfant" datasource="f8w_test">
                    Select NumClientI,parti,del from enfant use index(siret_numenfant) 
                    where siret='#arguments.siret#' and numenfant='#enfantclassequota.numenfant#'
                </cfquery>
                <cfif #verenfant.parti# eq 0 and #verenfant.del# eq 0>
					<!---<cfset retourresa = resaponctuelle1(#datedeb1#,#Arguments.siret#,#Arguments.prestation#,#enfantclassequota.numenfant#,#prestationquota.JoursExclus#,#prestationquota.DélaiRésaWebJ#,#prestationquota.DélaiRésaWebH#,0,0)>
                    <cfif #retourresa.bg# neq "##FF0000"<!--- rouge ---> and #retourresa.bg# neq "##666666"<!--- gris ---> and #retourresa.bg# neq "##FFFFFF"<!--- blanc ---> >
                        <!--- inscrit a la presta --->
                        <cfset TotResa = #TotResa# + 1>
                    </cfif> --->               
					<cfset retourresa = GetInscriptionV2(#Arguments.siret#,#verenfant.NumClientI#,#enfantclassequota.numenfant#,#Arguments.prestation#,#arguments.jour#,#arguments.jour#,0,#prestationquota.siret#)>
                     <cfset TotResa = #TotResa# + #retourresa.inscrit#>
                </cfif>
            </cfloop>
        <cfelse><!--- pas sur lieu donc pas classe donc tout enfant --->
        	<!--- modif 03/04/2019 
            <cfif #prestationmiseadispo.recordcount# eq 0>
                <cfquery name="enfantclassequota" datasource="f8w_test">
                    Select siret,numenfant from enfant use index(siret) where siret='#arguments.siret#' and parti=0 and del=0
                </cfquery>
			<cfelse>
            	<cfset lstsiret = #prestationmiseadispo.lst_siret_dispose# & "," & #prestationmiseadispo.siret_miseadispo#>
                <cfquery name="enfantclassequota" datasource="f8w_test">
                    Select siret,numenfant from enfant use index(siret) 
                    where find_in_set(siret,'#lstsiret#') and parti=0 and del=0
                </cfquery>                
            </cfif>
        	<cfloop query="enfantclassequota">
            	<cfset retourresa = resaponctuelle1(#datedeb1#,#enfantclassequota.siret#,#Arguments.prestation#,#enfantclassequota.numenfant#,#prestationquota.JoursExclus#,#prestationquota.DélaiRésaWebJ#,#prestationquota.DélaiRésaWebH#,0,0)>
				<cfif #retourresa.bg# neq "##FF0000"<!--- rouge ---> and #retourresa.bg# neq "##666666"<!--- gris ---> and #retourresa.bg# neq "##FFFFFF"<!--- blanc ---> >
                    <!--- inscrit a la presta --->
                    <cfset TotResa = #TotResa# + 1>
                </cfif> 
        	</cfloop>
        	--->
            <cfquery name="enfantclassequota" datasource="f8w_test">
                Select * from enfant use index(siret) where siret='#arguments.siret#' and parti=0 and del=0
            </cfquery>
            <cfloop query="enfantclassequota">
            	<cfset retourresa = GetInscriptionV2(#Arguments.siret#,#enfantclassequota.NumClientI#,#enfantclassequota.numenfant#,#Arguments.prestation#,#arguments.jour#,#arguments.jour#,0,#prestationquota.siret#,#Arguments.id_reserve_a#)>
                <cfset TotResa = #TotResa# + #retourresa.inscrit#>
            </cfloop>
        </cfif>
        <cfreturn TotResa>
    </cffunction>
	<cffunction name="prestation_reserve_a" returntype="string">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="NumPrestation" type="numeric" required="no" default="0">
        <cfargument name="NumClient" type="numeric" required="no" default="0">
       	<cfargument name="NumEnfant" type="numeric" required="no" default="0">
        <cfargument name="NumJourPerioPresta" type="numeric" required="no" default="0">
        <cfset retour = "OK"><!--- par defaut pas de blocage --->
        <cfset MatchEnreg = "KO">
    	<cfquery name="groupeclient" datasource="f8w_test">
        	Select NumGroupe from clientgroupe use index(siret_NumClient) 
            Where siret='#arguments.siret#' and NumClient='#arguments.NumClient#'
        </cfquery>
        
        <cfquery name="enfant" datasource="f8w_test">
        	Select DateNaissance from enfant use index(siret_NumEnfant) Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#'
        </cfquery>
        <cfset LAnneeNaissance = "">
        <cfset LAnneeNaissance = "">
        <cfset LAge = "">
        <cfif len(#enfant.DateNaissance#) eq 10>
			<cfset LAnneeNaissance = mid(#enfant.DateNaissance#,7,4)>
            <cftry> 
				<cfset LADateNaissance = createdate(mid(#enfant.DateNaissance#,7,4),mid(#enfant.DateNaissance#,4,2),mid(#enfant.DateNaissance#,1,2))>
            	<cfcatch>
                	<cfset LAnneeNaissance = year(now())>
                    <cfset LADateNaissance = now()>
                </cfcatch>
            </cftry>
			<cfset LAge = datediff("y",#LADateNaissance#,now())>
		</cfif>
        <cfquery name="enfantclasse" datasource="f8w_test">
        	Select NumClasse from enfantclasse use index(siret_NumEnfant) 
            Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#'
        </cfquery>
        <!--- reservé a un groupe ou a une classe ou a .... --->
        <cfquery name="reserve" datasource="f8w_test"><!--- reservé à un groupe client --->
    		Select * from prestation_reserve_a use index(siret_NumPrestation) 
            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumGroupeClient>0 
    	</cfquery> 
        <cfif #reserve.recordcount#>
        	
            <cfloop query="groupeclient">
                <cfquery name="reserve" datasource="f8w_test">
                    Select * from prestation_reserve_a use index(siret_NumPrestation) 
                    Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                    and NumGroupeClient='#groupeclient.NumGroupe#' order by id desc
                </cfquery>
				<cfif #reserve.recordcount# gt 0>
                	<cfbreak>
                </cfif>
            </cfloop>
        	<cfif #reserve.recordcount# eq 0>
            	<cfset retour = "Réservé à des groupes clients auquel n'appartient pas ce client">
            <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
            	<cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
				<cfset DateReservableDu = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
            	<cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
				<cfset DateReservableAu = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
				<cfif datecompare(now(),#DateReservableDu#) eq -1 or datecompare(now(),#DateReservableAu#) eq 1>
                	<!--- aujourdh'ui est plus ancien que DateReservableDu ou aujourd'hui est plus vieux que DateReservableAu --->
					<cfset retour = "Vous n'êtes pas dans la période de réservation !">
<!---					<cfset retour = "Vous n'êtes pas dans la période de réservation ! (">
                    <cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                    <cfset dtdt = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                    <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & " au ">
                    <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                    <cfset dtdt = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                    <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & ")">                
--->                
				</cfif>
                <cfif #retour# eq "OK">
					<cfif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #reserve.DebutPerioPresta#>
                        <cfif #reserve.FinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #reserve.FinPerioPresta#>
                            <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                        </cfif>
                    <cfelseif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# lt #reserve.DebutPerioPresta#>
                        <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                    </cfif>
				</cfif>          
            </cfif>  
              
    	</cfif>
        <cfif retour eq "OK">
            <cfquery name="reserve" datasource="f8w_test"><!--- reservé à une classe --->
                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumClasse>0 
            </cfquery> 
            <cfif #reserve.recordcount#>
                <cfquery name="reserve" datasource="f8w_test">
                    Select * from prestation_reserve_a use index(siret_NumPrestation) 
                    Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumClasse='#enfantclasse.NumClasse#'
                </cfquery>
				<cfset LEDebutPerioPresta = #reserve.DebutPerioPresta#>
                <cfset LEFinPerioPresta = #reserve.FinPerioPresta#>
                <cfif #reserve.recordcount# eq 0>
                    <cfset retour = "Réservé à des classes auquel n'appartient pas cet enfant">
                <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
					<cfloop query="reserve">
						<cfset LEDebutPerioPresta = #reserve.DebutPerioPresta#>
                        <cfset LEFinPerioPresta = #reserve.FinPerioPresta#>
						<cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                        <cfset DateReservableDu = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                        <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                        <cfset DateReservableAu = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                        <cfif datecompare(now(),#DateReservableDu#) eq -1 or datecompare(now(),#DateReservableAu#) eq 1>
                            <!--- aujourdh'ui est plus ancien que DateReservableDu ou aujourd'hui est plus vieux que DateReservableAu --->
                            <cfset retour = "Vous n'êtes pas dans la période de réservation ! (">
                            <cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                            <cfset dtdt = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                            <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & " au ">
                            <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                            <cfset dtdt = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                            <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & ")">                
                        <cfelse>
                        	<cfset retour = "OK">
                            <cfset LEDebutPerioPresta = #reserve.DebutPerioPresta#>
                            <cfset LEFinPerioPresta = #reserve.FinPerioPresta#>
                            <cfbreak>
                        </cfif>
                    </cfloop>         
					<cfif #retour# eq "OK">
						<cfif #LEDebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #LEDebutPerioPresta#>
                            <cfif #LEFinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #LEFinPerioPresta#>
                                <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                            </cfif>
                        <cfelseif #LEDebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# lt #LEDebutPerioPresta#>
                            <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                        </cfif>
                    </cfif>          
                </cfif>    
            </cfif>
            <cfif retour eq "OK">
                <cfquery name="reserve" datasource="f8w_test"><!--- reservé à une année de naissance --->
                    Select * from prestation_reserve_a use index(siret_NumPrestation) 
                    Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and AnneeNaissance<>''
                </cfquery> 
                <cfif #reserve.recordcount#>
                    <cfquery name="reserve" datasource="f8w_test">
                        Select * from prestation_reserve_a use index(siret_NumPrestation) 
                        Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and AnneeNaissance='#LAnneeNaissance#'
                    </cfquery>
                    <cfif #reserve.recordcount# eq 0>
                        <cfset retour = "Réservé à des année de naissance auquel n'appartient pas cet enfant">
                    <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
						<cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                        <cfset DateReservableDu = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                        <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                        <cfset DateReservableAu = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                        <cfif datecompare(now(),#DateReservableDu#) eq -1 or datecompare(now(),#DateReservableAu#) eq 1>
                            <!--- aujourdh'ui est plus ancien que DateReservableDu ou aujourd'hui est plus vieux que DateReservableAu --->
                            <cfset retour = "Vous n'êtes pas dans la période de réservation ! (">
                            <cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                            <cfset dtdt = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                            <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & " au ">
                            <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                            <cfset dtdt = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                            <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & ")">                
                        </cfif>
                        <cfif #retour# eq "OK">
                            <cfif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #reserve.DebutPerioPresta#>
                                <cfif #reserve.FinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #reserve.FinPerioPresta#>
                                    <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                                </cfif>
                            <cfelseif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# lt #reserve.DebutPerioPresta#>
                                <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                            </cfif>
                        </cfif>          
                    </cfif>    
                </cfif>
                <cfif retour eq "OK">
                    <cfquery name="reserve" datasource="f8w_test"><!--- reservé à un age --->
                        Select * from prestation_reserve_a use index(siret_NumPrestation) 
                        Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and Age>0
                    </cfquery> 
                    <cfif #reserve.recordcount#>
                        <cfquery name="reserve" datasource="f8w_test">
                            Select * from prestation_reserve_a use index(siret_NumPrestation) 
                            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and Age='#LAge#'
                        </cfquery>
                        <cfif #reserve.recordcount# eq 0>
                            <cfset retour = "Réservé à des age auquel n'appartient pas cet enfant">
                        <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
							<cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                            <cfset DateReservableDu = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                            <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                            <cfset DateReservableAu = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                            <cfif datecompare(now(),#DateReservableDu#) eq -1 or datecompare(now(),#DateReservableAu#) eq 1>
                                <!--- aujourdh'ui est plus ancien que DateReservableDu ou aujourd'hui est plus vieux que DateReservableAu --->
                                <cfset retour = "Vous n'êtes pas dans la période de réservation ! (">
                                <cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                                <cfset dtdt = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                                <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & " au ">
                                <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                                <cfset dtdt = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                                <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & ")">                
                            </cfif>
                            <cfif #retour# eq "OK">
                                <cfif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #reserve.DebutPerioPresta#>
                                    <cfif #reserve.FinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #reserve.FinPerioPresta#>
                                        <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                                    </cfif>
                                <cfelseif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# lt #reserve.DebutPerioPresta#>
                                    <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !" >
                                </cfif>
                            </cfif>          
                        </cfif>    
                    </cfif>
                </cfif>
            </cfif>
        </cfif>
    	<cfreturn retour>
    </cffunction>    
	<cffunction name="prestation_reserve_a1" returntype="string">
    	<cfargument name="siret" type="string" required="no" default="">
        <cfargument name="NumPrestation" type="numeric" required="no" default="0">
        <cfargument name="NumClient" type="numeric" required="no" default="0">
       	<cfargument name="NumEnfant" type="numeric" required="no" default="0">
        <cfargument name="NumJourPerioPresta" type="numeric" required="no" default="0">
        <cfargument name="type" type="string">
        <cfargument name="DateJour" type="string">
		<cfargument name="siretdispose" type="string" required="no" default="">
        <cfif len(#arguments.siretdispose#) eq 0>
        	<cfset arguments.siretdispose = #arguments.siret#>
        </cfif>
        <cfset retour = "OK"><!--- par defaut pas de blocage --->
        <cfset MatchEnreg = "KO">
    	<cfquery name="groupeclient" datasource="f8w_test">
        	Select NumGroupe from clientgroupe use index(siret_NumClient) 
            Where siret='#arguments.siret#' and NumClient='#arguments.NumClient#'
        </cfquery>
        <cfquery name="enfantgjhgjgjgjhgj" datasource="f8w_test">
        	Select DateNaissance from enfant use index(siret_NumEnfant) 
            Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#'
        </cfquery>
        <cfset LAnneeNaissance = "">
        <cfset LAnneeNaissance = "">
        <cfset LAge = "">
        <cfif len(#enfantgjhgjgjgjhgj.DateNaissance#) eq 10>
			<cfset LAnneeNaissance = mid(#enfantgjhgjgjgjhgj.DateNaissance#,7,4)>
            <cftry>
				<cfset LADateNaissance = createdate(mid(#enfantgjhgjgjgjhgj.DateNaissance#,7,4),mid(#enfantgjhgjgjgjhgj.DateNaissance#,4,2),mid(#enfantgjhgjgjgjhgj.DateNaissance#,1,2))>
            	<cfcatch>
                	<cfset LAnneeNaissance = year(now())>
                    <cfset LADateNaissance = now()>
                </cfcatch>
            </cftry>
			<cfset LAge = datediff("y",#LADateNaissance#,now())>
		</cfif>
        <cfquery name="enfantclasse" datasource="f8w_test">
        	Select NumClasse from enfantclasse use index(siret_NumEnfant) 
            Where siret='#arguments.siret#' and NumEnfant='#arguments.NumEnfant#' group by NumClasse
        </cfquery>
        <cfset lstnumclasseenfant = "">
        <cfloop query="enfantclasse">
        	<cfset lstnumclasseenfant = #lstnumclasseenfant# & #enfantclasse.NumClasse# & ",">
        </cfloop>
        <!--- reservé a un groupe ou a une classe ou a .... --->
        <cfquery name="reserve1" datasource="f8w_test"><!--- reservé à un groupe client --->
    		Select * from prestation_reserve_a use index(siret_NumPrestation) 
            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumGroupeClient>0 
    	</cfquery> 
        <cfif #reserve1.recordcount#>
        	<cfset LeGroupeClient = "">
            <cfloop query="groupeclient">
                <cfquery name="reserve" datasource="f8w_test">
                    Select * from prestation_reserve_a use index(siret_NumPrestation) 
                    Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                    and NumGroupeClient='#groupeclient.NumGroupe#'
                </cfquery>
				<cfif #reserve.recordcount# gt 0>
                	<cfset LeGroupeClient = #LeGroupeClient# & #groupeclient.NumGroupe# & ",">
                </cfif>
            </cfloop>
        	<cfif #LeGroupeClient# eq "">
            	<cfset retour = "Réservé à des groupes clients auquel n'appartient pas ce client...!">
            <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
				<cfif #arguments.type# eq "clic"><!--- reservation --->
					<cfset DateArgument = createdate(#mid(#arguments.DateJour#,7,4)#,#mid(#arguments.DateJour#,4,2)#,#mid(#arguments.DateJour#,1,2)#)>
                    <cfset AnDep = year(now())><!--- année de l'action --->
                    <cfset JourDep = dayofyear(now())><!--- jour de l'action --->
                    
                    <cfset AnDepcliquer = val(year(#DateArgument#))><!--- année du jour cliqué --->
                    <cfset JourDepcliquer = val(dayofyear(#DateArgument#))><!--- jour du jour cliqué --->
                    
                    <!--- enregistrement pour la prestation et la classe avec :
                    ReservableDu inférieur ou égal à aujourd'hui
                    ReservableDuAn inférieur ou égal à l'année d'aujourd'hui
                    ReservableAu supérieur ou égal à aujourd'hui
                    ReservableAuAn supérieur ou égal à l'année d'aujourd'hui
                    
                    DebutPerioPrestaAn inférieur ou égal à l'année cliqué
                    DebutPerioPresta inférieur ou égal au jour cliqué
                    FinPerioPresta supérieur ou égal au jour cliqué
                    FinPerioPrestaAn supérieur ou égal à l'année cliquée
                    --->
                    <cfset trouve = "non">
                    <cfloop list="#LeGroupeClient#" delimiters="," index="idx">
                        <cfquery name="testreserve" datasource="f8w_test">
                            Select * from prestation_reserve_a use index(siret_NumPrestation) 
                            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                            and NumGroupeClient='#idx#'
                            
                            And (ReservableDu <= #JourDep# And ReservableDuAn <= #AnDep#
                            or ReservableDuAn < #AnDep#)
                            
                            And ((ReservableAu >= #JourDep# And ReservableAuAn >= #AnDep#) or ReservableAuAn > #AnDep#)
                            
                            And (DebutPerioPrestaAn <= #AnDepcliquer# And DebutPerioPresta <= #JourDepcliquer#
                            or DebutPerioPrestaAn < #AnDepcliquer#)
                            
                            And ((FinPerioPresta >= #JourDepcliquer# And FinPerioPrestaAn >= #AnDepcliquer#) or FinPerioPrestaAn > #AnDepcliquer#) 
                        </cfquery>                        
						<cfif #testreserve.recordcount# gt 0>
                        	<cfset trouve = "oui">
                        </cfif>
					</cfloop>
                    <cfif #trouve# eq "non">
                        <cfset retour = "Vous n'êtes pas dans la période de réservation ...!">
                    <cfelse>
                        <cfset retour = "OK">
                    </cfif>
                <cfelse><!--- affichage --->
                    <cfset AnDep = val(year(#arguments.DateJour#))>
                    <cfset JourDep = val(dayofyear(#arguments.DateJour#))>
                    <cfset trouve = "non">
                    <cfloop list="#LeGroupeClient#" delimiters="," index="idx">
                        <cfquery name="testreserve" datasource="f8w_test">
                            Select * from prestation_reserve_a use index(siret_NumPrestation) 
                            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                            and NumGroupeClient='#idx#'
                            
                            and ((DebutPerioPresta<='#JourDep#' and DebutPerioPrestaAn='#AnDep#') or DebutPerioPrestaAn<'#AnDep#')
                            
                            And ((FinPerioPresta>='#JourDep#' And FinPerioPrestaAn='#AnDep#') or FinPerioPrestaAn>'#AnDep#')
                            
                        </cfquery>                        
						<cfif #testreserve.recordcount# gt 0>
                        	<cfset trouve = "oui">
                        </cfif>
                    </cfloop>
                    <cfif #trouve# eq "non">
                        <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                    </cfif>                    
                </cfif>                    
            </cfif>    
    	</cfif>
        <cfif retour eq "OK">
            <cfquery name="reserve1" datasource="f8w_test"><!--- reservé à une classe --->
                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                <!---Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and NumClasse>0
				Modif 20/12/2018 mise a dispo sivsc ---> 
                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                and NumGroupeClient=0
            </cfquery> 
            <cfif #reserve1.recordcount#>
                <cfquery name="reserve" datasource="f8w_test"><!--- concerne la classe de l'enfant ?? --->
                    Select * from prestation_reserve_a use index(siret_NumPrestation) 
                    Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                    and find_in_set(NumClasse,'#lstnumclasseenfant#')
                </cfquery>
                <cfif #reserve.recordcount# eq 0 and #reserve1.NumClasse# gt 0>
                    <cfset retour = "Réservé à des classes auquel n'appartient pas cet enfant">
                <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation ou affichage --->
					<cfif #arguments.type# eq "clic"><!--- reservation --->
                    	<cfset DateArgument = createdate(#mid(#arguments.DateJour#,7,4)#,#mid(#arguments.DateJour#,4,2)#,#mid(#arguments.DateJour#,1,2)#)>
						<cfset AnDep = year(now())><!--- année de l'action --->
                    	<cfset JourDep = dayofyear(now())><!--- jour de l'action --->
                        
						<cfset AnDepcliquer = val(year(#DateArgument#))><!--- année du jour cliqué --->
                        <cfset JourDepcliquer = val(dayofyear(#DateArgument#))><!--- jour du jour cliqué --->
                        
                        <!--- enregistrement pour la prestation et la classe avec :
						ReservableDu inférieur ou égal à aujourd'hui
						ReservableDuAn inférieur ou égal à l'année d'aujourd'hui
						ReservableAu supérieur ou égal à aujourd'hui
						ReservableAuAn supérieur ou égal à l'année d'aujourd'hui
						
						DebutPerioPrestaAn inférieur ou égal à l'année cliqué
						DebutPerioPresta inférieur ou égal au jour cliqué
						FinPerioPresta supérieur ou égal au jour cliqué
						FinPerioPrestaAn supérieur ou égal à l'année cliquée
						--->
                        <cfif #reserve1.NumClasse# gt 0>
                            <!---
                            <cfquery name="testreserve" datasource="f8w_test">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                                And find_in_set(NumClasse,'#lstnumclasseenfant#') 
                                
                                And ReservableDu <= #JourDep# And ReservableDuAn <= #AnDep#
                                
                                And ((ReservableAu >= #JourDep# And ReservableAuAn >= #AnDep#) or ReservableAuAn >= #AnDep#)
                                
                                And DebutPerioPrestaAn <= #AnDepcliquer# And DebutPerioPresta <= #JourDepcliquer#
                                
                                And ((FinPerioPresta >= #JourDepcliquer# And FinPerioPrestaAn >= #AnDepcliquer#) or FinPerioPrestaAn >= #AnDepcliquer#)
                            </cfquery> 
							Modif 05/09/2019 --->
                            <cfquery name="testreserve" datasource="f8w_test">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                                And find_in_set(NumClasse,'#lstnumclasseenfant#') 
                                
                                And (ReservableDu <= #JourDep# And ReservableDuAn <= #AnDep#
                                or ReservableDuAn < #AnDep#)
                                                            
                                And ((ReservableAu >= #JourDep# And ReservableAuAn >= #AnDep#) or ReservableAuAn >= #AnDep#)
                                
                                
                                And ((DebutPerioPrestaAn <= #AnDepcliquer# And DebutPerioPresta <= #JourDepcliquer#) or DebutPerioPrestaAn < #AnDepcliquer#)
                                
                                And ((FinPerioPresta >= #JourDepcliquer# And FinPerioPrestaAn >= #AnDepcliquer#) or FinPerioPrestaAn >= #AnDepcliquer#)
                            </cfquery>                             
						<cfelse>
                            <cfquery name="testreserve" datasource="f8w_test">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                                and NumGroupeClient=0 and NumClasse=0 
                                
                                And (ReservableDu <= #JourDep# And ReservableDuAn <= #AnDep#
                                or ReservableDuAn < #AnDep#)
                                                            
                                And ((ReservableAu >= #JourDep# And ReservableAuAn >= #AnDep#) or ReservableAuAn > #AnDep#)
                                
                                And DebutPerioPrestaAn <= #AnDepcliquer# And DebutPerioPresta <= #JourDepcliquer#
                                And ((FinPerioPresta >= #JourDepcliquer# And FinPerioPrestaAn >= #AnDepcliquer#) or FinPerioPrestaAn > #AnDepcliquer#)
                            </cfquery>                         
                        </cfif>                                               
                    	<cfif #testreserve.recordcount# eq 0>
                        	<cfset retour = "Vous n'êtes pas dans la période de réservation !">
                        <!---<cfelse>
                        	<cfset retour = "OK">--->
                        </cfif>
                    <cfelse><!--- affichage --->
                        <cfset AnDep = val(year(#arguments.DateJour#))>
                        <cfset JourDep = val(dayofyear(#arguments.DateJour#))>
                        <cfif #reserve.NumClasse# gt 0>
                            <cfquery name="testreserve" datasource="f8w_test">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                                and find_in_set(NumClasse,'#lstnumclasseenfant#') 
                                
                                <!--- modifié le 12/01/2023 --->
                                and (DebutPerioPresta<='#JourDep#' and DebutPerioPrestaAn='#AnDep#') or DebutPerioPrestaAn<='#AnDep#'
                            
                                And ((FinPerioPresta>='#JourDep#' And FinPerioPrestaAn>='#AnDep#') or FinPerioPrestaAn>'#AnDep#')
                                    
                                    
                                    
                            </cfquery>                        
						<cfelse>
                            <cfquery name="testreserve" datasource="f8w_test">
                                Select * from prestation_reserve_a use index(siret_NumPrestation) 
                                Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' 
                                and NumGroupeClient=0 and NumClasse=0 
                                and DebutPerioPresta<='#JourDep#' and DebutPerioPrestaAn<='#AnDep#' 
                                And ((FinPerioPresta>='#JourDep#' And FinPerioPrestaAn>='#AnDep#') or FinPerioPrestaAn>'#AnDep#')
                            </cfquery>                                                
                        </cfif>                        
						<cfif #testreserve.recordcount# eq 0>
                            <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                        </cfif>                    
                    </cfif>                    
                </cfif>    
            </cfif>
            <!---<cfif retour eq "OK">
                <cfquery name="reserve" datasource="f8w_test"><!--- reservé à une année de naissance --->
                    Select * from prestation_reserve_a use index(siret_NumPrestation) 
                    Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and AnneeNaissance<>''
                </cfquery> 
                <cfif #reserve.recordcount#>
                    <cfquery name="reserve" datasource="f8w_test">
                        Select * from prestation_reserve_a use index(siret_NumPrestation) 
                        Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and AnneeNaissance='#LAnneeNaissance#'
                    </cfquery>
                    <cfif #reserve.recordcount# eq 0>
                        <cfset retour = "Réservé à des année de naissance auquel n'appartient pas cet enfant">
                    <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
						<cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                        <cfset DateReservableDu = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                        <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                        <cfset DateReservableAu = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                        <cfif datecompare(now(),#DateReservableDu#) eq -1 or datecompare(now(),#DateReservableAu#) eq 1>
                            <!--- aujourdh'ui est plus ancien que DateReservableDu ou aujourd'hui est plus vieux que DateReservableAu --->
                            <cfset retour = "Vous n'êtes pas dans la période de réservation ! (">
                            <cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                            <cfset dtdt = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                            <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & " au ">
                            <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                            <cfset dtdt = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                            <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & ")">                
                        </cfif>
                        <cfif #retour# eq "OK">
                            <cfif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #reserve.DebutPerioPresta#>
                                <cfif #reserve.FinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #reserve.FinPerioPresta#>
                                    <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                                </cfif>
                            <cfelseif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# lt #reserve.DebutPerioPresta#>
                                <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                            </cfif>
                        </cfif>          
                    </cfif>    
                </cfif>
                <cfif retour eq "OK">
                    <cfquery name="reserve" datasource="f8w_test"><!--- reservé à un age --->
                        Select * from prestation_reserve_a use index(siret_NumPrestation) 
                        Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and Age>0
                    </cfquery> 
                    <cfif #reserve.recordcount#>
                        <cfquery name="reserve" datasource="f8w_test">
                            Select * from prestation_reserve_a use index(siret_NumPrestation) 
                            Where siret='#arguments.siret#' and NumPrestation='#arguments.NumPrestation#' and Age='#LAge#'
                        </cfquery>
                        <cfif #reserve.recordcount# eq 0>
                            <cfset retour = "Réservé à des age auquel n'appartient pas cet enfant">
                        <cfelseif #arguments.NumJourPerioPresta# gt 0><!--- tentative de réservation --->
							<cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                            <cfset DateReservableDu = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                            <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                            <cfset DateReservableAu = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                            <cfif datecompare(now(),#DateReservableDu#) eq -1 or datecompare(now(),#DateReservableAu#) eq 1>
                                <!--- aujourdh'ui est plus ancien que DateReservableDu ou aujourd'hui est plus vieux que DateReservableAu --->
                                <cfset retour = "Vous n'êtes pas dans la période de réservation ! (">
                                <cfset dt = createdate(#reserve.ReservableDuAn#,1,1)>
                                <cfset dtdt = dateadd("d",#reserve.ReservableDu#-1,#dt#)>
                                <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & " au ">
                                <cfset dt = createdate(#reserve.ReservableAuAn#,1,1)>
                                <cfset dtdt = dateadd("d",#reserve.ReservableAu#-1,#dt#)>
                                <cfset retour = #retour# & #lsdateformat(dtdt,"dd/MM/YYYY")# & ")">                
                            </cfif>
                            <cfif #retour# eq "OK">
                                <cfif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gte #reserve.DebutPerioPresta#>
                                    <cfif #reserve.FinPerioPresta# gt 0 and #arguments.NumJourPerioPresta# gt #reserve.FinPerioPresta#>
                                        <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                                    </cfif>
                                <cfelseif #reserve.DebutPerioPresta# gt 0 and #arguments.NumJourPerioPresta# lt #reserve.DebutPerioPresta#>
                                    <cfset retour = "Vous n'êtes pas dans la période d'ouverture de la prestation !">
                                </cfif>
                            </cfif>          
                        </cfif>    
                    </cfif>
                </cfif>
            </cfif>--->
        </cfif>
    	<cfreturn retour>
    </cffunction>    
</cfcomponent>