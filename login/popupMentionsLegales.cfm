<div class="popup-mentionslegales card scroll col-9" id="popupMentionsLegales">
    <div>
        <button class="close-btn" onclick="closePopUp()"><img src="/img/icon/icon_close.png" alt="icon en forme de croix pour fermer la fenêtre"></button>

        <h4 class="underline">MENTIONS LEGALES</h4>
        <p>Site édité par Jdéalise, SAS au capital de 10 000 euros</br>
        Siège Social : 42, allée René Bazin, 26000 VALENCE</br>
        N° SIRET 892 555 699 00016 RCS Romans</br>
        Directeur de publication : Ludovic BAYLE</br>
        Adresse mail : contact@jdealise.fr</br>
        Tel : 09 72 46 71 51</br>
        Coordonnées de l'hébergeur du site :</br>                                
        ON LINE SAS au capital de 214 410,50 €, BP 438 75366 PARIS CEDEX 08 RCS Paris B 433 115 904</br>
        Le site cantine-de-france.fr utilise uniquement des cookies de session (et pas de tracage) pour assurer son bon fonctionnement.</p>
    </div>
    <div>
        <h4 class="underline"> REGLES DE CONFIDENTIALITE</h4>
        <p>Dans ce qui suit, le client est l’établissement public ou l’association ayant acquis le droit d’usage du site cantine-de-france.fr auprès de JDEALISE SAS soit directement soit par l’intermédiaire d’un des distributeurs agréés de JDEALISE SAS.</p>
        <p>Les parents sont les responsables légaux des enfants consommateurs des prestations mises à leurs dispositions par le client dont, par exemple non exclusif, la cantine, les garderies et les travaux ou activités pédagogiques (TAP).</p>
        
        <h5 class="underline">Article 1 : Communication</h5>
        <p>JDEALISE s’engage à ne communiquer à quiconque, sauf aux réquisitions des autorités judiciaires, les données saisies par nos clients et par leurs parents et enregistrées dans la base de données de cantine-de-france.fr.</p>
        
        <h5 class="underline">Article 2 : Droit aux modifications</h5>
        <p>Aucune donnée n’étant saisie par JDEALISE, toutes les informations détenues dans la base de données de cantine-de-france.fr sont accessibles et modifiables par celui qui les a saisies (client ou parent).  On notera que les saisies effectuées par les parents utilisateurs sont accessibles et modifiables par le client.</p>
        <p>JDEALISE dégage toutes responsabilités dans les modifications apportées par nos clients aux informations saisies par leurs parents.</p>
        
        <h5 class="underline">Article 3 :  Secret professionnel du personnel JDEALISE</h5>
        <p>Le personnel de développement et d’assistance de JDEALISE a accès aux données contenues dans la base de données cantine-de-france.fr. JDEALISE a introduit dans les contrats de travail de ses employés les règles de secret professionnel concernant toutes les données, saisies par ses clients, dont il pourrait avoir connaissance.</p>
        
        <h5 class="underline">Article 4 : Sécurité de la base de données de cantine-de-france .fr</h5>
        <p>La base de données cantine-de-france est sauvegardée chaque soir, entre minuit et cinq heures du matin, chez un autre hébergeur afin de se garantir contre tous risques de destruction du serveur primaire.</p>
        
        <h5 class="underline">Article 5 : Déclaration CNIL</h5>
        <p>La base de données de cantine-de-France.fr a fait l’objet d’une déclaration CNIL simplifiée N°27 qui interdit la détention de données médicales autres que les vaccinations obligatoires et leurs dates.</p>
        <p>Nous informons nos clients et leurs parents que les informations relevant du domaine médical (allergies alimentaires par exemple) sont saisies sous leurs responsabilités car elles sont nécessaires à la réalisation correcte des prestations.</p>
        <p>Nous informons nos clients qu’il est impératif que les parents aient un droit de rectification des informations collectées.</p>
        <p>L’inscription dans la base de données de ces informations n’a pour seul but que d’être restitué au personnel d’exécution de la prestation en cas de besoin impératif.</p>
    </div>
</div>