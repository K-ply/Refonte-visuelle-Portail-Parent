﻿<cfdocument format="PDF" orientation="LANDSCAPE" filename="#leficpdf#" pagetype="A4" unit="cm" margintop="1" marginbottom="1" marginleft="1" marginright="1">
    <cfset laimg = replacenocase(#reqfin.logoedition#,"C:/inetpub/wwwroot/gestion1.cantine-de-france","")>        
    <table width="100%" border="0">
        <cfoutput>
        <cfset letotgen1 = 0>
        <cfset numligne = 0>
        <cfset savclasse = "">   
        <cfset page = 0>    
        <tbody>
            <cfset numligne = #numligne# + 2>
            <tr>
                <td colspan="9" align="center">Prestation #reqfin.prestation# #reqfin.ecole# #reqfin.classe#</td>
            </tr>
            <tr>
                <td><font size="-1" face="arial"><b>Nom</b></font></td>
                <td><font size="-1" face="arial"><b>Prénom</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.ltxt#</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.mtxt#</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.metxt#</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.jtxt#</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.vtxt#</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.stxt#</b></font></td>
                <td><font size="-1" face="arial"><b>#reqfin.dtxt#</b></font></td>
            </tr>
            <cfloop query="reqfin">
                <cfif #savclasse# eq "">
                    <cfset savclasse = #reqfin.Classe#>
                </cfif>
                <cfif #savclasse# neq #reqfin.Classe# and isdefined("form.lignevidesautpage") and #reqfin.currentrow# neq #reqfin.recordcount#>
                   <cfset savclasse = #reqfin.Classe#>    
                   <cfset resteligne = 31 - #numligne#>       
                   <cfset page = #page# + 1>
                   <cfswitch expression="#page#">
                       <cfcase value="2"><cfset resteligne = #resteligne# + 1></cfcase>     
                       <cfdefaultcase></cfdefaultcase>
                   </cfswitch>       
                       
                   <cfif #resteligne# gt 0>
                       <cfset numligne = 0>
                       <cfloop from="0" to="#resteligne#" index="idx">       
                            <tr>
                                <td colspan="9">&nbsp;</td>
                            </tr>
                       </cfloop>
                        <cfset numligne = #numligne# + 2>   
                       <tr>
                         <td colspan="9" align="center">Prestation #reqfin.prestation# #reqfin.ecole# #reqfin.classe#</td>
                        </tr>
                        <tr>
                            <td><font size="-1" face="arial"><b>Nom</b></font></td>
                            <td><font size="-1" face="arial"><b>Prénom</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.ltxt#</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.mtxt#</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.metxt#</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.jtxt#</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.vtxt#</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.stxt#</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqfin.dtxt#</b></font></td>
                        </tr>   
                    <cfelse>
                        <cfset numligne = 0>   
                    </cfif>
                </cfif>
                <!---<cfset letotgen1 = #letotgen1# + val(#reqchampierfin.totgen1#)>--->
                <cfif (#reqfin.nom# eq "ZZ" or #reqfin.Prenom# eq "TOTAL GENERAL :") and isdefined("form.editionblanche") is false>
                    <cfset fond = "##BBB4B4">
                <cfelse>
                    <cfset fond = "##FBF8F8">
                </cfif>
                <cfset numligne = #numligne# + 1>    
                <tr bgcolor="#fond#">
                    <td><font size="-1" face="arial"><b><cfif find(#reqfin.nom#,"ZZ") eq 0>#reqfin.Nom#</cfif></b></font></td>
                    <td><font size="-1" face="arial"><b><cfif find(#reqfin.prenom#,"ZZ") eq 0>#reqfin.Prenom#</cfif></b></font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.lval#</font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.mval#</font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.meval#</font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.jval#</font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.vval#</font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.sval#</font></td>
                    <td align="center"><font size="-1" face="arial">#reqfin.dval#</font></td>
                </tr>
                <cfif len(#reqfin.mention#)>
                    <cfset numligne = #numligne# + 1>
                    <tr bgcolor="#fond#">
                        <td></td>
                        <td colspan="8"><font color="##F80409" size="-1" face="arial"><b>#reqfin.mention#</b></font></td>
                    </tr>
                </cfif>
                <cfif #reqfin.Prenom# eq "TOTAL :" and isdefined("form.lignevidesautpage") is false>    
                    <!---<cfif #lignevidesautpage# eq 1>
                        <cfdocumentitem type="PAGEBREAK" evalAtPrint="True"></cfdocumentitem>
                    <cfelse>--->
                    <cfset numligne = #numligne# + 1>
                        <tr>
                        <td colspan="9">&nbsp;</td>
                        </tr>
                    <!---</cfif>--->
                <cfelse>
                    
                </cfif>
            </cfloop>
        </tbody>
        </cfoutput>
    </table>

    <cfdocumentitem type="HEADER">
        <cfoutput>
        <table width="100%" border="0">
            <tbody>
                <!---<tr>
                    <td rowspan="4"><img width="150" height="150" src="#laimg#"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><font size="+3">#reqchampierfin.etablissement#</font></td>
                </tr>
                <tr>
                    <td><font size="+3">Feuille de présence du #reqchampierfin.txtentete#</font></td>
                </tr>
                <tr>
                    <td></td>
                </tr>--->
                <tr>
                    <!---<td><img width="300" height="300" src="#laimg#"></td>--->
                    <td align="center"><font size="+4">#reqfin.etablissement# Feuille de présence du #reqfin.txtentete#</font></td>
                </tr>
                <cfif len(#reqfin.delai#)>
                    <tr>
                        <td align="center"><font size="+4" color="##F80409"><b>#reqfin.delai#</b></font></td>
                    </tr>
                </cfif>
            </tbody>
        </table>
        </cfoutput>
    </cfdocumentitem>
    <cfdocumentitem type="FOOTER">
        <cfoutput>
        <table width="100%" border="0">
            <tbody>
                <tr><td align="center"><font size="+4"><cfoutput>Imprimé par www.cantine-de-france.fr le #lsDateFormat(Now(), "dd mmmm yyyy")# à  #TimeFormat(Now(), "HH:mm:ss")# Page #cfdocument.currentpagenumber#/#cfdocument.totalpagecount#</cfoutput></font></td></tr>
            </tbody>
        </table>
        </cfoutput>
    </cfdocumentitem>
</cfdocument>