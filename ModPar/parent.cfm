﻿
<cfif affichemodifenfant eq "oui">
    <div class="row mt-5 content">
        <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintChildrenInformations()">Enfants</button>
        <cfif affichemodifparent eq "oui">
            <button type="button" class="box-message-active col-3 p-3 d-flex justify-content-center">Parents</button>
        </cfif>
        <cfif afficheajoutenfant eq "oui">
            <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintAddChild()">Ajouter un enfant</button>
        </cfif>
<cfelseif affichemodifparent eq "oui">
    <div class="row mt-5 content">
        <button type="button" class="box-message-active col-3 p-3 d-flex justify-content-center">Parents</button>
        <cfif afficheajoutenfant eq "oui">
            <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center" onclick="PrintAddChild()">Ajouter un enfant</button>
        </cfif>
<cfelse>
    <div class="row mt-5 content">
        <button type="button" class="box-message-inactive col-3 p-3 d-flex justify-content-center">Ajouter un enfant</button>
</cfif>
        <div class="p-2 p-lg-4 bg-white section scroll">
            <cfform name="mdparent1" id="mdparent1" method="post" onsubmit="return _CF_checkmdparent(this)">
                <div class="row d-flex justify-content-around change-infos">
                    <div class="p-3 m-2 m-lg-0 col-lg-5 bg-white section infos-parent">
                        <h5 class="bold img-plus d-flex align-items-center">Parent 1</h5>
                        <div>
                            <p class="border-new-section text-grey bold">Données personnelles</p>
                            <ul>
                                <li>
                                    Nom et prénom : <cfoutput>#lect_client.nom# #lect_client.prénom#</cfoutput>
                                </li>
                                <li>N° et adresse : 
                                    <cfinput name="NoRue" id="NoRue" type="text" value="#lect_client.NoRue#" maxlength="3" size="1"> 
                                    <cfinput name="adr1" id="adr1" type="text" required="yes" message="Saisir une adresse !" value="#lect_client.adr1#">
                                </li>
                                <li>Complément d'adresse : 
                                    <cfinput name="adr2" id="adr2" type="text" value="#lect_client.adr2#">
                                </li>
                                <li>Code Postal et ville : 
                                    <cfinput name="cp" id="cp" type="text" required="yes" message="Saisir un code postal !" value="#lect_client.cp#" size="5" onkeyup="mask_onValueChanged();" onfocus="mask_onSetFocus(this, '99999');" onblur="mask_onKillFocus();"> 
                                    <cfinput name="ville" id="ville" type="text" size="15" required="yes" message="Saisir une ville !" value="#lect_client.ville#">   
                                </li>
                            </ul>
                        </div>
                        <div>
                            <p class="border-new-section text-grey bold">Coordonnées</p>
                            <ul>
                                <li>Tel : 
                                    <cfinput name="tel" id="tel" type="text" value="#lect_client.tel#" size="10"> 
                                </li>
                                <li>Tel travail : 
                                    <cfinput name="teltravail" id="teltravail" type="text" value="#lect_client.teltravail#" size="10"> 
                                </li>
                                <li>Tel portable : 
                                    <cfinput name="telportable" id="telportable" type="text" value="#lect_client.telportable#" size="10"> 
                                </li>
                                <li>
                                    Email : <cfoutput>#lect_client.email#</cfoutput>
                                </li>
                                <li>N° Allocataire CAF : 
                                    <cfif #lect_client.numallocatairevalide# eq 0>
                                        <cfinput name="numallocataire" value="#lect_client.numallocataire#">
                                    <cfelse>
                                        <cfinput readonly="yes" name="numallocataire" value="#lect_client.numallocataire#">
                                    </cfif>
                                    <cfif #lect_client.numallocatairevalide# eq 1>
                                        <p class="text-small">Validé</p>
                                    <cfelse>
                                        <p class="text-small">En erreur ou MSA</p>
                                    </cfif>
                                </li>
                                <li>Quotient Familial : 
                                    <cfinput readonly="yes" name="QuotientFamilial" value="#lect_client.QuotientFamilial#" id="QuotientFamilial" type="text">
                                </li>
                                <li>Nom et Tel. de l'employeur : 
                                    <cfinput name="numtelemployeur" id="numtelemployeur" type="text" value="#lect_client.numtelemployeur#">
                                </li>
                            </div>

                            <div>
                                <cfif #lect_etablissement.saisieribonline# eq 1 and #lect_etablissement.siret# eq "45930643151811">
                                    <!--- berniere --->
                                    <p class="border-new-section text-grey bold">Pour adhérer au plélèvement, merci de saisir les informations ci-dessous :</p>

                                    <li>BIC : 
                                        <cfinput tooltip="BIC" size="20" name="bic" maxlength="11" value="#lect_client.bic#"> 
                                    </li>

                                    <li>
                                        IBAN : 
                                        <cfinput tooltip="Iban" maxlength="4" size="3" name="iban1" value="#mid(lect_client.iban,1,4)#">                  <cfinput tooltip="Iban" tabindex="17" maxlength="4" size="3" name="iban2" value="#mid(lect_client.iban,5,4)#">                  <cfinput tooltip="Iban" tabindex="18" maxlength="4" size="3" name="iban3" value="#mid(lect_client.iban,9,4)#">                  <cfinput tooltip="Iban" tabindex="19" maxlength="4" size="3" name="iban4" value="#mid(lect_client.iban,13,4)#">                 <cfinput tooltip="Iban" tabindex="20" maxlength="4" size="3" name="iban5" value="#mid(lect_client.iban,17,4)#">
                                        <cfinput tooltip="Iban" maxlength="4" size="3" name="iban6" value="#mid(lect_client.iban,21,4)#">
                                        <cfinput tooltip="Iban" maxlength="3" size="2" name="iban7" value="#mid(lect_client.iban,25,3)#">
                                    </li>
                                    
                                    <li>
                                        Banque : 
                                        <cfinput tooltip="Domiciliation" maxlength="100" name="ficapnom" value="#lect_client.ficapnom#">
                                    </li>
                                    <li>
                                        Titulaire du compte : 
                                        <cfinput tooltip="Titulaire du compte" name="bcnom" maxlength="100" value="#lect_client.bcnom#">
                                    </li>
                                    <li>
                                        <cfif #lect_client.LibPrel# eq "">
                                            <cfset lect_client.LibPrel = "Cantine Garderie">
                                        </cfif>
                                        Libellé de prélèvement : 
                                        <cfinput tooltip="Libellé du prélèvement" name="LibPrel" maxlength="140" value="#lect_client.LibPrel#">
                                    </li>
                                <cfelse>
                                    <cfinput type="hidden" name="bic" value="#lect_client.bic#">
                                    <cfinput type="hidden" name="iban1" value="#mid(lect_client.iban,1,4)#">
                                    <cfinput type="hidden" name="iban2" value="#mid(lect_client.iban,5,4)#">
                                    <cfinput type="hidden" name="iban3" value="#mid(lect_client.iban,9,4)#">
                                    <cfinput type="hidden" name="iban4" value="#mid(lect_client.iban,13,4)#">
                                    <cfinput type="hidden" name="iban5" value="#mid(lect_client.iban,17,4)#">
                                    <cfinput type="hidden" name="iban6" value="#mid(lect_client.iban,21,4)#">
                                    <cfinput type="hidden" name="iban7" value="#mid(lect_client.iban,25,3)#">
                                    <cfinput type="hidden" name="ficapnom" value="#lect_client.ficapnom#">
                                    <cfinput type="hidden" name="bcnom" value="#lect_client.bcnom#">
                                    <cfinput type="hidden" name="LibPrel" value="#lect_client.LibPrel#">
                                </cfif>

                                <li class="text-small">(Pour modifier l'adresse mail, le nom et prénom du parent 1, merci de contacter le <cfoutput>#lect_etablissement.tel#</cfoutput>)</li>
                                <cfinput type="hidden" name="id" value="#lect_client.id#">
                                <cfquery name="document" datasource="classactive">
                                    Select id,Désignation from documents 
                                    use index(siret_Id_Proprio_CodeAppli_Type_Proprio_Type_Doc) 
                                    where siret='#lect_etablissement.siret#' 
                                    and Id_Proprio='#lect_etablissement.id#' 
                                    and CodeAppli='F8' and Type_Proprio='etablissement' 
                                    and Type_Doc='REGLEMENT'
                                </cfquery>
                                <li class="text-red text-small">
                                    En cliquant sur Modifier vous déclarez sur l'honneur l'exactitude des informations saisies
                                    Je déclare avoir pris connaissance des documents ci-après et accepte les conditions générales d'utilisations et/ou les règlements intérieurs : 
                                    <cfoutput query="document"><a class="link-grey" href="/commun/VisuPdf.cfm?id=#document.id#" target="_blank">#document.désignation#</a>&nbsp;&nbsp;</cfoutput>
                                </li>
                                <li class="d-flex justify-content-center">
                                    <cfinput name="go1" id="go1" type="submit" value="Modifier" class="btn btn-primary">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="p-3 m-2 m-lg-0 col-lg-5 bg-white section infos-parent">
                    <h5 class="bold d-flex align-items-center">Parent 2</h5>
                        <div>
                            <p class="border-new-section text-grey bold">Données personnelles</p>
                            <ul>
                                <li>
                                    <cfselect name="titrecivil2">
                                        <cfif #lect_client.titrecivil2# eq 1>
                                            <option value="0">Mr</option>
                                            <option value="1" selected>Mme</option>
                                            <option value="3">Mr ou Mme</option>
                                        <cfelseif #lect_client.titrecivil2# eq 3>
                                            <option value="0">Mr</option>
                                            <option value="1">Mme</option>
                                            <option value="3" selected="selected">Mr ou Mme</option>                            
                                        <cfelse>
                                            <option value="0" selected>Mr</option>
                                            <option value="1">Mme</option>
                                            <option value="3">Mr ou Mme</option>                            
                                        </cfif>
                                    </cfselect> 
                                </li>
                                <li>
                                    Nom : 
                                    <cfinput name="nom2" id="nom2" type="text" value="#lect_client.nom2#"> 
                                </li>
                                <li>
                                    Prénom : 
                                    <cfinput name="prénom2" id="prénom2" type="text" value="#lect_client.prénom2#">
                                </li>
                                <li>N° et Adresse : 
                                    <cfinput name="NoRue2" id="NoRue2" type="text" value="#lect_client.NoRue2#" maxlength="3" size="1"> 
                                    <cfinput name="adr21" id="adr21" type="text" value="#lect_client.adr21#">
                                </li>
                                <li>Complément d'adresse : 
                                    <cfinput name="adr22" id="adr22" type="text" value="#lect_client.adr22#">
                                </li>
                                <li>Code Postal et ville : 
                                    <cfinput name="cp2" id="cp2" type="text" value="#lect_client.cp2#" size="5" onkeyup="mask_onValueChanged();" onfocus="mask_onSetFocus(this, '99999');" onblur="mask_onKillFocus();"> 
                                    <cfinput name="ville2" id="ville2" type="text" size="15" value="#lect_client.ville2#">   
                                </li>
                            </ul>
                        </div>
                        <div>
                            <p class="border-new-section text-grey bold">Coordonnées</p>
                            <ul>
                                <li>Tel : 
                                    <cfinput name="tel2" id="tel2" type="text" value="#lect_client.tel2#" size="10"> 
                                </li>
                                <li>Tel travail : 
                                    <cfinput name="teltravail2" id="teltravail2" type="text" size="10" value="#lect_client.teltravail2#"> 
                                </li>
                                <li>Tel portable : 
                                    <cfinput name="telportable2" id="telportable2" type="text" value="#lect_client.telportable2#" size="10"> 
                                </li>
                                <li>
                                    Email : <cfoutput>#lect_client.email2#</cfoutput>
                                </li>
                                <li>Nom et Tel. de l'employeur : 
                                    <cfinput name="numtelemployeur2" id="numtelemployeur2" type="text" value="#lect_client.numtelemployeur2#">
                                </li>
                                <li class="text-small">
                                    (Pour modifier l'adresse mail, le nom et prénom du parent 2, merci de contacter le <cfoutput>#lect_etablissement.tel#</cfoutput>)
                                </li>
                                <li class="text-red text-small">
                                    En cliquant sur Modifier vous déclarez sur l'honneur l'exactitude des informations saisies
                                    Je déclare avoir pris connaissance des documents ci-après et accepte les conditions générales d'utilisations et/ou les règlements intérieurs : 
                                    <cfoutput query="document"><a class="link-grey" href="/commun/VisuPdf.cfm?id=#document.id#" target="_blank">#document.désignation#</a>&nbsp;&nbsp;</cfoutput>
                                </li>
                                <li class="d-flex justify-content-center">
                                    <cfinput name="go2" id="go2" type="submit" value="Modifier" class="btn btn-primary">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </cfform>
        </div>
    </div>