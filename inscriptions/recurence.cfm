﻿<cfset msg = "">
<cfset msg1 = "">
<cfinclude template="/Query_Header.cfm">
<cfif isdefined("form.go")>
    <cfquery name="lect_enfant" datasource="f8w_test">
        select concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#form.sav_idenfant#'
    </cfquery>        
    <cfquery name="lect_prestation" datasource="f8w_test">
		Select * from prestation Where id = '#form.sav_idprestation#'
	</cfquery>
	<cfif #form.choix# eq "1"><!--- pas de recurence --->
    	<cflocation url="/inscriptions/?id_mnu=22&id_prestation=#form.sav_idprestation#&id_enfant=#form.sav_idenfant#&msg=#urlencodedformat(form.msg)#&msg1=#urlencodedformat(form.msg1)#" addtoken="no">
    <cfelse><!--- le bordel commence --->
		<cfif #form.action# neq "desinscr"><!--- verification si quota --->
			<!--- Quota ??? --->
            <cfquery name="enfantclasse" datasource="f8w_test">
                Select NumClasse From enfantclasse use index(siret_NumEnfant) Where siret = '#lect_user.siret#' 
                and NumEnfant = '#Form.NumEnfant#'
            </cfquery>
            <!--- modif 16/01/2020 --->
            <cfset r = queryaddrow(enfantclasse)>
            <cfset r = querysetcell(enfantclasse,"NumClasse",0)>
            <!--- --->
            <cfset NumLieuPrestation = 0>
            <cfset NumLieuPrestationQuota = 0>
            <cfset NumClassePrestationQuota = 0>
            <cfif #enfantclasse.recordcount# gt 0>         
                <cfloop query="enfantclasse">
                    <cfquery name="lieuprestationclasse" datasource="f8w_test" maxrows="1">
                        Select NumLieu,NumClasse,Quota from lieuprestationclasse use index(siret_NumClasse_NumPrestation) 
                        Where siret = '#lect_user.siret#' 
                        and NumClasse = '#enfantclasse.NumClasse#' and NumPrestation = '#Form.NumPrestation#' limit 0,1
                    </cfquery>
                    <cfif #lieuprestationclasse.recordcount#>
                        <cfset NumLieuPrestation = #lieuprestationclasse.NumLieu#>
                        <cfset NumLieuPrestationQuota = #lieuprestationclasse.Quota#>
                        <cfset NumClassePrestationQuota = #lieuprestationclasse.NumClasse#>
                        <cfbreak>
                    </cfif>
				</cfloop>
            </cfif>        
			<cfif #NumLieuPrestationQuota# gt 0 or #NumClassePrestationQuota# gt 0><!--- Quota --->
        		<!--- ont test le jour concerné sur 3 mois dans futur pour voir si recurence possible --->
        		<cfset dateapplicationFuture = createdate(mid(#form.datejour#,7,4),mid(#form.datejour#,4,2),mid(#form.datejour#,1,2))>
                <cfset Jourapplication = LsdateFormat(#dateapplicationFuture#,"dddd")>
                <cfloop from="1" to="12" index="idx">
        			<cfset dateapplicationFuture = DateAdd("d",7,#dateapplicationFuture#)>
        			<cfset txtdateapplicationFuture = LsdateFormat(#dateapplicationFuture#,"dd/MM/YYYY")>
                	<cfset Msgquota = Module.JourPrestationLieuQuota(#lect_user.siret#,#txtdateapplicationFuture#,#form.NumPrestation#,#NumLieuPrestation#,#NumClassePrestationQuota#)>
					<cfif #Msgquota# gte #NumLieuPrestationQuota#>
        				<cfset msg = "Désolé, vous ne pouvez pas inscrire " & #lect_enfant.lenfant# & " pour l'instant de manière régulière à cette prestation tous les " & #Jourapplication# & " . ">
        				<cfset msg = #msg# & "Dans les semaines qui viennent, certains " & #Jourapplication# & " sont complets (Nombre de place Maxi : "> 
                        <cfset msg = #msg# & #NumLieuPrestationQuota# & " )">
                        
                    	<cflocation url="/inscriptions/?id_mnu=22&id_prestation=#form.sav_idprestation#&id_enfant=#form.sav_idenfant#&msg=#urlencodedformat(msg)#&msg1=#urlencodedformat(msg1)#" addtoken="no">
        			</cfif>
        		</cfloop>
        	</cfif>
            <!--- gestion des délais spéciaux 19/12/2019 --->
			<!--- pas de resa recure possible pour un jour si ce jour a un delai spécial dans le futur --->
            <cfset dateapplicationFuture = createdate(mid(#form.datejour#,7,4),mid(#form.datejour#,4,2),mid(#form.datejour#,1,2))>
            <cfset dateapplicationFutureSTP = lsdateformat(#dateapplicationFuture#,"YYYYMMdd")>
            <cfset datejourstp = lsdateformat(now(),"YYYYMMdd") & "00">
            <cfset Jourapplication = LsdateFormat(#dateapplicationFuture#,"dddd")>        
            <cfquery name="ajustresaweb_date" datasource="f8w_test">
                Select * from ajustresaweb_date use index(siret) where siret='#lect_user.siret#' 
                and numprestation='#Form.NumPrestation#' 
                and DateApartirSTP<='#datejourstp#' and DatePourSTP>='#dateapplicationFutureSTP#'
            </cfquery>
            <cfif #ajustresaweb_date.recordcount#>
                <cfloop from="1" to="15" index="idx">
                    <cfset dateapplicationFuture = DateAdd("d",7,#dateapplicationFuture#)>
                    <cfset txtdateapplicationFuture = LsdateFormat(#dateapplicationFuture#,"dd/MM/YYYY")>
                    <cfquery name="verif" dbtype="query">
                        Select * from ajustresaweb_date where datepour='#txtdateapplicationFuture#'
                    </cfquery>
                    <cfif #verif.recordcount#>
                        <cfset msg = "Désolé, vous ne pouvez pas inscrire " & #lect_enfant.lenfant# & " pour l'instant de manière régulière à cette prestation tous les " & #Jourapplication# & " . ">
                        <cfset msg = #msg# & "Dans les semaines qui viennent, certains " & #Jourapplication# & " sont bloqués à la réservation (exemple le : " & #txtdateapplicationFuture# & ")"> 
                        <cflocation url="/inscriptions/?id_mnu=22&id_prestation=#form.sav_idprestation#&id_enfant=#form.sav_idenfant#&msg=#urlencodedformat(msg)#&msg1=#urlencodedformat(msg1)#" addtoken="no">
                    </cfif>
                </cfloop>
            </cfif>    
		</cfif>        
		<!--- recupération des enreg inscription recurentes trié du futur vers le passé --->
        <cfquery name="lect_inscription" datasource="f8w_test"><!--- selection des inscriptions de l'enfant pour la prestation --->
            Select * from inscription use index(siret_Numenfant) Where siret = '#lect_user.siret#' and NumEnfant = '#form.NumEnfant#' 
            and Numprestation = '#form.Numprestation#' Order by DatedebSTP DESC
        </cfquery>
		<cfset datestampapplication = mid(#form.datejour#,7,4) & mid(#form.datejour#,4,2) & mid(#form.datejour#,1,2)>
		<cfset dateapplication = createdate(mid(#form.datejour#,7,4),mid(#form.datejour#,4,2),mid(#form.datejour#,1,2))>
        <cfset NumJourdateapplication = DayOfWeek(#dateapplication#)>
        <cfset TxtJourdateapplication = lsdateformat(#dateapplication#,"dddd")>
        <cfset semaineJourdateapplication = Week(#dateapplication#)>
        <cfloop query="lect_inscription"><!--- ont boucle sur inscription récurente --->
        	<cfset dateapplicationenregencour = createdate(mid(#lect_inscription.DateDeb#,7,4),mid(#lect_inscription.DateDeb#,4,2),mid(#lect_inscription.DateDeb#,1,2))>
			<!--- reconstruction de InscriptionImpaire avec prise en compte de la modif --->
            <cfset NewInscriptionImpaire = 0>
            <cfloop from="1" to="7" index="NumJourAtester">
                <cfif #NumJourAtester# eq #NumJourdateapplication#><!--- ont est sur le jour concerné par l'inscription --->
					<cfset JourConcerné = true>
                <cfelse>
                	<cfset JourConcerné = False>
                </cfif>
                <cfset a = Module.Resajour(#lect_inscription.InscriptionImpaire#,#NumJourAtester#)>
				<cfif #a# eq 1><!--- jour inscrit --->
					<cfif #form.action# eq "desinscr">
						<cfif #JourConcerné# is false><!--- si ont demande une désincription et que l'ont est pas sur le jour concerné et que celui ci est inscrit ont inscrit --->
							<cfif #semaineJourdateapplication# eq Week(#dateapplicationenregencour#)><!--- meme semaine --->
                            	<!--- exemple inscri recurente date application lundi 16/09
								ont clique pour desinscrire vendredi 20/09
								ça ne marchais pas--->
                                
                            <cfelse>
								<cfset NewInscriptionImpaire = #NewInscriptionImpaire# + 2 ^ (#NumJourAtester# - 1)>
                			</cfif>
                        </cfif>
                	<cfelse>
                    	<cfset NewInscriptionImpaire = #NewInscriptionImpaire# + 2 ^ (#NumJourAtester# - 1)>
                    </cfif>
            	<cfelse><!--- jour pas inscrit --->
					<cfif #form.action# eq "inscr" or #form.action# eq "reinscr">
						<cfif #JourConcerné# is true><!--- si ont demande une incription et que l'ont est sur le jour concerné ont inscrit --->
							<cfset NewInscriptionImpaire = #NewInscriptionImpaire# + 2 ^ (#NumJourAtester# - 1)>
                		</cfif>
                    </cfif>
                </cfif>
            </cfloop>
			<cfif #lect_inscription.DateDebStp# gte #datestampapplication#><!--- pour le futur et le jour J par raport au jour selectionné ont ajoute la modif pour le jour selectionné et update--->
                <!---<cfif #NewInscriptionImpaire# gt 0>--->
                    <cfquery name="maj" datasource="f8w_test">
                        Update inscription set InscriptionImpaire = '#NewInscriptionImpaire#', NumLieu = '#url.NumLieuPrestation#' 
                        where id = '#lect_inscription.id#'
                    </cfquery>
                    <cfif #form.action# eq "desinscr">
						<cfif #session.log_mairie# is true>
                            <cfset log_action_web = "Désinscription RECURENTE par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                        <cfelse>
                            <cfset log_action_web = "Désinscription RECURENTE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                        </cfif>                    	                    
					<cfelse>
						<cfif #session.log_mairie# is true>
                            <cfset log_action_web = "Inscription RECURENTE par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                        <cfelse>
                            <cfset log_action_web = "Inscription RECURENTE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                        </cfif>                    	                                        
                    </cfif>
                    <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                    <cfset log_action_web = #log_action_web# & "<br>Tous les " & #TxtJourdateapplication# & " à partir du " & #form.datejour#>
					<cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                    <cfset dtapplication = #form.datejour#>
                    <cfquery name="log" datasource="f8w_test">
                        Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#',
                        '#session.use_id#',<cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,
                        '#dtaction#','#dtapplication#','#form.sav_idprestation#')
                    </cfquery>
					<cfif #lect_etablissement.FullWeb# eq 0>
						<cfset req_sql = "Update inscription set InscriptionImpaire =" & #NewInscriptionImpaire#>
                        <cfset req_sql = #req_sql# & " Where NumInscription=" & #lect_inscription.NumInscription#>
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (
                            <cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,'#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>
					</cfif>
				<cfif #lect_inscription.DateDebStp# eq #datestampapplication#><!--- ont esttombé sur jour J ont arrete le traitement --->
                	<cfbreak>
                </cfif>
            <cfelse><!--- ont prend l'inscription et ont ajoute la modif pour le jour selectionné puis insert a ladate selectionné et ont sort --->
        <!---    	<cfif #NewInscriptionImpaire# gt 0> --->
                    <cfif #semaineJourdateapplication# eq Week(#dateapplicationenregencour#)><!--- meme semaine ont update --->
                        <cfquery name="maj" datasource="f8w_test">
                            Update inscription set InscriptionImpaire = '#NewInscriptionImpaire#', NumLieu = '#url.NumLieuPrestation#' 
                            where id = '#lect_inscription.id#'
                        </cfquery>
						<cfif #form.action# eq "desinscr">
                            <cfif #session.log_mairie# is true>
                                <cfset log_action_web = "Désinscription RECURENTE par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            <cfelse>
                                <cfset log_action_web = "Désinscription RECURENTE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            </cfif>                    	                    
                        <cfelse>
                            <cfif #session.log_mairie# is true>
                                <cfset log_action_web = "Inscription RECURENTE par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            <cfelse>
                                <cfset log_action_web = "Inscription RECURENTE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            </cfif>                    	                                        
                        </cfif>
                        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                        <cfset log_action_web = #log_action_web# & "<br>Tous les " & #TxtJourdateapplication# & " à partir du " & #form.datejour#>
                        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfset dtapplication = #form.datejour#>
                        <cfquery name="log" datasource="f8w_test">
                            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#',
                            '#session.use_id#',<cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,
                            '#dtaction#','#dtapplication#','#form.sav_idprestation#')
                        </cfquery>                        
						<cfif #lect_etablissement.FullWeb# eq 0>
							<cfset req_sql = "Update inscription set InscriptionImpaire =" & #NewInscriptionImpaire#>
                            <cfset req_sql = #req_sql# & " Where NumInscription=" & #lect_inscription.NumInscription#>
                            <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                            <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                            <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                            <cfquery name="SYNCHRO" datasource="services">
                                Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (
                                <cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,'#lect_user.siret#','#datesoumission#','F8')
                            </cfquery>
						</cfif>
                    <cfelse><!--- sinon ont creer --->
						<cfset NumInscription1 = Module.NumSuivant6(#lect_user.siret#,"inscription","NumInscription")>
                        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfset dtapplication = #form.datejour#>                  
                        <cfquery name="cre" datasource="f8w_test">
                            insert into INSCRIPTION(NumInscription,Numenfant,NumPrestation,InscriptionImpaire,InscriptionPaire,NoSemaineDeb,
                            AnnéeDeb,DateDeb,NonFacturable,siret,DatedebSTP,NumLieu) values ('#numinscription1#','#form.numenfant#','#form.numprestation#',
                           '#NewInscriptionImpaire#','0','0','0','#form.datejour#','0','#lect_user.siret#','#datestampapplication#',
                            '#url.NumLieuPrestation#')
                        </cfquery>
						<cfif #form.action# eq "desinscr">
                            <cfif #session.log_mairie# is true>
                                <cfset log_action_web = "Désinscription RECURENTE par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            <cfelse>
                                <cfset log_action_web = "Désinscription RECURENTE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            </cfif>                    	                    
                        <cfelse>
                            <cfif #session.log_mairie# is true>
                                <cfset log_action_web = "Inscription RECURENTE par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            <cfelse>
                                <cfset log_action_web = "Inscription RECURENTE par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                            </cfif>                    	                                        
                        </cfif>
                        <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lect_prestation.Désignation#>
                        <cfset log_action_web = #log_action_web# & "<br>Tous les " & #TxtJourdateapplication# & " à partir du " & #form.datejour#>
                        <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfset dtapplication = #form.datejour#>
                        <cfquery name="log" datasource="f8w_test">
                            Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values ('#lect_user.siret#',
                            '#session.use_id#',<cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,
                            '#dtaction#','#dtapplication#','#form.sav_idprestation#')
                        </cfquery>                        
                        <cfif #lect_etablissement.FullWeb# eq 0>
							<cfset req_sql = "insert into INSCRIPTION(NumInscription,Numenfant,NumPrestation,InscriptionImpaire,InscriptionPaire,NoSemaineDeb,AnnéeDeb,DateDeb,NonFacturable) values (">
                            <cfset req_sql = #req_sql# & #numinscription1# & "," & #form.numenfant# & "," & #form.numprestation# & ",">
                            <cfset req_sql = #req_sql# & #NewInscriptionImpaire# & ",0,0,0,'" & #form.datejour# & "',0)">
                            <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                            <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                            <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                            <cfquery name="SYNCHRO" datasource="services">
                            	Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (
                            	<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,'#lect_user.siret#','#datesoumission#','F8')
                            </cfquery>
						</cfif>
					</cfif>
            	<cfbreak><!--- ont sort --->
            </cfif>
		</cfloop>
		<cfif #lect_inscription.recordcount# eq 0><!--- pas d'inscription donc insert --->
        	<cfif #form.action# eq "inscr" or #form.action# eq "reinscr"><!--- recurence en inscription --->    
                <cfquery name="lect_enfant" datasource="f8w_test">
                    select numenfant,Prénom, concat_ws(" ",Prénom,Nom) as lenfant from enfant where id = '#form.sav_idenfant#'
                </cfquery>
                <cfquery name="lectpresta" datasource="f8w_test">
                    select numprestation,Désignation from prestation where id = '#form.sav_idprestation#'
                </cfquery>
                <cfif #session.log_mairie# is true>
                    <cfset log_action_web = "Création d'une inscription récurente par la mairie pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>                    
                <cfelse>
                    <cfset log_action_web = "Création d'une inscription récurente par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                </cfif>
                <cfset log_action_web = #log_action_web# & "<br>Jours concerné(s) :<br>">
                <cfset JourSélectionné = 0>
                <cfset NombreJourSélectionné = 0>
                <cfswitch expression="#NumJourdateapplication#">
                    <cfcase value="1">		
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 0>
                        <cfset log_action_web = #log_action_web# & "Dimanche, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                    <cfcase value="2">
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 1>
                        <cfset log_action_web = #log_action_web# & "Lundi, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                    <cfcase value="3">
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 2>
                        <cfset log_action_web = #log_action_web# & "Mardi, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                    <cfcase value="4">
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 3>
                        <cfset log_action_web = #log_action_web# & "Mercredi, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                    <cfcase value="5">
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 4>
                        <cfset log_action_web = #log_action_web# & "Jeudi, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                    <cfcase value="6">
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 5>
                        <cfset log_action_web = #log_action_web# & "Vendredi, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                    <cfcase value="7">
                        <cfset JourSélectionné = #JourSélectionné# + 2 ^ 6>
                        <cfset log_action_web = #log_action_web# & "Samedi, ">
                        <cfset NombreJourSélectionné = #NombreJourSélectionné# + 1>
                    </cfcase>
                </cfswitch>                  	
                <cfset NumInscription = Module.NumSuivant6(#lect_user.siret#,"inscription","NumInscription")>
                <cfset log_action_web = #log_action_web# & "<br>Pour " & #lect_enfant.lenfant# & " à la prestation " & #lectpresta.Désignation#>
                <cfset log_action_web = #log_action_web# & "<br>Application le " & #form.datejour#>
                <cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                <cfset dtapplication = #form.datejour#>                  
                <cfquery name="log" datasource="f8w_test">
                    Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values
                     ('#lect_user.siret#','#session.use_id#',<cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#',
                    '#dtapplication#','#form.sav_idprestation#')
                </cfquery>
                <cfquery name="cre" datasource="f8w_test">
                    insert into INSCRIPTION(NumInscription,Numenfant,NumPrestation,InscriptionImpaire,InscriptionPaire,NoSemaineDeb,
                    AnnéeDeb,DateDeb,NonFacturable,siret,DatedebSTP,NumLieu) values ('#numinscription#','#lect_enfant.numenfant#',
                    '#lectpresta.numprestation#','#JourSélectionné#','0','0','0','#form.datejour#','0','#lect_user.siret#',
                    '#datestampapplication#','#url.NumLieuPrestation#')
                </cfquery>
                <cfif #lect_etablissement.FullWeb# eq 0>
					<cfset req_sql = "insert into INSCRIPTION(NumInscription,Numenfant,NumPrestation,InscriptionImpaire,InscriptionPaire,NoSemaineDeb,AnnéeDeb,DateDeb,NonFacturable) values (">
                    <cfset req_sql = #req_sql# & #numinscription# & "," & #lect_enfant.numenfant# & "," & #lectpresta.numprestation# & ",">
                    <cfset req_sql = #req_sql# & #JourSélectionné# & ",0,0,0,'" & #form.datejour# & "',0)">
                    <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                    <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                    <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                    <cfquery name="SYNCHRO" datasource="services">
                        Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                        '#lect_user.siret#','#datesoumission#','F8')
                    </cfquery>
				</cfif>
            </cfif>
        </cfif>
        <cfif #form.action# eq "desinscr"><!--- en desinscription donc ont desincrit tout les ponctuel futur pour ce jour --->
        	<cfquery name="inscriptionplanning" datasource="f8w_test">
        		Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) Where siret = '#lect_user.siret#' 
                and NumPrestation = '#form.NumPrestation#' and NumEnfant = '#form.NumEnfant#' and DateInscriptionSTP >= #datestampapplication#
        	</cfquery>
            <cfloop query="inscriptionplanning">
                <cfset DateInscriptionplanningEnreg = createdate(mid(#inscriptionplanning.DateInscription#,7,4),mid(#inscriptionplanning.DateInscription#,4,2),mid(#inscriptionplanning.DateInscription#,1,2))>
                <cfif DayOfWeek(#DateInscriptionplanningEnreg#) eq #NumJourdateapplication#><!--- meme jour --->
					<cfif #session.log_mairie# is true>
                        <cfset log_action_web = "Suppression d'une inscription ponctuelle par la mairie suite à une suppression d'inscription récurente pour les " & #TxtJourdateapplication# & " pour " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>                    
                    <cfelse>
                        <cfset log_action_web = "Suppression d'une inscription ponctuelle suite à une suppression d'inscription récurente pour les " & #TxtJourdateapplication# & " par " & #lect_client.Nom# & " " & #lect_client.Prénom# & " le " & lsdateformat(now(),"dd/MM/YYYY") & " à " & lstimeformat(now(),"HH:mm:ss")>
                    </cfif>
                	<cfset log_action_web = #log_action_web# & "<br>Jours concerné(s) :<br>" & #inscriptionplanning.DateInscription# & "<br>">
					<cfset dtaction = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                    <cfset dtapplication = #form.datejour#>                  
                    <cfquery name="log" datasource="f8w_test">
                        Insert into log_action_web (siret,id_user,Detail,Dateactionstp,Dateapplicationstp,id_prestation) Values
                         ('#lect_user.siret#','#session.use_id#',<cfqueryparam value="#log_action_web#" cfsqltype="cf_sql_longvarchar">,'#dtaction#',
                        '#dtapplication#','#form.sav_idprestation#')
                    </cfquery>                
                    <cfquery name="del" datasource="f8w_test">
                        Delete from inscriptionplanning where id = '#inscriptionplanning.id#'
                    </cfquery>
                    <cfif #lect_etablissement.FullWeb# eq 0>
						<cfset req_sql = "delete from inscriptionplanning where Numinscription=" & #inscriptionplanning.NumInscription#>
                        <cfset req_sql = #req_sql# & " and numprestation=" & #inscriptionplanning.numprestation# & " and numenfant=">
                        <cfset req_sql = #req_sql# & #inscriptionplanning.numenfant# >
                        <cfset req1 = replacenocase(#req_sql#,"'","lolocotlolo","all")>
                        <cfset req2 = replacenocase(#req1#,",","lolovirgulelolo","all")>
                        <cfset datesoumission = lsdateformat(now(),"dd/MM/YYYY") & " " & lstimeformat(now(),"HH:mm:ss")>
                        <cfquery name="SYNCHRO" datasource="services">
                            Insert into synchro (ReqSQL,BaseDeDonnées,DateSoumission,CodeAppli) Values (<cfqueryparam value="#req2#" cfsqltype="cf_sql_varchar">,
                            '#lect_user.siret#','#datesoumission#','F8')
                        </cfquery>
					</cfif>
				</cfif>            
            </cfloop>
        </cfif> 
        <cfif isdefined("form.unesemainesurdeux")>
			<cfif #form.unesemainesurdeux# eq 1 and #form.choix# eq 2><!--- garde alternée --->
                <cfset jouradesinscrire = createdate(mid(#form.datejour#,7,4),mid(#form.datejour#,4,2),mid(#form.datejour#,1,2))>
                <cfloop from="1" to="22" index="idx">
                    <!--- ont boucle sur 22 (11 mois maxi à 2 jours par mois)
                    pour désinscrire ce jour une semaine sur deux --->
                    <cfif #idx# eq 1>
                        <cfset jouradesinscrire = dateadd("d",7,#jouradesinscrire#)>
                    <cfelse>
                        <cfset jouradesinscrire = dateadd("d",14,#jouradesinscrire#)>
                    </cfif>
                    <cfif month(#jouradesinscrire#) eq 7 and day(#jouradesinscrire#) gte 10>
                        <!--- ont traite pas plus loin que le 10 juillet --->
                        <cfbreak>
                    </cfif>
                    <cfset NumInscription = Module.NumSuivant6(#lect_user.siret#,"inscriptionplanning","NumInscription")>
                    <cfset datedebstp = lsdateformat(#jouradesinscrire#,"YYYYMMdd")>
                    <cfset datejour = lsdateformat(#jouradesinscrire#,"dd/MM/YYYY")>
                    <cfquery name="add" datasource="f8w_test" result="leresultat">
                        Insert into inscriptionplanning (siret,NumInscription,numprestation,numenfant,dateinscription,Nature,
                        DateInscriptionSTP) Values ('#lect_user.siret#','#NumInscription#','#form.NumPrestation#',
                        '#form.NumEnfant#','#datejour#',1,'#datedebstp#')
                    </cfquery>
                </cfloop>
            </cfif>     
		</cfif>
    	<cflocation url="/inscriptions/?id_mnu=22&id_prestation=#form.sav_idprestation#&id_enfant=#form.sav_idenfant#&msg=#urlencodedformat(form.msg)#&msg1=#urlencodedformat(form.msg1)#" addtoken="no">    
    </cfif>
</cfif>

<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Inscription à la prestation</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

	<body onLoad="changeHashOnLoad();">
        <div id="bgBlur">
            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>

            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-7 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p><h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>
                            #lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3></p>
                        <p class="description ps-5 pe-5">Gérez ci-dessous les inscriptions de vos enfants.</p>
                    </div>
                    <!-- CONTENT -->

                    <div class="row d-flex justify-content-between mt-5 content">
                        <div class="p-2 p-lg-5 bg-white section scroll">
                            <cfform name="log" method="post">
                                <p class="text-red bold mb-1">Pour terminer votre modification, choisissez entre ces deux options en cliquant sur le bouton radio de votre choix</p>
                                <p class="text-red bold">Puis le bouton valider:</p>
                                <cfquery name="prestation" datasource="f8w_test">
                                    Select désignation from prestation where id='#url.id_prestation#'
                                </cfquery>
                                <cfquery name="enfant" datasource="f8w_test">
                                    Select prénom,sexe from enfant where id='#url.id_enfant#'
                                </cfquery>
                                <cfif #enfant.sexe# eq "F">
                                    <cfset sexenfant = "e">
                                <cfelse>
                                    <cfset sexenfant = "">
                                </cfif>                          
                                <cfset dateaff = createdate(#mid(url.datejour,7,4)#,#mid(url.datejour,4,2)#,#mid(url.datejour,1,2)#)>
                                <cfswitch expression="#url.action#">                          
                                    <cfcase value="inscr,reinscr"><!--- inscription --->
                                        <p class="mb-1"><cfinput type="radio" name="choix" value="1" checked="yes">
                                            <cfoutput> #enfant.prénom# <span class="text-red bold">sera présent#sexenfant#</span> à la #prestation.désignation# ce #lsdateformat(dateaff,"dddd")# #lsdateformat(dateaff,"dd")# #lsdateformat(dateaff,"mmmm")# #lsdateformat(dateaff,"yyyy")#</cfoutput> <span class="text-red">(action effectuée)</span>.
                                        </p>
                                        <p class="text-small">Cette modification a bien été enregistrée, choisissez cette option et <span class="bold text-red">cliquez sur le bouton valider pour retourner au menu "Réservations"</span>.</p>
                                        
                                        <p>Ou :</p>
                                        
                                        <p class="mb-1"><cfinput type="radio" name="choix" value="2" checked="no">
                                            <cfoutput> #enfant.prénom# <span class="text-red bold"> sera présent#sexenfant#</span> à la #prestation.désignation# le #lsdateformat(dateaff,"dddd")# #lsdateformat(dateaff,"dd")# #lsdateformat(dateaff,"mmmm")# #lsdateformat(dateaff,"yyyy")#</cfoutput><span class="text-red bold"> et tous les <cfoutput>#lsdateformat(dateaff,"dddd")#s</cfoutput> suivants</span>.
                                            <!---<p><b><cfinput type="radio" name="unesemainesurdeux" value="0" checked="yes"> et tous les <cfoutput>#lsdateformat(dateaff,"dddd")#s</cfoutput> suivants</b></p>
                                            <p><b><cfinput type="radio" name="unesemainesurdeux" value="1" checked="no"> et tous les <cfoutput>#lsdateformat(dateaff,"dddd")#s</cfoutput> suivants, une semaine sur deux (garde alternée)</b></p>--->
                                        </p>
                                        <p class="text-small"><cfoutput>#enfant.prénom# sera présent#sexenfant# de manière régulière à la #prestation.désignation# tous les #lsdateformat(dateaff,"dddd")# à partir du #url.datejour#</cfoutput>,choisissez cette option <!---, cocher éventuellement en plus 
                                        <cfinput type="checkbox" name="unesemainesurdeux" value="1" checked="no"><font color="#FF0000"><b> l'option une semaine sur deux (garde alternée)</b></font>  --->et <span class="text-red bold">cliquez sur le bouton valider pour enregistrer cette modification</span>.</p>
                                    </cfcase>
                                    <cfcase value="desinscr"><!--- desinscription --->
                                        <p class="mb-1"><cfinput type="radio" name="choix" value="1" checked="yes">
                                            <cfoutput>#enfant.prénom# <span class="text-red bold">sera absent#sexenfant#</span> à la #prestation.désignation# le #lsdateformat(dateaff,"dddd")# #lsdateformat(dateaff,"dd")# #lsdateformat(dateaff,"mmmm")# #lsdateformat(dateaff,"yyyy")#</cfoutput> seulement. <span class="text-red">(action effectuée)</span>.
                                        </p>
                                        <p class="text-small">Cette modification a bien été enregistrée, choisissez cette option et <span class="text-red bold">cliquez sur le bouton valider pour retourner au menu "Réservations"</span>.</p>

                                        <p>Ou :</p>

                                        <p class="mb-1"><cfinput type="radio" name="choix" value="2" checked="no">
                                            <cfoutput>#enfant.prénom# <span class="text-red bold">sera absent#sexenfant#</span> à la #prestation.désignation# le #lsdateformat(dateaff,"dddd")# #lsdateformat(dateaff,"dd")# #lsdateformat(dateaff,"mmmm")# #lsdateformat(dateaff,"yyyy")#</cfoutput> <span class="text-red bold">et tous les <cfoutput>#lsdateformat(dateaff,"dddd")#s</cfoutput> suivants</span>.
                                        </p>
                                        <p class="text-small"><cfoutput>#enfant.prénom# ne sera plus inscrit#sexenfant# à la #prestation.désignation# tous les #lsdateformat(dateaff,"dddd")# à partir du #url.datejour#</cfoutput>,choisissez cette option et <span class="text-red bold">cliquez sur le bouton valider pour enregistrer cette modification</span>.</p>
                                    </cfcase>
                                </cfswitch>
                                <cfinput class="btn btn-primary text-center" type="submit" name="go" value="Valider">

                                <cfinput type="hidden" name="sav_idprestation" value="#url.id_prestation#">
                                <cfinput type="hidden" name="sav_idenfant" value="#url.id_enfant#">
                                <cfinput type="hidden" name="msg" value="#URLDecode(url.msg)#">
                                <cfinput type="hidden" name="msg1" value="#URLDecode(url.msg1)#">
                                <cfinput type="hidden" name="numjour" value="#url.numjour#">
                                <cfinput type="hidden" name="datejour" value="#url.datejour#">
                                <cfinput type="hidden" name="numenfant" value="#url.numenfant#">
                                <cfinput type="hidden" name="numprestation" value="#url.numprestation#">
                                <cfinput type="hidden" name="action" value="#url.action#">
                                <cfinput type="hidden" name="nature" value="#url.nature#">
                            </cfform>
                        </div>
                    </div>
                </div>
                <!-- SECTION RIGHT -->
                <div class="col-lg-3 section-right">
                    <div class="mt-5">
                        <cfinclude template="/home/calendar.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions légales et confidentialité</a>
                <p class="m-0 pe-3 ps-3">N° de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" alt="bouton menu déroulant" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">

        <script type="text/javascript"> //Script gestion menu burger
            function openNav() {
                document.getElementById("navMobileItems").style.display= "block";
                document.getElementById("navMobileLabel").style.display= "none";
                const background = document.getElementById("bgBlur");
                background.className = "bg-blur";
                background.addEventListener("click", closeNav);
            }
            function closeNav() {
                document.getElementById("navMobileItems").style.display= "none";
                document.getElementById("bgBlur").classList.remove("bg-blur");
                document.getElementById("navMobileLabel").style.display= "block";
            }
            //The button onClick handler displays the message boxes. 
            function showMB(mbox)  { 
                ColdFusion.MessageBox.show(mbox); 
            }
            function changeHashOnLoad() { 
                window.location.href += "#"; setTimeout("changeHashAgain()", "50"); 
            } 
            function changeHashAgain() { 
                window.location.href += "1"; 
            } 
            var storedHash = window.location.hash; 
            window.setInterval(function () {
                if (window.location.hash != storedHash) { 
                    window.location.hash = storedHash; 
                } 
            }, 50); 
        </script>
    </body>
</html>