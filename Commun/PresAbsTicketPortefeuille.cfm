﻿    <cfif #url.siret# eq "DEMOLaurent">
    	<cffile action="write" file="c:\tempWeb\Logredpres.txt" output="">
    </cfif>
    <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
        Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
        where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
    </cfquery>
	<cfquery name="enfantclient" datasource="f8w">
    	Select * from enfant use index(siret_numclienti) where siret='#url.siret#' and NumClienti='#enfant.NumClientI#' 
        and parti=0 and del=0
    </cfquery>
    <cfquery name="prix" datasource="f8w">
        Select * from prixprestation use index(siret_NumPrestation) 
        Where siret='#url.siret#' and NumPrestation='#url.prestation#' and CodePrix=1 and del=0
    </cfquery>
    <cfset leprix.pu = #prix.pu#>
    <cfif #url.siret# eq "DEMOLaurent">
    	<cfset txt = "PrésentAbsent = " & #url.PresentAbsent# & #chr(10)# & #chr(13)#>
    	<cfset txt = #txt# & " Prix " & #prix.désignation# & " " & #leprix.pu# & " CodePrix " & #prix.CodePrix# & #chr(13)#>
    	<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
    </cfif>
    <cfif #prix.NonRéductionBarème# eq 0>
        <cfquery name="reductionachat" datasource="f8w">
            Select * from reductions use index(siret_numprestation) 
            where siret='#url.siret#' 
            and NumPrestation='#url.prestation#' 
            and QfDe<='#client.QuotientFamilial#' and QfA>='#client.QuotientFamilial#' 
            and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
        </cfquery>
        <cfloop query="reductionachat">
            <cfif #reductionachat.GroupeClient# gt 0>
				<cfif #url.siret# eq "DEMOLaurent">
                    <cfset txt = "Ligne 32 réduction " & #reductionachat.désignation# & #chr(13)#>
                    <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                </cfif>            
            	<cfquery name="verifgroupe" datasource="f8w">
                	Select id from groupeclient use index(siret_numgroupe)
                    Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' and del=0
                </cfquery>
                <cfif #verifgroupe.recordcount#>
                    <cfquery name="verifclient" datasource="f8w">
                        Select * from clientgroupe use index(siret_numgroupe) 
                        Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
                        and NumClient='#client.numclient#'
                    </cfquery>
                	<cfif #verifclient.recordcount#>
                    	<cfif #url.siret# eq "DEMOLaurent">
							<cfset txt = "Ligne 47 client dans groupe donc réduction pu= " & #leprix.pu# & " - " & #reductionachat.MontReduc# & #chr(13)#>
                            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                        </cfif>
                		<cfset leleprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
        				<cfbreak>
                	<cfelse>
                        <cfquery name="verifenfant" datasource="f8w">
                            Select * from clientgroupe use index(siret_numgroupe) 
                            Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
                            and NumEnfant='#enfant.numenfant#'
                        </cfquery>
                    	<cfif #verifenfant.recordcount#>
                        	<cfif #url.siret# eq "DEMOLaurent">
								<cfset txt = "Ligne 60 enfant dans groupe donc réduction pu= " & #leprix.pu# & " - " & #reductionachat.MontReduc# & #chr(13)#>
                            	<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                        	</cfif>
                    		<cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
        					<cfbreak>
                    	</cfif>
                    </cfif>
				</cfif>                
            <cfelse>
            	<cfif #reductionachat.ApartirDeNbrEnfant# eq 0 and #reductionachat.EnfantSuivantSiConsoMemeJour# eq 0 >
					<cfif #url.siret# eq "DEMOLaurent">
						<cfset txt = "Ligne 71 application réduction pu= " & #leprix.pu# & " - " & #reductionachat.MontReduc# & #chr(13)#>
                        <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                    </cfif>					
					<cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
                    <cfif #url.siret# eq "DEMOLaurent">
                    	<cfset txt = "Ligne 76 nouveau leprix.pu = " & #leprix.pu#>
                        <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                    </cfif>
        			<cfbreak>
            	<cfelseif #reductionachat.ApartirDeNbrEnfant# neq 0 >
                	<cfset nbrinscrit=0>
                    <cfloop query="enfantclient">
                    	<cfquery name="inscriptionplanning" datasource="f8w">
                        	Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                            Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
                            and DateInscription='#url.Date#' and nature=0
                        </cfquery>
                        <cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
                    </cfloop>
                    <cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
                    	<cfquery name="verver" datasource="f8w">
                            Select * from présenceabsence use index(siret_NumPrestation_Date) 
                            Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
                            and (PrésentAbsent=1 or PrésentAbsent=2)
                        </cfquery>
                        <cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
                        <cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
							<cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
        					<cfbreak>
                    	</cfif>
                    </cfif>
                </cfif>
            </cfif>
        </cfloop>
    </cfif>
    <cfset Leancienprix.pu = 0>
    <cfset Lenouveauprix.pu = 0>
    <cfif #ver.recordcount#><!--- il y a déjà une presenceabsence                                                                --->
		<cfif #url.siret# eq "DEMOLaurent">
			<cfset txt = "Ligne 106 il y a déjà une presabs !" & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>        
		<!--- ont annule eventuellement l'anciene action sur le portefeuille --->
        <cfset AncienCodeprix = #ver.PrésentAbsent# + 1>	            
        <cfquery name="ancienprix" datasource="f8w">
            Select * from prixprestation use index(siret_NumPrestation) 
            Where siret='#url.siret#' and NumPrestation='#url.prestation#' and CodePrix='#AncienCodeprix#' and del=0
        </cfquery>
        <cfset Leancienprix.pu = #ancienprix.pu#> 
		<cfif #url.siret# eq "DEMOLaurent">
			<cfset txt = "Ligne 116 ancien prix " & #ancienprix.désignation# & " " & #ancienprix.pu# & #chr(13)#>
            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
        </cfif>        
        <cfif #ancienprix.NonRéductionBarème# eq 0>
            <cfquery name="reductionachat" datasource="f8w">
                Select * from reductions use index(siret_numprestation) 
                where siret='#url.siret#' 
                and NumPrestation='#url.prestation#' 
                and QfDe<='#client.QuotientFamilial#' and QfA>='#client.QuotientFamilial#' 
                and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
            </cfquery>
            <cfloop query="reductionachat">
                <cfif #reductionachat.GroupeClient# gt 0>
                    <cfquery name="verifgroupe" datasource="f8w">
                        Select id from groupeclient use index(siret_numgroupe)
                        Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' and del=0
                    </cfquery>
                    <cfif #verifgroupe.recordcount#>
                        <cfquery name="verifclient" datasource="f8w">
                            Select * from clientgroupe use index(siret_numgroupe) 
                            Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
                            and NumClient='#client.numclient#'
                        </cfquery>
                        <cfif #verifclient.recordcount#>
                            <cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
                            <cfbreak>
                        <cfelse>
                            <cfquery name="verifenfant" datasource="f8w">
                                Select * from clientgroupe use index(siret_numgroupe) 
                                Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
                                and NumEnfant='#enfant.numenfant#'
                            </cfquery>
                            <cfif #verifenfant.recordcount#>
                                <cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
                                <cfbreak>
                            </cfif>
                        </cfif>
                    </cfif>                
                <cfelse>
                    <cfif #reductionachat.ApartirDeNbrEnfant# eq 0 and #reductionachat.EnfantSuivantSiConsoMemeJour# eq 0 >
						<cfif #url.siret# eq "DEMOLaurent">
                            <cfset txt = "Ligne 157 reduction " & #reductionachat.désignation# & " pu " & #leprix.pu# & " - " & #reductionachat.MontReduc# & #chr(13)#>
                            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                        </cfif>				
                		<cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
                        <cfbreak>
                    <cfelseif #reductionachat.ApartirDeNbrEnfant# neq 0 >
                        <cfset nbrinscrit=0>
                        <cfloop query="enfantclient">
                            <cfquery name="inscriptionplanning" datasource="f8w">
                                Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                                Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
                                and DateInscription='#url.Date#' and nature=0
                            </cfquery>
                            <cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
                        </cfloop>
                        <cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
                            <cfquery name="verver" datasource="f8w">
                                Select * from présenceabsence use index(siret_NumPrestation_Date) 
                                Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
                                and (PrésentAbsent=1 or PrésentAbsent=2)
                            </cfquery>
                            <cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
                            <cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
                                <cfset leprix.pu = #leprix.pu# - #reductionachat.MontReduc#>
                                <cfbreak>
                            </cfif>
                        </cfif>
                    </cfif>
                </cfif>
            </cfloop>
        </cfif>
        <cfif #url.PresentAbsent# eq 0>
            <cfset PresAbs = - 1>
        <cfelse>
            <cfset PresAbs = #url.PresentAbsent#>
        </cfif>
        <cfif #ver.PrésentAbsent# eq 1 or #ver.PrésentAbsent# eq 2>
			<cfif #url.siret# eq "DEMOLaurent">
                <cfset txt = "Ligne 195 anciennement absent justifie ou injustifié " & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>				        
            <!--- Absent justifié ou Absent injustifié--->
            <!--- ont debite éventuellement --->
            <cfset PrixAapplique = #ancienprix.pu# - #leprix.pu#>
			<cfif #url.siret# eq "DEMOLaurent">
                <cfset txt = "Ligne 202 PrixAapplique " & #PrixAapplique# & " ancienprix.pu " & #ancienprix.pu# & " - leprix.pu " & #leprix.pu# & #chr(13)#>
                <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
            </cfif>				                    
            <!--- Absent justifié à 0 donne -3.2 rebourssement ticket, Absent injustifié à 3.2 donne 0 
            ont rembourse pas , Absent injustifié à 6.4 donne 3.2 à débiter --->
            <cfif #PrixAapplique# neq 0>  
                <cfif #prestation_prepaye_portefeuille.recordcount#>
                    <cfif #PrixAapplique# lt 0><!--- ont debite --->
                        <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + (#ancienprix.pu# - #leprix.pu#)>
						<cfif #url.siret# eq "DEMOLaurent">
                            <cfset txt = "Ligne 211 NewMontant = #prestation_prepaye_portefeuille.Montant# + (#ancienprix.pu# - #leprix.pu#)" & #chr(13)#>
                            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                        </cfif>				                                            
                    <cfelse><!--- ont credite --->
                        <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#> 
						<cfif #url.siret# eq "DEMOLaurent">
                            <cfset txt = "Ligne 217 NewMontant = #prestation_prepaye_portefeuille.Montant# + #PrixAapplique#" & #chr(13)#>
                            <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                        </cfif>				                                                                
                    </cfif>                                
                    <cfquery name="majportefeuille" datasource="f8w">
                        Update prestation_prepaye_portefeuille 
                        set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
                    </cfquery>
                    <cfquery name="addhisto" datasource="f8w">
                        Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                        Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                        ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
                        '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
                        '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                        '#url.NumEnfant#','#url.date#',#PresAbs#)
                    </cfquery>
                <cfelse> 
                    <cfset NewMontant = #PrixAapplique#>
                    <cfquery name="add" datasource="f8w" result="Resultat">
                        Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                        values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
                    </cfquery>
                    <cfquery name="majhisto" datasource="f8w">
                        Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                        Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                        ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                        '0','#PrixAapplique#',
                        '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                        '#url.NumEnfant#','#url.date#',#PresAbs#)
                    </cfquery>
                </cfif>
            </cfif>
        </cfif>                    	
        <!---
        <cfif #ver.PrésentAbsent# eq 1 or #ver.PrésentAbsent# eq 2>
            <!--- Absent justifié ou Absent injustifié ont avait crédité--->
            <!--- ont débite éventuellement --->
            <cfif #ancienprix.pu# eq 0>
                <!--- Absent justifié à 0€ ont avait crédité du prix inscrit et présent --->
                <cfset ancienprix.pu = #prix.pu#>
            </cfif>
            <cfif #ancienprix.pu# gt 0>
                <cfif #prestation_prepaye_portefeuille.recordcount#>
                    <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# - #ancienprix.pu#> 
                    <cfset NewQteMontant = 0 - #ancienprix.pu#>
                    <cfquery name="majportefeuille" datasource="f8w">
                        Update prestation_prepaye_portefeuille 
                        set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
                    </cfquery>
                    <cfquery name="addhisto" datasource="f8w">
                        Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                        Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                        ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                        '#prestation_prepaye_portefeuille.Montant#','#NewQteMontant#',
                        '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                        '#url.NumEnfant#','#url.date#',#PresAbs#)
                    </cfquery>
                <cfelse> 
                    <cfset NewMontant = 0 - #ancienprix.pu#>               
                    <cfquery name="add" datasource="f8w" result="Resultat">
                        Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                        values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
                    </cfquery>
                    <cfquery name="majhisto" datasource="f8w">
                        Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                        Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                        ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                        '0','#NewMontant#',
                        '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                        '#url.NumEnfant#','#url.date#',#PresAbs#)
                    </cfquery>
                </cfif>
            </cfif>
        </cfif>
        --->
        <cfif #ver.PrésentAbsent# eq 3 or #ver.PrésentAbsent# eq 4>
            <!--- Présent justifié ou Présent injustifié ont avait débiter--->
            <!--- ont credite eventuellement --->
            <cfif #ancienprix.pu# gt 0>
                <cfif #prestation_prepaye_portefeuille.recordcount#>
                    <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + #ancienprix.pu#> 
                    <cfquery name="majportefeuille" datasource="f8w">
                        Update prestation_prepaye_portefeuille 
                        set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
                    </cfquery>
                    <cfquery name="addhisto" datasource="f8w">
                        Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                        Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                        ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                        '#prestation_prepaye_portefeuille.Montant#','#ancienprix.pu#',
                        '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                        '#url.NumEnfant#','#url.date#',#PresAbs#)
                    </cfquery>
                <cfelse> 
                    <cfset NewMontant = #ancienprix.pu#>               
                    <cfquery name="add" datasource="f8w" result="Resultat">
                        Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                        values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
                    </cfquery>
                    <cfquery name="majhisto" datasource="f8w">
                        Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                        Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                        ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                        '0','#NewMontant#',
                        '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                        '#url.NumEnfant#','#url.date#',#PresAbs#)
                    </cfquery>
                </cfif>
            </cfif>
        </cfif>    
        <cfquery name="prestation_prepaye_portefeuille" datasource="f8w">
            Select * from prestation_prepaye_portefeuille use index(siret_id_ent_portefeuille_NumClient) 
            where siret='#url.siret#' and id_ent_portefeuille='#idport#' and NumClient='#enfant.NumClientI#'
        </cfquery>
    </cfif>
    <!--- fin gestion anciene presence absence ------------------------------------------------------------------------------------>
    
    <cfset NewCodePrix = #url.PresentAbsent# + 1>
    <cfif #url.siret# eq "DEMOLaurent">
		<cfset txt = "Ligne 343 NewMontant = #prestation_prepaye_portefeuille.Montant# + (#Leancienprix.pu# - #Leprix.pu#)" & #chr(13)#>
        <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
    </cfif>
    <cfquery name="nouveauprix" datasource="f8w">
        Select pu,NonRéductionBarème from prixprestation use index(siret_NumPrestation) 
        Where siret='#url.siret#' and NumPrestation='#url.prestation#' and CodePrix='#NewCodePrix#' and del=0
    </cfquery>
    <cfset Lenouveauprix.pu = #nouveauprix.pu#>
    <cfif #nouveauprix.NonRéductionBarème# eq 0>
        <cfquery name="reductionachat" datasource="f8w">
            Select * from reductions use index(siret_numprestation) 
            where siret='#url.siret#' 
            and NumPrestation='#url.prestation#' 
            and QfDe<='#client.QuotientFamilial#' and QfA>='#client.QuotientFamilial#' 
            and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
        </cfquery>
        <cfloop query="reductionachat">
            <cfif #reductionachat.GroupeClient# gt 0>
            	<cfquery name="verifgroupe" datasource="f8w">
                	Select id from groupeclient use index(siret_numgroupe)
                    Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' and del=0
                </cfquery>
                <cfif #verifgroupe.recordcount#>
                    <cfquery name="verifclient" datasource="f8w">
                        Select * from clientgroupe use index(siret_numgroupe) 
                        Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
                        and NumClient='#client.numclient#'
                    </cfquery>
                	<cfif #verifclient.recordcount#>
                		<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
        				<cfbreak>
                	<cfelse>
                        <cfquery name="verifenfant" datasource="f8w">
                            Select * from clientgroupe use index(siret_numgroupe) 
                            Where siret='#url.siret#' and NumGroupe='#reductionachat.GroupeClient#' 
                            and NumEnfant='#enfant.numenfant#'
                        </cfquery>
                    	<cfif #verifenfant.recordcount#>
                    		<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
        					<cfbreak>
                    	</cfif>
                    </cfif>
				</cfif>                
            <cfelse>
				<cfif #reductionachat.ApartirDeNbrEnfant# eq 0 and #reductionachat.EnfantSuivantSiConsoMemeJour# eq 0 >
					<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
        			<cfbreak>
            	<cfelseif #reductionachat.ApartirDeNbrEnfant# neq 0 >
                	<cfset nbrinscrit=0>
                    <cfloop query="enfantclient">
                    	<cfquery name="inscriptionplanning" datasource="f8w">
                        	Select * from inscriptionplanning use index(siret_NumPrestation_NumEnfant_DateInscription) 
                            Where siret='#url.siret#' and NumPrestation='#url.prestation#' and numenfant='#enfantclient.numenfant#' 
                            and DateInscription='#url.Date#' and nature=0
                        </cfquery>
                        <cfset nbrinscrit = #nbrinscrit# + #inscriptionplanning.recordcount#>
                    </cfloop>
                    <cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
                    	<cfquery name="verver" datasource="f8w">
                            Select * from présenceabsence use index(siret_NumPrestation_Date) 
                            Where siret='#url.siret#' and NumPrestation='#url.prestation#' and Date='#url.date#' 
                            and (PrésentAbsent=1 or PrésentAbsent=2)
                        </cfquery>
                        <cfset nbrinscrit = #nbrinscrit# - #verver.recordcount#>
                        <cfif #nbrinscrit# gte #reductionachat.ApartirDeNbrEnfant#>
							<cfset Lenouveauprix.pu = #Lenouveauprix.pu# - #reductionachat.MontReduc#>
        					<cfbreak>
                    	</cfif>
                    </cfif>
                </cfif>
            </cfif>
        </cfloop>
    </cfif>
    <cfif #url.PresentAbsent# eq 1 or #url.PresentAbsent# eq 2>
        <!--- Absent justifié ou Absent injustifié--->
        <!--- ont re-crédite éventuellement --->
        <cfset PrixAapplique = #Lenouveauprix.pu# - #leprix.pu#>
        <cfif #url.siret# eq "DEMOLaurent">
			<cfset txt = "Ligne 413 PrixAapplique = #Lenouveauprix.pu# - #leprix.pu#" & #chr(13)#>
        	<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
    	</cfif>
        <!--- Absent justifié à 0 donne -3.2 rebourssement ticket, Absent injustifié à 3.2 donne 0 
        ont rembourse pas , Absent injustifié à 6.4 donne 3.2 à débiter --->
        <cfif #PrixAapplique# neq 0>  
            <cfif #prestation_prepaye_portefeuille.recordcount#>
                <cfif #PrixAapplique# lt 0><!--- ont rembourse --->
                    <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# + (#leprix.pu# - #Lenouveauprix.pu#)>
                    <cfset PrixAapplique = #leprix.pu# - #Lenouveauprix.pu#>
                    <cfif #url.siret# eq "DEMOLaurent">
                    	<cfset txt = "Ligne 423 NewMontant = #prestation_prepaye_portefeuille.Montant# + (#leprix.pu# - #Lenouveauprix.pu#)" & #chr(13)#>
                        <cfset txt = #txt# & "Ligne 423 PrixAapplique = #leprix.pu# - #Lenouveauprix.pu#" & #chr(13)#>
                        <cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                	</cfif>
                <cfelse><!--- ont debite --->
                    <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# - #PrixAapplique#> 
                    <cfif #url.siret# eq "DEMOLaurent">
                    	<cfset txt = "Ligne 430 NewMontant = #prestation_prepaye_portefeuille.Montant# - #PrixAapplique#" & #chr(13)#>
                    </cfif>
                </cfif>                                                
                <cfquery name="majportefeuille" datasource="f8w">
                    Update prestation_prepaye_portefeuille 
                    set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
                </cfquery>
                <cfquery name="addhisto" datasource="f8w">
                    Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                    Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                    ('#url.siret#','#url.Prestation#','#enfant.NumClientI#',
                    '#prestation_prepaye_portefeuille.Montant#','#PrixAapplique#',
                    '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                    '#url.NumEnfant#','#url.date#',#url.PresentAbsent#)
                </cfquery>
            <cfelse> 
                <cfif #PrixAapplique# lt 0><!--- ont rembourse --->
                    <cfset NewMontant = 0 + (#leprix.pu# - #Lenouveauprix.pu#)>
                    <cfif #url.siret# eq "DEMOLaurent">
                    	<cfset txt = "Ligne 446 NewMontant = 0 + (#leprix.pu# - #Lenouveauprix.pu#)" & #chr(13)#>
                    	<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                	</cfif> 
                <cfelse><!--- ont debite --->
                    <cfset NewMontant = 0 - #PrixAapplique#> 
                    <cfif #url.siret# eq "DEMOLaurent">
                    	<cfset txt = "Ligne 451 NewMontant = 0 - #PrixAapplique#" & #chr(13)#>
                    	<cffile action="append" file="c:\tempWeb\Logredpres.txt" output="#txt#">
                	</cfif>
                </cfif>                                                                                             
                <cfquery name="add" datasource="f8w" result="Resultat">
                    Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                    values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
                </cfquery>
                <cfquery name="majhisto" datasource="f8w">
                    Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                    Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                    ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                    '0','#PrixAapplique#',
                    '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                    '#url.NumEnfant#','#url.date#',#url.PresentAbsent#)
                </cfquery>
            </cfif>
        </cfif>
    </cfif>
    <cfif #url.PresentAbsent# eq 3 or #url.PresentAbsent# eq 4>
        <!---  Présent justifié ou Présent injustifié --->
        <!--- ont débite éventuellement --->
        <cfif #Lenouveauprix.pu# gt 0>
            <cfif #prestation_prepaye_portefeuille.recordcount#>
                <cfset NewMontant = #prestation_prepaye_portefeuille.Montant# - #Lenouveauprix.pu#> 
                <cfset NewMontantQ = 0 - #Lenouveauprix.pu#>
                <cfquery name="majportefeuille" datasource="f8w">
                    Update prestation_prepaye_portefeuille 
                    set Montant='#NewMontant#' where id='#prestation_prepaye_portefeuille.id#'
                </cfquery>
                <cfquery name="addhisto" datasource="f8w">
                    Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                    Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                    ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                    '#prestation_prepaye_portefeuille.Montant#','#NewMontantQ#',
                    '#NewMontant#','0','#NewMontant#','#prestation_prepaye_portefeuille.id#',
                    '#url.NumEnfant#','#url.date#',#url.PresentAbsent#)
                </cfquery>
            <cfelse> 
                <cfset NewMontant = 0 - #Lenouveauprix.pu#>               
                <cfquery name="add" datasource="f8w" result="Resultat">
                    Insert into prestation_prepaye_portefeuille (siret,id_ent_portefeuille,NumClient,Qte,Montant) 
                    values ('#url.siret#','#idport#','#enfant.NumClientI#','1','#NewMontant#')
                </cfquery>
                <cfquery name="majhisto" datasource="f8w">
                    Insert into prestation_prepaye_histo_portefeuille (siret,NumPrestation,NumClient,Qte_Precedante,Qte_Ajouté,
                    Qte_Nouvelle,FaitPar,Montant,id_portefeuille,NumEnfant,PourLe,PresentAbsent) values 
                    ('#url.siret#','#url.prestation#','#enfant.NumClientI#',
                    '0','#NewMontant#',
                    '#NewMontant#','0','#NewMontant#','#Resultat.GENERATED_KEY#',
                    '#url.NumEnfant#','#url.date#',#url.PresentAbsent#)
                </cfquery>
            </cfif>
        </cfif>
    </cfif>