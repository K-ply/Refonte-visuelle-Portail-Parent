﻿<cfdocument format="PDF" orientation="LANDSCAPE" filename="#leficpdf#" pagetype="A4" unit="cm" margintop="1" marginbottom="1" marginleft="1" marginright="1">
    <cfset laimg = replacenocase(#reqchampierfin.logoedition#,"C:/inetpub/wwwroot/gestion1.cantine-de-france","")>        
    <table width="100%" border="0">
        <cfoutput>
        <cfset letotgen1 = 0>
        <cfset numligne = 0>
        <cfset savclasse = "">   
        <cfset page = 0>    
        <tbody>
            <cfset numligne = #numligne# + 1>
            <tr>
                <td><font size="-1" face="arial"><b>Jour</b></font></td>
                <td><font size="-1" face="arial"><b>Ecole</b></font></td>
                <td><font size="-1" face="arial"><b>Classe</b></font></td>
                <td><font size="-1" face="arial"><b>Nom</b></font></td>
                <td><font size="-1" face="arial"><b>Prénom</b></font></td>
                <td><font size="-1" face="arial"><b>#reqchampierfin.txtpresta1#</b></font></td>
            </tr>    
            <cfloop query="reqchampierfin">
                <cfif #savclasse# eq "">
                    <cfset savclasse = #reqchampierfin.Classe#>
                </cfif>
                
                <cfif #savclasse# neq #reqchampierfin.Classe# and isdefined("form.lignevidesautpage") and #reqchampierfin.currentrow# neq #reqchampierfin.recordcount#>
                   <cfset savclasse = #reqchampierfin.Classe#>    
                   <cfset resteligne = 31 - #numligne#>
                   <cfset page = #page# + 1>
                   <cfswitch expression="#page#">
                       <cfcase value="3"><cfset resteligne = #resteligne# + 1></cfcase>       
                       <cfdefaultcase></cfdefaultcase>
                   </cfswitch>   
                   <cfif #resteligne# gt 0>
                       <cfset numligne = 0>
                       <cfloop from="0" to="#resteligne#" index="idx">       
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                       </cfloop>
                       <cfset numligne = #numligne# + 1>       
                       <tr>
                            <td><font size="-1" face="arial"><b>Jour</b></font></td>
                            <td><font size="-1" face="arial"><b>Ecole</b></font></td>
                            <td><font size="-1" face="arial"><b>Classe</b></font></td>
                            <td><font size="-1" face="arial"><b>Nom</b></font></td>
                            <td><font size="-1" face="arial"><b>Prénom</b></font></td>
                            <td><font size="-1" face="arial"><b>#reqchampierfin.txtpresta1#</b></font></td>
                        </tr>    
                    <cfelse>
                         <cfset numligne = 0>   
                    </cfif>
                </cfif>
                
                <cfset letotgen1 = #letotgen1# + val(#reqchampierfin.totgen1#)>
                <cfif (#reqchampierfin.Prenom# eq "TOTAL :" or #reqchampierfin.Prenom# eq "TOTAL GENERAL :") and isdefined("form.editionblanche")>
                    <cfset fond = "##BBB4B4">
                <cfelse>
                    <cfset fond = "##FBF8F8">
                </cfif>
                <cfset numligne = #numligne# + 1>    
                <tr bgcolor="#fond#">
                    <td><font size="-1" face="arial"><b><cfif find(#reqchampierfin.Jour#,"ZZ") eq 0>#reqchampierfin.Jour#<cfelse>&nbsp;</cfif></b></font></td>
                    <td><font size="-1" face="arial"><b><cfif find(#reqchampierfin.ecole#,"ZZ") eq 0>#reqchampierfin.Ecole#<cfelse>&nbsp;</cfif></b></font></td>
                    <td><font size="-1" face="arial"><b><cfif find(#reqchampierfin.classe#,"ZZ") eq 0>#reqchampierfin.Classe#<cfelse>&nbsp;</cfif></b></font></td>
                    <td><font size="-1" face="arial"><b><cfif find(#reqchampierfin.nom#,"ZZ") eq 0>#reqchampierfin.Nom#<cfelse>&nbsp;</cfif></b></font></td>
                    <td><font size="-1" face="arial"><b><cfif find(#reqchampierfin.prenom#,"ZZ") eq 0>#reqchampierfin.Prenom#<cfelse>&nbsp;</cfif></b></font></td>
                    <td align="center"><font size="-1" face="arial"><b>#reqchampierfin.presta1#
                        <cfif #reqchampierfin.Prenom# eq "TOTAL GENERAL :">#letotgen1#
                        <cfelseif #reqchampierfin.Prenom# eq "TOTAL :">#val(reqchampierfin.totclasse1)#</cfif>
                    </b></font></td>
                </tr>
                <cfif len(#reqchampierfin.mention#)>
                    <cfset numligne = #numligne# + 1>
                    <tr bgcolor="#fond#">
                        <td></td>
                        <td></td>
                        <td colspan="4"><font color="##F80409" size="-1" face="arial"><b>#reqchampierfin.mention#</b></font></td>
                    </tr>
                </cfif>    
                <cfif #reqchampierfin.Prenom# eq "TOTAL :" and isdefined("form.lignevidesautpage") is false>    
                    <!---<cfif #lignevidesautpage# eq 1>
                        <cfdocumentitem type="PAGEBREAK" evalAtPrint="True"></cfdocumentitem>
                    <cfelse>--->
                        <cfset numligne = #numligne# + 1>
                        <tr>
                        <td colspan="6">&nbsp;</td>
                        </tr>
                    <!---</cfif>--->
                </cfif>
            </cfloop>
        </tbody>
        </cfoutput>
    </table>

    <cfdocumentitem type="HEADER">
        <cfoutput>
        <table width="100%" border="0">
            <tbody>
                <!---<tr>
                    <td rowspan="4"><img width="150" height="150" src="#laimg#"></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><font size="+3">#reqchampierfin.etablissement#</font></td>
                </tr>
                <tr>
                    <td><font size="+3">Feuille de présence du #reqchampierfin.txtentete#</font></td>
                </tr>
                <tr>
                    <td></td>
                </tr>--->
                <tr>
                    <!---<td><img width="300" height="300" src="#laimg#"></td>--->
                    <td align="center"><font size="+4">#reqchampierfin.etablissement# Feuille de présence du #reqchampierfin.txtentete#</font></td>
                </tr>
                <cfif len(#delai#)>
                    <tr>
                        <td align="center"><font size="+4" color="##F80409"><b>#delai#</b></font></td>
                    </tr>
                </cfif>
            </tbody>
        </table>
        </cfoutput>
    </cfdocumentitem>
    <cfdocumentitem type="FOOTER">
        <cfoutput>
        <table width="100%" border="0">
            <tbody>
                <tr><td align="center"><font size="+4"><cfoutput>Imprimé par www.cantine-de-france.fr le #lsDateFormat(Now(), "dd mmmm yyyy")# à  #TimeFormat(Now(), "HH:mm:ss")# Page #cfdocument.currentpagenumber#/#cfdocument.totalpagecount#</cfoutput></font></td></tr>
            </tbody>
        </table>
        </cfoutput>
    </cfdocumentitem>
</cfdocument>