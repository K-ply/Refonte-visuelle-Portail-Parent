﻿<!--- soucis du 04/01/2022 ACHAT DESACTIVE --->
    <!---<cflocation url="/home/" addtoken="No">--->
    
    <cfquery name="autreetabaveccb" datasource="f8w_test">
        Select * from application_client where siret like '#lect_etablissement.siret#%' 
        and (CBBanque>0 or CBTipi=1)
    </cfquery>
    <cfif #lect_etablissement.CBBanque# eq 0 and #lect_etablissement.CBTipi# eq 0 and #autreetabaveccb.recordcount# eq 0>
        <cflocation url="/home/" addtoken="no">
    </cfif>
    
    <cfquery name="docattentvalidationbloqueachat" dbtype="query">
        Select * from Documents where bloqueachat=1 and statut<>'Validé'
    </cfquery>
    
    <!--- chargement panier non payé ou nouveau --->
    <cfquery name="panier" datasource="f8w_test" maxrows="1">
        Select uuid,CodeReponse from cbpanier use index(siret_numclient) where siret='#lect_etablissement.siret#'
        and NumClient='#lect_client.numclient#' order by dtmaj DESC limit 0,1
    </cfquery>
    <cfif #panier.CodeReponse# neq "00000" and #panier.CodeReponse# neq "P" and #panier.recordcount# eq 1 and #panier.uuid# neq "">
        <cfset session.uuidpanier = #panier.uuid#>
    <cfelse>
        <!---<cfset session.uuidpanier = createuuid()>
        Modif pour achat ticket avec tipi refdet à 30 .... --->
        <cfset leuuid = createuuid()>
        <cfset leuuid1 = replacenocase(#leuuid#,"-","","all")>
        <cfset session.uuidpanier = right(#leuuid1#,30)>
    </cfif>
    
    <!--- pour verouillage panier si achat en cour --->
    <cfquery name="cb" datasource="f8w_test" maxrows="1">
        Select * from cb use index(siret_uuid) Where siret='#lect_etablissement.siret#' 
        and uuid='#session.uuidpanier#' order by id desc limit 0,1
    </cfquery>
    <cfset verouxpanier = "non">
    <cfif #cb.resultrans# eq "V">
        <cfset delaimin = datediff("n",#cb.creamaj#,now())>
        <cfset delaiaattendre = 120 - #delaimin#>
        <cfif #delaiaattendre# gt 0>
            <cfset verouxpanier = "oui">
        </cfif>
    </cfif>
    <!--- verif si minimum d'achat demandé --->
    <cfquery name="groupeclientminimumachat" datasource="f8w_test">
      Select NumGroupe,NumProfil,SeuilReport from groupeclient use index(siret) where siret='#lect_etablissement.siret#' and SeuilReport>0 and del=0
    </cfquery>
    <cfset MinimumAchat = 0>
    <cfloop query="groupeclientminimumachat">
        <cfquery name="profilprestation" datasource="f8w_test">
            Select NumPrestation from profilprestation use index(siret_numprofil) where siret='#lect_etablissement.siret#' 
            and NumProfil='#groupeclientminimumachat.NumProfil#'
        </cfquery>
        <cfquery name="prestation" datasource="f8w_test">
            Select ticket from prestation use index(siret_numprestation) where siret='#lect_etablissement.siret#' 
            and numprestation='#profilprestation.numprestation#' and del=0
        </cfquery>
        <cfif #prestation.ticket# eq 1>
            <cfset MinimumAchat = #groupeclientminimumachat.SeuilReport#>
            <cfbreak>
        </cfif>
    </cfloop>

    <cfif #url.Numprestation# neq "" and #verouxpanier# eq "non" or isdefined("form.sav_prestation_prepaye_portefeuille_ent.id") and #verouxpanier# eq "non">
        <cfif #url.Numprestation# neq "">
            <cfquery name="panier" datasource="f8w_test">
                Select * from cbpanier use index(uuid) where uuid='#session.uuidpanier#' and siret='#lect_etablissement.siret#'
                and NumClient='#lect_client.numclient#' and NumPrestation='#url.numprestation#'
            </cfquery>
            <cfif #panier.recordCount#>
                <!--- on determine le prix --->
                <cfquery name="prixachat" datasource="f8w_test">
                    Select pu from prixprestation use index(siret_numprestation) Where siret='#lect_etablissement.siret#' 
                    and numprestation='#url.numprestation#' and CodePrix=1 and del=0
                </cfquery>
    
                <!--- IL FAUDRA VOIR A GERER LES REDUCTIONS....!!!! --->
                <cfquery name="reductionachat" datasource="f8w_test" maxrows="1">
                    Select * from reductions use index(siret_numprestation) 
                    where siret='#lect_etablissement.siret#' 
                    and NumPrestation='#url.numprestation#' 
                    and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                    and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
                </cfquery>
                <cfif #reductionachat.recordcount#>
                    <cfif #reductionachat.groupeclient# eq 0>
                        <cfset PrixReduitAchat = #prixachat.PU# - #reductionachat.MontReduc#>
                        <cfset NewMontant = #url.Qte# * #PrixReduitAchat#>
                        <cfset prixachat.pu = #PrixReduitAchat#>
                    <cfelse>
                        <cfquery name="clientgroupe" datasource="f8w_test">
                            Select * from clientgroupe use index(siret_numgroupe) Where siret='#lect_etablissement.siret#' 
                            and numgroupe='#reductionachat.groupeclient#' and numclient='#lect_client.numclient#'
                        </cfquery>
                        <cfif #clientgroupe.recordcount#>
                            <cfset PrixReduitAchat = #prixachat.PU# - #reductionachat.MontReduc#>
                            <cfset NewMontant = #url.Qte# * #PrixReduitAchat#>
                            <cfset prixachat.pu = #PrixReduitAchat#>
                        <cfelse>
                            <cfset NewMontant = #url.Qte# * #prixachat.PU#>
                        </cfif>
                    </cfif>
                <cfelse>
                    <cfset NewMontant = #url.Qte# * #prixachat.PU#>
                </cfif>            
                
                <!---<cfset NewMontant = #url.Qte# * #panier.PU#>--->
                
                <cfset lemailcorrige = replace(#lect_user.login#," ","","all")>
                <cfif #NewMontant# gte #MinimumAchat#>
                    <cfquery name="maj" datasource="f8w_test">
                        Update cbpanier set mail='#lect_user.login#', Qte='#url.Qte#', Montant='#NewMontant#',pu='#prixachat.pu#',
                        mail='#lemailcorrige#' Where 
                        uuid='#session.uuidpanier#' and siret='#lect_etablissement.siret#'
                        and NumClient='#lect_client.numclient#' and NumPrestation='#url.numprestation#'
                    </cfquery>
                </cfif>
            <cfelse>
                <!--- on determine le prix --->
                <cfquery name="prixachat" datasource="f8w_test">
                    Select pu from prixprestation use index(siret_numprestation) Where siret='#lect_etablissement.siret#' 
                    and numprestation='#url.numprestation#' and CodePrix=1 and del=0
                </cfquery>
                
                <!--- IL FAUDRA VOIR A GERER LES REDUCTIONS....!!!! --->
                <cfquery name="reductionachat" datasource="f8w_test" maxrows="1">
                    Select * from reductions use index(siret_numprestation) 
                    where siret='#lect_etablissement.siret#' 
                    and NumPrestation='#url.numprestation#' 
                    and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                    and MontReduc<>0 and del=0 order by ApartirDeNbrEnfant
                </cfquery>
                <cfif #reductionachat.recordcount#>
                    <cfif #reductionachat.groupeclient# eq 0>
                        <cfset PrixReduitAchat = #prixachat.PU# - #reductionachat.MontReduc#>
                        <cfset NewMontantAchat = #url.Qte# * #PrixReduitAchat#>
                        <cfset prixachat.pu = #PrixReduitAchat#>
                    <cfelse>
                        <cfquery name="clientgroupe" datasource="f8w_test">
                            Select * from clientgroupe use index(siret_numgroupe) Where siret='#lect_etablissement.siret#' 
                            and numgroupe='#reductionachat.groupeclient#' and numclient='#lect_client.numclient#'
                        </cfquery>
                        <cfif #clientgroupe.recordcount#>
                            <cfset PrixReduitAchat = #prixachat.PU# - #reductionachat.MontReduc#>
                            <cfset NewMontantAchat = #url.Qte# * #PrixReduitAchat#>
                            <cfset prixachat.pu = #PrixReduitAchat#>
                        <cfelse>
                            <cfset NewMontantAchat = #url.Qte# * #prixachat.PU#>
                        </cfif>
                    </cfif>
                <cfelse>
                    <cfset NewMontantAchat = #url.Qte# * #prixachat.PU#>
                </cfif>
                <cfset lemailcorrige = replace(#lect_user.login#," ","","all")>
                <cfif #NewMontantAchat# gte #MinimumAchat#>
                    <cfquery name="add" datasource="f8w_test">
                        Insert into cbpanier (uuid,siret,NumClient,NumPrestation,Qte,pu,Montant,mail) values ('#session.uuidpanier#',
                        '#lect_etablissement.siret#','#lect_client.numclient#','#url.numprestation#','#url.Qte#','#prixachat.pu#','#NewMontantAchat#',
                        '#lemailcorrige#')
                    </cfquery>
                </cfif>
            </cfif>
        <cfelseif isdefined("form.sav_prestation_prepaye_portefeuille_ent.id")>
            <cfset form.montant = replace(#form.montant#,",",".")>
            <cfset form.montant = replacenocase(#form.montant#,"€","")>
            <cfset form.montant = replacenocase(#form.montant#," ","")>
            <cfif #form.montant# lt #MinimumAchat# and #MinimumAchat# gt 0>
                <cfset form.montant = #MinimumAchat#>
            </cfif>
            <cfquery name="panier" datasource="f8w_test">
                Select * from cbpanier use index(uuid) where uuid='#session.uuidpanier#' and siret='#lect_etablissement.siret#'
                and NumClient='#lect_client.numclient#' 
                and id_prestation_prepaye_portefeuille_ent='#form.sav_prestation_prepaye_portefeuille_ent.id#'
            </cfquery>
            <cfif #panier.recordCount#>
                <cfset lemailcorrige = replace(#lect_user.login#," ","","all")>
                <cfquery name="maj" datasource="f8w_test">
                    Update cbpanier set mail='#lect_user.login#', PU='#form.montant#', Montant='#val(form.montant)#', 
                    mail='#lemailcorrige#' Where 
                    uuid='#session.uuidpanier#' and siret='#lect_etablissement.siret#'
                    and NumClient='#lect_client.numclient#' 
                    and id_prestation_prepaye_portefeuille_ent='#form.sav_prestation_prepaye_portefeuille_ent.id#'
                </cfquery>
            <cfelse>
                <cfset lemailcorrige = replace(#lect_user.login#," ","","all")>
                <cfquery name="add" datasource="f8w_test">
                    Insert into cbpanier (uuid,siret,NumClient,id_prestation_prepaye_portefeuille_ent,Qte,pu,Montant,mail) 
                    values ('#session.uuidpanier#','#lect_etablissement.siret#','#lect_client.numclient#',
                    '#form.sav_prestation_prepaye_portefeuille_ent.id#','1','#val(form.montant)#','#val(form.montant)#',
                    '#lemailcorrige#')
                </cfquery>
            </cfif>             
        </cfif>
        <cflocation url="/achats/?id_mnu=#url.id_mnu#" addtoken="no">
    </cfif>
    
    <!--- CONTENU --->
    <div class="row d-flex flex-wrap justify-content-center justify-content-lg-between ms-4 me-4 mt-4 mt-lg-5 content scroll">
        <!--- Ouverture div bloc central gauche--->
        <div class="col-lg-6 bg-white section p-3">
                
            <!--- Minimum d'achat à gérer dans le bloc de droite --->
            <cfif #MinimumAchat# gt 0>
                <p class="ms-4 text-red bold">ATTENTION, un montant minimum d'achat de <cfoutput>#MinimumAchat#</cfoutput>€ est demandé !</p>    
            </cfif>
            <!--- Fin minimum d'achat --->
    
            <!--- Ticket =  en prépaiement --->
            <cfquery name="prestation" datasource="f8w_test">
            Select * from prestation use index(siret) where siret='#lect_etablissement.siret#' and Ticket=1 
            and TopWeb=1 and ResaPeriode=0 and del=0 order by désignation
            </cfquery>
            <cfset disponible = "oui">
            <!--- pour gerer affichage ticket et/ou portefeuille --->
    
            <!--- Affichage partie ticket --->
            <cfset afficheticket = "non">
            <cfset recap_panier = []> 
            <cfset recap_qte_panier = []>
            <cfloop query="prestation">
                <cfquery name="verifporte" datasource="f8w_test">
                    Select id from prestation_prepaye_portefeuille_presta use index(siret_numprestation) 
                    where siret='#lect_etablissement.siret#' and NumPrestation='#prestation.NumPrestation#'
                </cfquery>
    
                <cfif #verifporte.recordcount# eq 0>
                    <cfset afficheticket = "oui">
                </cfif>
            </cfloop>
    
            <cfset totalttc = 0>
    
            <!--- Ouverture div bloc central prépaiement--->
            <div class="dropdown ms-4 text-grey m-1">
    
                <!--- Si ticket en prépaiement --->
                <cfif #afficheticket# eq "oui">
                    <p class="titleLarge bold text-grey d-flex align-items-center">
                        Augmentez le nombre de réservation possible pour une prestation(tout enfant confondu)
                    </p>
                    <p class="recap-sold-num bold">Vos prestations :</p>
    
                    <form name="poursel">
                        <cfoutput query="prestation">
                            <cfquery name="verportefeuille" datasource="f8w_test">
                                Select id from prestation_prepaye_portefeuille_presta use index(siret) 
                                where siret='#lect_etablissement.siret#' and NumPrestation='#prestation.NumPrestation#'
                            </cfquery>
    
                            <cfif #verportefeuille.recordcount# eq 0>
                                <cfquery name="panier" datasource="f8w_test">
                                    Select * from cbpanier use index(uuid) where uuid='#session.uuidpanier#' 
                                    and siret='#lect_etablissement.siret#'
                                    and NumClient='#lect_client.numclient#' and NumPrestation='#prestation.numprestation#'
                                </cfquery>
                                <cfquery name="prepqte" datasource="f8w_test">
                                    Select Qte from prestation_prepaye use index(siret_NumPrestation_NumClient)
                                    where siret='#lect_etablissement.siret#' and NumPrestation='#prestation.NumPrestation#' 
                                    and NumClient='#lect_client.NumClient#'
                                </cfquery>
                                <cfset Q = val(#prepqte.Qte#)>
                                
                                <div class="line_height text-grey m-0 ms-4">
                                    <cfset designation_presta_ticket = #prestation.désignation#>
                                    <p class="bold">#prestation.désignation#</p>
                                
                                    Quantité
                                    
                                    <cfif #panier.Codereponse# neq "99999" and #verouxpanier# eq "non" and #docattentvalidationbloqueachat.recordcount# eq 0>
                                        <select class="text-grey qty dropdown-border m-1" name="qte#prestation.id#" id="qte#prestation.id#" onChange="MM_jumpMenu('self',this,0)">
                                            <cfloop from="0" to="100" index="idx">
                                                <cfif #idx# eq #panier.Qte#>
                                                    <option value="/achats/?id_mnu=#url.id_mnu#&Numprestation=#prestation.Numprestation#&Qte=#idx#" selected>#idx#</option>
                                                <cfelse>
                                                    <option value="/achats/?id_mnu=#url.id_mnu#&Numprestation=#prestation.Numprestation#&Qte=#idx#">#idx#</option>
                                                </cfif>
                                            </cfloop>
                                        </select>
                                    <cfelse>
                                        <select class="text-grey qty dropdown-border m-1" disabled name="qte#prestation.id#" id="qte#prestation.id#" onChange="MM_jumpMenu('self',this,0)">
                                            <cfloop from="0" to="100" index="idx">
                                                <cfif #idx# eq #panier.Qte#>
                                                    <option value="/achats/?id_mnu=#url.id_mnu#&Numprestation=#prestation.Numprestation#&Qte=#idx#" selected>#idx#</option>
                                                <cfelse>
                                                    <option value="/achats/?id_mnu=#url.id_mnu#&Numprestation=#prestation.Numprestation#&Qte=#idx#">#idx#</option>
                                                </cfif>
                                            </cfloop>
                                        </select>
                                    </cfif>
                                  
                                    
                                    <p>Prix unitaire TTC : #panier.pu# €</p>
                                    <p>Prix total TTC : #panier.Montant# €</p>
    
                                    <!--- Blocage de l'accés si il manque des documents --->
                                    <cfif #docattentvalidationbloqueachat.recordcount# neq 0>
                                        <p class="text-red">Vous avez un document à transmettre avant de pouvoir procéder aux achats.</p>
                                    </cfif>
    
                                    <!--- Jours de réservation exclus --->
                                    <cfset r = #prestation.joursexclus#>
                                    <cfloop from="6" to="0" step="-1" index="i">
                                        <cfset valeurcheck = #r# \ (2 ^ #i#)>
                                        <cfswitch expression="#i#">
                                            <cfcase value="6">
                                                <cfset checksamedi = #valeurcheck#>
                                            </cfcase>
                                            <cfcase value="5">
                                                <cfset checkvendredi = #valeurcheck#>
                                            </cfcase>
                                            <cfcase value="4">
                                                <cfset checkjeudi = #valeurcheck#>
                                            </cfcase>
                                            <cfcase value="3">
                                                <cfset checkmercredi = #valeurcheck#>
                                            </cfcase>
                                            <cfcase value="2">
                                                <cfset checkmardi = #valeurcheck#>
                                            </cfcase>
                                            <cfcase value="1">
                                                <cfset checklundi = #valeurcheck#>
                                            </cfcase>
                                            <cfcase value="0">
                                                <cfset checkdimanche = #valeurcheck#>
                                            </cfcase>     
                                        </cfswitch>
                                        <cfset r = #r# Mod (2 ^ #i#)>
                                    </cfloop>
                                    <cfset txtouvert = "">
                                    <cfif #checklundi# eq 0><cfset txtouvert = #txtouvert# & "Lundi, "></cfif>
                                    <cfif #checkmardi# eq 0><cfset txtouvert = #txtouvert# & "Mardi, "></cfif>
                                    <cfif #checkmercredi# eq 0><cfset txtouvert = #txtouvert# & "Mercredi, "></cfif> 
                                    <cfif #checkjeudi# eq 0><cfset txtouvert = #txtouvert# & "Jeudi, "></cfif>                                    	
                                    <cfif #checkvendredi# eq 0><cfset txtouvert = #txtouvert# & "Vendredi, "></cfif>
                                    <cfif #checksamedi# eq 0><cfset txtouvert = #txtouvert# & "Samedi, "></cfif>
                                    <cfif #checkdimanche# eq 0><cfset txtouvert = #txtouvert# & "Dimanche"></cfif>
    
                                    <p class="text-small">Prestation ouverte les #txtouvert#</p>
                                    <cfset totalttc = #totalttc# + val(#panier.Montant#)>
                                    <cfset ArrayPush(recap_panier,[panier.Qte,panier.Montant, prestation.désignation])>
                                    <cfset MontantTotalPAnier = #totalttc#>
                                    <cfset ArrayPush(recap_qte_panier,[prestation.désignation,prepqte.Qte])>
                                    
                                </div>
                            </cfif>
                        </cfoutput>
                    </form>           
                </cfif>
            
            </div>
    
    
            <!--- Ouverture div bloc central protefeuille--->
            <div class="dropdown ms-4 text-grey m-1">

                <!--- Partie portefeuille --->
               
                <cfquery name="prestation_prepaye_portefeuille_ent" datasource="f8w_test">
                    Select * from prestation_prepaye_portefeuille_ent use index(siret)
                    where siret='#lect_etablissement.siret#'
                </cfquery>

                <cfif #prestation_prepaye_portefeuille_ent.recordcount# gt 0>
                    
                    <p class="titleLarge bold text-grey d-flex align-items-center">
                        Augmentez le nombre de réservation possible pour une prestation(tout enfant confondu)
                    </p>

                    <p class="recap-sold-num bold">Vos prestations :</p>

                    <div class="line_height text-grey m-0 ms-4">
                        
                        <cfoutput query="prestation_prepaye_portefeuille_ent">
                            <cfset lejourj = Day(now())>
        
                            <cfif #lejourj# gte #prestation_prepaye_portefeuille_ent.DispoDu# and #lejourj# lte #prestation_prepaye_portefeuille_ent.DispoAu#>
                                <cfset disponible = "oui">
                            <cfelse>
                                <cfset disponible = "non">
                            </cfif>
        
                            <cfif #prestation_prepaye_portefeuille_ent.DispoDu# neq 1 or #prestation_prepaye_portefeuille_ent.DispoAu# neq 31>
                                <cfset textdispo = "Attention, vous pouvez acheter du " & #prestation_prepaye_portefeuille_ent.DispoDu# & " au " & #prestation_prepaye_portefeuille_ent.DispoAu# & " de chaque mois"> 
                            <cfelse>
                                <cfset textdispo = "">
                            </cfif>

                            <cfquery name="poursolde" datasource="f8w_test">
                                Select Montant from prestation_prepaye_portefeuille use 
                                index(siret_id_ent_portefeuille_NumClient) where siret='#lect_etablissement.siret#' and 
                                id_ent_portefeuille='#prestation_prepaye_portefeuille_ent.id#' and 
                                NumClient='#lect_client.numclient#'
                            </cfquery>

                            <cfquery name="prestation_prepaye_portefeuille_presta" datasource="f8w_test">
                                Select * from prestation_prepaye_portefeuille_presta use index(siret) 
                                where siret='#lect_etablissement.siret#' and id_ent='#prestation_prepaye_portefeuille_ent.id#'
                            </cfquery>

                            <cfquery name="panier" datasource="f8w_test">
                                Select * from cbpanier use index(uuid) where uuid='#session.uuidpanier#' 
                                and siret='#lect_etablissement.siret#'
                                and NumClient='#lect_client.numclient#' 
                                and id_prestation_prepaye_portefeuille_ent='#prestation_prepaye_portefeuille_ent.id#'
                            </cfquery>

                            <cfset totalttc = #totalttc# + val(#panier.Montant#)>

                            <cfform name="dsds#prestation_prepaye_portefeuille_ent.id#">
                                <cfset recap_panier_portefeuille = []>

                                <span class="bold">#prestation_prepaye_portefeuille_ent.designation# </span><br><p class="pt-2 txt_small">(Solde actuel #val(poursolde.Montant)#€) <span class="text-red">#textdispo#</span></p>
                                Montant

                                <cfinput class="text-grey qty dropdown-border m-1" name="montant" value="#NumBerFormat(panier.Montant,'9999.99')#" size="8">
                                    <cfif #docattentvalidationbloqueachat.recordcount# neq 0>
                                        <cfinput type="submit" disabled name="go" value="Modifier">
                                    <cfelseif #disponible# eq "oui" and #verouxpanier# eq "non">
                                        <cfif val(#panier.Montant#) gt 0>
                                            <cfinput class="reload-btn btn btn-primary bold"type="submit" name="go" value="Modifier">
                                        <cfelse>
                                            <cfinput class="reload-btn btn btn-primary bold" type="submit" name="go" value="Ajouter">
                                        </cfif>
                                    <cfelse>
                                        <cfif val(#panier.Montant#) gt 0>
                                            <cfinput class="reload-btn btn btn-primary bold"type="submit" disabled name="go" value="Modifier">
                                        <cfelse>
                                            <cfinput class="reload-btn btn btn-primary bold"type="submit" disabled name="go" value="Ajouter">
                                        </cfif>
                                    </cfif>

                                    <cfif #docattentvalidationbloqueachat.recordcount# neq 0>
                                        <p>Vous avez un document a transmettre avant de pouvoir procéder aux achats !</p>
                                    </cfif>
                                <cfinput type="hidden" name="sav_prestation_prepaye_portefeuille_ent.id" value="#prestation_prepaye_portefeuille_ent.id#">
                                <cfset ArrayPush(recap_panier_portefeuille,[prestation_prepaye_portefeuille_ent.designation, poursolde.Montant, panier.Montant])>
                            </cfform>

                            <p class="recap_solde_dispo pt-3 ">Concerne les prestations :
                                <p class="txt_small">
                                <cfloop query="prestation_prepaye_portefeuille_presta">
                                    <div class="presta-portefeuille">
                                    <cfquery name="prest" datasource="f8w_test">
                                        Select Désignation from prestation use index(siret_numprestation) 
                                        where siret='#lect_etablissement.siret#' 
                                        and NumPrestation='#prestation_prepaye_portefeuille_presta.NumPrestation#'
                                    </cfquery>

                                    <cfquery name="tarif" datasource="f8w_test">
                                        Select PU from prixprestation use index(siret_Numprestation) 
                                        where siret='#lect_etablissement.siret#' 
                                        and NumPrestation='#prestation_prepaye_portefeuille_presta.NumPrestation#' 
                                        and CodePrix=1
                                    </cfquery>
                                    </div>
                                </p>
                            </p>

                            <!--- COLLIAS REDUC SUR QF ET NBR ENF --->
                            <cfquery name="reduction" datasource="f8w_test">
                                Select * from reductions use index(siret_numprestation) 
                                where siret='#lect_etablissement.siret#' 
                                and NumPrestation='#prestation_prepaye_portefeuille_presta.NumPrestation#' 
                                and QfDe<='#lect_client.QuotientFamilial#' and QfA>='#lect_client.QuotientFamilial#' 
                                and del=0
                                order by ApartirDeNbrEnfant
                            </cfquery>
                            <cfset txttar = "Tarif : ">
                            <cfif #reduction.recordcount#>
                                <cfloop query="reduction">
                                    <cfset prixreduit = #tarif.PU# - #reduction.MontReduc#>
                                    <cfset txttar = #txttar# & #reduction.Désignation# & " : " & #prixreduit# & " €, ">
                                </cfloop>
                            <cfelse>
                                <cfset txttar = #txttar# & #tarif.PU# & " €">
                            </cfif>
                            <cfset TarReduit = ModuleCDF.PrixReduitTicket(#lect_etablissement.siret#,#lect_client.NumClient#,0,#prestation_prepaye_portefeuille_presta.NumPrestation#,#LsDateformat(now(),"dd/MM/YYYY")#,1,1,"inscr",0)>                                        
                            <p class="mb-1">#prest.Désignation# :</p>
                            <cfloop query="TarReduit">
                                <pclass="mb-1">#TarReduit.Désignation# #NumBerformat(TarReduit.PU,"999.99")# €</p>
                            </cfloop>
                        </cfloop>
                    </cfoutput>
                </div>  
                </cfif>
                <!---tr>
                    <td>&nbsp;</td>
                    <td colspan="2" align="right"><b>Total à régler :</b></td>
                    <td><cfoutput><b>#numberformat(totalttc,"99999.99")#</b></cfoutput></td>
                </tr--->
                
                <cfquery name="cbbanque" datasource="f8w_test">
                    Select * from cbbanque use index(siret) where siret like '#lect_user.siret#%'
                </cfquery>





                            <!--- ICI --->





                <cfif #cbbanque.recordcount# gt 0>
                    <cfif #totalttc# gt 0>
                        <cfinclude template="#cbbanque.script#">                                
                    <cfelseif isdefined("form.data") and #cbbanque.script# eq "/achatlcl/index.cfm">
                        <!--- Initialisation du chemin du fichier pathfile (à modifier)
                        ex :	parm = parm & " pathfile=c:\\repertoire\\pathfile" --->
                        <cfset message = "pathfile=C:\\inetpub\\wwwroot\\gestion1.cantine-de-france\\cblcl\\param\\pathfile">
                        <cfset message = #message#& " message=" & #form.DATA#>
                        <!--- Initialisation du chemin de l'executable request (à modifier)
                        ex : cmdLine = "c:\\repertoire\\bin\\request.exe" --->
                        <cfset CmdLine = "C:\\inetpub\\wwwroot\\gestion1.cantine-de-france\\cblcl\\bin\\response.exe">
                        <!--- 	Lancement de la ligne de commande avec passage des paramètres --->
                        <cfexecute timeout="1200" name="#CmdLine#" arguments="#message#" variable="$result1">	
                        <!--- ont enleve le 1er ! de $result1 --->
                        <cfset $result2 = mid(#$result1#,2,len(#$result1#))>
                        <cfset $result3 = replacenocase(#$result2#,"!!","!_!","all")>
                        <cfset $result = replacenocase(#$result3#,"!!","!_!","all")>    
                        <!---//	Sortie de la fonction : !code!error!v1!v2!v3!...!v29
                        //		- code=0	: la fonction retourne les données de la transaction dans les variables v1, v2, ...
                        //				: Ces variables sont décrites dans le GUIDE DU PROGRAMMEUR
                        //		- code=-1 	: La fonction retourne un message d'erreur dans la variable error --->
                        <cfset n = 0>
                        <cfloop list="#$result#" delimiters="!" index="res">
                            <cfset n = #n# + 1>
                            <cfswitch expression="#n#">
                                <cfcase value="1">
                                    <cfset code = #res#>
                                </cfcase>
                                <cfcase value="2">
                                    <cfset error = #res#>
                                </cfcase>
                                <cfcase value="11">
                                    <cfset response_code = #res#>
                                    <cfif #response_code# eq "00">
                                        <cfset CodeReponserep = #response_code# & "000">
                                    <cfelse>
                                        <cfset CodeReponserep = #response_code#>
                                    </cfif>
                                </cfcase>
                                <cfdefaultcase></cfdefaultcase>
                            </cfswitch>
                        </cfloop>
                        <cfquery name="cbbanque_reponse" datasource="f8w_test">
                            Select DesiReponse from cbbanque_reponse where CodeReponse='#CodeReponserep#'
                        </cfquery>
                        <cfif #cbbanque_reponse.recordcount#>
                            <p class="text-red"><cfoutput>#cbbanque_reponse.DesiReponse#</cfoutput></p>
                        <cfelse>
                            <p class="text-red">Votre paiement n'a pas été effectué</p>
                        </cfif>
                    </cfif>
                <cfelseif #lect_etablissement.CBTipi# eq 1 and #lect_etablissement.Tipi# eq 1 and #lect_client.PaiementTipi# eq 1>
                    <cfif #totalttc# gt 0>
                        <cfinclude template="/CBTipi.cfm">
                    </cfif>
                <cfelseif #lect_client.PaiementTipi# eq 0>
                    <p class="text-red bold">Votre accès au paiement par CB a été désactivé !</p>
                </cfif>
                <cfif isdefined("url.CodeReponse")>
                    <cfquery name="cbbanque_reponse" datasource="f8w_test">
                        Select DesiReponse from cbbanque_reponse Where cbbanque_reponse='#cbbanque.cbbanque_reponse#' 
                        and Codereponse='#url.CodeReponse#'
                    </cfquery>
                    <p class"text-red bold"><cfoutput>#cbbanque_reponse.DesiReponse#</cfoutput></p>                                    
                </cfif>
                
            <!--- Fermeture div bloc central portefeuille--->
            </div>
        <!--- Fermeture div bloc central gauche--->
        </div>
    
        <!--- Ouverture div bloc central droit --->
        
            <div class="order-2 col-lg-5 col d-flex flex-column pt-4 ">
                <div class="scroll p-3 section mb-4">
                    <p class="titleLarge bold text-grey ms-4"><span>Votre solde disponible</span></p>
                    <cfloop from="1"to="#ArrayLen(recap_qte_panier)#" index="i">
                    <p class="recap-sold bold text-grey ms-4"><cfoutput>#recap_qte_panier[i][1]#</cfoutput>
                    <p class="fineTxtLabel text-grey d-flex align-items-center ms-4">Quantité actuelle : <span class="recap_solde_dispo"><cfoutput>#recap_qte_panier[i][2]#</cfoutput></span></p>
                    </cfloop>
                    <cfif isDefined("recap_panier_portefeuille")>
                        <cfloop from="1"to="#ArrayLen(recap_panier_portefeuille)#" index="i">
                            <p class="recap-sold bold text-grey ms-4"><cfoutput>#recap_panier_portefeuille[i][1]#</cfoutput>
                            <p class="fineTxtLabel text-grey d-flex align-items-center ms-4">Solde actuel : <span class="recap_solde_dispo"><cfoutput> #recap_panier_portefeuille[i][2]#€</cfoutput></span></p>
                        </cfloop>
                    </cfif>
                </div>
        
                <div class="scroll p-3 section mb-4">
                    <p class="titleLarge bold text-grey ms-4"><span>Récapitulatif de votre panier</span></p>
                    <cfloop from="1"to="#ArrayLen(recap_panier)#" index="i">
                    <p class="recap-sold bold text-grey ms-4"><cfoutput>#recap_panier[i][3]#</cfoutput></p>
                    <p class="fineTxtLabel text-grey d-flex align-items-center ms-4">Montant TTC : <cfoutput>#recap_panier[i][2]#€</cfoutput><br>Quantité : <cfoutput>#recap_panier[i][1]#</cfoutput></p>
                    </cfloop>
                    <cfif isDefined("recap_panier_portefeuille")>
                        <cfloop from="1"to="#ArrayLen(recap_panier_portefeuille)#" index="i">
                            <p class="recap-sold bold text-grey ms-4"><cfoutput>#recap_panier_portefeuille[i][1]#</cfoutput></p>
                            <p class="fineTxtLabel text-grey d-flex align-items-center ms-4">Montant TTC : <cfoutput>#recap_panier_portefeuille[i][3]#€</cfoutput>
                        </cfloop>
                    </cfif>
    
                    <p class="separation-bar recap-sold bold text-grey ms-4"></p>
                    <div class="totalAmount d-flex justify-content-between">
                        <p class="titleLarge bold text-grey ms-4">Montant total : <span class ="recap-sold-num"> <cfoutput> #numberformat(totalttc,"99999.99")#€</cfoutput></span></p>
                        <button class="reload-btn btn btn-primary bold" onclick="openPopUp()">Payer par CB</button>
                    </div>
            </div>
        </div>
    </div>