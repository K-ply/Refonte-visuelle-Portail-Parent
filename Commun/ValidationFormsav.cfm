﻿<cfparam name="Attributes.BaseDeDonnee" default=""><cfparam name="Attributes.Table" default=""><cfparam name="Attributes.Message" default=""><cfparam name="Attributes.form" default=""><cfparam name="Attributes.lstColonneValues" default="">
<cfset Caller.cf_ValidationForm.Message = "">
<cfif len(#Attributes.BaseDeDonnee#) eq 0>
	<cfset Caller.cf_ValidationForm.Message = "Une base de donnée doit être passée en paramètres...!">
<cfelseif len(#Attributes.Table#) eq 0>
	<cfset Caller.cf_ValidationForm.Message = "Une table doit être passée en paramètres...!">
<cfelseif isdefined("Attributes.Form") is false >
	<cfset Caller.cf_ValidationForm.Message = "Une Form doit être passée en paramètres...!">
<cfelseif len(#Attributes.lstColonneValues#) eq 0 >
	<cfset Caller.cf_ValidationForm.Message = "Les donnée de la form doivent être passée en paramètres (dans l'ordre des colonne de la table...!">
<cfelse>
	<cfset Attributes.lstColonneValues = replacenocase(#Attributes.lstColonneValues#,"||","|_|","all")>
    <cfset Attributes.lstColonneValues = replacenocase(#Attributes.lstColonneValues#,"||","|_|","all")>
	<cfif left(#Attributes.lstColonneValues#,1) eq "|"><!--- première donnée vide --->
    	<cfset Attributes.lstColonneValues = "_" & #Attributes.lstColonneValues#>
    </cfif>	
	<cfif right(#Attributes.lstColonneValues#,1) eq "|"><!--- dernière donnée vide --->
    	<cfset Attributes.lstColonneValues = #Attributes.lstColonneValues# & "_">
    </cfif>
    <cfset ficdump = "C:\TempWeb\" & createuuid() & ".txt">
    <cfdump var="#Attributes.form#" output="#ficdump#">
    <cfdbinfo datasource="#Attributes.BaseDeDonnee#" name="tableselect" type="columns" table="#Attributes.Table#"></cfdbinfo>
	<cfset lstColonne = "">
    <cfloop query="tableselect">
    	<cfset lstColonne = #lstColonne# & trim(#tableselect.column_name#) & ",">
    </cfloop>
	<cfset cptColonneForm = 0>
	<cfloop list="#lstColonne#" delimiters="," index="ColonneForm">	
		<cfif isdefined("Attributes.form.#ColonneForm#")>
			<cfset cptColonneForm = #cptColonneForm# + 1>
            <!---<cfset Caller.cf_ValidationForm.Message = #ColonneForm# & " " & #cptColonneForm#>--->
            <cfif len(#Caller.cf_ValidationForm.Message#) eq 0>
                <cfquery dbtype="query" name="InteroStructure"><!--- recup info sql de la colonne --->
                    Select * from tableselect Where column_name='#ColonneForm#'
                </cfquery>
                <cfif #InteroStructure.recordcount# eq 0 or #InteroStructure.column_name# neq #ColonneForm#>
                    <cfset Caller.cf_ValidationForm.Message = "probleme interne de récupération de structure...!">
                <cfelse>
                    <cfset cptColonneValue = 0>
                    <cfset ValueColonneFormValue = "">
                    <cfloop list="#Attributes.lstColonneValues#" delimiters="|" index="ColonneFormValue"><!--- recup valeur de la colonne--->
                        <cfset cptColonneValue = #cptColonneValue# + 1>
                        <cfif #cptColonneValue# eq #cptColonneForm#>
                            <cfset ValueColonneFormValue = #ColonneFormValue#>
                        </cfif>
                    </cfloop>
                    <cfswitch expression="#InteroStructure.type_name#">
                        <cfcase value="VARCHAR,CHAR" delimiters=",">
                        	<cfif #ValueColonneFormValue# eq "_">
                            	<cfif #InteroStructure.is_nullable# eq "NO">
                                	<cfset Caller.cf_ValidationForm.Message = 'Les champs avec une <font color="##FF0000">*</font> sont obligatoires...!'>
                                <cfelse>
                                	<cfset "Attributes.form.#ColonneForm#" = "">
                                </cfif>
                            </cfif>
                        </cfcase>
                        <cfcase value="INT">
                            <cfset ColonneForm = ucase(#ColonneForm#)>
                            <!---<cfif #InteroStructure.is_nullable# eq "NO" and isnull(#ColonneFormValue#)><!--- integer non null --->
                                <cfif isdefined("Attributes.form.#ColonneForm#")><!--- cochée ont passe à 1 --->
                                    <cfset "Attributes.form.#ColonneForm#" = 1>
                                <cfelse>
                                    <cfset "Attributes.form.#ColonneForm#" = 0><!--- pas coché ou null ont passe à 0 --->
                                </cfif>                
                            </cfif>--->
                            <cfif findnocase("_",#ValueColonneFormValue#)>
                                <cfset "Attributes.form.#ColonneForm#" = 0>
                            </cfif>    
                        </cfcase>
                        <cfcase value="DECIMAL">
                            <cfif findnocase("_",#ValueColonneFormValue#)>
                                <cfset "Attributes.form.#ColonneForm#" = 0>
                            <cfelseif findnocase(",",#ValueColonneFormValue#)>
                                <cfset "Attributes.form.#ColonneForm#" = replace(#ValueColonneFormValue#,",",".","all")>
                            <cfelse>
                                <!---<cfset Caller.cf_ValidationForm.Message = #ValueColonneFormValue#>--->
                            </cfif>
                        </cfcase>     
                        <cfdefaultcase></cfdefaultcase>
                    </cfswitch>
                </cfif>
            <cfelse>
                <cfbreak>
            </cfif>
    	</cfif>
    </cfloop>
	<cfif len(#Caller.cf_ValidationForm.Message#) eq 0>
    	<cfset Caller.cf_ValidationForm.Message = "OK">
    </cfif>
	<cfset Caller.cf_ValidationForm.Form = #Attributes.Form#>
</cfif>