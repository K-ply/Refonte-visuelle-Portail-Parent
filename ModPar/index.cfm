﻿<cfinclude template="/Query_Header.cfm">

<cfset mod_txt = "">
<cfset mod_html = "">
<cfset mod_txt1 = "">
<cfset mod_html1 = "">

<cfset msg = "">
<cfset msg1 = "">
<cfset iderr = "">
<cfparam name="url.siret" default="">
<cfparam name="url.cli" default="">
<cfparam name="url.computername" default="">
<cfparam name="url.UserNameWindow" default="">
<cfparam name="url.user_eurosyl" default="">
<cfparam name="url.CodeAppli" default="">
<cfparam name="url.demo" default="">

<cfif isdefined("form.nomenfant") and len(#form.nomenfant#) and len(#form.prénomenfant#) and len(#form.datenaissance#)><!--- clic sur enregistrer --->
	<cfquery name="lect" datasource="f8w_test">
        Select id,nval from parametres use index(siret_tabl_champs) 
        Where `siret` = '#lect_client.siret#' and `tabl` = 'enfant' 
        and `champs` = 'NumEnfant'
    </cfquery>
	<cfif #lect.recordcount#>
		<cfset NumSuivant6 = #lect.nval# + 1>
        <cfquery name="maj" datasource="f8w_test">
            Update parametres set nval = '#NumSuivant6#' Where id = '#lect.id#'
        </cfquery>
    <cfelse>
    	<cfset NumSuivant6 = 1000000000>
        <cfquery name="add" datasource="f8w_test">
            INSERT into parametres (`siret`,`tabl`,`champs`,`nval`) Values ('#lect_client.siret#','enfant','NumEnfant',
            '#NumSuivant6#')
        </cfquery>
    </cfif>
    <cfquery name="addenfant" datasource="f8w_test">
    	INSERT into enfant (siret,NumEnfant,nom,Prénom,DateNaissance,NumClientI,Allergie,Sexe,Mention) 
        values ('#lect_client.siret#','#NumSuivant6#','#form.nomenfant#','#form.prénomenfant#','#form.datenaissance#',
        '#lect_client.numclient#','#form.allergie#','#form.sexe#','#form.mention#')
    </cfquery>
	<cfif isdefined("form.enfantclasse")>
        <cfquery name="relectclasse" datasource="f8w_test">
            Select NumClasse from enfantclasse use index(siret_numenfant_numclasse) where siret='#lect_etablissement.siret#' 
            and numenfant='#NumSuivant6#' and numclasse='#form.enfantclasse#'
        </cfquery>
        <cfif #relectclasse.recordcount# eq 0 and #form.enfantclasse# gt 0>
            <cfquery name="classe" datasource="f8w_test">
                Select * from classe use index(siret_numclasse) where siret='#lect_etablissement.siret#'
                and numclasse='#form.enfantclasse#'
            </cfquery>
            <cfif #classe.recordcount#>
                <cfquery name="relectclasse" datasource="f8w_test">
                    Select id from enfantclasse use index(siret_numenfant) where siret='#lect_etablissement.siret#' 
                    and numenfant='#NumSuivant6#'
                </cfquery>
                <cfif #relectclasse.recordcount#>
                    <cfquery name="maj" datasource="f8w_test">
                        Update enfantclasse set numclasse='#form.enfantclasse#' where siret='#lect_etablissement.siret#' 
                        and numenfant='#NumSuivant6#'
                    </cfquery>
                <cfelse>
                    <cfquery name="add" datasource="f8w_test">
                        INSERT into enfantclasse(siret,numenfant,numclasse) values 
                        ('#lect_etablissement.siret#','#NumSuivant6#','#form.enfantclasse#')
                    </cfquery>
                </cfif>
            </cfif>        	
        </cfif>
    </cfif>    
    <cfset msg1 = "Enregistrement effectué avec succès, vous pouvez effectuer des réservations pour #form.nomenfant# #form.prénomenfant# !">
</cfif>

<cfif isdefined("form.nomcharge1") and len(#form.nomenfant#) and len(#form.prénomenfant#) and len(#form.datenaissance#)>
    <cfif #form.memeque# gt 0>
        <cfquery name="duplique" datasource="f8w_test">
            INSERT into enfantinfo (siret, NumEnfant, nomcharge1, prenomcharge1, telcharge1, gsmcharge1, liencharge1, nomcharge2, prenomcharge2, telcharge2, gsmcharge2, liencharge2, nomcharge3, prenomcharge3, telcharge3, gsmcharge3, liencharge3, nomcharge4, prenomcharge4, telcharge4, gsmcharge4, liencharge4, nomurgence1, prenomurgence1, telurgence1, gsmurgence1, lienurgence1, nomurgence2, prenomurgence2, telurgence2, gsmurgence2, lienurgence2, autorisemesure, lieuhospital, autorisephoto, autorisequiter, heurequiter, responsabilitecivil, individuelaccident, compagnieassurance, numcontrat, vaccin1, vaccin2, vaccin3, pai, prenommedecin, telmedecin, recommandation, nommedecin,modesortie)
            select siret, #NumSuivant6#, nomcharge1, prenomcharge1, telcharge1, gsmcharge1, liencharge1, nomcharge2, prenomcharge2, telcharge2, gsmcharge2, liencharge2, nomcharge3, prenomcharge3, telcharge3, gsmcharge3, liencharge3, nomcharge4, prenomcharge4, telcharge4, gsmcharge4, liencharge4, nomurgence1, prenomurgence1, telurgence1, gsmurgence1, lienurgence1, nomurgence2, prenomurgence2, telurgence2, gsmurgence2, lienurgence2, autorisemesure, lieuhospital, autorisephoto, autorisequiter, heurequiter, responsabilitecivil, individuelaccident, compagnieassurance, numcontrat, vaccin1, vaccin2, vaccin3, pai, prenommedecin, '0404', recommandation, nommedecin,modesortie
            from enfantinfo use index(siret_numenfant) where siret='#lect_client.siret#' and NumEnfant='#form.memeque#'
        </cfquery>
    <cfelse>
        <cfquery datasource="f8w_test">   
            INSERT INTO enfantinfo (siret, NumEnfant, nomcharge1, prenomcharge1, telcharge1, gsmcharge1, liencharge1, nomcharge2, prenomcharge2, telcharge2, gsmcharge2, liencharge2, nomcharge3, prenomcharge3, telcharge3, gsmcharge3, liencharge3, nomcharge4, prenomcharge4, telcharge4, gsmcharge4, liencharge4, nomurgence1, prenomurgence1, telurgence1, gsmurgence1, lienurgence1, nomurgence2, prenomurgence2, telurgence2, gsmurgence2, lienurgence2, autorisemesure, lieuhospital, autorisephoto, autorisequiter, heurequiter, responsabilitecivil, individuelaccident, compagnieassurance, numcontrat, vaccin1, vaccin2, vaccin3, pai, prenommedecin, telmedecin, recommandation, nommedecin,modesortie,autorisetransport)
            VALUES ('#lect_client.siret#','#NumSuivant6#',
            <cfif IsDefined("FORM.nomcharge1") AND #FORM.nomcharge1# NEQ "">
                <cfqueryparam value="#FORM.nomcharge1#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.prenomcharge1") AND #FORM.prenomcharge1# NEQ "">
                <cfqueryparam value="#FORM.prenomcharge1#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telcharge1") AND #FORM.telcharge1# NEQ "">
                <cfqueryparam value="#FORM.telcharge1#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.gsmcharge1") AND #FORM.gsmcharge1# NEQ "">
                <cfqueryparam value="#FORM.gsmcharge1#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.liencharge1") AND #FORM.liencharge1# NEQ "">
                <cfqueryparam value="#FORM.liencharge1#" cfsqltype="cf_sql_varchar" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.nomcharge2") AND #FORM.nomcharge2# NEQ "">
                <cfqueryparam value="#FORM.nomcharge2#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.prenomcharge2") AND #FORM.prenomcharge2# NEQ "">
                <cfqueryparam value="#FORM.prenomcharge2#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telcharge2") AND #FORM.telcharge2# NEQ "">
                <cfqueryparam value="#FORM.telcharge2#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.gsmcharge2") AND #FORM.gsmcharge2# NEQ "">
                <cfqueryparam value="#FORM.gsmcharge2#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.liencharge2") AND #FORM.liencharge2# NEQ "">
                <cfqueryparam value="#FORM.liencharge2#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.nomcharge3") AND #FORM.nomcharge3# NEQ "">
                <cfqueryparam value="#FORM.nomcharge3#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.prenomcharge3") AND #FORM.prenomcharge3# NEQ "">
                <cfqueryparam value="#FORM.prenomcharge3#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telcharge3") AND #FORM.telcharge3# NEQ "">
                <cfqueryparam value="#FORM.telcharge3#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.gsmcharge3") AND #FORM.gsmcharge3# NEQ "">
                <cfqueryparam value="#FORM.gsmcharge3#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.liencharge3") AND #FORM.liencharge3# NEQ "">
                <cfqueryparam value="#FORM.liencharge3#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.nomcharge4") AND #FORM.nomcharge4# NEQ "">
                <cfqueryparam value="#FORM.nomcharge4#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.prenomcharge4") AND #FORM.prenomcharge4# NEQ "">
                <cfqueryparam value="#FORM.prenomcharge4#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telcharge4") AND #FORM.telcharge4# NEQ "">
                <cfqueryparam value="#FORM.telcharge4#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.gsmcharge4") AND #FORM.gsmcharge4# NEQ "">
                <cfqueryparam value="#FORM.gsmcharge4#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.liencharge4") AND #FORM.liencharge4# NEQ "">
                <cfqueryparam value="#FORM.liencharge4#" cfsqltype="cf_sql_varchar" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.nomurgence1") AND #FORM.nomurgence1# NEQ "">
                <cfqueryparam value="#FORM.nomurgence1#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.prenomurgence1") AND #FORM.prenomurgence1# NEQ "">
                <cfqueryparam value="#FORM.prenomurgence1#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telurgence1") AND #FORM.telurgence1# NEQ "">
                <cfqueryparam value="#FORM.telurgence1#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.gsmurgence1") AND #FORM.gsmurgence1# NEQ "">
                <cfqueryparam value="#FORM.gsmurgence1#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
               ''
            </cfif>, 
            <cfif IsDefined("FORM.lienurgence1") AND #FORM.lienurgence1# NEQ "">
               <cfqueryparam value="#FORM.lienurgence1#" cfsqltype="cf_sql_varchar" maxlength="255">
            <cfelse>
               ''
            </cfif>, 
            <cfif IsDefined("FORM.nomurgence2") AND #FORM.nomurgence2# NEQ "">
                <cfqueryparam value="#FORM.nomurgence2#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.prenomurgence2") AND #FORM.prenomurgence2# NEQ "">
                <cfqueryparam value="#FORM.prenomurgence2#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telurgence2") AND #FORM.telurgence2# NEQ "">
                <cfqueryparam value="#FORM.telurgence2#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.gsmurgence2") AND #FORM.gsmurgence2# NEQ "">
                <cfqueryparam value="#FORM.gsmurgence2#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.lienurgence2") AND #FORM.lienurgence2# NEQ "">
                <cfqueryparam value="#FORM.lienurgence2#" cfsqltype="cf_sql_varchar" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.autorisemesure") AND #FORM.autorisemesure# NEQ "">
                <cfqueryparam value="#FORM.autorisemesure#" cfsqltype="cf_sql_numeric">
            <cfelse>
                NULL
            </cfif>, 
            <cfif IsDefined("FORM.lieuhospital") AND #FORM.lieuhospital# NEQ "">
                <cfqueryparam value="#FORM.lieuhospital#" cfsqltype="cf_sql_varchar" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.autorisephoto") AND #FORM.autorisephoto# NEQ "">
                <cfqueryparam value="#FORM.autorisephoto#" cfsqltype="cf_sql_numeric">
            <cfelse>
                NULL
            </cfif>, 
            <cfif IsDefined("FORM.autorisequiter") AND #FORM.autorisequiter# NEQ "">
                <cfqueryparam value="#FORM.autorisequiter#" cfsqltype="cf_sql_numeric">
            <cfelse>
                NULL
            </cfif>, 
            <cfif IsDefined("FORM.heurequiter") AND #FORM.heurequiter# NEQ "">
                <cfqueryparam value="#FORM.heurequiter#" cfsqltype="cf_sql_clob" maxlength="8">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.responsabilitecivil")>
                1
            <cfelse>
                0
            </cfif>, 
            <cfif IsDefined("FORM.individuelaccident")>
                1
            <cfelse>
                0
            </cfif>, 
            <cfif IsDefined("FORM.compagnieassurance") AND #FORM.compagnieassurance# NEQ "">
                <cfqueryparam value="#FORM.compagnieassurance#" cfsqltype="cf_sql_clob" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.numcontrat") AND #FORM.numcontrat# NEQ "">
                <cfqueryparam value="#FORM.numcontrat#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.vaccin1") AND #FORM.vaccin1# NEQ "">
                <cfqueryparam value="#FORM.vaccin1#" cfsqltype="cf_sql_clob" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.vaccin2") AND #FORM.vaccin2# NEQ "">
                <cfqueryparam value="#FORM.vaccin2#" cfsqltype="cf_sql_clob" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.vaccin3") AND #FORM.vaccin3# NEQ "">
                <cfqueryparam value="#FORM.vaccin3#" cfsqltype="cf_sql_clob" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.pai")>
                1
            <cfelse>
                0
            </cfif>, 
            <cfif IsDefined("FORM.prenommedecin") AND #FORM.prenommedecin# NEQ "">
                <cfqueryparam value="#FORM.prenommedecin#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.telmedecin") AND #FORM.telmedecin# NEQ "">
                <cfqueryparam value="#FORM.telmedecin#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.recommandation") AND #FORM.recommandation# NEQ "">
                <cfqueryparam value="#FORM.recommandation#" cfsqltype="cf_sql_clob" maxlength="255">
            <cfelse>
                ''
            </cfif>, 
            <cfif IsDefined("FORM.nommedecin") AND #FORM.nommedecin# NEQ "">
                <cfqueryparam value="#FORM.nommedecin#" cfsqltype="cf_sql_clob" maxlength="50">
            <cfelse>
                ''
            </cfif>,'#form.modesortie#', 
            <cfif IsDefined("FORM.autorisetransport") AND #FORM.autorisetransport# NEQ "">
                <cfqueryparam value="#FORM.autorisetransport#" cfsqltype="cf_sql_numeric">
            <cfelse>
                NULL
            </cfif>)
        </cfquery>
    </cfif>
</cfif>

<cfquery name="RechercheParent" datasource="f8w_test">
    Select * from client use index(siret_numclient) 
    where siret='#lect_etablissement.siret#' and numclient='#lect_client.numclient#'
</cfquery>
<cfquery name="enfantinfo" datasource="f8w_test">
    Select * from enfantinfo where id=0
</cfquery>
<cfquery name="autreenfant" datasource="f8w_test">
    Select NumEnfant,nom,prénom from enfant use index(siret_numclient) where siret='#lect_etablissement.siret#' and numclienti='#lect_client.numclient#' and parti=0 and del=0
</cfquery>
	
<cfif isdefined("form.modenfant")><!--- clic sur modifier --->
    <cfquery name="relectenf" datasource="f8w_test">
        Select * from enfant use index(siret_numenfant) where siret='#lect_etablissement.siret#' 
        and numenfant='#form.numenfant#'
    </cfquery>
    <cfif isdefined("form.enfantclasse")>
        <cfquery name="relectclasse" datasource="f8w_test">
            Select NumClasse from enfantclasse use index(siret_numenfant_numclasse) where siret='#lect_etablissement.siret#' 
            and numenfant='#form.numenfant#' and numclasse='#form.enfantclasse#'
        </cfquery>
        <cfif #relectclasse.recordcount# eq 0 and #form.enfantclasse# gt 0>
            <cfquery name="classe" datasource="f8w_test">
                Select * from classe use index(siret_numclasse) where siret='#lect_etablissement.siret#'
                and numclasse='#form.enfantclasse#'
            </cfquery>
            <cfif #classe.recordcount#>
                <cfset mod_txt = #mod_txt# & "Nouvelle classe : " & #classe.libclasse# & #chr(13)#> 
                <cfset mod_html = #mod_html# & "<p>Nouvelle classe : " & #classe.libclasse# & "</p>"> 
                <cfquery name="relectclasse" datasource="f8w_test">
                    Select id from enfantclasse use index(siret_numenfant) where siret='#lect_etablissement.siret#' 
                    and numenfant='#form.numenfant#'
                </cfquery>
                <cfif #relectclasse.recordcount#>
                    <cfquery name="maj" datasource="f8w_test_test">
                        Update enfantclasse set numclasse='#form.enfantclasse#' where siret='#lect_etablissement.siret#' 
                        and numenfant='#form.numenfant#'
                    </cfquery>
                <cfelse>
                    <cfquery name="add" datasource="f8w_test_test">
                        Insert into enfantclasse(siret,numenfant,numclasse) values 
                        ('#lect_etablissement.siret#','#form.numenfant#','#form.enfantclasse#')
                    </cfquery>
                </cfif>
            </cfif>
        </cfif>
    </cfif>
    <cfif #form.datenaissance# neq #relectenf.datenaissance#>
        <cfset mod_txt = #mod_txt# & "Anciene date de naissance : " & #relectenf.datenaissance# & ", Nouvelle : " & #form.datenaissance# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Anciene date de naissance : " & #relectenf.datenaissance# & ", Nouvelle : " & #form.datenaissance# & "</p>">        
    </cfif>
    <cfif #form.sexe# neq #relectenf.sexe#>
        <cfset mod_txt = #mod_txt# & "Ancien sexe : " & #relectenf.sexe# & ", Nouveau : " & #form.sexe# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien sexe : " & #relectenf.sexe# & ", Nouveau : " & #form.sexe# & "</p>">        
    </cfif>
    <cfif #relectenf.Allergie# neq #form.allergie# or #relectenf.mention# neq #form.mention#>
        <cfset form.AllergieAlimentaire = 1>
    </cfif>		
    <cfif #form.allergie# neq #relectenf.allergie#>
       <cfquery name="allergie" datasource="f8w_test">
            Select * from mention use index(siret) where siret='#lect_etablissement.siret#' order by id
       </cfquery> 
       <cfif #allergie.recordcount# eq 0>
           <cfquery name="allergie" datasource="f8w_test">
                Select * from mentions order by id
           </cfquery>                                
       </cfif>
        <cfquery name="old" dbtype="query">        
            Select desi from allergie where id=#relectenf.allergie#
        </cfquery>
        <cfquery name="new" dbtype="query">        
            Select desi from allergie where id=#form.allergie#
        </cfquery>
        <cfset mod_txt = #mod_txt# & "Anciene mention : " & #old.desi# & ", Nouvelle : " & #new.desi# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Anciene mention : " & #old.desi# & ", Nouvelle : " & #new.desi# & "</p>">        
    </cfif>
    <cfif #form.mention# neq #relectenf.mention#>
        <cfset mod_txt = #mod_txt# & "Ancien complément : " & #relectenf.mention# & ", Nouveau : " & #form.mention# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien complément : " & #relectenf.mention# & ", Nouveau : " & #form.mention# & "</p>">        
    </cfif>
    <cfif #mod_txt# neq "">
        <cfquery name="maj" datasource="f8w_test_test">
            Update enfant set datenaissance='#form.datenaissance#',sexe='#form.sexe#',allergie='#form.allergie#',
            mention='#form.mention#',AllergieAlimentaire='#form.AllergieAlimentaire#' 
            where siret='#lect_etablissement.siret#' and numenfant='#form.numenfant#'
        </cfquery>
        <cfset msg1 = "Modification effectuées avec succès !">
    </cfif>
    <cfif isdefined("form.nomcharge1")>
        <cfquery name="relect" datasource="f8w_test">
            Select * from enfantinfo where id='#form.id#'
        </cfquery>
        <cfif #form.nomcharge1# neq #relect.nomcharge1#>
            <cfset mod_txt = #mod_txt# & "Ancien nom personne autorisée : " & #relect.nomcharge1# & ", Nouveau : " & #form.nomcharge1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom personne autorisée : " & #relect.nomcharge1# & ", Nouveau : " & #form.nomcharge1# & "</p>">        
        </cfif>
        <cfif #form.prenomcharge1# neq #relect.prenomcharge1#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom personne autorisée : " & #relect.prenomcharge1# & ", Nouveau : " & #form.prenomcharge1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom personne autorisée : " & #relect.prenomcharge1# & ", Nouveau : " & #form.prenomcharge1# & "</p>">        
        </cfif>
        <cfif #form.telcharge1# neq #relect.telcharge1#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel personne autorisée : " & #relect.telcharge1# & ", Nouveau : " & #form.telcharge1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel personne autorisée : " & #relect.telcharge1# & ", Nouveau : " & #form.telcharge1# & "</p>">        
        </cfif>
        <cfif #form.gsmcharge1# neq #relect.gsmcharge1#>
            <cfset mod_txt = #mod_txt# & "Ancien Mobile personne autorisée : " & #relect.gsmcharge1# & ", Nouveau : " & #form.gsmcharge1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Mobile personne autorisée : " & #relect.gsmcharge1# & ", Nouveau : " & #form.gsmcharge1# & "</p>">        
        </cfif>
        <cfif #form.liencharge1# neq #relect.liencharge1#>
            <cfset mod_txt = #mod_txt# & "Ancien Lien personne autorisée : " & #relect.liencharge1# & ", Nouveau : " & #form.liencharge1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lien personne autorisée : " & #relect.liencharge1# & ", Nouveau : " & #form.liencharge1# & "</p>">        
        </cfif>
        <cfif #form.nomcharge2# neq #relect.nomcharge2#>
            <cfset mod_txt = #mod_txt# & "Ancien nom personne autorisée : " & #relect.nomcharge2# & ", Nouveau : " & #form.nomcharge2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom personne autorisée : " & #relect.nomcharge2# & ", Nouveau : " & #form.nomcharge2# & "</p>">        
        </cfif>
        <cfif #form.prenomcharge2# neq #relect.prenomcharge2#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom personne autorisée : " & #relect.prenomcharge2# & ", Nouveau : " & #form.prenomcharge2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom personne autorisée : " & #relect.prenomcharge2# & ", Nouveau : " & #form.prenomcharge2# & "</p>">        
        </cfif>
        <cfif #form.telcharge2# neq #relect.telcharge2#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel personne autorisée : " & #relect.telcharge2# & ", Nouveau : " & #form.telcharge2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel personne autorisée : " & #relect.telcharge2# & ", Nouveau : " & #form.telcharge2# & "</p>">        
        </cfif>
        <cfif #form.gsmcharge2# neq #relect.gsmcharge2#>
            <cfset mod_txt = #mod_txt# & "Ancien Mobile personne autorisée : " & #relect.gsmcharge2# & ", Nouveau : " & #form.gsmcharge2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Mobile personne autorisée : " & #relect.gsmcharge2# & ", Nouveau : " & #form.gsmcharge2# & "</p>">        
        </cfif>
        <cfif #form.liencharge2# neq #relect.liencharge2#>
            <cfset mod_txt = #mod_txt# & "Ancien Lien personne autorisée : " & #relect.liencharge2# & ", Nouveau : " & #form.liencharge2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lien personne autorisée : " & #relect.liencharge2# & ", Nouveau : " & #form.liencharge2# & "</p>">        
        </cfif>
        <cfif #form.nomcharge3# neq #relect.nomcharge3#>
            <cfset mod_txt = #mod_txt# & "Ancien nom personne autorisée : " & #relect.nomcharge3# & ", Nouveau : " & #form.nomcharge3# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom personne autorisée : " & #relect.nomcharge3# & ", Nouveau : " & #form.nomcharge3# & "</p>">        
        </cfif>
        <cfif #form.prenomcharge3# neq #relect.prenomcharge3#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom personne autorisée : " & #relect.prenomcharge3# & ", Nouveau : " & #form.prenomcharge3# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom personne autorisée : " & #relect.prenomcharge3# & ", Nouveau : " & #form.prenomcharge3# & "</p>">        
        </cfif>
        <cfif #form.telcharge3# neq #relect.telcharge3#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel personne autorisée : " & #relect.telcharge3# & ", Nouveau : " & #form.telcharge3# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel personne autorisée : " & #relect.telcharge3# & ", Nouveau : " & #form.telcharge3# & "</p>">        
        </cfif>
        <cfif #form.gsmcharge3# neq #relect.gsmcharge3#>
            <cfset mod_txt = #mod_txt# & "Ancien Mobile personne autorisée : " & #relect.gsmcharge3# & ", Nouveau : " & #form.gsmcharge3# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Mobile personne autorisée : " & #relect.gsmcharge3# & ", Nouveau : " & #form.gsmcharge3# & "</p>">        
        </cfif>
        <cfif #form.liencharge3# neq #relect.liencharge3#>
            <cfset mod_txt = #mod_txt# & "Ancien Lien personne autorisée : " & #relect.liencharge3# & ", Nouveau : " & #form.liencharge3# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lien personne autorisée : " & #relect.liencharge3# & ", Nouveau : " & #form.liencharge3# & "</p>">        
        </cfif>
        <cfif #form.nomcharge4# neq #relect.nomcharge4#>
            <cfset mod_txt = #mod_txt# & "Ancien nom personne autorisée : " & #relect.nomcharge4# & ", Nouveau : " & #form.nomcharge4# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom personne autorisée : " & #relect.nomcharge4# & ", Nouveau : " & #form.nomcharge4# & "</p>">        
        </cfif>
        <cfif #form.prenomcharge4# neq #relect.prenomcharge4#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom personne autorisée : " & #relect.prenomcharge4# & ", Nouveau : " & #form.prenomcharge4# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom personne autorisée : " & #relect.prenomcharge4# & ", Nouveau : " & #form.prenomcharge4# & "</p>">        
        </cfif>
        <cfif #form.telcharge4# neq #relect.telcharge4#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel personne autorisée : " & #relect.telcharge4# & ", Nouveau : " & #form.telcharge4# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel personne autorisée : " & #relect.telcharge4# & ", Nouveau : " & #form.telcharge4# & "</p>">        
        </cfif>
        <cfif #form.gsmcharge4# neq #relect.gsmcharge4#>
            <cfset mod_txt = #mod_txt# & "Ancien Mobile personne autorisée : " & #relect.gsmcharge4# & ", Nouveau : " & #form.gsmcharge4# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Mobile personne autorisée : " & #relect.gsmcharge4# & ", Nouveau : " & #form.gsmcharge4# & "</p>">        
        </cfif>
        <cfif #form.liencharge4# neq #relect.liencharge4#>
            <cfset mod_txt = #mod_txt# & "Ancien Lien personne autorisée : " & #relect.liencharge4# & ", Nouveau : " & #form.liencharge4# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lien personne autorisée : " & #relect.liencharge4# & ", Nouveau : " & #form.liencharge4# & "</p>">        
        </cfif>
        <cfif #form.nomurgence1# neq #relect.nomurgence1#>
            <cfset mod_txt = #mod_txt# & "Ancien nom personne urgence : " & #relect.nomurgence1# & ", Nouveau : " & #form.nomurgence1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom personne urgence : " & #relect.nomurgence1# & ", Nouveau : " & #form.nomurgence1# & "</p>">        
        </cfif>
        <cfif #form.prenomurgence1# neq #relect.prenomurgence1#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom personne urgence : " & #relect.prenomurgence1# & ", Nouveau : " & #form.prenomurgence1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom personne urgence : " & #relect.prenomurgence1# & ", Nouveau : " & #form.prenomurgence1# & "</p>">        
        </cfif>
        <cfif #form.telurgence1# neq #relect.telurgence1#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel personne urgence : " & #relect.telurgence1# & ", Nouveau : " & #form.telurgence1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel personne urgence : " & #relect.telurgence1# & ", Nouveau : " & #form.telurgence1# & "</p>">        
        </cfif>
        <cfif #form.gsmurgence1# neq #relect.gsmurgence1#>
            <cfset mod_txt = #mod_txt# & "Ancien Mobile personne urgence : " & #relect.gsmurgence1# & ", Nouveau : " & #form.gsmurgence1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Mobile personne urgence : " & #relect.gsmurgence1# & ", Nouveau : " & #form.gsmurgence1# & "</p>">        
        </cfif>
        <cfif #form.lienurgence1# neq #relect.lienurgence1#>
            <cfset mod_txt = #mod_txt# & "Ancien Lien personne urgence : " & #relect.lienurgence1# & ", Nouveau : " & #form.lienurgence1# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lien personne urgence : " & #relect.lienurgence1# & ", Nouveau : " & #form.lienurgence1# & "</p>">        
        </cfif>
        <cfif #form.nomurgence2# neq #relect.nomurgence2#>
            <cfset mod_txt = #mod_txt# & "Ancien nom personne urgence : " & #relect.nomurgence2# & ", Nouveau : " & #form.nomurgence2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom personne urgence : " & #relect.nomurgence2# & ", Nouveau : " & #form.nomurgence2# & "</p>">        
        </cfif>
        <cfif #form.prenomurgence2# neq #relect.prenomurgence2#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom personne urgence : " & #relect.prenomurgence2# & ", Nouveau : " & #form.prenomurgence2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom personne urgence : " & #relect.prenomurgence2# & ", Nouveau : " & #form.prenomurgence2# & "</p>">        
        </cfif>
        <cfif #form.telurgence2# neq #relect.telurgence2#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel personne urgence : " & #relect.telurgence2# & ", Nouveau : " & #form.telurgence2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel personne urgence : " & #relect.telurgence2# & ", Nouveau : " & #form.telurgence2# & "</p>">        
        </cfif>
        <cfif #form.gsmurgence2# neq #relect.gsmurgence2#>
            <cfset mod_txt = #mod_txt# & "Ancien Mobile personne urgence : " & #relect.gsmurgence2# & ", Nouveau : " & #form.gsmurgence2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Mobile personne urgence : " & #relect.gsmurgence2# & ", Nouveau : " & #form.gsmurgence2# & "</p>">        
        </cfif>
        <cfif #form.lienurgence2# neq #relect.lienurgence2#>
            <cfset mod_txt = #mod_txt# & "Ancien Lien personne urgence : " & #relect.lienurgence2# & ", Nouveau : " & #form.lienurgence2# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lien personne urgence : " & #relect.lienurgence2# & ", Nouveau : " & #form.lienurgence2# & "</p>">        
        </cfif>
        <cfif #form.autorisemesure# neq #relect.autorisemesure#>
            <cfif #relect.autorisemesure# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.autorisemesure# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.autorisemesure# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">                
            </cfif>
            <cfset mod_txt = #mod_txt# & "Anciene autorisation mesures : " & #ancien# & ", Nouveau : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Anciene autorisation mesures : " & #ancien# & ", Nouveau : " & #Nouveau# & "</p>">        
        </cfif>
        <cfif #form.lieuhospital# neq #relect.lieuhospital#>
            <cfset mod_txt = #mod_txt# & "Ancien Lieu d'hospitalisation : " & #relect.lieuhospital# & ", Nouveau : " & #form.lieuhospital# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Lieu d'hospitalisation : " & #relect.lieuhospital# & ", Nouveau : " & #form.lieuhospital# & "</p>">        
        </cfif>
        <cfif #form.autorisephoto# neq #relect.autorisephoto#>
            <cfif #relect.autorisephoto# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.autorisephoto# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.autorisephoto# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">                
            </cfif>            
            <cfset mod_txt = #mod_txt# & "Ancienne autorisation photo : " & #ancien# & ", Nouvelle : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne autorisation photo : " & #ancien# & ", Nouvelle : " & #Nouveau# & "</p>">        
        </cfif>
        <cfif #form.autorisequiter# neq #relect.autorisequiter#>
            <cfif #relect.autorisequiter# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.autorisequiter# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.autorisequiter# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">                
            </cfif>
            <cfset mod_txt = #mod_txt# & "Ancienne autorisation quiter : " & #ancien# & ", Nouvelle : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne autorisation quiter : " & #ancien# & ", Nouvelle : " & #Nouveau# & "</p>">        
        </cfif>
        <cfif #form.heurequiter# neq #relect.heurequiter#>
            <cfset mod_txt = #mod_txt# & "Ancienne heure sortie : " & #relect.heurequiter# & ", Nouvelle : " & #form.heurequiter# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne heure sortie : " & #relect.heurequiter# & ", Nouvelle : " & #form.heurequiter# & "</p>">        
        </cfif>
        <cfif #form.modesortie# neq #relect.modesortie#>
            <cfset mod_txt = #mod_txt# & "Ancien mode sortie : " & #relect.modesortie# & ", Nouveau : " & #form.modesortie# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien mode sortie : " & #relect.modesortie# & ", Nouveau : " & #form.modesortie# & "</p>">        
        </cfif>
        <cfif #form.autorisetransport# neq #relect.autorisetransport#>
            <cfif #relect.autorisetransport# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.autorisetransport# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.autorisetransport# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">
            </cfif>            
            <cfset mod_txt = #mod_txt# & "Ancienne autorisation transport : " & #ancien# & ", Nouvelle : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne autorisation transport : " & #ancien# & ", Nouvelle : " & #Nouveau# & "</p>">
        </cfif>
        <cfif isdefined("form.responsabilitecivil") is false>
            <cfset form.responsabilitecivil = 0>
        <cfelse>
            <cfset form.responsabilitecivil = 1>
        </cfif>
        <cfif #form.responsabilitecivil# neq #relect.responsabilitecivil#>
            <cfif #relect.responsabilitecivil# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.responsabilitecivil# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.responsabilitecivil# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">
            </cfif>
            <cfset mod_txt = #mod_txt# & "Ancienne responsabilité civil : " & #ancien# & ", Nouvelle : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne responsabilité civil : " & #ancien# & ", Nouvelle : " & #Nouveau# & "</p>">
        </cfif>
        <cfif isdefined("form.individuelaccident") is false>
            <cfset form.individuelaccident = 0>
        <cfelse>
            <cfset form.individuelaccident = 1>
        </cfif>
        <cfif #form.individuelaccident# neq #relect.individuelaccident#>
            <cfif #relect.individuelaccident# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.individuelaccident# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.individuelaccident# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">
            </cfif>
            <cfset mod_txt = #mod_txt# & "Ancienne individuelle accident : " & #ancien# & ", Nouvelle : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne individuelle accident : " & #ancien# & ", Nouvelle : " & #Nouveau# & "</p>">
        </cfif>
        <cfif #form.compagnieassurance# neq #relect.compagnieassurance#>
            <cfset mod_txt = #mod_txt# & "Ancienne compagnie d'assurance : " & #relect.compagnieassurance# & ", Nouvelle : " & #form.compagnieassurance# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne compagnie d'assurance : " & #relect.compagnieassurance# & ", Nouvelle : " & #form.compagnieassurance# & "</p>">        
        </cfif>
        <cfif #form.numcontrat# neq #relect.numcontrat#>
            <cfset mod_txt = #mod_txt# & "Ancien N° de contrat : " & #relect.numcontrat# & ", Nouveau : " & #form.numcontrat# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien N° de contrat : " & #relect.numcontrat# & ", Nouveau : " & #form.numcontrat# & "</p>">        
        </cfif>
        <cfif isdefined("form.pai") is false>
            <cfset form.pai = 0>
        <cfelse>
            <cfset form.pai = 1>
        </cfif>
        <cfif #form.pai# neq #relect.pai#>
            <cfif #relect.pai# eq 0>
                <cfset ancien = "Non">
                <cfset Nouveau = "Oui">
            <cfelseif #relect.pai# eq 1>
                <cfset ancien = "Oui">
                <cfset Nouveau = "Non">
            <cfelseif #form.pai# eq 0>
                <cfset ancien = "">
                <cfset Nouveau = "Non">
            <cfelse>
                <cfset ancien = "">
                <cfset Nouveau = "Oui">
            </cfif>
            <cfset mod_txt = #mod_txt# & "Ancien PAI : " & #ancien# & ", Nouveau : " & #Nouveau# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien PAI : " & #ancien# & ", Nouveau : " & #Nouveau# & "</p>">
        </cfif>
        <cfif #form.nommedecin# neq #relect.nommedecin#>
            <cfset mod_txt = #mod_txt# & "Ancien nom médecin : " & #relect.nommedecin# & ", Nouveau : " & #form.nommedecin# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien nom médecin : " & #relect.nommedecin# & ", Nouveau : " & #form.nommedecin# & "</p>">        
        </cfif>
        <cfif #form.prenommedecin# neq #relect.prenommedecin#>
            <cfset mod_txt = #mod_txt# & "Ancien prénom médecin : " & #relect.prenommedecin# & ", Nouveau : " & #form.prenommedecin# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien prénom médecin : " & #relect.prenommedecin# & ", Nouveau : " & #form.prenommedecin# & "</p>">        
        </cfif>
        <cfif #form.telmedecin# neq #relect.telmedecin#>
            <cfset mod_txt = #mod_txt# & "Ancien Tel médecin : " & #relect.telmedecin# & ", Nouveau : " & #form.telmedecin# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancien Tel médecin : " & #relect.telmedecin# & ", Nouveau : " & #form.telmedecin# & "</p>">        
        </cfif>
        <cfif #form.recommandation# neq #relect.recommandation#>
            <cfset mod_txt = #mod_txt# & "Ancienne recommandations : " & #relect.recommandation# & ", Nouvelle : " & #form.recommandation# & #chr(13)#> 
            <cfset mod_html = #mod_html# & "<p>Ancienne recommandation : " & #relect.recommandation# & ", Nouvelle : " & #form.recommandation# & "</p>">        
        </cfif>
        <cfif val(#form.id#) gt 0>
            <cfupdate datasource="f8w_test_test" tablename="enfantinfo" formfields="id,nomcharge1,prenomcharge1,telcharge1,gsmcharge1,liencharge1,nomcharge2,prenomcharge2,telcharge2,gsmcharge2,liencharge2,nomcharge3,prenomcharge3,telcharge3,gsmcharge3,liencharge3,nomcharge4,prenomcharge4,telcharge4,gsmcharge4,liencharge4,nomurgence1,prenomurgence1,telurgence1,gsmurgence1,lienurgence1,nomurgence2,prenomurgence2,telurgence2,gsmurgence2,lienurgence2,autorisemesure,lieuhospital,autorisephoto,autorisequiter,heurequiter,responsabilitecivil,individuelaccident,compagnieassurance,numcontrat,vaccin1,vaccin2,vaccin3,pai,prenommedecin,telmedecin,recommandation,nommedecin,modesortie,autorisetransport">
        <cfelse>
            <cfset form.siret = #lect_etablissement.siret#>
            <cfinsert datasource="f8w_test_test" tablename="enfantinfo" formfields="id,siret,NumEnfant,nomcharge1,prenomcharge1,telcharge1,gsmcharge1,liencharge1,nomcharge2,prenomcharge2,telcharge2,gsmcharge2,liencharge2,nomcharge3,prenomcharge3,telcharge3,gsmcharge3,liencharge3,nomcharge4,prenomcharge4,telcharge4,gsmcharge4,liencharge4,nomurgence1,prenomurgence1,telurgence1,gsmurgence1,lienurgence1,nomurgence2,prenomurgence2,telurgence2,gsmurgence2,lienurgence2,autorisemesure,lieuhospital,autorisephoto,autorisequiter,heurequiter,responsabilitecivil,individuelaccident,compagnieassurance,numcontrat,vaccin1,vaccin2,vaccin3,pai,prenommedecin,telmedecin,recommandation,nommedecin,modesortie,autorisetransport">
        </cfif>
    </cfif>

    <cfif #mod_txt# neq ""><!--- envoi mail --->
        <cfset msg1 = "Modification effectuées avec succès !">
        <cfset sujet = "Modification de données par " & #lect_client.nom# & " " & #lect_client.prénom#>
        <cfset txt_body = "Des données ont été modifiées dans l'espace parent pour l'enfant " & #relectenf.nom# & " " & #relectenf.prénom# & " :" & #chr(13)#>
        <cfset html_body = "<p>Des données ont été modifiées dans l'espace parent pour l'enfant " & #relectenf.nom# & " " & #relectenf.prénom# & " :</p>">
        <cfset txt_body = #txt_body# & #mod_txt#>
        <cfset html_body = #html_body# & #mod_html#>
        <cfquery name="cptmailserv" datasource="mission">
            Select * from cpt_appel_msg where pour='MSG'
        </cfquery>        <!---
        <cfquery name="addspool" datasource="services">
            Insert into spool_mail (de,pour,sujet,html_body,txt_body,joint,date_crea,
            fail_to,EmailAccuseRecep,EmailNotifRecep,siret,leserveur,
            utilisateur,mailutilisateur,motdepasse) 
            values('message@cantine-de-france.fr','#lect_etablissement.EmailE#',
            <cfqueryparam value="#sujet#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
            <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,
            '',#now()#,'#lect_etablissement.EmailE#',
            '#lect_etablissement.EmailAccuseRecep#','#lect_etablissement.EmailNotifRecep#',
            '#lect_etablissement.siret#',
            '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
        </cfquery>--->
        <cfset article = "">
        <cfswitch expression="#mid(lect_etablissement.nomE,1,2)#">
            <cfcase delimiters="," value="CO,MA">
                <cfset article = "La">
            </cfcase>
            <cfcase delimiters="," value="SI,CE">
                <cfset article = "Le">
            </cfcase>
            <cfcase value="AS">
                <cfset article = "L'">
            </cfcase>
            <cfdefaultcase>
                <cfset article = "">
            </cfdefaultcase>
        </cfswitch>
        <cfset msg1 = "Modification effectuées avec succès, " & #article# & " " & #lect_etablissement.nomE# & " a été averti par mail.">
    </cfif>
</cfif>

<cfparam name="url.id" default="0">
<cfquery name="RechercheParent" datasource="f8w_test">
    Select * from client use index(siret_numclient) 
    where siret='#lect_etablissement.siret#' and numclient='#lect_client.numclient#'
</cfquery>
<cfquery name="toutenfant" datasource="f8w_test">
    Select id,nom,prénom from enfant use index(siret_numclientI) 
    where siret='#lect_etablissement.siret#' and numclientI='#lect_client.numclient#' 
    and parti=0 and del=0 order by nom,prénom
</cfquery>
<cfif #url.id# eq 0>
    <cfset url.id = #toutenfant.id#>
<cfelse>
    <cfquery name="verid" dbtype="query">
        select * from toutenfant where id='#url.id#'
    </cfquery>
    <cfif #verid.recordcount# eq 0>
        <cflocation url="/ModPar/?id_mnu=#url.id_mnu#" addtoken="no">
    </cfif>
</cfif>	
<cfquery name="enfant" datasource="f8w_test">
    Select * from enfant where id='#url.id#'
</cfquery>
<cfquery name="enfantinfo" datasource="f8w_test">
    Select * from enfantinfo use index(siret_NumEnfant) 
    where siret='#lect_etablissement.siret#' and NumEnfant='#enfant.NumEnfant#'
</cfquery>

<cfif isdefined("form.go1")>
    <cfif #form.NoRue# neq #lect_client.NoRue#>
        <cfset mod_txt = #mod_txt# & "Ancien N° de rue : " & #lect_client.norue# & ", Nouveau N° : " & #form.norue# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien N° de rue : " & #lect_client.norue# & ", Nouveau N° : " & #form.norue# & "</p>">
    </cfif>
    <cfif #form.adr1# neq #lect_client.adr1#>
        <cfset mod_txt = #mod_txt# & "Anciene adresse : " & #lect_client.adr1# & ", Nouvelle : " & #form.adr1# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Anciene adresse : " & #lect_client.adr1# & ", Nouvelle : " & #form.adr1# & "</p>">
    </cfif>
    <cfif #form.adr2# neq #lect_client.adr2#>
        <cfset mod_txt = #mod_txt# & "Ancien complément d'adresse : " & #lect_client.adr2# & ", Nouveau : " & #form.adr2# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien complément d'adresse : " & #lect_client.adr2# & ", Nouveau : " & #form.adr2# & "</p>">
    </cfif>
    <cfif #form.cp# neq #lect_client.cp#>
        <cfset mod_txt = #mod_txt# & "Ancien code postal : " & #lect_client.cp# & ", Nouveau : " & #form.cp# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien code postal : " & #lect_client.cp# & ", Nouveau : " & #form.cp# & "</p>">
    </cfif>
    <cfif #form.ville# neq #lect_client.ville#>
        <cfset mod_txt = #mod_txt# & "Anciene ville : " & #lect_client.ville# & ", Nouvelle : " & #form.ville# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Anciene ville : " & #lect_client.ville# & ", Nouvelle : " & #form.ville# & "</p>">
    </cfif>
    <cfif #form.tel# neq #lect_client.tel#>
        <cfset mod_txt = #mod_txt# & "Ancien Tel : " & #lect_client.tel# & ", Nouveau : " & #form.tel# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien Tel : " & #lect_client.tel# & ", Nouveau : " & #form.tel# & "</p>">
    </cfif>
    <cfif #form.teltravail# neq #lect_client.teltravail#>
        <cfset mod_txt = #mod_txt# & "Ancien Tel travail : " & #lect_client.teltravail# & ", Nouveau : " & #form.teltravail# & #chr(13)#> 
         <cfset mod_html = #mod_html# & "<p>Ancien Tel travail : " & #lect_client.teltravail# & ", Nouveau : " & #form.teltravail# & "</p>">
    </cfif>
    <cfif #form.telportable# neq #lect_client.telportable#>
        <cfset mod_txt = #mod_txt# & "Ancien Tel portable : " & #lect_client.telportable# & ", Nouveau : " & #form.telportable# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien Tel portable : " & #lect_client.telportable# & ", Nouveau : " & #form.telportable# & "</p>">
    </cfif>
    <cfif #form.QuotientFamilial# neq #lect_client.QuotientFamilial#>
        <cfif findnocase(",",#form.QuotientFamilial#)>
            <cfset form.QuotientFamilial = replacenocase(#form.QuotientFamilial#,",",".")>
        </cfif>
        <cfset mod_txt = #mod_txt# & "Ancien Quotient Familial : " & #lect_client.QuotientFamilial# & ", Nouveau : " & #form.QuotientFamilial# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien QuotientFamilial : " & #lect_client.QuotientFamilial# & ", Nouveau : " & #form.QuotientFamilial# & "</p>">
    </cfif>
    <cfif #form.numallocataire# neq #lect_client.numallocataire#>
        <cfset mod_txt = #mod_txt# & "Ancien N° Allocataire : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien N° Allocataire : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & "</p>">
    </cfif>
    <cfif #form.numtelemployeur# neq #lect_client.numtelemployeur#>
        <cfset mod_txt = #mod_txt# & "Ancien Nom et Tel de l'employeur : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & #chr(13)#> 
        <cfset mod_html = #mod_html# & "<p>Ancien Nom et Tel de l'employeur : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & "</p>">
    </cfif>
    <cfif #form.bic# neq #lect_client.bic#><!--- modif iban --->
        <cfif #form.bic# eq "">
            <cfquery name="maj" datasource="f8w_test">
                Update client set bic='', iban='', ficapnom='', bcnom='', LibPrel='', ModePaiement=0, NumRolePremSepa=0 
                Where id='#form.id#'
            </cfquery>
        <cfelse>
            <cfset ModePaiement = 1>
            <cfset bcnom = module.ConformiteSepa(#ucase(form.bcnom)#)>
            <cfset ficapnom = module.ConformiteSepa(#ucase(form.ficapnom)#)>
            <cfset titulaire = #form.bcnom#>
            <cfset iban = #ucase(form.iban1)# & #form.iban2# & #form.iban3# & #form.iban4# & #form.iban5# & #form.iban6# & #form.iban7#>
            <cfset rum = CreateUUID()>
            <cfset rum = mid(#rum#,1, 35)>
            <cfset DateSignature = lsdateformat(now(),"dd/mm/YYYY")>    
            <cfset NumRolePremSepa = 0>
            <cfquery name="maj" datasource="f8w_test">
                Update client set bic='#form.bic#', iban='#iban#', ficapnom='#ficapnom#', 
                bcnom='#bcnom#', LibPrel='#form.LibPrel#', ModePaiement=1, NumRolePremSepa=0, rum='#rum#'
                Where id='#form.id#'
            </cfquery>
        </cfif>
    </cfif>
</cfif>

<cfif isdefined("form.go2")>
    <cfif #form.nom2# neq #lect_client.nom2#> 
        <cfset mod_txt1 = #mod_txt1# & "Ancien nom : " & #lect_client.nom2# & ", Nouveau : " & #form.nom2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien nom : " & #lect_client.nom2# & ", Nouveau : " & #form.nom2# & "</p>">
    </cfif>
    <cfif #form.prénom2# neq #lect_client.prénom2#> 
        <cfset mod_txt1 = #mod_txt1# & "Ancien prénom : " & #lect_client.prénom2# & ", Nouveau : " & #form.prénom2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien prénom : " & #lect_client.prénom2# & ", Nouveau : " & #form.prénom2# & "</p>">
    </cfif>
    <cfif #form.NoRue2# neq #lect_client.NoRue2#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien N° de rue : " & #lect_client.norue2# & ", Nouveau N° : " & #form.norue2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien N° de rue : " & #lect_client.norue2# & ", Nouveau N° : " & #form.norue2# & "</p>">
    </cfif>
    <cfif #form.adr21# neq #lect_client.adr21#>
        <cfset mod_txt1 = #mod_txt1# & "Anciene adresse : " & #lect_client.adr21# & ", Nouvelle : " & #form.adr21# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Anciene adresse : " & #lect_client.adr21# & ", Nouvelle : " & #form.adr21# & "</p>">
    </cfif>
    <cfif #form.adr22# neq #lect_client.adr22#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien complément d'adresse : " & #lect_client.adr22# & ", Nouveau : " & #form.adr22# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien complément d'adresse : " & #lect_client.adr22# & ", Nouveau : " & #form.adr22# & "</p>">
    </cfif>
    <cfif #form.cp2# neq #lect_client.cp2#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien code postal : " & #lect_client.cp2# & ", Nouveau : " & #form.cp2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien code postal : " & #lect_client.cp2# & ", Nouveau : " & #form.cp2# & "</p>">
    </cfif>
    <cfif #form.ville2# neq #lect_client.ville2#>
        <cfset mod_txt1 = #mod_txt1# & "Anciene ville : " & #lect_client.ville2# & ", Nouvelle : " & #form.ville2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Anciene ville : " & #lect_client.ville2# & ", Nouvelle : " & #form.ville2# & "</p>">
    </cfif>
    <cfif #form.tel2# neq #lect_client.tel2#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien Tel : " & #lect_client.tel2# & ", Nouveau : " & #form.tel2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien Tel : " & #lect_client.tel2# & ", Nouveau : " & #form.tel2# & "</p>">
    </cfif>
    <cfif #form.teltravail2# neq #lect_client.teltravail2#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien Tel travail : " & #lect_client.teltravail2# & ", Nouveau : " & #form.teltravail2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien Tel travail : " & #lect_client.teltravail2# & ", Nouveau : " & #form.teltravail2# & "</p>">
    </cfif>
    <cfif #form.telportable2# neq #lect_client.telportable2#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien Tel portable : " & #lect_client.telportable2# & ", Nouveau : " & #form.telportable2# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien Tel portable : " & #lect_client.telportable2# & ", Nouveau : " & #form.telportable2# & "</p>">
    </cfif>
    <cfif #form.numtelemployeur2# neq #lect_client.numtelemployeur2#>
        <cfset mod_txt1 = #mod_txt1# & "Ancien Nom et Tel de l'employeur : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & #chr(13)#> 
        <cfset mod_html1 = #mod_html1# & "<p>Ancien Nom et Tel de l'employeur : " & #lect_client.numallocataire# & ", Nouveau : " & #form.numallocataire# & "</p>">
    </cfif>
</cfif>

<cfif len(#mod_txt#) or len(#mod_txt1#)>
    <!--- Si modif parent 1 --->
    <cfif len(#mod_txt#)>
        <cfupdate datasource="f8w_test" tablename="client" formfields="id,Norue,adr1,adr2,cp,ville,tel,teltravail,telportable,Quotientfamilial,numtelemployeur,numallocataire">
        <cfset sujet = "Modification de données par " & #lect_client.nom# & " " & #lect_client.prénom#>
        <cfset txt_body = "Des données ont été modifiées dans l'espace parent :" & #chr(13)#>
        <cfset html_body = "<p>Des données ont été modifiées dans l'espace parent :</p>">
        <cfset txt_body = #txt_body# & "Pour le parent 1 (" & #lect_client.nom# & " " & #lect_client.prénom# & ") :" & #chr(13)#>
        <cfset txt_body = #txt_body# & #mod_txt#>
        <cfset html_body = #html_body# & "<p>Pour le parent 1 (" & #lect_client.nom# & " " & #lect_client.prénom# & ") :</p>">
        <cfset html_body = #html_body# & #mod_html#>
    </cfif>
    <!--- Si modif parent 2 --->
    <cfif len(#mod_txt1#)>
        <cfupdate datasource="f8w_test" tablename="client" formfields="id,nom2,prénom2,Norue2,adr21,adr22,cp2,ville2,tel2,teltravail2,telportable2,numtelemployeur2">
        <cfset sujet = "Modification de données par " & #lect_client.nom# & " " & #lect_client.prénom#>
        <cfset txt_body = "Des données ont été modifiées dans l'espace parent :" & #chr(13)#>
        <cfset html_body = "<p>Des données ont été modifiées dans l'espace parent :</p>">
        <cfset txt_body = #txt_body# & "Pour le parent 2 (" & #lect_client.nom2# & " " & #lect_client.prénom2# & ") :" & #chr(13)#>
        <cfset txt_body = #txt_body# & #mod_txt1#>
        <cfset html_body = #html_body# & "<p>Pour le parent 2 (" & #lect_client.nom2# & " " & #lect_client.prénom2# & ") :</p>">
        <cfset html_body = #html_body# & #mod_html1#>
    </cfif>

    <cfquery name="cptmailserv" datasource="mission">
        Select * from cpt_appel_msg where pour='MSG'
    </cfquery><!---
    <cfquery name="addspool" datasource="services">
        Insert into spool_mail (de,pour,sujet,html_body,txt_body,joint,date_crea,
        fail_to,EmailAccuseRecep,EmailNotifRecep,siret,leserveur,
        utilisateur,mailutilisateur,motdepasse) 
        values('message@cantine-de-france.fr','#lect_etablissement.EmailE#',
        <cfqueryparam value="#sujet#" cfsqltype="cf_sql_varchar">,
        <cfqueryparam value="#html_body#" cfsqltype="cf_sql_varchar">,
        <cfqueryparam value="#txt_body#" cfsqltype="cf_sql_varchar">,
        '',#now()#,'#lect_etablissement.EmailE#',
        '#lect_etablissement.EmailAccuseRecep#','#lect_etablissement.EmailNotifRecep#',
        '#lect_etablissement.siret#',
        '#cptmailserv.serveur#','#cptmailserv.utilisateur#','#cptmailserv.mailutilisateur#','#cptmailserv.psw#')
    </cfquery>--->
    <cfset article = "">
    <cfswitch expression="#mid(lect_etablissement.nomE,1,2)#">
        <cfcase delimiters="," value="CO,MA">
            <cfset article = "La">
        </cfcase>
        <cfcase delimiters="," value="SI,CE">
            <cfset article = "Le">
        </cfcase>
        <cfcase value="AS">
            <cfset article = "L'">
        </cfcase>
        <cfdefaultcase>
            <cfset article = "">
        </cfdefaultcase>
    </cfswitch>
    <cfset url.msg = "Modification effectuées avec succès, " & #article# & " " & #lect_etablissement.nomE# & " a été averti par mail.">
    <cfquery name="lect_client" datasource="f8w_test">
        Select * from client use index(siret_NumClient) Where siret = '#lect_user.siret#' and NumClient = <!---'#lect_user.NumClient#'--->"1"
    </cfquery>
</cfif>
<cfinclude template="/Query_Header.cfm">


<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>C2F - Données personnelles</title>
        <link rel="icon" type="image/png" href="/img/favicon_16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/img/favicon_32x32.png" sizes="32x32">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
        <link rel="stylesheet" href="/style.css">
    </head>

    <body>
        <div id="bgBlur">
            <!--BANNIERE-->
            <cfif len(#lect_etablissement.Bandeau_haut_objet#) eq 0>
                <header style="background-image: url(<cfoutput>'#lect_etablissement.Bandeau_haut#'</cfoutput>)">
                    <cfif lect_etablissement.Bandeau_haut is "/img/BandeauCantine.jpg">
                        <a href="/home/?id_mnu=149"><img src="/img/logo.png" alt="Logo Cantine de France" class="logo"></a>
                    </cfif>
                    <a href="/login/?id_mnu=146" class="disconnect p-2"><img src="/img/icon/icon_logoff_white.png" alt="bouton de déconnexion" class="pe-2" >Se déconnecter</a>
                </header>
            <cfelse>
                <header>
                    <cfoutput>#lect_etablissement.Bandeau_haut_objet#</cfoutput>
                </header>
            </cfif>

            <main class="lg-row">
                <!-- NAV -->
                <cfinclude template="/menu.cfm">

                <div class="col-lg-7 main">
                    <!-- NAME AND DESCRIPTION -->
                    <div>
                        <p>
                            <h3 class="name pt-3 ps-5 pe-5 bold text-grey">Bonjour <span class="text-blue"><cfoutput>#lect_client.prénom# #lect_client.nom#,</cfoutput></span></h3>
                        </p>
                        <p class="description ps-5 pe-5">Veuillez trouver ci-dessous vos données personnelles. Vous pouvez les modifier si nécessaire.</p>
                    </div>

                    <!-- CONTENT -->
                    <div id="content-informations">
                        <cfif #lect_etablissement.ModifEnfantParParent# eq 1 and #aumoinsunenfant.recordcount# neq 0><!--- Modif Données Enfant --->
                            <cfset affichemodifenfant = "oui">
                        </cfif>
                        <cfif affichemodifenfant eq "oui">
                            <cfif isDefined("form.go1") or isdefined("form.go2")>
                                <cfinclude template="/ModPar/parent.cfm">
                            <cfelseif isDefined("form.enreg")>
                                <cfinclude template="/ModPar/nouvelEnfant.cfm">
                            <cfelse>
                                <cfinclude template="/ModPar/enfant.cfm">
                            </cfif>
                        <cfelseif affichemodifparent eq "oui">
                            <cfif isDefined("form.enreg")>
                                <cfinclude template="/ModPar/nouvelEnfant.cfm">
                            <cfelse>
                                <cfinclude template="/ModPar/parent.cfm">
                            </cfif>
                        <cfelse>
                            <cfinclude template="/ModPar/nouvelEnfant.cfm">
                        </cfif>
                    </div>
                </div>

                    <!-- SECTION RIGHT -->
                <div class="col-lg-3 section-right">
                    <div class="mt-5">
                        <cfinclude template="/home/calendar.cfm">
                    </div>
                </div>
            </main>
            <footer class="text-center d-flex justify-content-center align-items-center text-small text-grey">
                <a class="pe-3 ps-3" href="http://www.cantine-de-france.fr" target="_blank">&copy;Cantine de France 2012 - 2022</a>
                <a class="pe-3 ps-3 border-left-right" href="/mentionslegales">Mentions l&eacute;gales et confidentialit&eacute;</a>
                <p class="m-0 pe-3 ps-3">N&deg; de licence CF<cfoutput>#lect_etablissement.siret#</cfoutput> </p>
            </footer>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
        
        <!-- POP-UP MENU BURGER -->
        <button id="navMobileLabel"><img src="/img/icon/icon_menu_burger_white.png" alt="bouton menu déroulant" onclick="openNav()"></button>
        <cfinclude template="/menuBurger.cfm">
        
        <script type="text/javascript"> //Script gestion menu burger
            function openNav() {
                document.getElementById("navMobileItems").style.display= "block";
                document.getElementById("navMobileLabel").style.display= "none";
                const background = document.getElementById("bgBlur");
                background.className = "bg-blur";
                background.addEventListener("click", closeNav);
            }

            function closeNav() {
                document.getElementById("navMobileItems").style.display= "none";
                document.getElementById("bgBlur").classList.remove("bg-blur");
                document.getElementById("navMobileLabel").style.display= "block";
            }
        </script>

        <script>
            function PrintParentsInformations() {
                let contentInformations = document.getElementById("content-informations");
                contentInformations.innerHTML = `<cfinclude template="/ModPar/parent.cfm">`;
            }

            function PrintChildrenInformations() {
                let contentInformations = document.getElementById("content-informations");
                contentInformations.innerHTML = `<cfinclude template="/ModPar/enfant.cfm">`;
            }

            function PrintAddChild() {
                let contentInformations = document.getElementById("content-informations");
                contentInformations.innerHTML = `<cfinclude template="/ModPar/nouvelEnfant.cfm">`;
            }

            function MM_jumpMenu(targ,selObj,restore) { 
                eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
                if (restore) selObj.selectedIndex=1;
            }

            function showMB(mbox)  { 
                ColdFusion.MessageBox.show(mbox); 
            } 
        </script>

        <script type="text/javascript">/* <![CDATA[ */
            if (window.ColdFusion) {
                ColdFusion.required['nomenfant']=true;
                ColdFusion.required['prénomenfant']=true;
                ColdFusion.required['datenaissance']=true;
                ColdFusion.required['QuotientFamilial']=true;
            }
        /* ]]> */</script>

        <!--- Script ajouter un nouvel enfant--->
        <script type="text/javascript">
            function AlerteAllergie() {
                if (document.getElementById("allergie").value == 3) {
                    txt = 'Attention, saisir dans la zone compléments quelle(s) Alergie(s) ' + document.getElementById("prénomenfant").value;
                    txt = txt + ' a !!!' ;
                    alert(txt);	
                }
            }
        
            function chargeprevenir1() {
                document.getElementById("nomurgence1").value = document.getElementById("nomcharge1").value;
                document.getElementById("prenomurgence1").value = document.getElementById("prenomcharge1").value;
                document.getElementById("telurgence1").value = document.getElementById("telcharge1").value;
                document.getElementById("gsmurgence1").value = document.getElementById("gsmcharge1").value;
                document.getElementById("lienurgence1").value = document.getElementById("liencharge1").value;
            }
        
            function chargeprevenir2() {
                document.getElementById("nomurgence2").value = document.getElementById("nomcharge2").value;
                document.getElementById("prenomurgence2").value = document.getElementById("prenomcharge2").value;
                document.getElementById("telurgence2").value = document.getElementById("telcharge2").value;
                document.getElementById("gsmurgence2").value = document.getElementById("gsmcharge2").value;
                document.getElementById("lienurgence2").value = document.getElementById("liencharge2").value;
            }
        </script>
    </body>
</html>